export default (_, context) => {
    context.error({ statusCode: 304, message: 'Server error' });
};
