export default function(ctx, inject) {
    const mobileBreakPoint = 600;
    const tabletBreakPoint = 720;

    const mobile = width => width <= mobileBreakPoint;
    const mobileOrTablet = width => width <= tabletBreakPoint;

    const viewport = {
        isMobile: mobile,
        isMobileOrTablet: mobileOrTablet,
        isTablet: width => !mobile(width) && mobileOrTablet(width),
        isDesktop: width => !mobileOrTablet(width),
    };

    ctx.$viewport = viewport;
    inject('viewport', viewport);
}
