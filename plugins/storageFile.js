export default function({ app }, inject) {
    const googleStorageLink =
        'https://storage.googleapis.com/owliphy-storage-bucket/';

    const storageFile = fileName => `${googleStorageLink}${fileName}`;
    inject('storageFile', storageFile);
}
