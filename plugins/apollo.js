export default function(ctx) {
    const { app } = ctx;

    /** @return Promise */
    app.gqlRequest = (gqlQuery, variables = {}) => {
        const client = app.apolloProvider.defaultClient;
        return client.mutate({
            mutation: gqlQuery,
            variables,
        });
    };
}
