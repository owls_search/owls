export default function(ctx, inject) {
    const applicant = 'applicant';
    const recruiter = 'recruiter';

    const isUser = val => val === applicant;
    const isRecruiter = val => val === recruiter;

    const userTypePlugin = {
        isUser,
        isRecruiter,
    };

    ctx.$userType = userTypePlugin;
    inject('userType', userTypePlugin);
}
