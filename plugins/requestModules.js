import { requestModules } from '../util';

export default function(ctx, inject) {
    ctx.$requestModules = requestModules;
    inject('requestModules', requestModules);
}
