export default ({ store }) => {
    process.client &&
        window.onNuxtReady(() => {
            store.dispatch('userLanded');
        });
};
