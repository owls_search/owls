import LOG from '~/api/apollo/log.gql';
import { isBot } from '~/util';

export default function(ctx) {
    const { app } = ctx;
    app.log = (event, details) => {
        if (!process.client || !navigator || isBot(navigator.userAgent)) {
            return;
        }

        if (process.env.showLog) {
            // eslint-disable-next-line no-console
            console.info(
                `💻 Event: ${event}; Details: ${JSON.stringify(details)}`,
            );
        }
        if (process.env.enableLog) {
            const client = app.apolloProvider.defaultClient;
            client.mutate({
                mutation: LOG,
                variables: {
                    event,
                    details: JSON.stringify(details),
                    timestamp: Date.now(),
                },
            });
        }
    };
}
