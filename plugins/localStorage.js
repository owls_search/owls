import createPersistedState from 'vuex-persistedstate';
import dataStorage from '../helpers/dataStorage.js';

const paths = [
    'environment.cookieNotificationClosed',
    'user.clientId',
    'user.token',
    'user.userData',
    'user.savedSearches',
];

export default ({ store }) => {
    window.onNuxtReady(() => {
        dataStorage.initialize();
        store.dispatch(
            'envSetAllowedToWriteCookies',
            dataStorage.isAllowedToCookie(),
        );
        createPersistedState({
            paths,
            getState: key => dataStorage.get(key),
            setState: (key, state) => {
                dataStorage.set(key, state);
            },
        })(store);
    });
};
