const https = require('https');
const fs = require('fs');
const { Nuxt, Builder } = require('nuxt');
const isProd = process.env.NODE_ENV === 'production';
const port = process.env.PORT || 3000;

// We instantiate nuxt.js with the options
const config = require('./nuxt.config.js');
config.dev = !isProd;
const nuxt = new Nuxt(config);

// Build only in dev mode with hot-reloading
if (config.dev) {
    new Builder(nuxt)
        .build()
        .then(listen)
        .catch(error => {
            console.error(error);
            process.exit(1);
        });
} else {
    listen();
}

function listen() {
    const options = {
        key: fs.readFileSync(`${__dirname}/static/server.key`),
        cert: fs.readFileSync(`${__dirname}/static/server.crt`),
    };

    https.createServer(options, nuxt.render).listen(port);
    console.log(`Server listening on \`localhost:${port}\`.`);
}
