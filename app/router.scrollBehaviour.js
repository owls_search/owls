export default function({ name }) {
    return name.includes('search-query') ||
        name.includes('search-by-cv') ||
        name.includes('search-popular')
        ? null
        : { x: 0, y: 0 };
}
