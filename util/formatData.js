import { head } from '~/util/lodash';

export const getFormattedDate = dateToFormat =>
    dateToFormat && head(dateToFormat.toISOString().split('T'));

export const getFormattedSalary = (from, to, dictionary) => {
    if (from > 0 && to > 0) {
        return dictionary.range;
    } else if (from > 0) {
        return dictionary.from;
    } else if (to > 0) {
        return dictionary.to;
    } else {
        return null;
    }
};
