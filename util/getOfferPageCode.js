export const getOfferPageCode = (id, title, code = '') =>
    code
        ? `${id}-${code}`
        : `${id}-${title
              .trim()
              .split(' ')
              .join('-')
              .split(String.fromCharCode(160)) // non-breaking space
              .join('-')
              .split('/')
              .join('-')
              .toLowerCase()}`;
