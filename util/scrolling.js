export const scrollToElement = (el, parent = window, time = 200) => {
    const fps = 60;
    const totalFrames = time ? Math.ceil(fps * (time / 1000)) : 1;

    return (
        el &&
        new Promise(resolve => {
            requestAnimationFrame(() => {
                const isWindow = parent === window;
                let parentOffset;
                let targetPosition;

                if (isWindow) {
                    parentOffset = window.pageYOffset;
                    targetPosition = el.getBoundingClientRect().top;
                } else {
                    parentOffset = parent.scrollTop;
                    targetPosition = getOffsetTopUntil(el, parent);
                }

                const iterations = flip(
                    applyEasing(
                        accumulate(
                            distributeEvenly(targetPosition, totalFrames),
                        ),
                    ).map(nr => parentOffset + nr),
                );

                if (!iterations.length) return resolve();

                const animateScroll = () => {
                    const currentIteration = iterations.pop();

                    if (iterations.length) {
                        requestAnimationFrame(animateScroll);
                    }

                    if (isWindow) {
                        parent.scrollTo(0, currentIteration);
                    } else {
                        parent.scrollTop = currentIteration;
                    }

                    if (!iterations.length) resolve();
                };

                requestAnimationFrame(animateScroll);
            });
        })
    );
};

export const distributeEvenly = (desiredNr, desiredLength) => {
    const values = [];
    const nr = Math.ceil(desiredNr);
    const absNr = Math.abs(nr);
    const length = absNr < desiredLength ? absNr : desiredLength;
    if (length === 0) return [];

    const rest = nr % length;
    const currentValue = (nr - rest) / length;
    const distributedRest = distributeEvenly(rest, length - 1);

    for (let i = 0; i < length; i += 1) {
        values.push(currentValue + (distributedRest[i] || 0));
    }

    return values;
};

/**
 * Takes an array and accumulates its values.
 *
 * For example:
 * - `accumulate([1, 1, 1])` would result in [1, 2, 3]
 * - `accumulate([1, 2, 3])` would result in [1, 3, 6]
 */
export const accumulate = arr =>
    arr.reduce((acc, nr) => acc.concat((acc[acc.length - 1] || 0) + nr), []);

// flips the order of an array
export const flip = arr =>
    arr.reduceRight((flipped, el) => {
        flipped.push(el);
        return flipped;
    }, []);

const getOffsetTopUntil = (el, until) => {
    let currentEl = el;
    let offsetTop = 0;
    while (currentEl !== until) {
        offsetTop += el.offsetTop;
        currentEl = currentEl.offsetParent;
    }
    return offsetTop;
};

const applyEasing = arr => {
    const total = arr[arr.length - 1];
    return arr.map(nr => {
        const completion = nr / total;
        return nr * (completion * (2 - completion));
    });
};
