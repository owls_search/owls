export const getRecruiterMenu = componentContext => [
    {
        text: componentContext.$t('recruiterMenu.account'),
        path: { name: 'cabinet-recruiter-account' },
    },
    {
        text: componentContext.$t('recruiterMenu.offersList'),
        path: { name: 'cabinet-recruiter-offers-list' },
    },
    {
        text: componentContext.$t('recruiterMenu.addOffer'),
        path: { name: 'cabinet-recruiter-add-offer' },
    },
    {
        text: componentContext.$t('recruiterMenu.findForCandidate'),
        path: { name: 'cabinet-recruiter-find-for-candidate' },
    },
];

export const getUserMenu = componentContext => [
    {
        text: componentContext.$t('userMenu.account'),
        path: { name: 'cabinet-account' },
    },
    {
        text: componentContext.$t('userMenu.updateProfile'),
        path: { name: 'cabinet-profile' },
    },
    {
        text: componentContext.$t('userMenu.seeRelevantOffers'),
        path: { name: 'cabinet-relevant-offers' },
    },
    {
        text: componentContext.$t('userMenu.seeSavedSearches'),
        path: { name: 'cabinet-saved-searches' },
    },
];
