export const isBot = agent =>
    /bot|google|baidu|bing|msn|duckduckbot|teoma|slurp|yandex|crawler|spider|crawling/i.test(
        agent,
    );
