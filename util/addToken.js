export const addToken = data => {
    if (!['production', 'dev'].includes(process.env.NODE_ENV)) {
        data['API-KEY'] = process.env.searchApiKey;
    }

    return data;
};

export const addPhpKey = data => {
    if (!['production'].includes(process.env.NODE_ENV)) {
        data.key = process.env.phpApiKey;
    }

    return data;
};
