export const processAlternateLinks = links =>
    links.map(link => ({
        ...link,
        href: link.href.replace('http://', 'https://'),
    }));
