/* eslint-disable camelcase */
import { isString } from '~/util/lodash';

export const getTagsFromRemote = tags => {
    const keywords = [];
    for (const tagType in tags) {
        if (
            tagType !== 'remaining_text' &&
            tags.hasOwnProperty(tagType) &&
            tags[tagType].length > 0
        )
            keywords.push(
                ...tags[tagType].map(tagText => ({
                    text: tagText,
                    type: tagType,
                })),
            );
    }

    return keywords;
};

export const getTagsFromString = (type, tagsString) => {
    const keywords = [];
    tagsString &&
        tagsString.split(',').forEach(tag =>
            keywords.push({
                text: tag,
                type,
            }),
        );

    return keywords;
};

export const getOfferTagsFromRemote = (type, tags) => {
    if (isString(tags)) return getTagsFromString(type, tags);

    const keywords = [];
    tags &&
        tags.forEach(({ text, highlight }) =>
            keywords.push({
                text,
                highlight,
                type,
            }),
        );

    return keywords;
};

export const mapTagsToString = tags =>
    Array.isArray(tags) && tags.length
        ? tags.map(tag => tag.text).join(',')
        : '';

export const getPartnerLoggingName = ({
    import_partner_name,
    created_user_id,
}) =>
    import_partner_name
        ? `partner_${import_partner_name}`
        : `direct_${created_user_id}`;

export const mapFromRemoteApiOffer = offerData => ({
    id: offerData.id,
    isIndexed: offerData.is_indexed,
    title: offerData.title,
    miniDescription: offerData.description,
    // eslint-disable-next-line camelcase
    programmingLanguages: offerData.programming,
    benefits: offerData.benefits,
    skills: offerData.skills,
    destinations: offerData.location,
    url: offerData.url,
    score: offerData.score,
    companyName: offerData.name,
    logo: offerData.logo,
    bid: offerData.bid,
    code: offerData.code || '',
    // eslint-disable-next-line camelcase
    partner: getPartnerLoggingName(offerData),
});

export const mapFromGqlApiOffer = offerData => ({
    ...offerData,
    partner: getPartnerLoggingName(offerData),
});

export const mapRemoteTag = ({ text, type }) => ({
    text,
    type: mapRemoteTagToSiteTagType(type),
});

export const mapSuggestionsFromRemote = remoteSsgs =>
    remoteSsgs.map(({ suggestion = '', type = '' }, index) => ({
        id: index,
        text: suggestion,
        type,
    }));

export function mapRemoteTagToSiteTagType(ssgType) {
    const mapping = {
        skill: 'skills',
        title: 'titles',
        path: 'destinations',
        language: 'languages',
    };

    return mapping[ssgType];
}
