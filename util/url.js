export function getUrlWithTracking({
    partnerParam = null,
    partnerValue = null,
    url,
}) {
    if (partnerParam && partnerValue) {
        return url.includes('?')
            ? `${url}&${partnerParam}=${partnerValue}`
            : `${url}?${partnerParam}=${partnerValue}`;
    } else {
        return url;
    }
}
