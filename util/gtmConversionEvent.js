export function pushEvent(event) {
    window['<%= options.layer %>'] = window['<%= options.layer %>'] || [];
    window['<%= options.layer %>'].push(event);
}
