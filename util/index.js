export * from './scrolling';
export * from './cabinetMenu';
export * from './mapRemote';
export * from './formTagsSuggestions';
export * from './formatError';
export * from './formatData';
export * from './searchParams';
export * from './addToken';
export * from './url';
export * from './gtmConversionEvent';
export * from './handleError';
export * from './getOfferPageCode';
export * from './processAlternateLinks';
export * from './isBot';

export const requestModules = {
    SAVE_SEARCH: 'saveSearch',
    USER: 'user',
    USER_PASSWORD: 'userPassword',
    DELETE_USER: 'userDelete',
    OFFER_FORM: 'offerForm',
    FEEDBACK_FORM: 'feedbackForm',
    RELEVANT_OFFERS: 'relevantOffers',
    OFFERS_LIST: 'offersList',
    FIND_FOR_CANDIDATE: 'findForCandidate',
    STUDENTS: 'students',
};

export const responseStatus = {
    success: 'success',
    error: 'error',
};

export const elClasses = {
    keywords: 'js-keywords',
    searchForm: 'js-search-form',
    searchFormWrapper: 'js-main-search-form',
    searchFormKeywords: 'js-keywords-search-form',
    resultsPage: 'js-results-page',
    input: 'js-results-page',
};

export const userType = {
    APPLICANT: 'applicant',
    RECRUITER: 'recruiter',
};

export const waitTillElementAppears = (parentEl, desiredElClass, callback) => {
    const observer = new MutationObserver(mutations => {
        mutations.forEach(mutation => {
            if (!mutation.addedNodes.length) return;

            mutation.addedNodes.forEach(addedNode => {
                if (
                    addedNode.classList &&
                    addedNode.classList.contains(desiredElClass)
                ) {
                    observer.disconnect();
                    callback();
                }
            });
        });
    });

    observer.observe(parentEl, {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false,
    });
};

export const getFromCookie = (req, userParamName) => {
    const vuexCookieString = req.headers.cookie
        .split(';')
        .find(c => c.trim().startsWith('vuex='));

    if (!vuexCookieString) return false;

    const jsonString = vuexCookieString.split('=')[1];
    const decoded = decodeURIComponent(jsonString);
    const vuexCookie = JSON.parse(decoded);

    return vuexCookie.user[userParamName] || null;
};

export const blobToFile = blob => {
    const fd = new FormData();
    fd.set('a', blob);
    return fd.get('a');
};
