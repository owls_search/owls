export const getErrorMessage = ({
    message = '',
    errors = {},
    defaultErrorMessage = 'message.defaultFailed',
}) => {
    const errorMessage = message.length ? `${message}` : defaultErrorMessage;
    const errorDetails =
        typeof errors !== 'string'
            ? ` ${getResponseErrorsMessage(errors)}`
            : ` ${errors}`;

    return {
        message: errorMessage,
        details: errorDetails,
    };
};

export const getErrorObject = error => {
    const { response = {} } = error;
    const { data = {} } = response;
    const responseMessage = getErrorMessage(data);
    return Object.keys(responseMessage).length
        ? responseMessage
        : { message: 'message.userFailed' };
};

const getFromValidation = (field, validationErrorString) => {
    const validationType = validationErrorString.split('.')[1];
    return validationType === 'email'
        ? `Not valid ${field}!`
        : `Field '${field}' is ${validationErrorString.split('.')[1]}!`;
};

const getResponseErrorsMessage = (errors = {}) => {
    const processedErrors = Object.keys(errors).map(errorField =>
        errors[errorField][0].includes('validation.')
            ? getFromValidation(errorField, errors[errorField][0])
            : errors[errorField],
    );
    return processedErrors.join('<br>');
};
