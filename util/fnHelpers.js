import curry from 'lodash.curry';
import isEqual from 'lodash.isequal';

const curriedIsEqual = curry(isEqual);
const concatenate = concatenated => target => `${target}${concatenated}`;
const preconcatenate = concatenated => target => `${concatenated}${target}`;

export { curriedIsEqual, concatenate, preconcatenate };
