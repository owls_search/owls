const getLanguageApiParam =
    (languages, currentLang) =>
        languages.hasOwnProperty(currentLang)
            ? languages[currentLang].param
            : 'all';

export const getStateSearchParams = ({
    pageSize,
    offerLanguages,
    offerLanguage,
    currentPage,
    minBid,
    partnerFilter = null
}) => ({
    pageSize,
    language: getLanguageApiParam(offerLanguages, offerLanguage),
    page: currentPage,
    minBid,
    createdUserId: partnerFilter
});
