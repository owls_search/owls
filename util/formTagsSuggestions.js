import {
    mapSuggestionsFromRemote,
    mapRemoteTagToSiteTagType,
} from './mapRemote';
import { apiByTypeSuggestions } from '~/api/suggestions';
import { curry } from '~/util/lodash';

const requestTagsByTypeSuggestions = type => apiFn => (query, updateSsgsFn) => {
    setTimeout(
        () => apiFn(query, type).then(processRequestResults(updateSsgsFn)),
        80,
    );
};

const processRequestResults = updateSsgsFn => (result = {}) => {
    const { data = {} } = result;
    const { suggestions = [] } = data;

    const mapped = mapSuggestionsFromRemote(suggestions);
    updateSsgsFn(
        mapped.map(ssg => ({
            ...ssg,
            type: mapRemoteTagToSiteTagType(ssg.type),
        })),
    );
};

export const requestLanguageSuggestions = requestTagsByTypeSuggestions(
    'language',
)(apiByTypeSuggestions);
export const requestSkillsSuggestions = requestTagsByTypeSuggestions('skill')(
    apiByTypeSuggestions,
);
export const requestTitlesSuggestions = requestTagsByTypeSuggestions('title')(
    apiByTypeSuggestions,
);
export const requestLocationsSuggestions = requestTagsByTypeSuggestions('path')(
    apiByTypeSuggestions,
);

const addTag = (tagsContainer, newTag) => {
    const tags = Array.isArray(tagsContainer) ? tagsContainer : [];

    if (!newTag) {
        return tags;
    }

    return [
        ...tags,
        {
            text: newTag.text,
            type: newTag.type,
        },
    ];
};

export const curriedAddTag = curry(addTag);

const removeTag = (tagsContainer, index) => {
    const tags = Array.isArray(tagsContainer) ? tagsContainer : [];

    if (!index) {
        return tags;
    }

    if (tags.length === 1) {
        return [];
    } else {
        const newTags = tags.slice();
        newTags.splice(index, 1);
        return newTags;
    }
};

export const curriedRemoveTag = curry(removeTag);
