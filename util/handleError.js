import { getErrorMessage } from './formatError';
import { isObject, head } from './lodash';

export const handleError = (
    dispatch,
    component,
    defaultMessage = 'message.defaultFailed',
) => response => {
    let error = {
        message: defaultMessage,
        details: [],
    };

    if (isObject(response)) {
        const { errors = [] } = response;
        const errorObject = errors.length ? head(errors) : {};
        error = getErrorMessage(errorObject);
    }

    dispatch('requestFailed', {
        ...error,
        component,
    });
};
