export default {
    alphabet: {
        title: 'Работы по алфавиту',
    },
    required: 'Это обязательное поле',
    validationPassword: 'Мин. 6 знаков - цифр и букв',
    eg: '',
    jobsFor: 'Предложения для',
    showMore: 'Больше предложений',
    seo: {
        general: {
            siteName: 'Clusterjobs - умный поиск ИТ-вакансий',
        },
        accountRemoved: {
            title: 'Аккаунт clusterjobs был удален',
        },
        homepage: {
            title: 'clusterjobs - IT работа в Европе',
            keywords: 'работа, работа в Европе, IT работа, программист Европа',
            description:
                'clusterjobs поможет найти IT работу мечты в Европе. ' +
                'Мы размещаем более 200,000 вакансий с работой в Германии на английском и немецком языках.' +
                'Используйте поиск по резюме или полнотекстовый поиск по ключевым словам.',
        },
        search: {
            jobsFor: '{query} - вакансии в Германии',
            emptySearch: 'clusterjobs - IT работа в Европе',
            keywords:
                'работа, работа в Европе, IT работа, программист Европа, {keywords}',
            description:
                'Найти {query} в Европе. {number} открытых вакансий по запросу {query} на clusterjobs.de',
        },
        offer: {
            title: '{title} в {companyName} - найди свою работу на clusterjobs',
            keywords: '{skills}, {programmingLanguages}, {destinations}',
            description:
                'Открытая вакансия {title} в {companyName}. Мы ищем {skills}, {programmingLanguages} в {destinations}',
        },
        offerNotFound: {
            title: 'Вакансия не найдена',
        },
        impressum: {
            title: 'clusterjobs - Информация о компании',
            keywords: 'clusterjobs, Информация о компании, контакты',
            description: 'clusterjobs - Информация о компании',
        },
        termsAndCond: {
            title: 'clusterjobs - Условия и положения',
            keywords: 'clusterjobs - Условия и положения',
            description: 'clusterjobs Условия и положения',
        },
        privacyPolicy: {
            title: 'clusterjobs - Политика конфиденциальности',
            keywords: 'clusterjobs - Политика конфиденциальности',
            description: 'clusterjobs Политика конфиденциальности',
        },
        company: {
            title: 'clusterjobs - О нас',
            keywords: 'clusterjobs - О нас',
            description: 'clusterjobs О нас',
        },
    },
    homepage: {
        imgAlt: 'Мужчина с ноутбуком',
        ctaTitle: 'Умный поиск работы в IT',
        recentlyAddedTitle: 'Новые предложения',
    },
    search: {
        label: 'Поиск по тексту',
        cvLabel: 'Узнай, куда подходит твой CV',
        or: 'или',
        buttonText: 'Поиск',
        cvButtonText: 'Поиск по CV',
        cvMobileButtonText: 'CV',
        defaultButtonText: 'Поиск',
        defaultCvButtonText: 'Поиск по CV',
        defaultCvMobileButtonText: 'CV',
        defaultQuery: 'fullstack php berlin',
        searchByQuery: 'Поиск по тексту:',
        searchByFile: 'Поиск по файлу:',
        emptySearchText:
            'Пожалуйста, опишите специальность, технический стек, место работы одним запросом',
    },
    keywords: {
        label: 'Теги:',
        inputTitle: 'Добавить теги к текущему поиску:',
        inputLabel: 'Фильтровать текущий запрос:',
        inputPlaceholder: 'Microservices, React, TDD...',
        btnText: 'Добавить тег',
        resetLabel: 'Сбросить теги',
        resetBtnText: 'Сбросить',
    },
    links: {
        home: 'Главная',
        blog: 'Наш блог',
        alphabet: 'Работы по алфавиту',
        termsAndConditions: 'Условия и положения',
        impressum: 'Юридическая информация',
        privacyPolicy: 'Политика конфиденциальности',
    },
    header: {
        logIn: 'Войти',
        signUp: 'Зарегистрироваться',
        signUpCta: 'Нет аккаунта?',
        logInCta: 'Уже есть аккаунт?',
        logInTitle: 'Войти в аккаут',
        signUpTitle: 'Зарегистрировать аккаунт',
        email: 'E-Mail',
        repeatEmail: 'Пожалуйста, повторите E-Mail',
        password: 'Пароль',
        language: 'Язык сайта:',
        offerLanguage: 'Язык вакансий:',
        cabinet: 'Личный кабинет:',
        cabinetLabel: 'Иконка личного кабинета',
        selectSiteLanguage: 'Выберите язык сайта:',
        selectOfferLanguage: 'Я могу разговаривать на:',
        userProfile: {
            headerTitle: 'Профиль',
            menu: 'Меню',
            logOut: 'Выйти',
        },
        menuTitle: 'Меню сайта',
        mobile: {
            menu: 'Меню',
            menuTitle: 'Меню сайта',
        },
        isRecruiter: 'Хочу разместить вакансию',
    },
    footer: {
        ourCompany: 'Наша компания',
        contactFormTitle: 'Написать нам',
        copyright: 'Copyright clusterjobs ©',
        rightsReserved: 'All Rights Reserved',
        selectLanguage: 'Язык сайта',
        socialMedia: 'Наши медиа',
    },
    offers: {
        h1: 'Результаты для {currentQuery}',
        h1CV: 'Результаты для поиска по CV',
        offers: 'Предложения',
        page: 'Страница',
        match: 'Совпадение:',
        competences: 'Компетенции:',
        location: 'Где:',
        benefits: 'Бонусы:',
        programmingLanguages: 'Стек технологий:',
        view: 'Детали',
        apply: 'К Вакансии',
        goToOffer: 'Детали',
        update: 'Обновить',
        delete: 'Удалить',
        indexed: 'Опубликовано',
        btnText: 'Перейти к вакансии',
        notIndexed: 'Публикация в процессе',
        in: 'в',
        companyName: '{companyName}',
        saveSearch: 'Отслеживать вакансии',
    },
    saveSearch: {
        popoverTitle: 'Создать оповещение о вакансиях',
        popoverText:
            'Мы зарегистрируем для вас кабинет, где вы смодете изменить расписание уведомлений.' +
            ' Для начала мы можем отправлять письмо еженедельно по четвергам.',
        popoverSubtitle:
            'Мы можем оповещать вас о новых вакансиях, просто оставьте email:',
        submitNewcomer: 'Отслеживать вакансии',
        saveSearchSubtitle: 'Кликните, чтобы следить за новыми вакансиями',
        saveSearchPopoverText:
            'Мы будем отправлять письмо каждый четверг.' +
            ' Сейчас мы работаем над страницей, где бы вы смогли изменить расписание как вам удобно, скоро она будет доступна.',
        submitUser: 'Отслеживать вакансии',
        successPopoverTitle: 'Ваш поиск сохранен!',
        successPopoverText:
            'Мы будем оповещать о вакансиях из этого поиска  каждый четверг.' +
            ' Мы разрабатываем новое расписание и вскоре вы сможете сами им управлять.',
        saveSearchNoQuery:
            'To be able to save search of your CV file, please sign up and fill Profile.' +
            'You will be able to access relevant offers page and set mailing schedule',
    },
    company: {
        email: 'E-Mail:',
        call: 'Позвонить:',
    },
    contactUs: {
        name: 'Имя:',
        email: 'E-Mail:',
        message: 'Сообщение:',
        send: 'Отправить',
    },
    featuresSeo: {
        title: 'Наши особенности',
        aiTitle: 'Поисковая система на базе AI',
        aiText:
            'Используя преимущества AI и NLP, ' +
            'clusterjobs может сопоставлять по смыслу ' +
            'описание вакансии с запросами пользователя. ' +
            'Несмотря на то, что пользовательские запросы ' +
            'иногда могут не совпадать с содержанием вакансии, ' +
            'clusterjobs способен всегда предложить очень подходящие результаты. ',
        cvTitle: 'Поиск по резюме ',
        cvText:
            'clusterjobs использует новейшие технологии AI и ' +
            'Natural Language Processing (NLP), что позволяет ' +
            'пользователям совершать поиск, просто используя свое резюме. ' +
            'clusterjobs использует AI и NLP с целью распознать суть ' +
            'резюме пользователей и сопоставить их с подходящими ' +
            'вакансиями о работе. ',
        fullTextTitle: 'Полнотекстовая поисковая система',
        fullTextText:
            'clusterjobs - это усовершенствованная система полнотекстоваго поиска, ' +
            'которая позволяет пользователям легко и просто осуществлять поиск.' +
            'Эта функция, к примеру, дает возможность пользователям искать ' +
            'вакансии, используя одновременно название должности, личные навыки' +
            ' и даже желаемое месторасположение. ',
    },
    impressum: {
        title: 'Юридическая информация',
        text:
            'owliphy UG (haftungsbeschränkt)<br>' +
            'Parkstraße 72<br>' +
            '40477 Дюссельдорф<br>' +
            'Германия<br>' +
            '<br>' +
            'Регистрационный офис: Amtsgericht Düsseldorf<br>' +
            'Регистрационный номер: HRB 85801 <br>' +
            'VAT I.D./ TAX I.D. в соответствии с § 27a UStG: DE323427817<br>' +
            'E-Mail: info@clusterjobs.com <br>' +
            'Вебсайт: clusterjobs.de<br>',
    },
    aboutUs: {
        title: 'О компании',
    },
    termsAndCond: {
        title: 'Условия и положения',
    },
    privacy: {
        title: 'Политика конфиденциальности',
    },
    about: {
        title: 'О компании',
    },
    cookieNotification: {
        title: 'Использование cookies',
        close: 'Закрыть',
        infoLink: 'Дополнительно',
        text:
            'Используя этот сайт, вы соглашаетесь с условиями использования cookies. Подробнee об использовании cookies вы узнаете в разделе Политика конфиденциальности',
    },
    infographics: {
        title: 'Как мы работаем',
        searchTitle: 'Простой и умный поиск',
        search:
            'Опишите то что вы ищете или <i>ищите по CV</i>, и мы распознаем ключевые слова для подбора лучших предложений.',
        engineTitle: 'Наша поисковая система',
        engine:
            'Основываясь на вашем техническом стеке, компетенциях, месте работы, предпочитаемом языке ' +
            'мы подбираем <i>лучшие IT-предложения</i> в Германии',
        contentTitle: 'Найдите работу, которая подходит именно вам!',
        content:
            'Зарегистрируйтесь на clusterjobs, чтобы <i>сохранять ваши поиски ' +
            'и регулярно получать уведомления</i> о новых возможностях.',
        searchAlt: 'Девушка сидит в интернете с ноутбука',
        engineAlt: 'Парень показывает связи между данными',
        contentAlt: 'Парень показывает результат',
    },
    account: {
        title: 'Аккаунт и настройки',
        formTitle: 'Информация об аккаунте',
        name: 'Имя',
        namePlaceholder: '',
        lastName: 'Фамилия',
        email: 'E-Mail',
        updateButton: 'Обновить',
        successUpdate: 'Успешно обновлено!',
        nameUsageDetails:
            'Мы используем ваше имя только для отображения на сайте и в E-Mail',
        emailUsageDetails:
            'E-Mail будет ипользован как логин и для восстановления пароля',
        formPasswordTitle: 'Изменить пароль',
        password: 'Текущий пароль',
        updatePasswordText:
            'Нам нужен ваш текущий пароль, чтобы подтвердить доступ',
        newPassword: 'Новый пароль',
        newPasswordConfirmation: 'Пожалуйста, повторите новый пароль',
        updatePasswordButton: 'Изменить',
        deleteUserTitle: 'Удалить аккаунт',
        deleteText:
            'Выбирая эту опцию, ваш аккаунт и вся, связанная с ним информация (CV и личные данные), будут полностью удалены.',
        deleteLabel: 'Удалить',
    },
    profile: {
        title: 'Заполните профиль, чтобы помочь нам найти вам лучшую вакансию',
        formTitle: 'Мы используем информацию, чтобы ',
        formTitleLink: 'найти самые релевантные предложения',
        addCv: 'Загрузить CV',
        successUpdate: 'Профиль успешно обновлен',
        cvLabel: 'Мы используем ваш CV, чтобы найти вам лучшие вакансии',
        recogniseTags: 'Распознать теги',
        recogniseTagsLabel:
            'Выбирая эту опцию, система распознает теги из вашего резюме',
        skills: 'Навыки',
        skillsPlaceholder:
            'например, Architecture, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'например, PHP, Javascript, Java, Go',
        offerTitles: 'Специальность',
        offerTitlesPlaceholder: 'например, Project Manager, Software Developer',
        locations: 'Местоположение',
        locationsPlaceholder: 'например, Munich, Berlin, Cologne',
        button: 'Обновить профиль',
        offerTypes: 'Тип работы',
        offerTypeFull: 'Полный рабочий день',
        offerTypePart: 'Неполная занятость',
        offerTypeStudent: 'Стажировка',
        offerTypeRemote: 'Удаленная работа',
        offerTypesPlaceholder: 'Какую работу вы ищете?',
        menu: 'Ваш кабинет:',
    },
    addOffer: {
        indexedData: 'Главная ифнормация о предложении',
        contactData: 'Контактная информация',
        otherData: 'Остальное',
        button: 'Добавить вакансию',
        title: 'Разместите свою вакансию',
        offerTitle: 'Должность',
        offerTitlePlaceholder: 'Пожалуйста, добавьте должность',
        description: 'Описание',
        miniDescription: 'Краткое описание',
        miniDescriptionPlaceholder:
            'Это описание будет отображаться на карточке вакансии в списке, если в предложении указано менее 4 тегов',
        skills: 'Необходимые навыки',
        skillsPlaceholder:
            'например, Microservices, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'например, PHP, Javascript, Java, Go',
        destinations: 'Местоположение',
        destinationsPlaceholder: 'Местоположение вакансии',
        offerTypes: 'Тип занятости',
        offerTypesPlaceholder: 'Выберите тип занятости',
        companyName: 'Название компании',
        companyNamePlaceholder: 'Вы можете использовать краткое название',
        uploadLogo: 'Добавить логотип',
        emptyText:
            'Вакансия с логотипом будет выглядеть более примечательно в списке',
        email: 'E-Mail',
        emailPlaceholder: 'E-Mail, чтобы кандидаты отправляли свои CV',
        url: 'Ссылка на вакансию',
        urlPlaceholder: 'Куда нам перенаправлять кандидатов?',
        startsFrom: 'Начало занятости',
        salaryFrom: 'Годовая зарплата от',
        salaryFromPlaceholder: 'Будет отображено `от X в год`',
        salaryTo: 'Годовая зарплата до',
        salaryToPlaceholder: 'Будет отображено `до X в год`',
        successMessage: 'Вакансия успешно добавлена',
        errorNoId: 'Произошла ошибка, и мы не получили ID добавленной вакансии',
        recogniseTags: 'Распознать теги',
        recogniseTagsText:
            'Выбирая эту опцию, система распознает теги (навыки и языки программирования) из вашего описания',
    },
    offerAdded: {
        title: 'Ваша вакансия добавлена',
        goToList: 'Список ваших вакансий',
        successText:
            'Нам необходимо некоторое время, чтобы проверить добавленные данные. Это может занять до 4х часов.',
    },
    updateJob: {
        title: 'Обновить вакансию',
        button: 'Обновить',
        formSuccess: 'Вакансия успешно обновлена',
        formError: 'Произошла ошибка, пожалуйста, свяжитесь с нами',
    },
    offersList: {
        title: 'Ваши вакансии',
        descriptionText:
            'Вакансии в этом списке выглядят так же, как их видят кандидаты',
    },
    recruiterMenu: {
        account: 'Аккаунт и настройки',
        offersList: 'Мои вакансии',
        findForCandidate: 'Найти для кандидата',
        addOffer: 'Разместить вакансию',
    },
    userMenu: {
        seeRelevantOffers: 'Подходящие вакансии',
        updateProfile: 'Профиль',
        account: 'Аккаунт и настройки',
    },
    findForCandidate: {
        title: 'Подходящие вакансии для вашего кандидата',
        searchByFile: 'Ключевые слова из `{fileName}`:',
        pageDescription:
            'Загрузите CV кандидата, и мы постараемся найти подходящую вакансию из ваших предложений - ',
        button: 'Загрузите CV кандидата',
    },
    offerPage: {
        title: '{title} в {companyName}',
        requirements: 'Требования',
        destinationsLabel: 'Местоположение',
        benefitsLabel: 'Бонусы',
        skillsLabel: 'Навыки',
        programmingLanguagesLabel: 'Tech stack',
        description: 'Описание',
        viewOffer: 'К Вакансии',
        workType: 'Мы предлагаем <b itemprop="employmentType">{workType}</b>',
        createdAt: 'Размещено: ',
        backToList: 'К списку вакансий',
        startsAsap: 'Начните <b>как можно раньше</b>',
        startsAt: 'Начните <b>{date}</b>',
        salaryRange:
            'и получайте <b>от <span itemprop="minValue">{from}</span> {currency} до <span itemprop="maxValue">{to}</span> {currency}</b>',
        salaryFrom:
            'и получайте <b>от <span itemprop="minValue">{salary}</span> {currency}</b>',
        salaryTo:
            'и получайте <b>до <span itemprop="maxValue">{salary}</span> {currency}</b>',
    },
    logoText: '{companyName} логотип',
    relevantOffers: {
        title: 'Подходящие вакансии',
        notFound:
            'Unfortunately we couldn`t find any relevant offers for you. Please, try to refine the search',
        emptyProfileLink: 'Пожалуйста, заполните ваш профиль',
        emptyProfile:
            ', чтобы мы могли найти вам наиболее соответсвующие предложения',
        pageDescription:
            'Мы использовали ваш профиль и CV, чтобы найти наиболее подходящие предложения',
    },
    message: {
        defaultFailed: 'Что-то пошло не так, пожалуйста, свяжитесь с нами',
        missingToken:
            'Токен пользователя не действителен. Пожалуйста, войдите в аккаунт снова.',
        badRequest: 'Ложные параметры запроса',

        jobNotFound: 'Предложение не найдено',
        jobFail: 'Извините, запрос не удался.',
        jobSuccessAdd: 'Предложение успешно добавлено',
        jobSuccessUpdate: 'Предложение успешно обновлено',
        jobSuccessRemove: 'Предложение успешно удалено',

        userSuccess: 'User request was successfully sent.',
        userFailed:
            'Извините, запрос пользователя не удался. Свяжитесь с нами.',
        userNotFound: 'Извините, пользователь не найден. Свяжитесь с нами.',
        userEmptyUpdateFields: 'Поля для обновления пользователя пустые.',
        userBadCredentials:
            'Извините, мы не можем найти пользователя с таким E-Mail и паролем.',
        userBadPassword: 'We think you have a mistake in current password.',

        feedbackFail:
            'К сожалению, ваш отзыв не был сохранен. Пожалуйста, попробуйте позже.',
        feedbackSuccess: 'Отзыв был успешно добавлен',

        saveSearchFailed:
            'Unfortunately, we cannot save your search at the moment',
        saveSearchSuccess: 'Search is successfully saved!',
        saveSearchUpdateSuccess: 'Schedule is successfully updated!',
    },
    accountRemoved: {
        title:
            'Ваш аккаунт и все личные данные, связанные с ним, успешно удалены.',
        text:
            'Вся информция, которую вы предоставили для поиска вакансий, была удалена.<br>' +
            'Личные данные и ваш аккаунт были полностью удалены и не могут быть использованы нигде более.',
    },
    aria: {
        toggleMenuSection: 'Раздел Toggle-меню',
        goToOffer: 'Перейти к вакансии',
        send: 'Отправить',
        boldText: 'Жирный текст',
        italicText: 'Курсив',
        strikeText: 'Зачеркнутый текст',
        underlineText: 'Подчеркнутый текст',
        makeCodeBlock: 'Добавить блок кода',
        addParagraph: 'Добавить новый абзац',
        addH1: 'Добавить заголовок 1',
        addH2: 'Добавить заголовок 2',
        addH3: 'Добавить заголовок 3',
        bulletList: 'Добавить список',
        orderedList: 'Добавить нумерованный список',
        quote: 'Добавить цитату',
        undo: 'Отмена',
        redo: 'Повтор',
        addImage: 'Добавить изображение',
        addLink: 'Добавить ссылку',
        removeLink: 'Удалить ссылку',
        removeTag: 'Удалить тег',
        changeSiteLanguage: 'Изменить язык сайта',
        changeOffersLanguage: 'Изменить язык вакансий',
        openLogInPopover: 'Открыть окно входа',
        openProfile: 'Зайти в аккаунт',
        openSiteMenu: 'Открыть меню сайта',
        close: 'Закрыть',
        submitForm: 'Отправить',
        openSignUpPopover: 'Открыть окно регистрации',
    },
    popularOffers: 'Популярные предложения',
    popularQueries: {
        cityPagesTitle: 'Популярные города',
        title: 'Популярные запросы',
        offerTitle: 'Популярные должности в этом городе',
        linkText: '{programming} в {location}',
    },
    similarOffersTitle: 'Похожие вакансии',
    error: {
        noOfferTitle: 'Job offer is not found',
        noOfferText:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check our Homepage, we are sure, you can find something that suits you well!',
        noOfferTextSubstitutes:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check similar offers, we are sure, you can find something that suits you well!',
        notFound: 'Мы потеряли страницу',
        notfoundText: 'К сожалению, страница которую вы искали, не найдена',
        toHomepage: 'Вот ссылка, чтобы вернуться на главную',
        error: 'Произошло что-то непонятное',
        errorText: 'К сожалению, произошла ошибка, и мы над ней уже работаем!',
    },
    blog: {
        title: '> clusterjobs magazine',
        nextArticle: 'Дальше',
        backToMain: 'Назад к блогу',
        seoTitle: 'clusterjobs - мы покажем как найти работу',
        seoKeywords:
            'поиск работы, работа за границей, работа в германии, clusterjobs, работа в германии на английском',
        seoDescription:
            'Мы фокусируемся на статьях которые помогут найти работу за рубежом, ' +
            'трендах в IT и AI, и особенностях IT профессий',
    },
    students: {
        features: {
            hidden: 'Our features',
            jobs: {
                title: 'More than 100,000 IT-jobs in Germany',
                text:
                    'clusterjobs aggregates offers from partners worldwide to show only best one and most relevant for our users.',
            },
            it: {
                title: 'Student and after-graduate jobs in IT',
                text:
                    'Find your werkstudent job or your early-career job in case you are graduating. clusterjobs helps you to get the best suit for you!',
            },
            ai: {
                title: 'Use smart AI search by your CV',
                text:
                    'Just upload your CV and our AI engine will recognise your strengths and will show relevant jobs.',
            },
        },
        interestedQuestion:
            'Interested? We can notify you about new relevant jobs',
        headTitle: `Find best student's jobs in Germany. Start your career in IT with clusterjobs.<br> AI will find best suiting job for you!`,
        notifyMe: 'Notify me',
        register: 'Register',
        studentJobs: 'Student jobs',
        graduateJobs: 'After-graduate jobs',
        imgAlt: 'Man with a binoculars',
        showMore: 'Show me more',
        logoTitle: 'clusterjobs - use tech search to find tech job',
        registerCta: 'Register to be notified about new and related offers',
        registerForm: {
            signUpTitle: 'Find your dream job in three steps',
        },
    },
    authentication: {
        signUp: 'Register',
        email: 'Email address',
        repeatEmail: 'Please repeat your email',
        password: 'Password',
        isRecruiter: 'I am recruiter',
    },
    partners: {
        title: 'Наши партнеры',
    },
};
