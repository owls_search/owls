export default '' +
    '<h3>Privacy Policy</h3>\n' +
    '<p><span>At clusterjobs, accessible from clusterjobs.de, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by clusterjobs.de and how we use it.</span></p>\n' +
    '<p><span>If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us through email at in <a href="mailto:info@owliphy.com">info@owliphy.com</a></span></p>\n' +
    '<h3>Why do we collect personal data?</h3>\n' +
    '<p><span>We collect personal data to be able to offer a tailored service for your job- and candidates-hunting &nbsp;needs. The services available depend on whether you have registered with the Jobs website and whether or not you decide to upload your CV to it or to create a Profile. By uploading your CV, you give clusterjobs your full permission to store it and to use it for analytical purposes. </span></p>\n' +
    '<h3>What personal data do we collect?</h3>\n' +
    '<p><span>We collect:</span></p>\n' +
    '<ul>\n' +
    '<li><span> your name;</span></li>\n' +
    '<li><span> your address;</span></li>\n' +
    '<li><span> the country you live in;</span></li>\n' +
    '<li><span> your email address;</span></li>\n' +
    '<li><span> your phone numbers;</span></li>\n' +
    '<li><span> your nationality;</span></li>\n' +
    '<li><span> your IP-address;</span></li>\n' +
    '<li><span> details of your education and working career;</span></li>\n' +
    '<li><span> your personal interests; and</span></li>\n' +
    '<li><span> details of how you interact with the Jobs website and vacancies you might be interested in.</span></li>\n' +
    '</ul>\n' +
    '<p><span>When you register and upload your CV to the website, you can also benefit from:</span></p>\n' +
    '<ul>\n' +
    '<li><span> a facility for recruiters to see your CV for specific roles you have applied for or that they believe you might be interested in;</span></li>\n' +
    '<li><span> a private and secure area on the website to manage your job preferences, privacy settings and to update your CV and details;</span></li>\n' +
    '<li><span> opting in to job match emails;</span></li>\n' +
    '<li><span> storing your CV on the Jobs website database.</span></li>\n' +
    '</ul>\n' +
    '<h3>General Data Protection Regulation (GDPR)</h3>\n' +
    '<p><span>We are a Data Controller of your information. If you\'d like to learn more about GDPR and your rights under GDPR, please read the </span><a href="https://www.freeprivacypolicy.com/blog/gdpr-privacy-policy/"><span>GDPR guide</span></a><span>.</span></p>\n' +
    '<p><span>clusterjobs legal basis for collecting and using the personal information described in this Privacy Policy depends on the Personal Information we collect and the specific context in which we collect the information:</span></p>\n' +
    '<ul>\n' +
    '<li><span> clusterjobs needs to perform a contract with you;</span></li>\n' +
    '<li><span> you have given clusterjobs permission to do so;</span></li>\n' +
    '<li><span> processing your personal information is in clusterjobs legitimate interests;</span></li>\n' +
    '<li><span> clusterjobs needs to comply with the law.</span></li>\n' +
    '</ul>\n' +
    '<p><span>clusterjobs will retain your personal information only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use your information to the extent necessary to comply with our legal obligations, resolve disputes, and enforce our policies.</span></p>\n' +
    '<p><span>If you are a resident of the European Economic Area (EEA), you have certain data protection rights. If you wish to be informed what Personal Information we hold about you and if you want it to be removed from our systems, please contact us.</span></p>\n' +
    '<p><span>In certain circumstances, you have the following data protection rights:</span></p>\n' +
    '<ul>\n' +
    '<li><span> the right to access, update or to delete the information we have on you;</span></li>\n' +
    '<li><span> the right of rectification;</span></li>\n' +
    '<li><span> the right to object;</span></li>\n' +
    '<li><span> the right of restriction;</span></li>\n' +
    '<li><span> the right to data portability;</span></li>\n' +
    '<li><span> &nbsp;the right to withdraw consent.</span></li>\n' +
    '</ul>\n' +
    '<h3>Log Files</h3>\n' +
    "<p><span>clusterjobs.com follows a standard procedure of using log files. These files log visitors when they visit websites. All hosting companies do this and a part of hosting services' analytics. The information collected by log files include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the website, and gathering demographic information.</span></p>\n" +
    '<h3>Cookies and Web Beacons</h3>\n' +
    "<p><span>Like any other website, clusterjobs.de uses 'cookies'. These cookies are used to store information including visitors' preferences, and the pages on the website that the visitor accessed or visited. The information is used to optimize the users' experience by customizing our web page content based on visitors' browser type and/or other information.</span></p>\n" +
    '<p><span>You can learn how to manage cookies on your web browser by following the </span><a href="https://privacypolicies.com/blog/browser-cookies-guide/"><span>Browser Cookies Guide</span></a><span>.</span></p>\n' +
    '<h3>Privacy Policies</h3>\n' +
    '<p><span>You may consult this list to find the Privacy Policy for each of the advertising partners of clusterjobs.de.</span></p>\n' +
    "<p><span>Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements and links that appear on clusterjobs.de, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.</span></p>\n" +
    '<p><span>Note that clusterjobs.de has no access to or control over these cookies that are used by third-party advertisers.</span></p>\n' +
    '<h3>Third Party Privacy Policies</h3>\n' +
    "<p><span>clusterjobs.com's Privacy Policy does not apply to other advertisers or websites. Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers for more detailed information. It may include their practices and instructions about how to opt-out of certain options. You may find a complete list of these Privacy Policies and their links here: Privacy Policy Links.</span></p>\n" +
    "<p><span>You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific web browsers, it can be found at the browsers' respective websites.</span></p>\n" +
    '<h3>User Content</h3>\n' +
    "<p><span>You are prohibited from publishing user content that contains official identification information (applies to both your own and other people's data) such as: for example, social security, passport, identity card, insurance, driving license and immigration numbers or any comparable numbers, codes or IDs. Publishing such identifying information can lead to identity theft and other adverse consequences. clusterjobs may remove such identification data, but is not obligated or responsible, and excludes any liability for the publication of such identification data. clusterjobs reserves the right to change the appearance of the CV on its website, including by fading fields, rearranging the format and changing visual elements.</span></p>\n" +
    "<p><span>Although clusterjobs is not required to do so, clusterjobs can monitor user content. In addition, clusterjobs reserves the right to wholly or partially delete any User Content that, at the sole discretion of clusterjobs, violates the above rules. This includes User Content that is unrelated to the portion of the Site on which it was posted, or that is advertising, recruitment or other commercial communications, or that clusterjobs deems inappropriate in its sole discretion. If you believe that User Content violates this Agreement or our policies, please contact clusterjobs immediately so we can consider their removal. As a clarification: clusterjobs is in no way obliged to remove user content, and the decision whether user content violates clusterjobs's policies is always at the sole discretion of clusterjobs.</span></p>\n" +
    '<p><span> clusterjobs reserves the right to disclose all User Content and other relevant information, as well as information about the circumstances of the transfer to any third party in connection with the operation of this website, to protect itself, its affiliates, partners and visitors, and any legal or regulatory obligations correspond to. This means, for example, that clusterjobs may disclose the electronic address and identity of a user or other duly requested data to comply with subpoenas, search warrants, law enforcement requests, or court-ordered requests. If you, as an employer, post a job advertisement on clusterjobs that is listed as confidential at your request, your identity may be disclosed upon request by the government or law enforcement agencies.</span></p>\n' +
    '<h3>Licensing of User Content</h3>\n' +
    '<p><span>If you post or submit materials to clusterjobs, such as photos or materials for job adverts sent to clusterjobs, you grant to the extent permitted by law the non-exclusive, worldwide, indefinite (revocable only as described below), fully paid, toll-free, (in Sublicense levels) sublicensable right to create, use, sell, sublicense, reproduce, distribute, perform, perform, make derivative works from, and otherwise use material and materials on the clusterjobs website without limitation or publish its publisher partners, maintain the owlpihy website and promote clusterjobs. In addition, you grant clusterjobs the right to use your name, usernames, and / or your trademarks and logos in connection with this User Content or the marketing materials or acts of clusterjobs to promote or publish such User Content (eg, job advertisements), including for use of Keywords in third-party Internet search engines. Notwithstanding the above, clusterjobs publishes photos submitted by users for certain job advertisements only in the job advertisement of the respective users.</span></p>\n' +
    "<h3>Children's Information</h3>\n" +
    '<p><span>Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.</span></p>\n' +
    '<p><span>The minimum age for use of the website is 16 years; however, if local laws require a higher age for clusterjobs to lawfully provide you with the services of the Website, then that older age is the minimum age. If you are in a jurisdiction outside the European Union and have not reached the age of 18 or the age of majority in the relevant jurisdiction, you may only use clusterjobs under the supervision of a parent or legal guardian. clusterjobs uses various measures to ensure that people who have not reached the minimum age or otherwise meet age requirements cannot use the website.</span></p>\n' +
    '<p><span>clusterjobs.com does not knowingly collect any Personal Identifiable Information from children under the age of 16. If you think that your child provided this kind of information on our website, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.</span></p>\n' +
    '<h3>Online Privacy Policy Only</h3>\n' +
    '<p><span>Our Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in clusterjobs.de. This policy is not applicable to any information collected offline or via channels other than this website. </span></p>\n' +
    '<h3>Consent</h3>\n' +
    '<p><span>By using our website, you hereby consent to our Privacy Policy and agree to its terms.</span></p>';
