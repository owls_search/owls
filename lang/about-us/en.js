export default '' +
    '<p>clusterjobs is a meta-search job engine that is empowered by Artificial Intelligence (AI) and aims to allow the people to find their dream jobs. In addition, clusterjobs aims to help IT companies to hire software engineers, data scientists, and data engineers, etc. to find the best candidates that fit ideally to their jobs profiles.<br>' +
    '<br>' +
    'clusterjobs is using the latest technology of AI and Natural Language Processing (NLP) to allow the users to search by using simply their Resume/Curriculum Vitae (CV). clusterjobs engine is using AI and NLP to understand the semantic of the users’ CV and match them against job profiles.<br>' +
    ' <br>' +
    'With the advances of AI and Natural Language Processing (NLP), clusterjobs can perform semantic matching between the job descriptions and the user queries. Even though, the user queries may not be exactly matched with the job’s content, clusterjobs is capable of giving very relevant results.<br>' +
    ' <br>' +
    'clusterjobs is an advanced full-text search engine that allows users to search freely and easily. This feature for instance allows users to search jobs by using the job titles, theirs skills, and even the location they are looking all in one.</p>';
