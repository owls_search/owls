export default {
    alphabet: {
        title: 'Jobs by alphabet',
    },
    required: 'الرقم السري مطلوب',
    validationPassword: 'Min. 6 symbols - digits and letters',
    eg: 'e.g. ',
    jobsFor: 'وظيفه في',
    showMore: 'المزيد من العروض',
    seo: {
        general: {
            siteName: 'Clusterjobs - be smart in your job search!',
        },
        accountRemoved: {
            title: 'تم حدذف حسابك بنجاح',
        },
        homepage: {
            title:
                'clusterjobs - استخدم محرك البحث الذكي للبحث عن وظيفه احلامك',
            keywords: 'find job, jobs for, get a job, tech job, IT job',
            description:
                'Find your dream IT job on clusterjobs.de. ' +
                'We have more than 200,000 english and german jobs in Germany. ' +
                'Search with your CV, or using natural text search',
        },
        search: {
            jobsFor: '{query} وظائف في المانيا',
            emptySearch: 'clusterjobs - tech search of tech job',
            keywords:
                'find job, jobs for, job, offer, in Europe, IT job, job in English, {keywords}',
            description:
                'Search {query} in Europe with AI search engine. {number} open jobs in {query} on clusterjobs.de',
        },
        offer: {
            title: '{title} وظيفه شاغره في {companyName}',
            keywords:
                'job, offer, IT, find job, jobs, {skills}, {programmingLanguages}, {destinations}',
            description:
                'Open {title}. We are looking for {skills}, {programmingLanguages} in {destinations}',
        },
        offerNotFound: {
            title: 'Job offer is not found',
        },
        impressum: {
            title: 'clusterjobs - Imprint',
            keywords: 'clusterjobs, contacts',
            description: 'clusterjobs imprint - information about company',
        },
        termsAndCond: {
            title: 'clusterjobs - Terms & Conditions',
            keywords: 'clusterjobs, terms & Conditions',
            description: 'clusterjobs Terms & Conditions',
        },
        privacyPolicy: {
            title: 'clusterjobs - Privacy Policy',
            keywords: 'clusterjobs, privacy Policy',
            description: 'clusterjobs Privacy Policy',
        },
        company: {
            title: 'clusterjobs - About us',
            keywords: 'clusterjobs, about us, about company',
            description: 'clusterjobs - about company',
        },
    },
    homepage: {
        imgAlt: 'Man with a laptop',
        ctaTitle: 'محرك بحث ذكي للبحث عن الوظائف التقنية',
        recentlyAddedTitle: 'وظائف اضيفت حديثا',
    },
    search: {
        label: 'البحث عن طريق الكتابه',
        cvLabel: 'اكتشف الوظائف التى تطابق سيرتك الذاتية',
        or: 'او',
        buttonText: 'ابحث',
        cvButtonText: ' بحث بال CV',
        cvMobileButtonText: 'CV',
        defaultButtonText: 'ابحث',
        defaultCvButtonText: 'بحث بال CV',
        defaultCvMobileButtonText: 'CV',
        defaultQuery: 'fullstack php berlin',
        searchByQuery: ': ابحث بكتابة',
        searchByFile: ': ابحث بالملف',
        emptySearchText:
            'Describe in a free way where and what job you are looking for, what tech stack you are interested',
    },
    keywords: {
        label: 'كلمات دلاليه',
        inputTitle: 'اضف الكلمات الداله لبحثك',
        inputLabel: 'تعديل البحث',
        inputPlaceholder: 'Microservices, Berlin, Homeoffice',
        btnText: 'اضف كلمه',
        resetLabel: 'امسح الكلمات',
        resetBtnText: 'اعاده البحث',
    },
    links: {
        home: 'الصفحه الرئيسيه',
        blog: 'Our blog',
        alphabet: 'Jobs by Alphabet',
        impressum: 'Impressum',
        termsAndConditions: 'شروط الاستخدام ',
        privacyPolicy: 'بيان الخصوصية',
    },
    header: {
        logIn: 'دخول',
        signUp: 'تسجيل',
        signUpCta: 'لا تملك حساب؟',
        logInCta: 'هل تملك حساب حاليا؟',
        logInTitle: 'الدخول لحسابك',
        signUpTitle: 'تسجيل حساب جديد',
        email: 'عنوان بريدك الالكتروني',
        repeatEmail: 'الرجاء ادخال بريدك الاكتروني مره اخرى',
        password: 'الرقم السري',
        isRecruiter: 'انا شركه توظيف',
        language: 'لغه الموقع',
        offerLanguage: 'لغه الوظائف',
        cabinet: 'Profile',
        cabinetLabel: 'Profile icon',
        selectSiteLanguage: 'لغه الوقع',
        selectOfferLanguage: 'لغه الوظائف',
        userProfile: {
            headerTitle: 'معلوماتك',
            menu: 'الدخول لمعلوماتك',
            logOut: 'خروج',
        },
        menuTitle: 'Website menu',
        mobile: {
            menu: 'قائمه',
            menuTitle: 'قائمة الموقع',
        },
    },
    footer: {
        ourCompany: 'عن الشركه',
        contactFormTitle: 'تواصل معنا',
        copyright: 'Copyright clusterjobs ©',
        rightsReserved: 'All Rights Reserved',
        selectLanguage: 'اللغه',
        socialMedia: 'مواقع التواصل الاجتماعي',
    },
    offers: {
        h1: 'Results for {currentQuery}',
        h1CV: 'Results for search by CV',
        offers: 'Jobs',
        page: 'Page',
        match: 'مطابقة الوظيفة',
        location: 'Where',
        benefits: 'فوائد',
        competences: 'الكفاءات',
        programmingLanguages: 'التكنولوجيا المطلوبة',
        view: 'التفاصيل',
        apply: 'اذهب للعرض ',
        btnText: 'Open offer on a new page',
        goToOffer: 'Job page',
        update: 'Update',
        delete: 'Delete',
        indexed: 'Published',
        in: 'in',
        notIndexed: 'Publish in process',
        companyName: 'من {companyName}',
        saveSearch: 'تفعيل تنبيه الوظائف',
    },
    saveSearch: {
        popoverTitle: 'تفعيل تنبيه الوظائف',
        popoverText:
            'We are going to create a cabinet for you, where you can manage email schedule' +
            ' For the start it will be sent weekly on Thursdays',
        popoverSubtitle:
            'We can notify you about new offers from this list, just leave the email',
        submitNewcomer: 'Notify me',
        saveSearchSubtitle: 'Click below to track this search and get notified',
        saveSearchPopoverText:
            'We will send you mail every Thursday' +
            ' We are working on schedule interface, and soon you ll be able to change schedule as you like',
        submitUser: 'Track offers',
        successPopoverTitle: 'Your search is saved',
        successPopoverText:
            'We will notify you about new offers each Thursday' +
            ' We are working on schedule now and soon you will be able to change it as you wish',
        saveSearchNoQuery:
            'To be able to save search of your CV file, please sign up and fill Profile.' +
            'You will be able to access relevant offers page and set mailing schedule',
    },
    company: {
        email: 'Email',
        call: 'Call',
    },
    contactUs: {
        name: 'الاسم الكامل',
        email: 'البريد الاكتروني',
        message: 'النص',
        send: 'ارسل',
    },
    featuresSeo: {
        title: 'الميزات الرئيسية لدينا',
        aiTitle: 'محرك بحث وظائف بالاعتماد على الذكاء الاصطناعي',
        aiText:
            'مع تقدم الذكاء الاصطناعى ومعالجة اللغة الطبيعية ، يمكن لـموقعنا إجراء المطابقة الدلالية بين توصيف الوظائف واستعلامات الوظيفة. على الرغم من أن استعلامات المستخدم قد لا تتطابق تمامًا مع محتوى الوظيفة ، فإن  موقعنا قادر على إعطاء النتائج الأكثر صلة',
        cvTitle: 'ابحث عن طريق سيرتك الذاتية',
        cvText:
            'موقعنا يستخدم أحدث التقنيات في الذكاء الاصطناعى ومعالجة اللغة الطبيعية للسماح للمستخدمين بالبحث باستخدام سيرتهم الذاتية الخاصة بهم. موقعنا يستخدم تقنيات الذكاء الاصطناعي لفهم دلالة السيرة الذاتية للمستخدمين ومواءمتها مع الوظائف',
        fullTextTitle: 'محرك بحث للنصوص الكاملة',
        fullTextText:
            'موقعنا هو محرك بحث نصي كامل يسمح للمستخدمين بالبحث عن الوظائف بحرية وسهولة. يتيح للمستخدمين البحث عن وظائف باستخدام عناوين الوظائف ومهاراتهم اماكنهم المفضلة.',
    },
    impressum: {
        title: 'Imprint',
        text:
            'owliphy UG (haftungsbeschränkt)<br>' +
            'Parkstraße 72<br>' +
            '40477 Duesseldorf<br>' +
            'Germany<br>' +
            '<br>' +
            'Registration office: Amtsgericht Düsseldorf<br>' +
            'Registration number: HRB 85801 <br>' +
            'VAT I.D./ TAX I.D. in accordance with § 27a UStG:  DE323427817 <br>' +
            'E-Mail: info@clusterjobs.com <br>' +
            'Website: clusterjobs.de<br>',
    },
    aboutUs: {
        title: 'عن الشركه',
    },
    termsAndCond: {
        title: 'شروط الاستخدام',
    },
    privacy: {
        title: ' بيان الخصوصية',
    },
    about: {
        title: 'الشركه',
    },
    cookieNotification: {
        title: 'Use of Cookies',
        close: 'Close',
        infoLink: 'Info',
        text:
            'By using this website, you agree to the use of cookies. Details on the use of cookies can be found in our Privacy Policy',
    },
    infographics: {
        title: 'كيف نعمل',
        searchTitle: 'محرك بحث بسيط وذكي',
        search:
            'اكتب ما تبحث عنه أو ابحث عن طريق السيرة الذاتية  وسوف نجد بحثك',
        engineTitle: 'نظام البحث لدينا',
        engine:
            'استنادًا إلى مجموعتك التقنية ، والكفاءات ، والموقع ، ولغة العمل المفضلة لديك ، نطابقك مع أفضل عروض تكنولوجيا المعلومات في ألمانيا',
        contentTitle: 'جد لنفسك خطوة الوظيفي المقبلة',
        content:
            'سجل على الموقع لتتمكن من حفظ عمليات البحث الخاصة بك وإخطارك بالفرص القادمة',
        searchAlt: 'Woman surfing web with a laptop',
        engineAlt: 'A man pointing on connections in data',
        contentAlt: 'A man showing output',
    },
    account: {
        title: 'Account and settings',
        formTitle: 'Account information',
        name: 'Name',
        namePlaceholder: '',
        lastName: 'Last name',
        email: 'Email',
        updateButton: 'Update',
        successUpdate: 'Successfully updated',
        nameUsageDetails:
            'We will use your name only to display it on website and in emails',
        emailUsageDetails: 'Email is used as a login and to restore password',
        formPasswordTitle: 'Change password',
        password: 'Current password',
        updatePasswordText: 'We need your current password to confirm access',
        newPassword: 'New password',
        newPasswordConfirmation: 'Please repeat new password',
        updatePasswordButton: 'Change',
        deleteUserTitle: 'Delete account',
        deleteText:
            'On clicking this we will delete your account and all related data, including CV and all profile information',
        deleteLabel: 'Delete',
        recruiter: {
            title: 'Recruiter cabinet settings',
            variableName: 'Source variable name',
            variableValue: 'Source variable value',
            variableNameText:
                'If you define these params, we will append variable with certain value to each your url (ex. ?utm_source=clusterjobs)',
            variableValueText: 'Both parameters need to be defined',
        },
    },
    profile: {
        title: 'Fill you profile to let us find you the best job',
        formTitle: 'We will use this information to find ',
        formTitleLink: 'most relevant results for you',
        addCv: 'Upload CV',
        successUpdate: 'We successfully updated your profile',
        cvLabel: 'We will use your CV to refine offers and show best results',
        recogniseTags: 'Recognise tags',
        recogniseTagsLabel:
            'On click we will recognise some data from your resume',
        skills: 'Skills and Frameworks',
        skillsPlaceholder:
            'ex. Architecture, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'ex. PHP, Javascript, Java, Go',
        offerTitles: 'Position name',
        offerTitlesPlaceholder: 'ex. Project Manager, Software Developer',
        locations: 'Locations',
        locationsPlaceholder: 'ex. Munich, Berlin, Cologne',
        button: 'Update profile',
        offerTypes: 'Job type',
        offerTypeFull: 'Full-time job',
        offerTypePart: 'Part-time job',
        offerTypeStudent: 'Student',
        offerTypeRemote: 'Remote job',
        offerTypesPlaceholder: 'What job are you looking for',
        menu: 'Your cabinet',
    },
    addOffer: {
        indexedData: 'Main information about an offer',
        contactData: 'Contact information',
        otherData: 'Other',
        button: 'Add a job',
        title: 'Post your job',
        offerTitle: 'Job title',
        offerTitlePlaceholder: 'Please put the name of the job',
        description: 'Job description',
        miniDescription: 'Short description',
        miniDescriptionPlaceholder:
            'This description will appear in search list on job card, if less than 4 keywords are extracted from job',
        skills: 'Required skills or frameworks',
        skillsPlaceholder:
            'ex. Microservices, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'ex. PHP, Javascript, Java, Go',
        destinations: 'Job location',
        destinationsPlaceholder: 'Where job takes place',
        offerTypes: 'Job type',
        offerTypesPlaceholder: 'Select type of job you offer',
        companyName: 'Company name',
        companyNamePlaceholder: 'You may use short name',
        uploadLogo: 'Add company logo',
        emptyText: 'Upload logo of company to get more emphasized offer',
        email: 'Email',
        emailPlaceholder: 'Email for users to send CV`s',
        url: 'Url for applying',
        urlPlaceholder:
            'Add url with `source` parameter here, or set parameter in `Account settings`',
        startsFrom: 'Starts from',
        salaryFrom: 'Year salary starts at',
        salaryFromPlaceholder: 'Displays as `from X annually`',
        salaryTo: 'Year salary up to',
        salaryToPlaceholder: 'Displays as `up to X annually`',
        successMessage: 'Job is successfully added',
        errorNoId:
            'Sorry, something happened and we have no id of the added job',
        recogniseTags: 'Recognise tags',
        recogniseTagsText:
            '- we can retrieve some tags(skills and languages) from your description, if you click here',
        offerLanguage: 'Required language',
        offerLanguagePlaceholder: 'Please, specify required speaking language',
    },
    offerAdded: {
        title: 'Your job is added',
        goToList: 'List of your jobs',
        successText:
            'We need a bit of time to verify the data and approve it. It may take up to 4 hours and you will be notified, when it appears on the website',
    },
    updateJob: {
        title: 'Update the job',
        button: 'Update',
        formSuccess: 'Job is successfully updated',
        formError: 'An error occurred, please contact us through feedback form',
    },
    offersList: {
        title: 'List of your job offers',
        descriptionText: 'Jobs in this list looks the same way user sees them',
    },
    recruiterMenu: {
        account: 'Account and settings',
        offersList: 'My jobs',
        findForCandidate: 'Find for candidate',
        addOffer: 'Post a job',
    },
    userMenu: {
        seeRelevantOffers: 'Relevant jobs',
        updateProfile: 'Profile',
        account: 'Account and settings',
    },
    findForCandidate: {
        title: 'Relevant offers for your candidate',
        searchByFile: 'Keywords from `{fileName}`',
        pageDescription:
            'Upload CV of a candidate and we will try to find him a job among your offers - ',
        button: 'Upload candidate CV',
    },
    offerPage: {
        title: '{title}',
        requirements: 'What is needed',
        destinationsLabel: 'مكان العمل',
        benefitsLabel: 'فوائد',
        skillsLabel: 'الكفاءات',
        programmingLanguagesLabel: 'التكنولوجيا المطلوبة',
        description: 'What is it about',
        viewOffer: 'اذهب للعرض ',
        workType: 'we offer <b itemprop="employmentType">{workType}</b>',
        createdAt: 'was posted on: ',
        backToList: 'Back to offers',
        startsAsap: 'begin <b>as soon as possible</b>',
        startsAt: 'begin <b>at {date}</b>',
        salaryRange:
            'and get from <b><span itemprop="minValue">{from}</span> {currency} to <span itemprop="maxValue">{to}</span> {currency}</b>',
        salaryFrom:
            'and get from <b><span itemprop="minValue">{salary}</span> {currency}</b>',
        salaryTo:
            'and get up to <b><span itemprop="maxValue">{salary}</span> {currency}</b>',
    },
    logoText: '{companyName} logo',
    relevantOffers: {
        title: 'عروض العمل ذات الصلة',
        notFound:
            'Unfortunately we couldn`t find any relevant offers for you. Please, try to refine the search',
        emptyProfileLink: 'Please, fill your profile',
        emptyProfile: ', so we can find best offers for you',
        pageDescription:
            'We used your profile and CV to find most relevant offers',
    },
    message: {
        defaultFailed: 'Sorry, something went wrong. Please contact us',
        missingToken: 'User token is invalid. Please, log in again',
        badRequest: 'Bad request parameters',

        jobNotFound: 'Offer is not found',
        jobFail: 'Sorry, job offer request has failed',
        jobSuccessAdd: 'Job offer was successfully added',
        jobSuccessUpdate: 'Job offer was successfully updated',
        jobSuccessRemove: 'Job offer successfully removed',

        userSuccess: 'User request was successfully sent.',
        userFailed: 'Sorry, user request has failed. Contact us',
        userNotFound: 'Sorry, user was not found. Contact us',
        userEmptyUpdateFields: 'User updated fields were empty',
        userBadCredentials:
            'Sorry, we didn`t find user with this email and password',
        userBadPassword: 'We think you have a mistake in current password.',

        feedbackFail:
            'Unfortunately feedback was not sent, please try again later',
        feedbackSuccess: 'Feedback was successfully sent',

        saveSearchFailed:
            'Unfortunately, we cannot save your search at the moment',
        saveSearchSuccess: 'Search is successfully saved!',
        saveSearchUpdateSuccess: 'Schedule is successfully updated!',
    },
    accountRemoved: {
        title: 'Your account and all related data were removed',
        text:
            'All information that you provided to get relevant offers searches was removed.<br>' +
            'It and your account were removed and will not be reused anywhere',
    },
    aria: {
        toggleMenuSection: 'Toggle menu section',
        goToOffer: 'Go to offer',
        send: 'Submit form',
        boldText: 'Make text bold',
        italicText: 'Make text italic',
        strikeText: 'Strike text through',
        underlineText: 'Underline text',
        makeCodeBlock: 'Add block of code',
        addParagraph: 'Add new paragraph',
        addH1: 'Add heading level 1',
        addH2: 'Add heading level 2',
        addH3: 'Add heading level 3',
        bulletList: 'Add bullet list',
        orderedList: 'Add ordered list',
        quote: 'Add quote',
        undo: 'Undo',
        redo: 'Redo',
        addImage: 'Add image',
        addLink: 'Add link',
        removeLink: 'Remove link',
        removeTag: 'Remove tag',
        changeSiteLanguage: 'Change site language',
        changeOffersLanguage: 'Change offers language',
        openLogInPopover: 'Open log in popover',
        openProfile: 'Open profile',
        openSiteMenu: 'Open site menu',
        close: 'Close',
        submitForm: 'Submit form',
        openSignUpPopover: 'Open sign up popover',
    },
    popularOffers: 'Popular job offers',
    popularQueries: {
        cityPagesTitle: 'Popular cities',
        title: 'وظائف مشهورة',
        offerTitle: 'Popular jobs in this location',
        linkText: '{programming} in {location}',
    },
    similarOffersTitle: 'عروض مماثلة',
    error: {
        noOfferTitle: 'Job offer is not found',
        noOfferText:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check our Homepage, we are sure, you can find something that suits you well!',
        noOfferTextSubstitutes:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check similar offers, we are sure, you can find something that suits you well!',
        notFound: 'Oops, page is not found',
        notfoundText: 'Unfortunately page, you are looking for is not found',
        toHomepage: 'Here is a link to go back to homepage',
        error: 'Something weird has happened',
        errorText:
            'Unfortunately some error occurred and we are working on it already',
    },
    blog: {
        title: '> clusterjobs magazine',
        backToMain: 'Back to blog',
        nextArticle: 'Next article',
        seoTitle: 'clusterjobs blog - we write about job searching industry',
        seoKeywords:
            'job search, AI in HR, HR, work abroad, job abroad, clusterjobs, get a job abroad',
        seoDescription:
            'We offer articles about tech and non-tech trends in job' +
            ' search and HR. We want to help people from abroad to find job all over the globe',
    },
    students: {
        features: {
            hidden: 'Our features',
            jobs: {
                title: 'More than 100,000 IT-jobs in Germany',
                text:
                    'clusterjobs aggregates offers from partners worldwide to show only best one and most relevant for our users.',
            },
            it: {
                title: 'Student and after-graduate jobs in IT',
                text:
                    'Find your werkstudent job or your early-career job in case you are graduating. clusterjobs helps you to get the best suit for you!',
            },
            ai: {
                title: 'Use smart AI search by your CV',
                text:
                    'Just upload your CV and our AI engine will recognise your strengths and will show relevant jobs.',
            },
        },
        interestedQuestion:
            'Interested? We can notify you about new relevant jobs',
        headTitle: `Find best student's jobs in Germany. Start your career in IT with clusterjobs.<br> AI will find best suiting job for you!`,
        notifyMe: 'Notify me',
        register: 'Register',
        studentJobs: 'Student jobs',
        graduateJobs: 'After-graduate jobs',
        imgAlt: 'Man with a binoculars',
        showMore: 'Show me more',
        logoTitle: 'clusterjobs - use tech search to find tech job',
        registerCta: 'Register to be notified about new and related offers',
        registerForm: {
            signUpTitle: 'Find your dream job in three steps',
        },
    },
    authentication: {
        signUp: 'Register',
        email: 'Email address',
        repeatEmail: 'Please repeat your email',
        password: 'Password',
        isRecruiter: 'I am recruiter',
    },
    partners: {
        title: 'Partners',
    },
};
