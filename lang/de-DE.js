export default {
    alphabet: {
        title: 'Jobs nach Alphabet',
    },
    required: 'Pflichtfeld',
    validationPassword: 'Min. 6 Symbole - Ziffern und Buchstaben',
    eg: 'z.B.',
    jobsFor: 'Jobs für',
    showMore: 'Weitere Angebote',
    seo: {
        general: {
            siteName: 'Clusterjobs - Intelligente Tech-fokussierte Jobsuche',
        },
        accountRemoved: {
            title: 'clusterjobs-Konto wurde entfernt',
        },
        homepage: {
            title: 'clusterjobs - finden Sie Ihren Tech-Job!',
            keywords:
                'clusterjobs, clusterjobs, Job finden, Jobs für, Job suchen, perfekten Job finden, Jobsuche, Jobs, job, Suchmaschine für Stellenangebote, Stellenangebot, Job, Jobsuchmaschine, Jobbörse, Stellenanzeigen, Jobs suchen, Karriere, Einstellung, Arbeit, Jobs finden, Stellenanzeige, Stellenmarkt, Anstellung, Arbeit, Jobangebote, Stellenbörse',
            description:
                'Finden Sie Ihren IT-Traumjob auf clusterjobs.de. ' +
                'Wir haben mehr als 200,000 deutsche und englische Jobs in Deutschland. ' +
                'Suchen Sie mit Ihrem CV, oder mittels der Volltextsuche.',
        },
        search: {
            jobsFor: '{query} Jobs in Deutschland',
            emptySearch: 'clusterjobs - Tech-Suche nach Tech-Job',
            keywords:
                'Job finden, Jobs für, Job, Jobangebot,' +
                ' in Europa, {keywords}, Jobsuche, Jobs, job, Suchmaschine für Stellenangebote,' +
                ' Stellenangebote, Job, Jobsuchmaschine, Jobbörse, Stellenanzeigen, Jobs suchen,' +
                ' Karriere, Einstellung, Arbeit, Jobs finden, Stellenanzeige, Stellenmarkt, Anstellung,' +
                ' Job Suchmachschine, Alerts, Beschäftigung' +
                ' Arbeit, Jobangebote, Stellenbörse, {keywords}',
            description:
                'Suchen Sie {query} in Europa mit der AI-Suchmaschine ' +
                '✓ {number} offene Jobs in {query} auf clusterjobs.de ' +
                '✓ alle IT-Jobbörsen und IT-Jobs in einer Jobsuche ' +
                '✓ Immer alle aktuellen Jobs',
        },
        offer: {
            title: '{title} - Finden Sie Ihren Job auf Clusterjobs',
            keywords:
                '{skills}, {programmingLanguages}, {destinations}, Jobsuche, Jobs, job, Suchmaschine für Stellenangebote, Stellenangebot, Job, Jobsuchmaschine, Jobbörse, Stellenanzeigen, Jobs suchen, Karriere, Einstellung, Arbeit, Jobs finden, Stellenanzeige, Stellenmarkt, Anstellung, Arbeit, Jobangebote, Stellenbörse',
            description:
                ' {title} öffnen. Job ist über - {skills}, {programmingLanguages} in {destinations}',
        },
        offerNotFound: {
            title: 'Jobangebot ist nicht mehr aktiv',
        },
        impressum: {
            title: 'clusterjobs - Impressum',
            keywords: 'Information zum Unternehmen',
            description: 'clusterjobs Impressum - Information zum Unternehmen',
        },
        termsAndCond: {
            title: 'clusterjobs - Geschäftsbedingungen',
            keywords: 'clusterjobs - Geschäftsbedingungen',
            description: 'clusterjobs Geschäftsbedingungen',
        },
        privacyPolicy: {
            title: 'clusterjobs - Datenschutzerklärung',
            keywords: 'clusterjobs - Datenschutzerklärung',
            description: 'clusterjobs Datenschutzerklärung',
        },
        company: {
            title: 'clusterjobs - Über uns',
            keywords: 'clusterjobs - Über uns',
            description: 'clusterjobs Über uns',
        },
    },
    homepage: {
        imgAlt: 'Man with a laptop',
        ctaTitle: 'Intelligente Tech-fokussierte Jobsuche',
        recentlyAddedTitle: 'Kürzlich hinzugefügte Jobs',
    },
    search: {
        label: 'Suche nach Text',
        cvLabel: 'Erfahren Sie, wo Ihr CV besser passt',
        or: 'oder',
        buttonText: 'Suche',
        cvButtonText: 'Suche nach CV',
        cvMobileButtonText: 'CV',
        defaultButtonText: 'Suche',
        defaultCvButtonText: 'Suche nach CV',
        defaultCvMobileButtonText: 'CV',
        defaultQuery: 'fullstack php berlin',
        searchByQuery: 'Suche nach Abfrage:',
        searchByFile: 'Suche nach Datei:',
        emptySearchText:
            'Beschreiben Sie, wo und nach welchem Job Sie suchen, an welchem Tech-Stack Sie interessiert sind',
    },
    keywords: {
        label: 'Stichwörter:',
        inputTitle: 'Stichwörter zur aktuellen Suche hinzufügen:',
        inputLabel: 'Verbessern Sie Ihre Suche:',
        inputPlaceholder: 'Microservices, React, TDD...',
        btnText: 'Tag hinzufügen',
        resetLabel: 'Löshen',
        resetBtnText: 'Löshen',
    },
    links: {
        home: 'Home',
        blog: 'Unser Blog',
        alphabet: 'Jobs nach Alphabet',
        termsAndConditions: 'Geschäftsbedingungen',
        impressum: 'Impressum',
        privacyPolicy: 'Datenschutzerklärung',
    },
    header: {
        logIn: 'Einloggen',
        signUp: 'Anmelden',
        signUpCta: 'Haben Sie noch kein Konto?',
        logInCta: 'Haben Sie schon ein Konto?',
        logInTitle: 'Ins Konto einloggen',
        signUpTitle: 'Melden Sie sich bei Ihrem Konto an',
        email: 'Email-Adresse',
        repeatEmail: 'Bitte wiederholen Sie Ihre E-Mail',
        password: 'Kennwort',
        language: 'Website-Sprache:',
        offerLanguage: 'Job-Sprache:',
        cabinet: 'Profil:',
        cabinetLabel: 'Profile icon',
        selectSiteLanguage: 'Website-Sprache ändern:',
        selectOfferLanguage: 'Ich spreche:',
        userProfile: {
            headerTitle: 'Konto',
            menu: 'Besten Jobs finden:',
            logOut: 'Abmelden',
        },
        menuTitle: 'Website-Menü',
        mobile: {
            menu: 'Menü',
            menuTitle: 'Website-Menü',
        },
        isRecruiter: 'Ich bin Recruiter',
    },
    footer: {
        ourCompany: 'Unseres Unternehmen',
        contactFormTitle: 'Kontaktieren Sie uns',
        copyright: 'Copyright clusterjobs ©',
        rightsReserved: 'All Rights Reserved',
        selectLanguage: 'Sprache:',
        socialMedia: 'Unsere Medien',
    },
    offers: {
        h1: 'Ergebnisse für {currentQuery}',
        h1CV: 'Ergebnisse für Suche nach Lebenslauf',
        offers: 'Jobs',
        page: 'Seite',
        match: 'Gleichheit:',
        location: 'Wo:',
        benefits: 'Leistungen:',
        competences: 'Kompetenzen:',
        programmingLanguages: 'Tech-Stack:',
        view: 'Details',
        apply: 'Offer anzeigen',
        btnText: 'Offer anzeigen',
        goToOffer: 'Jobseite',
        update: 'Aktualisieren',
        delete: 'Löschen',
        indexed: 'Veröffentlicht',
        notIndexed: 'Im Process veröffentlichen',
        in: 'in',
        companyName: 'bei {companyName}',
        saveSearch: 'Job alert',
    },
    saveSearch: {
        popoverTitle: 'Create email alert',
        popoverText:
            'We are going to create a cabinet for you, where you can manage email schedule.' +
            ' For the start it will be sent weekly on Thursdays.',
        popoverSubtitle:
            'We can notify you about new offers from this list, just leave the email:',
        submitNewcomer: 'Notify me',
        saveSearchSubtitle: 'Click below to track this search and get notified',
        saveSearchPopoverText:
            'We will send you mail every Thursday.' +
            ' We are working on schedule interface, and soon you ll be able to change schedule as you like.',
        submitUser: 'Track offers',
        successPopoverTitle: 'Your search is saved!',
        successPopoverText:
            'We will notify you about new offers each Thursday.' +
            ' We are working on schedule now and soon you will be able to change it as you wish.',
        saveSearchNoQuery:
            'To be able to save search of your CV file, please sign up and fill Profile.' +
            'You will be able to access relevant offers page and set mailing schedule',
    },
    company: {
        email: 'E-Mail:',
        call: 'Anruf:',
    },
    contactUs: {
        name: 'Ihre Name:',
        email: 'E-Mail:',
        message: 'Nachricht:',
        send: 'Senden',
    },
    featuresSeo: {
        title: 'Unsere Hauptmerkmale',
        aiTitle: 'AI-powered Suchmaschine',
        aiText:
            'Mit den Fortschritten von AI und Natural Language Processing (NLP) ' +
            'kann clusterjobs einen semantischen Abgleich zwischen den ' +
            'Jobbeschreibungen und den Benutzerabfragen durchführen.' +
            ' Auch wenn die Benutzeranfragen möglicherweise nicht genau ' +
            'mit dem Inhalt des Jobs übereinstimmen, kann clusterjobs sehr ' +
            'relevante Ergebnisse liefern.',
        cvTitle: 'Suche nach Lebenslauf / CV',
        cvText:
            'clusterjobs verwendet die neueste Technologie von AI und Natural ' +
            'Language Processing (NLP), um den Benutzern die Suche nach ' +
            'Lebenslauf / CV zu ermöglichen. Die clusterjobs-Engine verwendet ' +
            'AI und NLP, um die Semantik des Lebenslaufs der Benutzer zu ' +
            'verstehen und sie mit Jobs abzugleichen.',
        fullTextTitle: 'Volltext-Suchmaschine',
        fullTextText:
            'clusterjobs ist eine erweiterte Volltext-Suchmaschine, mit der ' +
            'Benutzer frei und einfach suchen können. Diese Funktion ' +
            'ermöglicht es beispielsweise Benutzern, Jobs zu suchen, ' +
            'indem sie die Jobtitel, ihre Fähigkeiten und sogar den Ort, ' +
            'an dem sie suchen, in einem verwenden.',
    },
    impressum: {
        title: 'Impressum',
        text:
            'owliphy UG (haftungsbeschränkt)<br>' +
            'Parkstraße 72<br>' +
            '40477 Düsseldorf<br>' +
            'Deutschland<br>' +
            '<br>' +
            'Registergericht: Amtsgericht Düsseldorf<br>' +
            'Registernummer: HRB 85801<br>' +
            'Umsatzsteuer-Identifikationsnummer gemäß § 27a UStG: DE323427817<br>' +
            'E-Mail: info@clusterjobs.com <br>' +
            'Website: clusterjobs.de<br>',
    },
    aboutUs: {
        title: 'Über clusterjobs',
    },
    termsAndCond: {
        title: 'Geschäftsbedingungen',
    },
    privacy: {
        title: 'Datenschutzerklärung',
    },
    about: {
        title: 'Unternehmen',
    },
    cookieNotification: {
        title: 'Verwendung von Cookies',
        close: 'Schließen',
        infoLink: 'Info',
        text:
            'Durch die weitere Nutzung der Webseite stimmen Sie der Verwendung von Cookies zu. Details zur Verwendung von Cookies finden Sie in unserer Datenschutzerklärung.',
    },
    infographics: {
        title: 'Wie arbeiten wir',
        searchTitle: 'AI Suchmaschine - einfach und intelligent',
        search:
            'Schreiben Sie einfach, wonach Sie suchen, oder <i>mithilfe Ihres Lebenslaufes suchen</i>, ' +
            'und wir führen anhand der Schlüsselwörter eine personalisierte Suche durch.',
        engineTitle: 'Unser Suchsystem',
        engine:
            'Basierend auf Ihren Tech Stack, Kompetenzen, Arbeitsort und der ausgewählten Arbeitssprache ' +
            'bieten wir Ihnen <i>die besten IT-Angebote in Deutschland</i> an.',
        contentTitle: 'Suchen Sie sich einen nächsten Karriereschritt!',
        content:
            'Registrieren Sie sich bei clusterjobs, um <i>Ihre Suchanfragen speichern zu ' +
            'können und über die neuen Angebote informiert</i> zu werden.',
        searchAlt: 'Woman surfing web with a laptop',
        engineAlt: 'A man pointing on connections in data',
        contentAlt: 'A man showing output',
    },
    account: {
        title: 'Konto und Einstellungen',
        formTitle: 'Kontoinformation',
        name: 'Vorname',
        namePlaceholder: '',
        lastName: 'Nachname',
        email: 'E-Mail',
        updateButton: 'Aktualisieren',
        successUpdate: 'Erfolgreich aktualisiert!',
        nameUsageDetails:
            'Wir werden Ihren Namen nur für die Anzeige auf der Website und in E-Mails verwenden',
        emailUsageDetails:
            'E-Mail wird als Login und zum Wiederherstellen des Kennworts verwendet',
        formPasswordTitle: 'Kennwort ändern',
        password: 'Aktuelles Kennwort',
        updatePasswordText:
            'Wir benötigen Ihr aktuelles Kennwort, um den Zugang zu bestätigen',
        newPassword: 'Neues Kennwort',
        newPasswordConfirmation: 'Wiederholen Sie bitte neues Kennwort',
        updatePasswordButton: 'Ändern',
        deleteUserTitle: 'Konto löschen',
        deleteText:
            'Wenn Sie darauf klicken, werden wir Ihr Konto und alle zugehörigen Daten löschen, einschließlich Lebenslauf und alle Profilinformation',
        deleteLabel: 'Löschen',
    },
    profile: {
        title:
            'Füllen Sie Ihr Profil aus, damit wir Ihnen den besten Job finden können',
        formTitle: 'Wir werden diese Information verwenden, um  ',
        formTitleLink: 'relevanteste Ergebnisse für Sie zu finden ',
        addCv: 'CV hochladen',
        successUpdate: 'Wir haben Ihr Profil erfolgreich aktualisiert',
        cvLabel:
            'Wir verwenden Ihren Lebenslauf, um Angebote zu verbessern und die besten Ergebnisse zu zeigen',
        recogniseTags: 'Tags erkennen',
        recogniseTagsLabel:
            'Auf Klick erkennen wir einige Daten aus Ihrem Lebenslauf',
        skills: 'Fähigkeiten und Rahmenbedingungen',
        skillsPlaceholder:
            'bsw. Architecture, Neural Networks, Relational Databases',
        langs: 'Tech-Stack',
        langsPlaceholder: 'bsw. PHP, Javascript, Java, Go',
        offerTitles: 'Positionsname',
        offerTitlesPlaceholder: 'bsw. Project Manager, Software Developer',
        locations: 'Locations',
        locationsPlaceholder: 'bsw. Munich, Berlin, Cologne',
        button: 'Profil aktualisieren',
        offerTypes: 'Jobtyp',
        offerTypeFull: 'Vollzeit Job',
        offerTypePart: 'Teilzeit Job',
        offerTypeStudent: 'Student',
        offerTypeRemote: 'Remote-Job',
        offerTypesPlaceholder: 'Welchen Job suchen Sie?',
        menu: 'Ihr Konto:',
    },
    addOffer: {
        indexedData: 'Hauptinformation zum Angebot',
        contactData: 'Kontaktinformation',
        otherData: 'Sonstige',
        button: 'Job hinzufügen!',
        title: 'Job veröffentlichen',
        offerTitle: 'Berufsbezeichnung',
        offerTitlePlaceholder: 'Bitte geben Sie eine Berufsbezeichnung an',
        description: 'Jobbeschreibung',
        miniDescription: 'Kurze Beschreibung',
        miniDescriptionPlaceholder:
            'Diese Beschreibung erscheint in der Suchliste auf der Jobkarte, wenn weniger als 4 Schlüsselwörter aus dem Job extrahiert werden',
        skills: 'Erforderliche Fähigkeiten oder Rahmenbedingungen',
        skillsPlaceholder:
            'bsw. Microservices, Neural Networks, Relational Databases',
        langs: 'Tech-Stack',
        langsPlaceholder: 'bsw. PHP, Javascript, Java, Go',
        destinations: 'Arbeitsort',
        destinationsPlaceholder: 'Wo findet der Job statt?',
        offerTypes: 'Jobtyp',
        offerTypesPlaceholder: 'Wählen Sie den Jobtyp aus, den Sie anbieten',
        companyName: 'Name der Firma',
        companyNamePlaceholder: 'Sie können einen Kurznamen verwenden',
        uploadLogo: 'Firmenlogo hinzufügen',
        emptyText:
            'Laden Sie das Firmenlogo hoch, um ein stärkeres Angebot zu erhalten',
        email: 'E-Mail',
        emailPlaceholder: 'E-Mail für Nutzer zum Versenden von CVs',
        url: 'URL für Bewerbung',
        urlPlaceholder: 'Wohin sollten wir Benutzer umleiten?',
        startsFrom: 'Beginnt ab',
        salaryFrom: 'Jahresgehalt beginnt ab',
        salaryFromPlaceholder: 'Displays as `from X annually`',
        salaryTo: 'Jahresgehalt bis zu',
        salaryToPlaceholder: 'Displays as `up to X annually`',
        successMessage: 'Job wurde erfolgreich hinzugefügt!',
        errorNoId:
            'Sorry, es ist etwas passiert und wir haben keine ID des hinzugefügten Jobs!',
        recogniseTags: 'Tags erkennen',
        recogniseTagsText:
            '- Wir können einige Tags (Fähigkeiten und Sprachen) aus Ihrer Beschreibung abrufen, wenn Sie hier klicken',
    },
    offerAdded: {
        title: 'Ihr Job wurde hinzugefügt!',
        goToList: 'Liste Ihrer Jobs',
        successText:
            'Wir brauchen etwas Zeit, um die Daten zu überprüfen und zu genehmigen. Es kann bis zu 4 Stunden dauern. Sie werden benachrichtigt, wenn es auf der Website erscheint.',
    },
    updateJob: {
        title: 'Job aktualisieren',
        button: 'Aktualisieren',
        formSuccess: 'Job wurde erfolgreich aktualisiert!',
        formError: 'Fehler ist aufgetreten, bitte kontaktieren Sie uns.',
    },
    offersList: {
        title: 'Liste Ihrer Stellenangebote',
        descriptionText:
            'Jobs in dieser Liste sehen genauso aus, wie der Benutzer sie sieht.',
    },
    recruiterMenu: {
        account: 'Konto und Einstellungen',
        offersList: 'Meine Jobs',
        findForCandidate: 'Find for candidate',
        addOffer: 'Job veröffentlichen',
    },
    userMenu: {
        seeRelevantOffers: 'Relevante Jobs',
        updateProfile: 'Profil',
        account: 'Konto und Einstellungen',
    },
    findForCandidate: {
        title: 'Relevante Angebote für Ihren Kandidaten',
        searchByFile: 'Kennwörter von `{fileName}`:',
        pageDescription:
            'Laden Sie ein CV eines Bewerbers hoch und wir versuchen, einen Job unter Ihren Angeboten zu finden - ',
        button: 'CV des Kandidaten hochladen',
    },
    offerPage: {
        title: '{title}',
        requirements: 'Was ist benötigt?',
        destinationsLabel: 'Location',
        benefitsLabel: 'Leistungen',
        skillsLabel: 'Fähigkeiten',
        programmingLanguagesLabel: 'Tech-Stack',
        description: 'Worum geht es?',
        viewOffer: 'Offer anzeigen',
        workType: 'wir bieten <b itemprop="employmentType">{workType}</b>',
        createdAt: 'was posted on: ',
        backToList: 'Back to offers',
        startsAsap: 'Beginn <b>as soon as possible</b>',
        startsAt: 'Beginn <b>at {date}</b>',
        salaryRange:
            'und Gehalt ab <b><span itemprop="minValue">{from}</span> {currency} bis zu <span itemprop="maxValue">{to}</span> {currency}</b>',
        salaryFrom:
            'und Gehalt ab <b><span itemprop="minValue">{salary}</span> {currency}</b>',
        salaryTo:
            'und Gehalt bis zu <b><span itemprop="maxValue">{salary}</span> {currency}</b>',
    },
    logoText: '{companyName} Logo',
    relevantOffers: {
        title: 'Relevante Angebote',
        notFound:
            'Unfortunately we couldn`t find any relevant offers for you. Please, try to refine the search',
        emptyProfileLink: 'Bitte füllen Sie Ihr Profil aus',
        emptyProfile: ', damit wir die besten Angebote für Sie finden können',
        pageDescription:
            'Wir haben Ihr Profil und Ihr CV verwendet, um die relevanteste Angebote zu finden',
    },
    message: {
        defaultFailed:
            'Sorry, etwas ist schief gelaufen. Bitte kontaktieren Sie uns.',
        missingToken:
            'Benutzertoken ist ungültig. Bitte melden Sie sich erneut an.',
        badRequest: 'Ungültige Anforderungsparameter.',

        jobNotFound: 'Angebot wurde nicht gefunden.',
        jobFail: 'Sorry, Angebotsanfrage ist fehlgeschlagen.',
        jobSuccessAdd: 'Jobangebot wurde erfolgreich hinzugefügt!',
        jobSuccessUpdate: 'Jobangebot wurde erfolgreich aktualisiert!',
        jobSuccessRemove: 'Jobangebot wurde erfolgreich entfernt!',

        userSuccess: 'User request was successfully sent.',
        userFailed:
            'Sorry, Benutzeranfrage ist fehlgeschlagen. Kontaktieren Sie uns.',
        userNotFound:
            'Sorry, Benutzer wurde nicht gefunden. Kontaktieren Sie uns.',
        userEmptyUpdateFields:
            'Die vom Benutzer aktualisierten Felder waren leer.',
        userBadCredentials:
            'Leider haben wir keinen Benutzer mit dieser E-Mail und diesem Kennwort gefunden',
        userBadPassword: 'We think you have a mistake in current password.',

        feedbackFail:
            'Leider wurde kein Feedback gesendet, bitte versuchen Sie es später erneut.',
        feedbackSuccess: 'Feedback wurde erfolgreich gesendet',

        saveSearchFailed:
            'Unfortunately, we cannot save your search at the moment',
        saveSearchSuccess: 'Search is successfully saved!',
        saveSearchUpdateSuccess: 'Schedule is successfully updated!',
    },
    accountRemoved: {
        title: 'Ihr Konto und alle zugehörigen Daten wurden entfernt',
        text:
            'Alle Information, die Sie für die Suche nach relevanten Angeboten bereitgestellt haben, wurde entfernt.<br>' +
            'Ihr Konto wurde entfernt und werden nirgendwo wiederverwendet.',
    },
    aria: {
        toggleMenuSection: 'Toggle-Menü',
        goToOffer: 'Zum Angebot',
        send: 'Formular absenden',
        boldText: 'fett',
        italicText: 'kursiv',
        strikeText: 'durchstreichen',
        underlineText: 'unterstreichen',
        makeCodeBlock: 'Fügen Sie einen Codeblock hinzu',
        addParagraph: 'Neuen Absatz hinzufügen',
        addH1: 'Überschrift 1 hinzufügen',
        addH2: 'Überschrift 2 hinzufügen',
        addH3: 'Überschrift 3 hinzufügen',
        bulletList: 'Bullet-Liste hinzufügen',
        orderedList: 'Sortierte Liste hinzufügen',
        quote: 'Add quote',
        undo: 'Rückgang',
        redo: 'Wiederholen',
        addImage: 'Bild hinzufügen',
        addLink: 'Link hinzufügen',
        removeLink: 'Link löschen',
        removeTag: 'Tag löschen',
        changeSiteLanguage: 'Website-Sprache ändern',
        changeOffersLanguage: 'Angebote-Sprache ändern',
        openLogInPopover: 'Log-In-Popover öffnen',
        openProfile: 'Profil öffnen',
        openSiteMenu: 'Menü öffnen',
        close: 'Schließen',
        submitForm: 'Absenden',
        openSignUpPopover: 'Sign-Up-Popover öffnen',
    },
    popularOffers: 'Beliebte Stellenangebote',
    popularQueries: {
        cityPagesTitle: 'Beliebte Städte',
        title: 'Beliebte Jobs',
        offerTitle: 'Beliebte Jobs an diesem Ort',
        linkText: '{programming} in {location}',
    },
    similarOffersTitle: 'Ähnliche Angebote',
    error: {
        noOfferTitle: 'Jobangebot ist leider nicht mehr aktiv.',
        noOfferTextSubstitutes:
            'Leider können wir Ihnen gesuchte Angebot nicht finden. <br> Bitte prüfen Sie ähnliche Angebote. Wir sind sicher, dass Sie etwas finden können, das zu Ihnen passt!',
        noOfferText:
            'Leider können wir Ihnen gesuchte Angebot nicht finden. <br> Bitte besuchen Sie unsere Homepage. Wir sind sicher, dass Sie etwas finden können, das zu Ihnen passt!',
        notFound: 'Uups, die Seite wurde nicht gefunden',
        notfoundText: 'Leider Seite, die Sie suchen, wurde nicht gefunden.',
        toHomepage: 'Hier ist ein Link, um zur Startseite zurückzukehren',
        error: 'Etwas Seltsames ist passiert',
        errorText:
            'Leider ist ein Fehler aufgetreten und wir arbeiten bereits daran!',
    },
    blog: {
        title: '> clusterjobs magazine',
        backToMain: 'Zurück zum Blog',
        nextArticle: 'Nächster Artikel',
        seoTitle: 'clusterjobs blog - wir schreiben über Jobsuchende Industrie',
        seoKeywords:
            'Stellensuche, AI in HR, HR, Arbeit im Deutschland, wwliphy, Arbeit im Deutschland bekommen',
        seoDescription:
            'Wir bieten Artikel über Tech- und Non-Tech-Trends in der Jobsuche ' +
            'und im HR-Bereich. Wir möchten Menschen aus dem Ausland helfen, ' +
            'auf der ganzen Welt Arbeit zu finden',
    },
    students: {
        features: {
            hidden: 'Our features',
            jobs: {
                title: 'More than 100,000 IT-jobs in Germany',
                text:
                    'clusterjobs aggregates offers from partners worldwide to show only best one and most relevant for our users.',
            },
            it: {
                title: 'Student and after-graduate jobs in IT',
                text:
                    'Find your werkstudent job or your early-career job in case you are graduating. clusterjobs helps you to get the best suit for you!',
            },
            ai: {
                title: 'Use smart AI search by your CV',
                text:
                    'Just upload your CV and our AI engine will recognise your strengths and will show relevant jobs.',
            },
        },
        interestedQuestion:
            'Interested? We can notify you about new relevant jobs',
        headTitle: `Find best student's jobs in Germany. Start your career in IT with clusterjobs.<br> AI will find best suiting job for you!`,
        notifyMe: 'Notify me',
        register: 'Register',
        studentJobs: 'Student jobs',
        graduateJobs: 'After-graduate jobs',
        imgAlt: 'Man with a binoculars',
        showMore: 'Show me more',
        logoTitle: 'clusterjobs - use tech search to find tech job',
        registerCta: 'Register to be notified about new and related offers',
        registerForm: {
            signUpTitle: 'Find your dream job in three steps',
        },
    },
    authentication: {
        signUp: 'Register',
        email: 'Email address',
        repeatEmail: 'Please repeat your email',
        password: 'Password',
        isRecruiter: 'I am recruiter',
    },
    partners: {
        title: 'Partners',
    },
};
