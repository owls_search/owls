export default '' +
    '<h3>Gesch&auml;ftsbedingungen</h3>\n' +
    '<p><span>Willkommen bei clusterjobs.de!</span></p>\n' +
    '<p><span>Diese Allgemeinen Gesch&auml;ftsbedingungen beschreiben die Regeln und Bestimmungen f&uuml;r die Nutzung der Website von clusterjobs, die sich auf clusterjobs.de befindet.</span></p>\n' +
    '<p><span>Durch den Zugriff auf diese Website erkl&auml;ren Sie sich mit diesen Bedingungen einverstanden. Verwenden Sie clusterjobs.de nicht weiter, wenn Sie nicht damit einverstanden sind, alle auf dieser Seite aufgef&uuml;hrten Gesch&auml;ftsbedingungen zu akzeptieren.</span></p>\n' +
    '<p><span>F&uuml;r diese Allgemeinen Gesch&auml;ftsbedingungen, Datenschutzerkl&auml;rung und Haftungsausschluss und alle Vereinbarungen gilt die folgende Terminologie: "Kunde", "Sie" und "Ihr" bezieht sich auf Sie, die Person, die sich auf dieser Website anmeldet, und die den Gesch&auml;ftsbedingungen des Unternehmens entspricht. "Das Unternehmen", "uns selbst", "wir", "unser" und "uns" bezieht sich auf unser Unternehmen. "Partei", "Parteien" oder "uns" bezieht sich sowohl auf den Kunden als auch auf uns. Alle Bedingungen beziehen sich auf das Angebot, die Annahme und die Gegenleistung der Zahlung, die erforderlich ist, um den Prozess unserer Unterst&uuml;tzung f&uuml;r den Kunden auf die am besten geeignete Weise durchzuf&uuml;hren, um ausdr&uuml;cklich die Bed&uuml;rfnisse des Kunden hinsichtlich der Erbringung der angegebenen Dienstleistungen des Unternehmens gem&auml;&szlig; und unterliegen den geltenden niederl&auml;ndischen Gesetzen. "Lebenslauf", "Resume" or "Curriculum Vitae (CV)" ist eine kurze schriftliche Darstellung Ihrer pers&ouml;nlichen Daten, Ihrer Ausbildung und Ihrer Jobs. Jegliche Verwendung der obigen Terminologie oder anderer W&ouml;rter im Singular, Plural, in der Gro&szlig;schreibung und / oder er / sie oder sie wird als austauschbar angesehen und bezieht sich daher auf diese.</span></p>\n' +
    '<h3>Dienstleistungen von clusterjobs</h3>\n' +
    '<p><span>clusterjobs stellt bestimmte Stellenanzeigen und weitere jobrelevante Inhalte, darunter Links zu Webseiten von Dritten (solche Anzeigen und weiteren Inhalte werden als &bdquo;Stellenanzeigen&ldquo; bezeichnet) in den Suchergebnissen von clusterjobs oder an anderer Stelle auf der Webseite zur Verf&uuml;gung. Die Stellenanzeigen werden von Dritten erstellt und bereitgestellt, die nicht von clusterjobs kontrolliert werden, und Sie akzeptieren, dass wir keine Kontrolle &uuml;ber Stellenanzeigen aus&uuml;ben. clusterjobs ist in keiner Weise verpflichtet, Stellenanzeigen zu pr&uuml;fen und in die Suchergebnisse oder andere Listings einzubeziehen, und kann aus beliebigem Grund Stellenanzeigen von der Webseite ausschlie&szlig;en oder entfernen. Wir best&auml;tigen weder die Richtigkeit noch die Vollst&auml;ndigkeit von Stellenanzeigen oder anderen von einem Arbeitgeber oder sonstigem Nutzer &uuml;bermittelten Daten. Dies gilt unter anderem f&uuml;r die Identit&auml;t des Arbeitgebers oder Nutzers. clusterjobs ist f&uuml;r Inhalt, Richtigkeit, Vollst&auml;ndigkeit, Rechtm&auml;&szlig;igkeit, Zuverl&auml;ssigkeit oder Verf&uuml;gbarkeit von Stellenanzeigen weder verantwortlich noch haftbar zu machen. Sie willigen ein, dass clusterjobs auch Suchoptionen bereitstellen kann, um die Suchergebnisse der Stellenanzeigen nach Jobtyp einzuschr&auml;nken (z. B. Vollzeit, Teilzeit usw.), und dass solche Jobtypen auf unabh&auml;ngige Weise und g&auml;nzlich durch clusterjobs erstellt werden und es sich dabei nicht um eine direkte Wiedergabe der tats&auml;chlichen Stellenanzeige handelt.</span></p>\n' +
    '<p><span><br /><br /></span></p>\n' +
    '<h3>Regeln f&uuml;r die Webseite</h3>\n' +
    '<p><span><br /></span><span>Sie versichern, ausschlie&szlig;lich &uuml;ber die von clusterjobs bereitgestellte Schnittstelle auf die Webseite zuzugreifen (oder dies zu versuchen), sofern Ihnen nicht ein anderes Verfahren in einer separaten, schriftlichen Vereinbarung von clusterjobs genehmigt wurde. Sie stimmen zu, sich an keinerlei Aktivit&auml;ten zu beteiligen, die die Webseite (oder die mit dieser verbundenen Server und Netzwerke) st&ouml;ren oder unterbrechen. Sofern Ihnen dies nicht in einer separaten, schriftlichen Vereinbarung von clusterjobs genehmigt wurde, sichern Sie zu, dass Sie die Webseite zu keinerlei Zwecken crawlen, scrapen, reproduzieren, duplizieren, kopieren, verkaufen, vermarkten oder weiterverkaufen. Sie versichern, dass Sie allein f&uuml;r jegliche Verst&ouml;&szlig;e gegen Ihre Verpflichtungen im Rahmen dieser Vereinbarung und die Folgen solcher Verst&ouml;&szlig;e (einschlie&szlig;lich der ggf. von clusterjobs erlittenen Sch&auml;den oder Verluste) verantwortlich sind (und dass clusterjobs keiner Verantwortung oder Haftung gegen&uuml;ber Ihnen oder Dritten unterliegt).</span></p>\n' +
    '<p><span><br /></span><span>Sie versichern, dass Sie keinen sch&auml;dlichen Code an clusterjobs &uuml;bertragen oder auf bzw. &uuml;ber die Webseite (sei es als Nutzerinhalt oder anderweitig) hochladen und die Webseite nicht zu Ihrem eigenen wirtschaftlichen Gewinn nutzen oder missbrauchen. &bdquo;Sch&auml;dlicher Code&ldquo; meint jede Software (mitunter als &bdquo;Viren&ldquo;, &bdquo;W&uuml;rmer&ldquo;, &bdquo;Trojaner&ldquo;, &bdquo;Zeitbomben&ldquo;, &bdquo;Zeitschl&ouml;sser&ldquo;, &bdquo;Absturzcode&ldquo;, &bdquo;Fallen&ldquo;, &bdquo;Zugangscodes&ldquo;, &bdquo;Cancelbots&ldquo; oder &bdquo;Fallt&uuml;ren&ldquo; bezeichnet), die (a) beliebige Daten, Speichermedien, Programme, Systeme sowie Ausr&uuml;stungen oder Kommunikationsvorg&auml;nge besch&auml;digen, unterbrechen, beeintr&auml;chtigen, st&ouml;ren, abh&ouml;ren, enteignen oder anderweitig auf beliebige Weise und auf beliebigen Ereignissen beruhend behindern soll, darunter beispielsweise (i) das &Uuml;berschreiten einer Anzahl an Kopien, (ii) das &Uuml;berschreiten einer Anzahl von Nutzern, (iii) das Verstreichen eines Zeitraums, (iv) das Vorr&uuml;cken auf ein bestimmtes Datum oder einen anderen Zahlwert oder (v) die Verwendung einer bestimmten Funktion, oder (b) es einer nicht autorisierten Person erm&ouml;glicht, derartige Resultate zu verursachen, oder (c) es einer nicht autorisierten Person erm&ouml;glicht, ohne deren Wissen und Genehmigung auf die Daten einer anderen Person zuzugreifen. Ohne Einschr&auml;nkung des Vorangegangenen und lediglich als Beispiel gilt, dass Nutzer folgende Aktionen nicht durchf&uuml;hren d&uuml;rfen:</span></p>\n' +
    '<ul>\n' +
    '<li><span>Unaufgeforderte gewerbliche E-Mails erzeugen oder erm&ouml;glichen (&bdquo;Spam&ldquo;). Derartige Aktionen umfassen, sind jedoch nicht beschr&auml;nkt auf:</span></li>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>das Senden von Nachrichten unter Versto&szlig; gegen geltende Anti-Spam-Gesetze,</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>das Imitieren oder Nachahmen anderer Personen und deren E-Mail-Adressen oder das Erstellen falscher Konten, um Spam zu senden,</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>Datamining in Bezug auf jegliches Eigentum von clusterjobs,</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>das Senden von Nachrichten an Nutzer, die angegeben haben, nicht kontaktiert werden zu wollen,</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>das Verkaufen, Austauschen oder Verteilen von Kontaktdaten einer Person an Dritte, ohne die Kenntnis und die fortlaufende Genehmigung f&uuml;r eine solche Offenlegung durch diese Person, und</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>das Verwenden der Lebenslauf-Kontakte unter Versto&szlig; gegen die Richtlinien von clusterjobs, z. B. bei geringen R&uuml;cklaufquoten der kontaktierten Personen.</span></p>\n' +
    '<li><span>jegliche illegalen, diffamierenden, beleidigenden, betr&uuml;gerischen, gegen Regeln versto&szlig;ende, obsz&ouml;ne oder anderweitig anst&ouml;&szlig;ige Inhalte senden, hochladen, verteilen oder verbreiten oder ein solches Vorgehen anbieten. clusterjobs kann die &Uuml;bermittlung solcher Inhalte blockieren.</span></li>\n' +
    '<li><span>absichtlich Viren, W&uuml;rmer, Fehler, Trojaner, besch&auml;digte Dateien, Falschmeldungen oder beliebigen anderen sch&auml;dlichen Code oder zerst&ouml;rerische oder irref&uuml;hrende Elemente verbreiten,</span></li>\n' +
    '<li><span>Schneeballsysteme oder &auml;hnliche Programme umsetzen oder weiterleiten,</span></li>\n' +
    '<li><span>f&uuml;r Minderj&auml;hrige sch&auml;dliche Inhalte &uuml;bermitteln,</span></li>\n' +
    '<li><span>sich als andere Personen ausgeben (durch die Verwendung einer E-Mail-Adresse oder anderweitig) oder auf andere Weise sich selbst oder die Quelle einer E-Mail falsch ausweisen,</span></li>\n' +
    '<li><span>das geistige Eigentum anderer oder andere gesch&uuml;tzte oder vertrauliche Daten ohne die Genehmigung des Eigent&uuml;mers oder Lizenzgebers &uuml;bertragen. Nutzer, die wiederholt geistiges Eigentum von Anderen posten, werden bei clusterjobs gesperrt.</span></li>\n' +
    '<li><span>gegen die Rechte (z. B. die Datenschutz- oder &Ouml;ffentlichkeitsrechte) anderer versto&szlig;en,</span></li>\n' +
    '<li><span> &nbsp;</span><span>illegale Aktivit&auml;ten bewerben oder zu diesen aufrufen,</span></li>\n' +
    '<li><span>die Nutzung der Webseite anderer clusterjobs-Nutzer beeintr&auml;chtigen,</span></li>\n' +
    '<li><span>unter Versto&szlig; gegen die Vereinbarung mehrere Nutzerkonten anlegen oder Nutzerkonten automatisch oder unter Vorspiegelung falscher Tatsachen erstellen,</span></li>\n' +
    '<li><span> &nbsp;</span><span>clusterjobs-Konten f&uuml;r beliebige nicht autorisierte gewerbliche Zwecke verkaufen, handeln, weiterverkaufen oder anderweitig ausnutzen,</span></li>\n' +
    '<li><span>einen beliebigen Teil der Webseite ver&auml;ndern, &uuml;bernehmen, &uuml;bersetzen oder r&uuml;ckentwickeln,</span></li>\n' +
    '<li><span> &nbsp;</span><span>jegliche Urheberrechts-, Marken- oder andere Eigentumshinweise von der Webseite entfernen,</span></li>\n' +
    '<li><span>ohne die ausdr&uuml;ckliche schriftliche Genehmigung von clusterjobs jegliche Bestandteile zur Webseite geh&ouml;render Seiten neu formatieren oder gestalten,</span></li>\n' +
    '<li><span>sich in Bezug auf Multi-Level-Marketing-Programme (MLM), Stellen, die eine Vorabzahlung erfordern, oder jegliche Themen, die owlpihy als nachteilig f&uuml;r die Nutzer einstuft, an andere owlpihy-Nutzer wenden,</span></li>\n' +
    '<li><span>ohne Genehmigung mehrere owlpihy-Konten erstellen,</span></li>\n' +
    '<li><span>mehrere clusterjobs-Konten erstellen, f&uuml;r die Sie nicht Ihren Unternehmens- oder Firmennamen verwenden, um eine h&ouml;here Sichtbarkeit im Free-to-Post-System zu erzielen,</span></li>\n' +
    '<li><span>Funktionseinschr&auml;nkungen oder -aufhebungen umgehen,</span></li>\n' +
    '<li><span>falsche Angaben machen,</span></li>\n' +
    '<li><span>die clusterjobs-Datenbank scrapen,</span></li>\n' +
    '<li><span>jegliche clusterjobs-Inhalte zu Wettbewerbszwecken scrapen oder anderweitig replizieren.</span></li>\n' +
    '</ul>\n' +
    '<p><span>clusterjobs beh&auml;lt sich das Recht vor, beliebige Nutzerinhalte (einschlie&szlig;lich der Inhalte von &uuml;ber die oder an die Webseite oder clusterjobs gesendeten Nachrichten und Materialien) zu verwenden, um potenziell illegale oder unerlaubte Handlungen oder jegliche andere, f&uuml;r andere Nutzer nachteilige Aktivit&auml;ten zu unterbinden. clusterjobs beh&auml;lt sich das Recht vor, verschiedene Methoden zum Erkennen und Blockieren der o. g. irregul&auml;ren Aktivit&auml;ten sowie zum &Uuml;berpr&uuml;fen von Nutzerinhalten einzusetzen, um Missbrauch wie z. B. Spam oder Betrug zu verhindern.</span></p>\n' +
    '<h3>Anmeldung: Kontaktdaten</h3>\n' +
    '<p><span>F&uuml;r einige Bereiche der Website, in denen Sie ein Konto erstellen k&ouml;nnen, m&uuml;ssen Sie sich registrieren oder eine E-Mail-Adresse angeben. Wenn Sie sich registrieren, erkl&auml;ren Sie sich mit (a) korrekt, genau, aktuell und vollst&auml;ndig (einschlie&szlig;lich E-Mail-Adresse) &uuml;ber sich selbst und (b) Ihren Benutzernamen und Ihr Passwort ein. Wenn Sie ein Arbeitgeber oder ein Personalvermittler sind, wird Ihr Konto f&uuml;r kommerzielle Zwecke verwendet, nicht f&uuml;r private Zwecke. Sie ist nicht f&uuml;r den Missbrauch oder den Missbrauch Ihrer E-Mail-Adresse verantwortlich. Durch die Einrichtung eines clusterjobs-Kontos stimmen Sie zu, obligatorische E-Mail-Updates zu erhalten.</span></p>\n' +
    '<p><span>Sie erkl&auml;ren sich damit einverstanden, dass Sie im Rahmen der von Ihnen erbrachten Dienstleistungen mit Dritten kommunizieren oder diesen zustimmen. E-Mail, Telefon (einschlie&szlig;lich Mobiltelefon) oder E-Mail unter Verwendung der Kontaktdaten, die Sie angeben oder die Sie von Dritten erhalten. Wenn Sie eine Mobiltelefonnummer &uuml;bermitteln, erkl&auml;ren Sie sich damit einverstanden, Mitteilungen &uuml;ber diese Mobiltelefonnummer zu erhalten.</span></p>\n' +
    '<h3>Cookies</h3>\n' +
    '<p><span>Wir setzen Cookies ein. Durch den Zugriff auf clusterjobs.de stimmten Sie der Verwendung von Cookies in &Uuml;bereinstimmung mit den Datenschutzbestimmungen von clusterjobs zu.</span></p>\n' +
    '<p><span>Die meisten interaktiven Websites verwenden Cookies, um die Daten des Benutzers bei jedem Besuch abzurufen. Cookies werden von unserer Website verwendet, um die Funktionalit&auml;t bestimmter Bereiche zu erm&ouml;glichen, um es den Besuchern unserer Website zu erleichtern. Einige unserer Partner- / Werbepartner verwenden m&ouml;glicherweise auch Cookies.</span></p>\n' +
    '<p><em><span>Session-Cookies</span></em></p>\n' +
    '<p><span>Wir verwenden Cookies, um es der Website zu erm&ouml;glichen, die Browsersitzung eines Benutzers auf der clusterjobs-Website eindeutig zu identifizieren, und um diese Informationen mit unserem Website-Server zu koordinieren.</span></p>\n' +
    '<p><em><span>Analytics</span></em></p>\n' +
    '<p><span>Wir verwenden Google Analytics-Cookies, mit denen wir in Verbindung mit den Protokolldateien unseres Webservers eindeutige, aber anonyme Benutzer identifizieren k&ouml;nnen. Diese Cookies k&ouml;nnen somit vom Benutzer auf der Website angezeigt und verwendet werden. Wir verwenden auch Systeme zur Nachverfolgung und Sortierung von Bewerbern, um Statistiken &uuml;ber die Besucher unserer Website zu erhalten und um zu verstehen, von wo aus die Benutzer auf clusterjobs landen. Dies hilft uns dabei, Feedback zu sammeln, damit wir die Website verbessern und unsere Nutzer besser bedienen k&ouml;nnen.</span></p>\n' +
    '<p><em><span>Plattform&uuml;bergreifende Werbung und Benutzererkennung</span></em></p>\n' +
    '<p><span>Wir verwenden auch Cookies von Facebook. Diese Cookies funktionieren auf unterschiedliche Weise, werden jedoch beide in Verbindung mit der Werbung von clusterjobs verwendet, die einem Nutzer unserer Website auf Facebook zur Verf&uuml;gung gestellt wird, der Erkennung dieser Nutzer und der Ger&auml;te, die der Nutzer f&uuml;r den Zugriff auf Facebook und clusterjobs verwendet.</span></p>\n' +
    '<p><span>Ihre Nutzung von Websites und Plattformen Dritter wie Facebook sowie die Datenschutzpraktiken dieser Plattformen unterliegen separaten Bedingungen, Bedingungen und Richtlinien, f&uuml;r die clusterjobs nicht verantwortlich ist. Sie sollten die Allgemeinen Gesch&auml;ftsbedingungen, Richtlinien und Richtlinien von Facebook lesen, in denen Sie weitere Informationen dazu finden, wie Informationen &uuml;ber Sie auf ihren Plattformen verwendet werden und wie Sie Ihre Privatsph&auml;re-Einstellungen festlegen k&ouml;nnen.</span></p>\n' +
    '<p><span>clusterjobs verwendet die AdWords- und Remarketing-Listenfunktionen von Google Analytics f&uuml;r Inserenten im Display-Netzwerk. Tats&auml;chlich und Google verwenden Erstanbieter-Cookies (z. B. Google Analytics-Cookies) und Drittanbieter-Cookies (z. B. DoubleClick-Cookies) zu Informations- und Optimierungszwecken sowie zur Bereitstellung von Anzeigen, die auf Ihren vorherigen Besuchen der clusterjobs-Website basieren. Dies bedeutet, dass Anbieter wie Google clusterjobs auf anderen Websites, die Sie im Internet besuchen, Anzeigenmaterial anzeigen.</span></p>\n' +
    '<p><span>Sie k&ouml;nnen Google Analytics f&uuml;r Werbetreibende im Display-Netzwerk, einschlie&szlig;lich AdWords, sowie personalisierte Anzeigen im Google Display-Netzwerk im Google Ads Preferences Manager deaktivieren. Um den Besuchern der Website mehr Kontrolle dar&uuml;ber zu geben, welche Daten von Google Analytics erfasst werden, und um Google-Programmen zu stoppen, hat Google das Add-On f&uuml;r die Deaktivierung von Browsern entwickelt. Dies ist hier verf&uuml;gbar:</span><a href="https://tools.google.com/dlpage/gaoptout?hl=de"> <span>Browser-Add-On zum Deaktivieren von Google Analytics</span></a><span>.</span></p>\n' +
    '<p><span>Weitere Informationen zu den einzelnen von uns verwendeten Cookies und den Zwecken, f&uuml;r die sie verwendet werden, finden Sie in der folgenden Tabelle:</span></p>\n' +
    '<table>\n' +
    '<tbody>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<h3>Cookie-Typ </h3>\n' +
    '</td>\n' +
    '<td>\n' +
    '<h3> Zweck</h3>\n' +
    '</td>\n' +
    '</tr>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<p><span>Google Analytics</span></p>\n' +
    '<p><span>_ga</span></p>\n' +
    '</td>\n' +
    '<td>\n' +
    '<p><span>Dieses Cookie wird von Google gesetzt. Sie erm&ouml;glicht es clusterjobs, Informationen &uuml;ber die Nutzung der clusterjobs-Website durch unsere Benutzer zu erhalten, wie z. B. die Besuchszeit, die besuchten Seiten, ob der Benutzer zuvor besucht hat, und die Website, die vor dem Besuch der clusterjobs-Website besucht wurde.</span></p>\n' +
    '<p><span>Weitere Informationen zu Google Analytics finden Sie unter: </span></p>\n' +
    '<p><a href="https://www.google.com/policies/technologies/types/"><span>https://www.google.com/policies/technologies/types/</span></a></p>\n' +
    '</td>\n' +
    '</tr>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<p><span>Facebook Pixel</span></p>\n' +
    '</td>\n' +
    '<td>\n' +
    '<p><span>Dieses Cookie wird von Facebook gesetzt. Sie erm&ouml;glicht es clusterjobs, Werbekampagnen auf Facebook zu messen, zu optimieren und zu erstellen. Insbesondere erm&ouml;glicht es clusterjobs, zu sehen, wie sich unsere Nutzer zwischen Ger&auml;ten bewegen, wenn sie auf die clusterjobs-Website und Facebook zugreifen, um sicherzustellen, dass die Werbung von clusterjobs bei Facebook von Nutzern am wahrscheinlichsten f&uuml;r diese Werbung gesehen wird, indem analysiert wird, welche Inhalte ein Nutzer gesehen und mit ihm interagiert hat mit auf der clusterjobs-Website. Weitere Informationen zum Facebook-Pixel finden Sie unter:</span></p>\n' +
    '<p><a href="https://en-gb.facebook.com/business/help/651294705016616"><span>https://en-gb.facebook.com/business/help/651294705016616</span></a></p>\n' +
    '</td>\n' +
    '</tr>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<p><span>vuex &nbsp;</span></p>\n' +
    '</td>\n' +
    '<td>\n' +
    '<p><span>Dieses Cookie wird von clusterjobs gesetzt. Es wird verwendet, um ein anonymisiertes Benutzer-Token vom clusterjobs-Website-Server aufzubewahren. Au&szlig;erdem werden die Daten des Benutzers bez&uuml;glich seines Profils bevorzugte Fertigkeiten, Programmiersprachen, Jobtitel, bevorzugte Arbeitsorte, Jobtyp (vollzeit, teilzeit, Praktikum, Home-Office) nicht ber&uuml;cksichtigt , Benutzertyp (&uuml;blich oder Personalbeschaffer) sowie Name der gespeicherten CV-Datei, falls diese hochgeladen wurde. Wir speichern auch Informationen, wenn der Benutzer die Benachrichtigung &uuml;ber die Cookie-Richtlinie geschlossen hat, um sie nicht mehr anzuzeigen.</span></p>\n' +
    '</td>\n' +
    '</tr>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<p><span>i18n_redirected</span></p>\n' +
    '</td>\n' +
    '<td>\n' +
    '<p><span>Dieses Cookie wird von owlipjy gesetzt. Es wird verwendet, um aufzuzeichnen, wann ein Benutzer der clusterjobs-Website die Seite besucht, und die Website erkennt, dass die Sprache des Browsers die Sprache nur beim ersten Besuch wechselt.</span></p>\n' +
    '</td>\n' +
    '</tr>\n' +
    '</tbody>\n' +
    '</table>\n' +
    '<p><span>Most of our cookies expire within 30 days, although our analytics cookies may persist for 1 year. </span></p>\n' +
    '<h3>Lizenz</h3>\n' +
    '<p><span>Soweit nicht anders angegeben, besitzen clusterjobs und / oder ihre Lizenzgeber die geistigen Eigentumsrechte f&uuml;r alles Material auf clusterjobs.de. Alle geistigen Eigentumsrechte sind vorbehalten. Sie k&ouml;nnen von clusterjobs.de darauf zugreifen, um Ihren pers&ouml;nlichen Gebrauch zu machen, der den in diesen Bedingungen festgelegten Einschr&auml;nkungen unterliegt.</span></p>\n' +
    '<p><span>Du darfst nicht:</span></p>\n' +
    '<ul>\n' +
    '<li><span> &nbsp;&nbsp;</span> <span>Ver&ouml;ffentlichen Sie das Material von clusterjobs.de erneut</span></li>\n' +
    '<li><span> &nbsp;&nbsp;</span> <span>Verkaufen, mieten oder Unterlizenzen von Material von clusterjobs.de</span></li>\n' +
    '<li><span> &nbsp;&nbsp;</span> <span>Reproduzieren, kopieren oder kopieren Sie Material von clusterjobs.de</span></li>\n' +
    '<li><span> &nbsp;&nbsp;</span> <span>Verteilen Sie Inhalte von clusterjobs.de</span></li>\n' +
    '</ul>\n' +
    '<p><span>Diese Vereinbarung beginnt am Datum dieses Vertrags.</span></p>\n' +
    '<h3>Hyperlinks zu unseren Inhalten</h3>\n' +
    '<p><span>Die folgenden Organisationen k&ouml;nnen ohne vorherige schriftliche Genehmigung auf unsere Website verlinken:</span></p>\n' +
    '<ul>\n' +
    '<li><span>Regierungsbeh&ouml;rden;</span></li>\n' +
    '<li><span>Suchmaschinen;</span></li>\n' +
    '<li><span>Nachrichtenorganisationen;</span></li>\n' +
    '<li><span>Online-Verzeichnis-Distributoren k&ouml;nnen auf dieselbe Weise auf unsere Website verlinken, wie sie auf die Websites anderer b&ouml;rsennotierter Unternehmen verweisen. und</span></li>\n' +
    '<li><span>Systemweit anerkannte Unternehmen, mit Ausnahme der Anwerbung von gemeinn&uuml;tzigen Organisationen, Einkaufszentren f&uuml;r Wohlt&auml;tigkeitsorganisationen und Wohlt&auml;tigkeitsorganisationen f&uuml;r Wohlt&auml;tigkeitsorganisationen, die m&ouml;glicherweise keinen Hyperlink zu unserer Website haben.</span></li>\n' +
    '</ul>\n' +
    '<p><span>Diese Organisationen k&ouml;nnen auf unsere Homepage, auf Ver&ouml;ffentlichungen oder auf andere Website-Informationen verweisen, sofern der Link: (a) in keiner Weise t&auml;uscht; (b) impliziert nicht f&auml;lschlicherweise Sponsoring, Best&auml;tigung oder Genehmigung der verbindenden Partei und ihrer Produkte und / oder Dienstleistungen; und (c) passt in den Kontext der Website der verlinkenden Partei.</span></p>\n' +
    '<p><span>Wir k&ouml;nnen andere Link-Anfragen von folgenden Arten von Organisationen pr&uuml;fen und genehmigen:</span></p>\n' +
    '<ul>\n' +
    '<li><span>allgemein bekannte Verbraucher- und / oder Gesch&auml;ftsinformationsquellen;</span></li>\n' +
    '<li><span>dot.com-Community-Websites;</span></li>\n' +
    '<li><span>Vereinigungen oder andere Gruppen, die Wohlt&auml;tigkeitsorganisationen vertreten;</span></li>\n' +
    '<li><span>Online-Verzeichnisverteiler;</span></li>\n' +
    '<li><span>Internetportale;</span></li>\n' +
    '<li><span>Wirtschaftspr&uuml;fungs-, Rechts- und Beratungsunternehmen; und</span></li>\n' +
    '<li><span>Bildungseinrichtungen und Berufsverb&auml;nde.</span></li>\n' +
    '</ul>\n' +
    '<p><span>Wir werden Link-Anfragen dieser Organisationen genehmigen, wenn wir entscheiden, dass: (a) der Link uns nicht dazu bringen w&uuml;rde, sich selbst oder unsere akkreditierten Unternehmen nachteilig zu betrachten; (b) die Organisation hat keine negativen Aufzeichnungen bei uns; (c) der Vorteil, den uns die Sichtbarkeit des Hyperlinks bringt, kompensiert die Abwesenheit von Euliphie; und (d) die Verbindung befindet sich im Kontext allgemeiner Ressourceninformationen.</span></p>\n' +
    '<p><span>Diese Organisationen k&ouml;nnen auf unsere Homepage verlinken, solange der Link: (a) in keiner Weise t&auml;uscht; (b) impliziert nicht f&auml;lschlicherweise Sponsoring, Best&auml;tigung oder Genehmigung der verbindenden Partei und ihrer Produkte oder Dienstleistungen; und (c) passt in den Kontext der Website der verlinkenden Partei.</span></p>\n' +
    '<p><span>Wenn Sie eine der in Absatz 2 genannten Organisationen sind und an einer Verkn&uuml;pfung zu unserer Website interessiert sind, m&uuml;ssen Sie uns dies per E-Mail an clusterjobs mitteilen. Bitte geben Sie Ihren Namen, den Namen Ihrer Organisation, Ihre Kontaktinformationen sowie die URL Ihrer Website an, eine Liste aller URLs, von denen Sie einen Link auf unsere Website erstellen m&ouml;chten, und eine Liste der URLs auf unserer Website, zu denen Sie m&ouml;chten Verkn&uuml;pfung. Warten Sie 2-3 Wochen auf eine Antwort.</span></p>\n' +
    '<p><span>Zugelassene Organisationen k&ouml;nnen wie folgt auf unsere Website verlinken:</span></p>\n' +
    '<ul>\n' +
    '<li><span>durch Verwendung unseres Firmennamens; oder</span></li>\n' +
    '<li><span>Verwendung des Uniform Resource Locators, mit dem verkn&uuml;pft wird; oder</span></li>\n' +
    '<li><span>Durch die Verwendung einer anderen Beschreibung unserer Website, auf die verlinkt wird, ist dies im Kontext und Format des Inhalts auf der Website des Linkers sinnvoll.</span></li>\n' +
    '</ul>\n' +
    '<p><span>Ohne die Verwendung eines Markenlizenzvertrags ist die Verwendung des Logos oder anderer Grafiken von clusterjobs nicht gestattet.</span></p>\n' +
    '<p><span>Es ist Ihnen nicht gestattet, die Webseite von clusterjobs oder deren Inhalte f&uuml;r andere Zwecke als nicht gewerbliche Zwecke zu nutzen. Die Nutzung von automatisierten Systemen oder Software, ist f&uuml;r Dritte oder anderweitig betriebene, zur Extrahierung von Daten von der Webseite verboten (etwa Bildschrim-Scraping oder -Crawling). In der Regel bedeutet dies, dass die M&ouml;glichkeit einer unbefugten Nutzung der Webseite gegeben ist. Wenn Sie die Webseite gewerblich oder in einem anderen Bereich als eines Jobsuchenden oder Arbeitgebers von clusterjobs nutzen m&ouml;chten, oder wenn Sie Dienstleistungen ben&ouml;tigen, m&uuml;ssen Sie die Website nutzen, um die Online-Nutzungsbedingungen von clusterjobs zustimmen zu k&ouml;nnen . Wenden Sie sich an uns, wenn Sie weitere Informationen erhalten m&ouml;chten. Wir behalten uns jederzeit (ohne verbindliche) das Recht vor, Benutzernamen und URLs aus verschiedenen Gr&uuml;nden zur&uuml;ckzufordern.</span></p>\n' +
    '<p>&nbsp;</p>\n' +
    '<h3>iFrames</h3>\n' +
    '<p><span>Ohne vorherige Genehmigung und schriftliche Genehmigung d&uuml;rfen Sie keine Rahmen um unsere Webseiten erstellen, die die visuelle Darstellung oder das Erscheinungsbild unserer Website in irgendeiner Weise ver&auml;ndern.</span></p>\n' +
    '<p>&nbsp;</p>\n' +
    '<h3>Haftung f&uuml;r Inhalte</h3>\n' +
    '<p><span>Wir sind nicht f&uuml;r den Inhalt Dritter Website verantwortlich. Sie erkl&auml;ren sich damit einverstanden, uns zu sch&uuml;tzen und gegen alle Anspr&uuml;che zu sch&uuml;tzen, die sich auf Dritter Website ergeben. Auf keiner Website sollten Links angezeigt werden, die als verleumderisch, obsz&ouml;n oder kriminell ausgelegt werden oder die Rechte Dritter verletzen, anderweitig verletzen oder deren Verletzung bef&uuml;rworten.</span></p>\n' +
    '<p><span>Sie d&uuml;rfen keine illegalen, betr&uuml;gerischen, diskriminierenden, bedrohlichen, beleidigenden, diffamierenden, obsz&ouml;nen oder anderweitig anst&ouml;&szlig;igen Nutzerinhalte ver&ouml;ffentlichen. Dies gilt auch f&uuml;r sexuell, ethnisch, rassistisch oder anderweitig diskriminierende Verunglimpfungen oder irrelevante bzw. unkonstruktive Inhalte. Zudem d&uuml;rfen Sie (ohne ausdr&uuml;ckliche schriftliche Genehmigung unsererseits) keine Nutzerinhalte ver&ouml;ffentlichen, die gesch&uuml;tzte Informationen, Gesch&auml;ftsgeheimnisse, vertrauliche Daten, Werbung, Anzeigen, Kettenbriefe, Schneeballsysteme, Investitionsangebote oder andere nicht angeforderte gewerbliche Mitteilungen enthalten oder zu Spam oder Massenmails auffordern bzw. diese verursachen.</span></p>\n' +
    '<h3>Ihre Privatsph&auml;re</h3>\n' +
    '<p><span>Bitte lesen Sie unsere Datenschutzbestimmungen.</span></p>\n' +
    '<h3>Sicherheit</h3>\n' +
    '<p><span>clusterjobs wendet angemessene Sicherheitsma&szlig;nahmen an, um Verlust, Missbrauch oder Ver&auml;nderung der sich unter der Kontrolle von clusterjobs befindenden personenbezogenen Daten zu vermeiden. Jedoch ist keine Internet&uuml;bertragungs- oder elektronische Speichermethode vollkommen sicher. Beachten Sie zudem, dass E-Mails, &uuml;ber Ihren Internetbrowser gesendete Nachrichten und andere &auml;hnliche Mittel zur Kommunikation mit Nutzern nicht verschl&uuml;sselt sind. Daher k&ouml;nnen wir bei allem Bem&uuml;hen um den Schutz Ihrer Daten deren Sicherheit nicht garantieren.</span></p>\n' +
    '<p><span>Beachten Sie zudem, dass wir m&ouml;glicherweise externe Cloud-Dienstanbieter einsetzen, die Hosting, Datenspeicherung und weitere Dienste anbieten, die m&ouml;glicherweise nicht verhandelbaren Standardnutzungsbedingungen unterliegen. Diese Dienstanbieter haben uns oder die &Ouml;ffentlichkeit davon in Kenntnis gesetzt, dass sie von ihnen als angemessen erachtete Sicherheitsma&szlig;nahmen zum Schutz der Daten innerhalb ihres Systems umsetzen oder dass sie allgemein daf&uuml;r bekannt sind, solche Ma&szlig;nahmen durchzuf&uuml;hren. Wir sind jedoch (soweit gesetzlich zul&auml;ssig) nicht f&uuml;r Sch&auml;den haftbar, die aus dem Missbrauch von Daten, einschlie&szlig;lich personenbezogener Daten, durch diese Unternehmen entstehen.</span></p>\n' +
    '<p>&nbsp;</p>\n' +
    '<h3>Vorbehalt von Rechten</h3>\n' +
    '<p><span>Wir behalten uns das Recht vor, alle Links oder einen bestimmten Link zu unserer Website zu entfernen. Sie stimmen zu, alle Links zu unserer Website auf Anfrage umgehend zu entfernen. Wir behalten uns au&szlig;erdem das Recht vor, diese Allgemeinen Gesch&auml;ftsbedingungen und die Linking-Richtlinien jederzeit zu &auml;ndern. Durch die st&auml;ndige Verlinkung auf unsere Website erkl&auml;ren Sie sich damit einverstanden, an diese Linkbedingungen gebunden zu sein und diese einzuhalten.</span></p>\n' +
    '<p>&nbsp;</p>\n' +
    '<h3>Entfernung von Links von unserer Website</h3>\n' +
    '<p><span>Wenn Sie auf unserer Website einen Link finden, der aus irgendeinem Grund beleidigend ist, k&ouml;nnen Sie uns jederzeit kontaktieren und uns dar&uuml;ber informieren. Wir werden Antr&auml;ge auf Entfernung von Links pr&uuml;fen, sind aber nicht dazu verpflichtet oder Ihnen direkt zu antworten. Wir stellen nicht sicher, dass die Informationen auf dieser Website korrekt sind, wir &uuml;bernehmen keine Gew&auml;hr f&uuml;r die Vollst&auml;ndigkeit oder Richtigkeit. Wir versprechen auch nicht sicherzustellen, dass die Website verf&uuml;gbar bleibt oder dass das Material auf der Website auf dem neuesten Stand ist.</span></p>\n' +
    '<p>&nbsp;</p>\n' +
    '<h3>Haftungsausschluss</h3>\n' +
    '<p><span>Soweit dies gesetzlich zul&auml;ssig ist, schlie&szlig;en wir alle Zusicherungen, Garantien und Bedingungen in Bezug auf unsere Website und die Nutzung dieser Website aus. Nichts in diesem Haftungsausschluss wird</span></p>\n' +
    '<ul>\n' +
    '<li><span>unsere oder Ihre Haftung f&uuml;r Tod oder K&ouml;rperverletzung begrenzen oder ausschlie&szlig;en;</span></li>\n' +
    '<li><span>unsere oder Ihre Haftung f&uuml;r Betrug oder betr&uuml;gerische Falschdarstellung begrenzen oder ausschlie&szlig;en;</span></li>\n' +
    '<li><span>Beschr&auml;nken Sie unsere oder Ihre Verbindlichkeiten auf eine Weise, die nach geltendem Recht nicht zul&auml;ssig ist. oder</span></li>\n' +
    '<li><span>unsere oder Ihre Verbindlichkeiten ausschlie&szlig;en, die nach geltendem Recht nicht ausgeschlossen werden k&ouml;nnen.</span></li>\n' +
    '</ul>\n' +
    '<p><span>Die in diesem Abschnitt und an anderen Stellen in diesem Haftungsausschluss festgelegten Haftungsverbote und -einschr&auml;nkungen: (a) unterliegen dem vorstehenden Absatz; und (b) alle Verbindlichkeiten regeln, die sich aus dem Haftungsausschluss ergeben, einschlie&szlig;lich Verbindlichkeiten, die aus einem Vertrag entstehen, aus unerlaubter Handlung und wegen Verletzung der gesetzlichen Pflicht.</span></p>\n' +
    '<p><span>Solange die Website und die Informationen und Dienste auf der Website kostenlos zur Verf&uuml;gung gestellt werden, haften wir nicht f&uuml;r Verluste oder Sch&auml;den irgendwelcher Art.</span></p>';
