export default '' +
    '<h3>Terms & Conditions</h3>\n' +
    '<p><span>Welcome to clusterjobs.de!</span></p>\n' +
    "<p><span>These terms and conditions outline the rules and regulations for the use of clusterjobs's website, located at clusterjobs.de.</span></p>\n" +
    '<p><span>By accessing this website we assume you accept these terms and conditions. Do not continue to use clusterjobs.de if you do not agree to take all of the terms and conditions stated on this page.</span></p>\n' +
    '<p><span>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company&rsquo;s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client&rsquo;s needs in respect of provision of the Company&rsquo;s stated services, in accordance with and subject to, prevailing law of Germany. "Curriculum Vitae (CV)" or "Resume" is a brief written account of your personal details, your education, and the jobs you have had. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</span></p>\n' +
    '<h3>Services of clusterjobs</h3>\n' +
    '<p><span>clusterjobs provides certain job advertisements and other job-related content, including links to third-party websites (such advertisements and other content are referred to as "job advertisements") in clusterjobs\'s search results or elsewhere on the website. The job advertisements are created and provided by third parties who are not controlled by clusterjobs and you accept that we have no control over job advertisements. clusterjobs is in no way required to review job advertisements and include them in the search results or other listings, and may for any reason exclude or remove job advertisements from the web site. We do not confirm the accuracy or completeness of job advertisements or other data submitted by an employer or other user. This applies among other things to the identity of the employer or user. clusterjobs shall not be held responsible or liable for the content, accuracy, completeness, legality, reliability or availability of job advertisements. You agree that clusterjobs may also provide search options to limit job site search results by job type (eg, full-time, part-time, etc.), and that such job types are created independently and wholly by clusterjobs and not is a direct representation of the actual job advertisement.</span></p>\n' +
    '<h3>Rules for the Website</h3>\n' +
    '<p><span>You warrant accessing (or attempting to) the Site solely through the interface provided by clusterjobs, unless you have been granted a different process in a separate, written agreement from clusterjobs. You agree not to participate in any activity that disturbs or breaks the Site (or the servers and networks connected to it). Unless you have been given permission to do so in a separate, written agreement by clusterjobs, you warrant that you will not crawl, scan, reproduce, duplicate, copy, sell, market, or resell the Website for any purpose. You represent that you are solely responsible for any breach of your obligations under this Agreement and the consequences of such violations (including any damages or losses sustained by clusterjobs) (and that clusterjobs is not responsible or liable to you or any third party).</span></p>\n' +
    '<p><span>You represent that you will not transfer any malicious code to clusterjobs or upload it to or through the Website (whether as User Content or otherwise) and will not use or misuse the Website for your own economic gain. "Malicious Code" means any software (sometimes referred to as "Viruses", "Worms", "Trojans", "Time Bombs", "Time Locks", "Crash Code", "Traps", "Access Codes", "Cancelbots" or "Trap Doors" ) that (a) seeks to damage, interrupt, impair, disturb, intercept, expropriate, or otherwise interfere with any data, storage media, programs, systems, equipment, or communications in any manner whatsoever, including, but not limited to, (i) exceeding one Number of copies, (ii) exceeding a number of users, (iii) the elapse of a period, (iv) advancing to a specific date or value, or (v) using a particular function, or (b) it allows an unauthorized person to cause such results, or (c) allows an unauthorized person to work without their knowledge and approval on d the data of another person. Without limiting the foregoing, and by way of example only, users may not:</span></p>\n' +
    '<ul>\n' +
    '<li><span>create or enable unsolicited commercial e-mail ("spam"). Such actions include, but are not limited to:</span>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>sending messages in violation of applicable anti-spam laws,</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>mimicking or mimicking others and their e-mail addresses or creating false accounts to send spam</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>data mining with respect to any property of clusterjobs,</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>sending messages to users who indicated they did not want to be contacted,</span></p>\n' +
    '<p><span> &nbsp;&nbsp;</span><span>-</span><span>selling, exchanging or distributing contact information of a person to a third party, without the knowledge and the continuous approval for such disclosure by that person, and</span></p>\n' +
    "<p><span> &nbsp;&nbsp;</span><span>-</span><span>using resume contacts in violation of </span><span>clusterjobs</span><span>'s guidelines; B. at low response rates of the contacted persons.</span></p></li>\n" +
    '<li><span>send, upload, distribute or distribute any illegal, defamatory, offensive, fraudulent, infringing, obscene or otherwise offensive content. clusterjobs may block the transmission of such content.</span></li>\n' +
    '<li><span>deliberately spread viruses, worms, bugs, trojans, corrupted files, bogus messages or any other malicious code or destructive or misleading elements,</span></li>\n' +
    '<li><span>implement or redirect pyramid schemes or similar programs,</span></li>\n' +
    '<li><span>transmit harmful content to minors;</span></li>\n' +
    '<li><span>impersonate others (by using an e-mail address or otherwise) or otherwise misrepresent themselves or the source of an e-mail,</span></li>\n' +
    '<li><span>transfer the intellectual property of others or other proprietary or confidential data without the permission of the owner or licensor. Users who repeatedly post intellectual property from others will be banned from clusterjobs.</span></li>\n' +
    '<li><span>violate the rights (eg privacy or publicity rights) of others,</span></li>\n' +
    '<li><span>promote or call for illegal activities,</span></li>\n' +
    '<li><span>interfere with the use of the website of other clusterjobs users,</span></li>\n' +
    '<li><span>create multiple user accounts or create user accounts automatically or under false pretenses, in violation of the Agreement;</span></li>\n' +
    '<li><span>sell, trade, resell or otherwise exploit clusterjobs accounts for any unauthorized commercial purpose;</span></li>\n' +
    '<li><span>modify, incorporate, translate or reverse engineer any part of the Website,</span></li>\n' +
    '<li><span>remove any copyright, trademark or other proprietary notices from the Website,</span></li>\n' +
    '<li><span>reformat or reshape any parts of pages belonging to the Site without the express written consent of </span><span>clusterjobs</span><span>;</span></li>\n' +
    '<li><span>turn to other owlpihy users for multi-level marketing programs (MLM), posts that require an upfront payment, or any topics that owlpihy deems to be detrimental to users;</span></li>\n' +
    '<li><span>create multiple owlpihy accounts without approval</span></li>\n' +
    '<li><span>create multiple clusterjobs accounts for which you do not use your company or company name to achieve greater visibility in the free-to-post system,</span></li>\n' +
    '<li><span>bypass function restrictions or cancellations,</span></li>\n' +
    '<li><span>give false information,</span></li>\n' +
    '<li><span>scrap the clusterjobs database,</span></li>\n' +
    '<li><span>scrap or otherwise replicate any clusterjobs content for competitive purposes.</span></li>\n' +
    '</ul>\n' +
    '<p><span>clusterjobs reserves the right to use any user content to prevent potentially illegal or unauthorized acts or any other activities that are detrimental to other users. clusterjobs reserves the right to use various methods to detect and block the above mentioned &nbsp;irregular activities and to review user content in order to prevent misuse such as to prevent spam or fraud.</span></p>\n' +
    '<h3>Registration: Contact Details</h3>\n' +
    '<p><span>For some areas of the website where you can set up an clusterjobs account, you will need to register or provide an email address. When you register, you agree to (a) correctly, accurately, up-to-date and complete (including e-mail address) about yourself and (b) your username and password your use of the website or your access to the Website. If you are an employer or a recruiter, your account will be used for commercial purposes, not for private purposes. In fact, it is not responsible for the misuse or misuse of your e-mail address by third parties and related liability. By setting up an clusterjobs account, you agree to receive mandatory email updates about unusual activities related to your user account.</span></p>\n' +
    '<p><span>You agree that clusterjobs communicates with you or initiates communication with you on behalf of third parties as part of the services you provide. This can be done through your clusterjobs account or other communication channels such as: E-mail, telephone (including mobile) or mail, using the contact information that you may provide or that clusterjobs may receive from third parties. If you submit clusterjobs a mobile phone number, you agree to receive communications through that mobile number.</span></p>\n' +
    '<h3>Cookies</h3>\n' +
    "<p><span>We employ the use of cookies. By accessing clusterjobs.de, you agreed to use cookies in agreement with the clusterjobs's Privacy Policy.</span></p>\n" +
    '<p><span>Most interactive websites use cookies to let us retrieve the user&rsquo;s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</span></p>\n' +
    '<p><em><span>Session Cookies</span></em></p>\n' +
    '<p><span>We use Cookies to allow the website to uniquely identify a user&rsquo;s browsing session on the clusterjobs website and to allow us to coordinate this information with data from our website server.</span></p>\n' +
    '<p><em><span>Analytics</span></em></p>\n' +
    '<p><span>We use Google&rsquo;s &ldquo;analytics&rdquo; Cookies which, in conjunction with our web server&rsquo;s log files, allow us to identify unique, but anonymous users. These Cookies can also calculate the aggregate number of people visiting clusterjobs website, the date and time of a user&rsquo;s visit to the website, the pages a user has viewed and the time spent by users at the website.</span></p>\n' +
    "<p><span>We also use applicant tracking and sorting systems in order to get statistics about our website's visitors and to understand from where users land on clusterjobs. This helps us gather feedback so that we can improve the clusterjobs website and better serve our users.</span></p>\n" +
    '<p><em><span>Cross platform advertising and user recognition</span></em></p>\n' +
    '<p><span>We also use Cookies provided by Facebook. These Cookies operate in different ways but they are both used in connection with clusterjobs advertising served to a user of our website on Facebook, the recognition of such users and the devices used by the user to access Facebook and clusterjobs.</span></p>\n' +
    '<p><span>Your use of third party websites and platforms such as Facebook, and the privacy practices of these platforms, are governed by separate terms, conditions and policies for which clusterjobs is not responsible. You should review Facebook&rsquo;s terms, conditions and policies where you will find more details on how information about you is used on their platforms and how you can set your privacy preferences. &nbsp;&nbsp;</span></p>\n' +
    '<p><span>clusterjobs uses the AdWords and remarketing list features of Google Analytics for Advertisers on the Display Network. clusterjobs and Google use first-party cookies (such as Google Analytics cookies) and third-party cookies (such as DoubleClick cookies) for informational and optimization purposes, and to provide ads based on your previous visits to the clusterjobs website. This means that vendors like Google clusterjobs display ad material on other sites you visit on the Internet.</span></p>\n' +
    '<p><span>You can disable Google Analytics for Advertisers on the Display Network, including AdWords, as well as personalized ads on the Google Display Network, in Google Ads Preferences Manager. In order to give visitors to the site more control over what data is collected by Google Analytics and to give them the ability to stop Google programs, Google has developed the browser deactivation add-on. This is available here:</span><a href="https://tools.google.com/dlpage/gaoptout?hl=en"> <span>browser add-on to disable Google Analytics</span></a><span>. </span></p>\n' +
    '<p><span>You can find more information about the individual Cookies we use and the purposes for which they are used in the table below: </span></p>\n' +
    '<table>\n' +
    '<tbody>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<h3>Cookie Type </h3>\n' +
    '</td>\n' +
    '<td>\n' +
    '<h3> Purpose</h3>\n' +
    '</td>\n' +
    '</tr>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<p><span>Google Analytics</span></p>\n' +
    '<p><span>_ga</span></p>\n' +
    '</td>\n' +
    '<td>\n' +
    '<p><span>This Cookie is placed by Google. It enables clusterjobs to learn information about our users use of the clusterjobs website such as the time of visit, the pages viewed, whether the user has visited before and the website visited prior to visiting the clusterjobs website. </span></p>\n' +
    '<p><span>For further information about Google Analytics please see: </span></p>\n' +
    '<p><a href="https://www.google.com/policies/technologies/types/"><span>https://www.google.com/policies/technologies/types/</span></a></p>\n' +
    '</td>\n' +
    '</tr>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<p><span>Facebook Pixel</span></p>\n' +
    '</td>\n' +
    '<td>\n' +
    '<p><span>This Cookie is placed by Facebook. It enables owliphyto measure, optimize and build audiences for advertising campaigns served on Facebook. In particular it enables clusterjobs to see how our users move between devices when accessing the clusterjobs website and Facebook, to ensure that clusterjobs&rsquo;s Facebook advertising is seen by our users most likely to be interested in such advertising by analysing which content a user has viewed and interacted with on the clusterjobs website. For further information about the Facebook Pixel please see:</span></p>\n' +
    '<p><a href="https://en-gb.facebook.com/business/help/651294705016616"><span>https://en-gb.facebook.com/business/help/651294705016616</span></a></p>\n' +
    '</td>\n' +
    '</tr>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<p><span>vuex &nbsp;</span></p>\n' +
    '</td>\n' +
    '<td>\n' +
    '<p><span>This Cookie is placed by clusterjobs. It is used to keep an anonymized user token by the clusterjobs website server.Also to keep user insensitive data regarding his profile, which is preferred skills, programming languages, job titles, preferred job locations, job type (full-time, part-time, intern, remote), user type (usual or recruiter) and also name of stored CV file if it is uploaded. Also we store information if user is closed notification about cookies policy not to show it again.</span></p>\n' +
    '</td>\n' +
    '</tr>\n' +
    '<tr>\n' +
    '<td>\n' +
    '<p><span>i18n_redirected</span></p>\n' +
    '</td>\n' +
    '<td>\n' +
    '<p><span>This Cookie is placed by clusterjobs. It is used to record when a user of the clusterjobs website visits the page and website detects his browser language to switch language only on first visit.</span></p>\n' +
    '</td>\n' +
    '</tr>\n' +
    '</tbody>\n' +
    '</table>\n' +
    '<p><span>Most of our cookies expire within 30 days, although our analytics cookies may persist for 1 year. </span></p>\n' +
    '<h3>License</h3>\n' +
    '<p><span>Unless otherwise stated, clusterjobs and/or its licensors own the intellectual property rights for all material on clusterjobs.de. All intellectual property rights are reserved. You may access this from clusterjobs.de for your own personal use subjected to restrictions set in these terms and conditions.</span></p>\n' +
    '<p><span>You must not:</span></p>\n' +
    '<ul>\n' +
    '<li><span>Republish material from clusterjobs.de</span></li>\n' +
    '<li><span>Sell, rent or sub-license material from clusterjobs.de</span></li>\n' +
    '<li><span>Reproduce, duplicate or copy material from clusterjobs.de</span></li>\n' +
    '<li><span>Redistribute content from clusterjobs.de</span></li>\n' +
    '</ul>\n' +
    '<p><span>This Agreement shall begin on the date hereof.</span></p>\n' +
    '<h3>Hyperlinking to our Content</h3>\n' +
    '<p><span>The following organizations may link to our website without prior written approval:</span></p>\n' +
    '<ul>\n' +
    '<li><span>Government agencies;</span></li>\n' +
    '<li><span> &nbsp;</span><span>Search engines;</span></li>\n' +
    '<li><span>News organizations;</span></li>\n' +
    '<li><span>Online directory distributors may link to our website in the same manner as they hyperlink to the websites of other listed businesses; and</span></li>\n' +
    '<li><span> &nbsp;</span><span>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our website.</span></li>\n' +
    '</ul>\n' +
    '<p><span>These organizations may link to our home page, to publications or to other website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party&rsquo;s site.</span></p>\n' +
    '<p><span>We may consider and approve other link requests from the following types of organizations:</span></p>\n' +
    '<ul>\n' +
    '<li><span>commonly-known consumer and/or business information sources;</span></li>\n' +
    '<li><span>dot.com community sites;</span></li>\n' +
    '<li><span>associations or other groups representing charities;</span></li>\n' +
    '<li><span>online directory distributors;</span></li>\n' +
    '<li><span>internet portals;</span></li>\n' +
    '<li><span>accounting, law and consulting firms; and</span></li>\n' +
    '<li><span>educational institutions and trade associations.</span></li>\n' +
    '</ul>\n' +
    '<p><span>We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of clusterjobs; and (d) the link is in the context of general resource information.</span></p>\n' +
    '<p><span>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party&rsquo;s site.</span></p>\n' +
    '<p><span>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to clusterjobs. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.</span></p>\n' +
    '<p><span>Approved organizations may hyperlink to our website as follows:</span></p>\n' +
    '<ul>\n' +
    '<li><span>By use of our corporate name; or</span></li>\n' +
    '<li><span>By use of the uniform resource locator being linked to; or</span></li>\n' +
    '<li><span>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party&rsquo;s site.</span></li>\n' +
    '</ul>\n' +
    "<p><span>No use of clusterjobs's logo or other artwork will be allowed for linking absent a trademark license agreement.</span></p>\n" +
    '<p><span>You are not permitted to use the clusterjobs website or its content for purposes other than non-commercial purposes. The use of any automated systems or software, whether third party or otherwise, is prohibited from extracting data from the Website (such as screen scraping or crawling). As a rule, this means that the possibility of unauthorized use of the website is given. If you wish to use the Website commercially or in any area other than a job seeker or employer of clusterjobs, or if you need services, you must use the Website to accept the Online Terms of Use of clusterjobs. Contact us for more information. We reserve the right at any time (without binding) to reclaim user names and URLs for various reasons.</span></p>\n' +
    '<h3>iFrames</h3>\n' +
    '<p><span>Without prior approval and written permission, you may not create frames around our webpages that alter in any way the visual presentation or appearance of our website.</span></p>\n' +
    '<h3>Content Liability</h3>\n' +
    '<p><span>We shall not be hold responsible for any content that appears on third-party websites. You agree to protect and defend us against all claims that is rising on third-party websites. No link(s) should appear on any website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</span></p>\n' +
    '<p><span>You may not post any illegal, fraudulent, discriminatory, threatening, offensive, defamatory, obscene or otherwise offensive user content. This also applies to sexual, ethnic, racial or otherwise discriminatory denunciations or irrelevant or unconstructive content. In addition, you may not (without our express written permission) disclose User Content containing proprietary information, trade secrets, confidential information, advertisements, advertisements, chain letters, pyramid schemes, investment offers, or other unsolicited commercial communications, or prompting or causing spam or mass mailing.</span></p>\n' +
    '<h3>Your Privacy</h3>\n' +
    '<p><span>Please read our Privacy Policy.</span></p>\n' +
    '<h3>Security</h3>\n' +
    '<p><span>clusterjobs will use reasonable security measures to prevent the loss, misuse or alteration of personal information under the control of clusterjobs. However, no Internet or electronic storage method is completely secure. Also note that emails, messages sent through your web browser, and other similar means of communicating with users are not encrypted. Therefore, we cannot guarantee their safety with all efforts to protect your data.</span></p>\n' +
    '<p><span>Also note that we may use external cloud service providers that offer hosting, data storage, and other services that may be subject to non-negotiable standard terms of service. These service providers have informed us or the public that they have implemented security measures that they deem appropriate to protect the data within their systems, or that they are generally known to perform such actions. However, to the extent permitted by law, we shall not be liable for any damages resulting from the misuse of data, including personal data, by such companies.</span></p>\n' +
    '<h3>Reservation of Rights</h3>\n' +
    '<p><span>We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it&rsquo;s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</span></p>\n' +
    '<h3>Removal of links from our website</h3>\n' +
    '<p><span>If you find any link on our website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</span></p>\n' +
    '<p><span>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</span></p>\n' +
    '<h3>Disclaimer</h3>\n' +
    '<p><span>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</span></p>\n' +
    '<ul>\n' +
    '<li><span>limit or exclude our or your liability for death or personal injury;</span></li>\n' +
    '<li><span>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</span></li>\n' +
    '<li><span>limit any of our or your liabilities in any way that is not permitted under applicable law; or</span></li>\n' +
    '<li><span>exclude any of our or your liabilities that may not be excluded under applicable law.</span></li>\n' +
    '</ul>\n' +
    '<p><span>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</span></p>\n' +
    '<p><span>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</span></p>';
