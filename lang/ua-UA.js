export default {
    alphabet: {
        title: 'Работы по алфавиту',
    },
    required: 'Будь ласка, заповніть поле',
    validationPassword: 'Мін. 6 знаків-цифр і букв',
    eg: '',
    jobsFor: 'Пропозиції для',
    showMore: 'Більше пропозицій',
    seo: {
        general: {
            siteName: 'Clusterjobs - розумний пошук роботи в IT',
        },
        accountRemoved: {
            title: 'Профіль clusterjobs був видалений',
        },
        homepage: {
            title: 'clusterjobs - IT робота в Європі',
            keywords: 'робота, робота в Європі, IT робота, програміст Європа',
            description:
                'clusterjobs допоможе знайти IT роботу мрії в Європі. ' +
                'Ми розміщуємо більше 200,000 вакансій з роботою в Німеччині англійською та німецькою мовами.' +
                'Використовуйте пошук по резюме або повнотекстовий пошук за ключовими словами.',
        },
        search: {
            jobsFor: '{query} - вакансії в Німеччині',
            emptySearch: 'clusterjobs - IT робота в Європі',
            keywords:
                'робота, робота в Європі, IT робота, програміст Європа, {keywords}',
            description:
                'Знайти {query} в Європі. {number} відкритих вакансій за запитом {query} на clusterjobs.de',
        },
        offer: {
            title: '{title} в {companyName} - найди свою работу на clusterjobs',
            keywords: '{skills}, {programmingLanguages}, {destinations}',
            description:
                'Відкрита вакансія {title} в {companyName}. Ми шукаємо {skills}, {programmingLanguages} в {destinations}',
        },
        offerNotFound: {
            title: 'Вакансия не найдена',
        },
        impressum: {
            title: 'clusterjobs - Інформація про компанію',
            keywords: 'clusterjobs, Інформація про компанію, контакт',
            description: 'clusterjobs - Інформація про компанію',
        },
        termsAndCond: {
            title: 'clusterjobs - Умови та положення',
            keywords: 'clusterjobs - Умови та положення',
            description: 'clusterjobs Умови та положення',
        },
        privacyPolicy: {
            title: 'clusterjobs - Політика конфіденційності',
            keywords: 'clusterjobs - Політика конфіденційності',
            description: 'clusterjobs Політика конфіденційності',
        },
        company: {
            title: 'clusterjobs - Про нас',
            keywords: 'clusterjobs - Про нас',
            description: 'clusterjobs Про нас',
        },
    },
    homepage: {
        imgAlt: 'Чоловік з ноутбуком',
        ctaTitle: 'Розумний пошук роботи в IT',
        recentlyAddedTitle: 'Нові пропозиції',
    },
    search: {
        label: 'Пошук по тексту',
        cvLabel: 'Дізнайся, куди підходить твій CV',
        or: 'або',
        buttonText: 'Пошук',
        cvButtonText: 'Пошук по CV',
        cvMobileButtonText: 'CV',
        defaultButtonText: 'Пошук',
        defaultCvButtonText: 'Пошук по CV',
        defaultCvMobileButtonText: 'CV',
        defaultQuery: 'fullstack php developer berlin',
        searchByQuery: 'Пошук по тексту:',
        searchByFile: 'Пошук по файлу:',
        emptySearchText:
            'Будь ласка, опишіть спеціальність, технічний стек, місце роботи одним запитом',
    },
    keywords: {
        label: 'Теги:',
        inputTitle: 'Додати теги до поточного пошуку:',
        inputLabel: 'Фільтрувати поточний запит:',
        inputPlaceholder: 'Microservices, React, TDD...',
        btnText: 'Додати тег',
        resetLabel: 'Скинути теги',
        resetBtnText: 'Скинути',
    },
    links: {
        home: 'Головна',
        blog: 'Наш блог',
        alphabet: 'Работы по алфавиту',
        termsAndConditions: 'Умови та положення',
        impressum: 'Юридична інформація',
        privacyPolicy: 'Політика конфіденційності',
    },
    header: {
        logIn: 'Увійти',
        signUp: 'Зареєструватися',
        signUpCta: 'Немає профілю?',
        logInCta: 'Вже є профіль?',
        logInTitle: 'Увійти в профіль',
        signUpTitle: 'Зареєструвати профіль',
        email: 'E-Mail',
        repeatEmail: 'Будь ласка, повторіть E-Mail',
        password: 'Пароль',
        language: 'Мова сайту:',
        offerLanguage: 'Мова вакансій:',
        cabinet: 'Особистий кабінет:',
        cabinetLabel: 'Іконка особистого кабінету',
        selectSiteLanguage: 'Виберіть мову сайту:',
        selectOfferLanguage: 'Я можу розмовляти на:',
        userProfile: {
            headerTitle: 'Профіль',
            menu: 'Меню',
            logOut: 'Вийти',
        },
        menuTitle: 'Меню сайту',
        mobile: {
            menu: 'Меню',
            menuTitle: 'Меню сайту',
        },
        isRecruiter: 'Хочу розмістити вакансію',
    },
    footer: {
        ourCompany: 'Наша компанія',
        contactFormTitle: 'Написати нам',
        copyright: 'Copyright clusterjobs ©',
        rightsReserved: 'All Rights Reserved',
        selectLanguage: 'Мова сайту',
        socialMedia: 'Наши медiа',
    },
    offers: {
        h1: 'Результаты для {currentQuery}',
        h1CV: 'Результаты для поиска по CV',
        offers: 'Пропозиція',
        page: 'Сторінка',
        match: 'Збіг:',
        competences: 'Компетенція:',
        location: 'Де:',
        benefits: 'Бонус:',
        programmingLanguages: 'Стек технологій:',
        view: 'Деталi',
        apply: 'До вакансії',
        goToOffer: 'Деталi',
        update: 'Оновити',
        delete: 'Видалити',
        indexed: 'Опубліковано',
        btnText: 'Перейти до вакансії',
        notIndexed: 'Публікація в процесі',
        in: 'в',
        companyName: '{companyName}',
        saveSearch: 'Отслеживать вакансии',
    },
    saveSearch: {
        popoverTitle: 'Создать оповещение о вакансиях',
        popoverText:
            'Мы зарегистрируем для вас кабинет, где вы смодете изменить расписание уведомлений.' +
            ' Для начала мы можем отправлять письмо еженедельно по четвергам.',
        popoverSubtitle:
            'Мы можем оповещать вас о новых вакансиях, просто оставьте email:',
        submitNewcomer: 'Отслеживать вакансии',
        saveSearchSubtitle: 'Кликните, чтобы следить за новыми вакансиями',
        saveSearchPopoverText:
            'Мы будем отправлять письмо каждый четверг.' +
            ' Сейчас мы работаем над страницей, где бы вы смогли изменить расписание как вам удобно, скоро она будет доступна.',
        submitUser: 'Отслеживать вакансии',
        successPopoverTitle: 'Ваш поиск сохранен!',
        successPopoverText:
            'Мы будем оповещать о вакансиях из этого поиска  каждый четверг.' +
            ' Мы разрабатываем новое расписание и вскоре вы сможете сами им управлять.',
        saveSearchNoQuery:
            'To be able to save search of your CV file, please sign up and fill Profile.' +
            'You will be able to access relevant offers page and set mailing schedule',
    },
    company: {
        email: 'E-Mail:',
        call: 'Зателефонувати:',
    },
    contactUs: {
        name: 'Имя:',
        email: 'E-Mail:',
        message: 'Повідомлення:',
        send: 'Надіслати',
    },
    featuresSeo: {
        title: 'Наші особливості',
        aiTitle: 'Пошукова система на базі AI',
        aiText:
            'Використовуючи переваги AI і NLP, ' +
            'clusterjobs може зіставляти за змістом ' +
            'опис вакансії із запитами користувача. ' +
            'Незважаючи на те, що призначені для користувача запити ' +
            'іноді можуть не збігатися з вмістом вакансії, ' +
            'clusterjobs здатний завжди запропонувати дуже підходящі результати. ',
        cvTitle: 'Пошук по резюме ',
        cvText:
            'clusterjobs використовує новітні технології AI і ' +
            'Natural Language Processing (NLP), що дозволяє ' +
            'користувачам здійснювати пошук, просто використовуючи своє резюме. ' +
            'clusterjobs використовує AI і NLP з метою розпізнати суть ' +
            'резюме користувачів і зіставити їх з відповідними ' +
            'вакансіями про роботу. ',
        fullTextTitle: 'Повнотекстова пошукова система',
        fullTextText:
            'clusterjobs - це вдосконалена система полнотекстоваго пошуку, ' +
            'яка дозволяє користувачам легко і просто здійснювати пошук.' +
            'Ця функція, наприклад, дає можливість користувачам шукати ' +
            'вакансії, використовуючи одночасно назву посади, особисті навички' +
            ' і навіть бажане місце розташування. ',
    },
    impressum: {
        title: 'Юридична інформація',
        text:
            'owliphy UG (haftungsbeschränkt)<br>' +
            'Parkstraße 72<br>' +
            '40477 Дюссельдорф<br>' +
            'Німеччина<br>' +
            '<br>' +
            'Реєстраційний офіс: Amtsgericht Düsseldorf<br>' +
            'Реєстраційний номер: HRB 85801 <br>' +
            'VAT I.D./ TAX I.D. відповідно до § 27a UStG: DE323427817 <br>' +
            'E-Mail: info@clusterjobs.com <br>' +
            'Вебсайт: clusterjobs.de<br>',
    },
    aboutUs: {
        title: 'Про компанію',
    },
    termsAndCond: {
        title: 'Умови та положення',
    },
    privacy: {
        title: 'Політика конфіденційності',
    },
    about: {
        title: 'Про компанію',
    },
    cookieNotification: {
        title: 'Використання cookies',
        close: 'Закрити',
        infoLink: 'Додатково',
        text:
            'Використовуючи цей сайт, ви погоджуєтеся з умовами використання cookies. Подробнee про використання cookies ви дізнаєтеся в розділі Політика конфіденційності',
    },
    infographics: {
        title: 'Як ми працюємо',
        searchTitle: 'Простий і розумний пошук',
        search:
            'Просто введіть в поле пошуку, що ви шукаєте, <i>або шукайте по CV</i> і ми знайдемо вам найкращі пропозиції.',
        engineTitle: 'Наша пошукова система',
        engine:
            'Грунтуючись на ваших tech stack, компетенціях, місце роботи, а також ' +
            'на основі потрібної для роботи мови' +
            'ми надаємо <i> кращі IT-пропозиції в Німеччині</i>',
        contentTitle: 'Знайдіть роботу, яка підходить саме вам!',
        content:
            'Зареєструйтеся на <i>clusterjobs </i>, щоб <i>зберігати ваші пошуки</i> ' +
            'і регулярно <i>отримувати сповіщення про нові можливостіх</i>.',
        searchAlt: 'Девушка сидит в интернете с ноутбука',
        engineAlt: 'Парень показывает связи между данными',
        contentAlt: 'Парень показывает результат',
    },
    account: {
        title: 'Аккаунт і настройки',
        formTitle: 'Інформація про аккаунт',
        name: 'Iм`я',
        namePlaceholder: '',
        lastName: 'Прізвище',
        email: 'E-Mail',
        updateButton: 'Оновити',
        successUpdate: 'Успішно оновлено!',
        nameUsageDetails:
            'Ми використовуємо ваше ім`я тільки для відображення на сайті і в E-Mail',
        emailUsageDetails:
            'E-Mail буде використонний як логін і для відновлення пароля',
        formPasswordTitle: 'Змінити пароль',
        password: 'Поточний пароль',
        updatePasswordText:
            'Нам потрібен ваш поточний пароль, щоб підтвердити доступ',
        newPassword: 'Новий пароль',
        newPasswordConfirmation: 'Будь ласка, повторіть новий пароль',
        updatePasswordButton: 'Змінити',
        deleteUserTitle: 'Видалити аккаунт',
        deleteText:
            'Вибираючи цю опцію, ваш аккаунт і вся, повязана з ним інформація (CV та особисті дані), будуть повністю вилучені.',
        deleteLabel: 'Видалити',
        recruiter: {
            title: 'Recruiter cabinet settings',
            variableName: 'Source variable name',
            variableValue: 'Source variable value',
            variableNameText:
                'If you define these params, we will append variable with certain value to each your url (ex. ?utm_source=clusterjobs)',
            variableValueText: 'Both parameters need to be defined',
        },
    },
    profile: {
        title: 'Заповніть профіль, щоб допомогти нам знайти вам кращу вакансію',
        formTitle: 'Ми використовуємо інформацію, щоб ',
        formTitleLink: 'знайти самі релевантні пропозиції',
        addCv: 'Завантажити CV',
        successUpdate: 'Профіль успішно оновлений',
        cvLabel: 'Ми використовуємо ваш CV, щоб знайти вам кращі вакансії',
        recogniseTags: 'Розпізнати теги',
        recogniseTagsLabel:
            'Вибираючи цю опцію, система розпізнає теги з вашого резюме',
        skills: 'Навички',
        skillsPlaceholder:
            'наприклад, Architecture, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'наприклад, PHP, Javascript, Java, Go',
        offerTitles: 'Спеціальність',
        offerTitlesPlaceholder:
            'наприклад, Project Manager, Software Developer',
        locations: 'Місцезнаходження',
        locationsPlaceholder: 'наприклад, Munich, Berlin, Cologne',
        button: 'Оновити профіль',
        offerTypes: 'Тип роботи',
        offerTypeFull: 'Повний робочий день',
        offerTypePart: 'Неповна зайнятість',
        offerTypeStudent: 'Стажування',
        offerTypeRemote: 'Дистанційна робота',
        offerTypesPlaceholder: 'Яку роботу ви шукаєте?',
        menu: 'Ваш кабінет:',
    },
    addOffer: {
        indexedData: 'Головна іфнормація про пропозицію',
        contactData: 'Контактна інформація',
        otherData: 'Інше',
        button: 'Додати вакансію',
        title: 'Додайте свою вакансію',
        offerTitle: 'Посада',
        offerTitlePlaceholder: 'Будь ласка, додайте посаду',
        description: 'Опис',
        miniDescription: 'Короткий опис',
        miniDescriptionPlaceholder:
            'Це опис буде відображатися на картці вакансії в списку, якщо в реченні зазначено менше 4 тегів',
        skills: 'Необхідні навички',
        skillsPlaceholder:
            'наприклад, Microservices, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'наприклад, PHP, Javascript, Java, Go',
        destinations: 'Місцезнаходження',
        destinationsPlaceholder: 'Місцезнаходження вакансії',
        offerTypes: 'Тип зайнятості',
        offerTypesPlaceholder: 'Виберіть тип зайнятості',
        companyName: 'Назва компанії',
        companyNamePlaceholder: 'Ви можете використовувати коротку назву',
        uploadLogo: 'Додати логотип',
        emptyText:
            'Вакансія з логотипом буде виглядати більш примітно в списку',
        email: 'E-Mail',
        emailPlaceholder: 'E-Mail, щоб кандидати відправляли свої CV',
        url: 'Посилання на вакансію',
        urlPlaceholder: 'Куди нам перенаправляти кандидатів?',
        startsFrom: 'Початок зайнятості',
        salaryFrom: 'Річна зарплата від',
        salaryFromPlaceholder: 'Буде відображено `від X в рік`',
        salaryTo: 'Річна зарплата до',
        salaryToPlaceholder: 'Буде відображено `до X в рік`',
        successMessage: 'Вакансия успешно добавлена',
        errorNoId: 'Сталася помилка, і ми не отримали ID доданої вакансії',
        recogniseTags: 'Розпізнати теги',
        recogniseTagsText:
            'Вибираючи цю опцію, система розпізнає теги (навички і мови програмування) з вашого опису',
    },
    offerAdded: {
        title: 'Ваша вакансія додана',
        goToList: 'Список ваших вакансій',
        successText:
            'Нам потрібен певний час, щоб перевірити додані дані. Це може зайняти до 4-х годин.',
    },
    updateJob: {
        title: 'Оновити вакансію',
        button: 'Оновити',
        formSuccess: 'Вакансія успішно оновлена',
        formError: 'Сталася помилка, будь ласка, зв`яжіться з нами',
    },
    offersList: {
        title: 'Ваші вакансії',
        descriptionText:
            'Вакансії в цьому списку виглядають так само, як їх бачать кандидати',
    },
    recruiterMenu: {
        account: 'Аккаунт і настройки',
        offersList: 'Мої вакансії',
        findForCandidate: 'Знайти для кандидата',
        addOffer: 'Розміщення вакансії',
    },
    userMenu: {
        seeRelevantOffers: 'Відповідні вакансії',
        updateProfile: 'Профіль',
        account: 'Аккаунт і настройки',
    },
    findForCandidate: {
        title: 'Відповідні вакансії для вашого кандидата',
        searchByFile: 'Ключові слова з `{fileName}`:',
        pageDescription:
            'Завантажте CV кандидата, і ми постараємося знайти підходящу вакансію з ваших пропозицій - ',
        button: 'Завантажте CV кандидата',
    },
    offerPage: {
        title: '{title} в {companyName}',
        requirements: 'Вимоги',
        destinationsLabel: 'Місцезнаходження',
        benefitsLabel: 'Бонуси',
        skillsLabel: 'Навички',
        programmingLanguagesLabel: 'Tech stack',
        description: 'Опис',
        viewOffer: 'До вакансії',
        workType: 'Ми пропонуємо <b itemprop="employmentType">{workType}</b>',
        createdAt: 'Розміщено: ',
        backToList: 'К списку вакансий',
        startsAsap: 'Почніть <b> якомога раніше</b>',
        startsAt: 'Почніть <b>{date}</b>',
        salaryRange:
            'і отримуйте <b> від <span itemprop="minValue">{from}</span> {currency} кілька разів <span itemprop="maxValue">{to}</span> {currency}</b>',
        salaryFrom:
            'і отримуйте <b> від <span itemprop="minValue">{salary}</span> {currency}</b>',
        salaryTo:
            'і отримуйте <b> до <span itemprop="maxValue">{salary}</span> {currency}</b>',
    },
    logoText: '{companyName} логотип',
    relevantOffers: {
        title: 'Відповідні вакансії',
        notFound:
            'Unfortunately we couldn`t find any relevant offers for you. Please, try to refine the search',
        emptyProfileLink: 'Будь ласка, заповніть ваш профіль',
        emptyProfile:
            ', щоб ми могли знайти вам найбільш відповідні пропозиції',
        pageDescription:
            'Ми використовували ваш профіль і CV, щоб знайти найбільш підходящі пропозиції',
    },
    message: {
        defaultFailed: 'Щось пішло не так, будь ласка, зв`яжіться з нами',
        missingToken:
            'Токен користувача недійсний. Будь ласка, увійдіть в аккаунт знову.',
        badRequest: 'Помилкові параметри запиту',

        jobNotFound: 'Пропозиція не знайдена',
        jobFail: 'Вибачте, запит не вдався.',
        jobSuccessAdd: 'Пропозиція успішно додана',
        jobSuccessUpdate: 'Пропозиція успішно додана',
        jobSuccessRemove: 'Пропозиція успішно додана',

        userSuccess: 'User request was successfully sent.',
        userFailed: 'Вибачте, запит користувача не вдався. Звяжіться з нами.',
        userNotFound: 'Вибачте, користувач не знайдений. Звяжіться з нами.',
        userEmptyUpdateFields: 'Поля для поновлення користувача порожні.',
        userBadCredentials:
            'Вибачте, ми не можемо знайти користувача з таким E-Mail і паролем.',
        userBadPassword: 'We think you have a mistake in current password.',

        feedbackFail:
            'На жаль, ваш відкликання не був збережений. Будь ласка, спробуйте пізніше.',
        feedbackSuccess: 'Відгук був успішно доданий',

        saveSearchFailed:
            'Unfortunately, we cannot save your search at the moment',
        saveSearchSuccess: 'Search is successfully saved!',
        saveSearchUpdateSuccess: 'Schedule is successfully updated!',
    },
    accountRemoved: {
        title:
            'Ваш аккаунт і всі особисті дані, повязані з ним, успішно видалені.',
        text:
            'Вся iнформція, яку ви надали для пошуку вакансій, була видалена.<br>' +
            'Особисті дані і ваш аккаунт були повністю видалені і не можуть бути використані ніде більше.',
    },
    aria: {
        toggleMenuSection: 'Розділ Toggle-меню',
        goToOffer: 'Перейти до вакансії',
        send: 'Надіслати',
        boldText: 'Жирний текст',
        italicText: 'Курсив',
        strikeText: 'Закреслений текст',
        underlineText: 'Підкреслений текст',
        makeCodeBlock: 'Додати блок коду',
        addParagraph: 'Додати новий абзац',
        addH1: 'Додати заголовок 1',
        addH2: 'Додати заголовок 2',
        addH3: 'Додати заголовок 3',
        bulletList: 'Додати список',
        orderedList: 'Додати нумерований список',
        quote: 'Додати цитату',
        undo: 'Скасування',
        redo: 'Повтор',
        addImage: 'Додати зображення',
        addLink: 'Додати посилання',
        removeLink: 'Видалити посилання',
        removeTag: 'Видалити тег',
        changeSiteLanguage: 'Змінити мову сайту',
        changeOffersLanguage: 'Змінити мову вакансій',
        openLogInPopover: 'Відкрити вікно входу',
        openProfile: 'Зайти в аккаунт',
        openSiteMenu: 'Відкрити меню сайту',
        close: 'Закрити',
        submitForm: 'Надіслати',
        openSignUpPopover: 'Відкрити вікно реєстрації',
    },
    popularOffers: 'Популярні пропозиції',
    popularQueries: {
        cityPagesTitle: 'Популярные города',
        title: 'Популярні запити',
        offerTitle: 'Популярные должности в этом городе',
        linkText: '{programming} в {location}',
    },
    similarOffersTitle: 'Схожі вакансії',
    error: {
        noOfferTitle: 'Job offer is not found',
        noOfferText:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check our Homepage, we are sure, you can find something that suits you well!',
        noOfferTextSubstitutes:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check similar offers, we are sure, you can find something that suits you well!',
        notFound: 'Ми втратили сторінку',
        notfoundText: 'На жаль, сторінка яку ви шукали, не знайдена',
        toHomepage: 'Ось посилання, щоб повернутися на головну',
        error: 'Сталося щось незрозуміле',
        errorText: 'На жаль, сталася помилка, і ми над нею вже працюємо!',
    },
    blog: {
        nextArticle: 'Дальше',
        title: '> clusterjobs magazine',
        backToMain: 'Назад до статей',
        seoTitle: 'clusterjobs - ми покажемо, як знайти роботу',
        seoKeywords:
            'пошук роботи, робота за кордоном, робота в Німеччині, clusterjobs, робота в Німеччині англійською',
        seoDescription:
            'Ми фокусуємося на статтях які допоможуть знайти роботу за кордоном, ' +
            'трендах в IT і AI, і особливості IT професій',
    },
    students: {
        features: {
            hidden: 'Our features',
            jobs: {
                title: 'More than 100,000 IT-jobs in Germany',
                text:
                    'clusterjobs aggregates offers from partners worldwide to show only best one and most relevant for our users.',
            },
            it: {
                title: 'Student and after-graduate jobs in IT',
                text:
                    'Find your werkstudent job or your early-career job in case you are graduating. clusterjobs helps you to get the best suit for you!',
            },
            ai: {
                title: 'Use smart AI search by your CV',
                text:
                    'Just upload your CV and our AI engine will recognise your strengths and will show relevant jobs.',
            },
        },
        interestedQuestion:
            'Interested? We can notify you about new relevant jobs',
        headTitle: `Find best student's jobs in Germany. Start your career in IT with clusterjobs.<br> AI will find best suiting job for you!`,
        notifyMe: 'Notify me',
        register: 'Register',
        studentJobs: 'Student jobs',
        graduateJobs: 'After-graduate jobs',
        imgAlt: 'Man with a binoculars',
        showMore: 'Show me more',
        logoTitle: 'clusterjobs - use tech search to find tech job',
        registerCta: 'Register to be notified about new and related offers',
        registerForm: {
            signUpTitle: 'Find your dream job in three steps',
        },
    },
    authentication: {
        signUp: 'Register',
        email: 'Email address',
        repeatEmail: 'Please repeat your email',
        password: 'Password',
        isRecruiter: 'I am recruiter',
    },
    partners: {
        title: 'Наші партнери',
    },
};
