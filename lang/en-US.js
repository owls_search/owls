export default {
    alphabet: {
        title: 'Jobs by alphabet',
    },
    required: 'This field is required',
    validationPassword: 'Min. 6 symbols - digits and letters',
    eg: 'e.g.',
    jobsFor: 'Jobs for',
    showMore: 'More offers',
    seo: {
        general: {
            siteName: 'Clusterjobs - be smart in your job search!',
        },
        accountRemoved: {
            title: 'clusterjobs account was removed',
        },
        homepage: {
            title: 'clusterjobs - use tech search to find tech job',
            keywords: 'find job, jobs for, get a job, tech job, IT job',
            description:
                'Find your dream IT job on clusterjobs.de. ' +
                'We have more than 200,000 english and german jobs in Germany. ' +
                'Search with your CV, or using natural text search.',
        },
        search: {
            jobsFor: '{query} jobs in Germany',
            emptySearch: 'clusterjobs - tech search of tech job',
            keywords:
                'find job, jobs for, job, offer, in Europe, IT job, job in English, {keywords}',
            description:
                'Search {query} in Europe with AI search engine. {number} open jobs in {query} on clusterjobs.de',
        },
        offer: {
            title: '{title} - find your job on clusterjobs',
            keywords:
                'job, offer, IT, find job, jobs, {skills}, {programmingLanguages}, {destinations}',
            description:
                'Open {title}. We are looking for {skills}, {programmingLanguages} in {destinations}',
        },
        offerNotFound: {
            title: 'Job offer is not found',
        },
        impressum: {
            title: 'clusterjobs - Imprint',
            keywords: 'clusterjobs, contacts',
            description: 'clusterjobs imprint - information about company',
        },
        termsAndCond: {
            title: 'clusterjobs - Terms & Conditions',
            keywords: 'clusterjobs, terms & Conditions',
            description: 'clusterjobs Terms & Conditions',
        },
        privacyPolicy: {
            title: 'clusterjobs - Privacy Policy',
            keywords: 'clusterjobs, privacy Policy',
            description: 'clusterjobs Privacy Policy',
        },
        company: {
            title: 'clusterjobs - About us',
            keywords: 'clusterjobs, about us, about company',
            description: 'clusterjobs - about company',
        },
    },
    homepage: {
        imgAlt: 'Man with a laptop',
        ctaTitle: 'Smart Tech-focused Job Search',
        recentlyAddedTitle: 'Recently added jobs',
    },
    search: {
        label: 'Search by text',
        cvLabel: 'Get to know where your CV fits better',
        or: 'or',
        buttonText: 'Search',
        cvButtonText: 'Search by CV',
        cvMobileButtonText: 'CV',
        defaultButtonText: 'Search',
        defaultCvButtonText: 'Search by CV',
        defaultCvMobileButtonText: 'CV',
        defaultQuery: 'fullstack php berlin',
        searchByQuery: 'Search by query:',
        searchByFile: 'Search by file:',
        emptySearchText:
            'Describe in a free way where and what job you are looking for, what tech stack you are interested',
    },
    keywords: {
        label: 'Keywords:',
        inputTitle: 'Add keywords to current search:',
        inputLabel: 'Refine your search:',
        inputPlaceholder: 'Microservices, Berlin, Homeoffice...',
        btnText: 'Add Keyword',
        resetLabel: 'Reset Keywords',
        resetBtnText: 'Reset',
    },
    links: {
        home: 'Home',
        blog: 'Our blog',
        alphabet: 'Jobs by Alphabet',
        impressum: 'Impressum',
        termsAndConditions: 'Terms & Conditions',
        privacyPolicy: 'Privacy Policy',
    },
    header: {
        logIn: 'Log In',
        signUp: 'Sign Up',
        signUpCta: 'Don`t have an account?',
        logInCta: 'Already have an account?',
        logInTitle: 'Log in to your account',
        signUpTitle: 'Sign up to your account',
        email: 'Email address',
        repeatEmail: 'Please repeat your email',
        password: 'Password',
        isRecruiter: 'I am recruiter',
        language: 'Site Language',
        offerLanguage: 'Job Language',
        cabinet: 'Profile',
        cabinetLabel: 'Profile icon',
        selectSiteLanguage: 'Site Language',
        selectOfferLanguage: 'Language of job',
        userProfile: {
            headerTitle: 'Cabinet',
            menu: 'Your cabinet',
            logOut: 'Log Out',
        },
        menuTitle: 'Website menu',
        mobile: {
            menu: 'Menu',
            menuTitle: 'Website menu',
        },
    },
    footer: {
        ourCompany: 'Our Company',
        contactFormTitle: 'Contact Us',
        copyright: 'Copyright clusterjobs ©',
        rightsReserved: 'All Rights Reserved',
        selectLanguage: 'Language',
        socialMedia: 'Our media',
    },
    offers: {
        h1: 'Results for {currentQuery}',
        h1CV: 'Results for search by CV',
        offers: 'Jobs',
        page: 'Page',
        match: 'Job Match',
        location: 'Where:',
        benefits: 'Benefits:',
        competences: 'Competences:',
        programmingLanguages: 'Tech stack:',
        view: 'Details',
        apply: 'Show Offer',
        btnText: 'Open offer on a new page',
        goToOffer: 'Job page',
        update: 'Update',
        delete: 'Delete',
        indexed: 'Published',
        in: 'in',
        notIndexed: 'Publish in process',
        companyName: 'from {companyName}',
        saveSearch: 'Job alert',
    },
    saveSearch: {
        popoverTitle: 'Create email alert',
        popoverText:
            'We are going to create a cabinet for you, where you can manage email schedule.' +
            ' For the start it will be sent weekly on Thursdays.',
        popoverSubtitle:
            'We can notify you about new offers from this list, just leave the email:',
        submitNewcomer: 'Notify me',
        saveSearchSubtitle: 'Click below to track this search and get notified',
        saveSearchPopoverText:
            'We will send you mail every Thursday.' +
            ' We are working on schedule interface, and soon you ll be able to change schedule as you like.',
        submitUser: 'Track offers',
        successPopoverTitle: 'Your search is saved!',
        successPopoverText:
            'We will notify you about new offers each Thursday.' +
            ' We are working on schedule now and soon you will be able to change it as you wish.',
        saveSearchNoQuery:
            'To be able to save search of your CV file, please sign up and fill Profile.' +
            'You will be able to access relevant offers page and set mailing schedule',
        manageSchedule: 'Manage notifications',
    },
    company: {
        email: 'Email:',
        call: 'Call:',
    },
    contactUs: {
        name: 'Your Name:',
        email: 'Email:',
        message: 'Message:',
        send: 'Send',
    },
    featuresSeo: {
        title: 'Our key features',
        aiTitle: 'AI-powered search engine',
        aiText:
            'With the advances of AI and Natural ' +
            'Language Processing (NLP), clusterjobs can' +
            ' perform semantic matching between the' +
            ' job descriptions and the job queries .' +
            ' Even though user queries may not be exactly' +
            ' matched with the job`s content clusterjobs is capable of giving the most ' +
            'relevant results.',
        cvTitle: 'Search by your Resume/CV',
        cvText:
            'clusterjobs is using the latest technology of AI' +
            ' and Natural Language Processing (NLP) to allow' +
            ' the users to search by using simply their ' +
            'Resume/Curriculum Vitae (CV). clusterjobs engine is using AI ' +
            'and NLP technologies to understand the semantic of ' +
            'the users’ CV and match them with jobs.',
        fullTextTitle: 'Full-text Search Engine',
        fullTextText:
            'clusterjobs is an advanced full-text search engine ' +
            'that allows users to search jobs freely and easily. ' +
            'It allows users to search jobs by using the job titles, ' +
            'theirs skills, and their favourite locations. For example, ' +
            'you can type "Data Scientist Python Tensorflow in Berlin" ' +
            'and the system will find the relevant jobs in Berlin.',
    },
    impressum: {
        title: 'Imprint',
        text:
            'owliphy UG (haftungsbeschränkt)<br>' +
            'Parkstraße 72<br>' +
            '40477 Duesseldorf<br>' +
            'Germany<br>' +
            '<br>' +
            'Registration office: Amtsgericht Düsseldorf<br>' +
            'Registration number: HRB 85801 <br>' +
            'VAT I.D./ TAX I.D. in accordance with § 27a UStG: DE323427817 <br>' +
            'E-Mail: info@clusterjobs.com <br>' +
            'Website: clusterjobs.de<br>',
    },
    aboutUs: {
        title: 'About clusterjobs',
    },
    termsAndCond: {
        title: 'Terms & Conditions',
    },
    privacy: {
        title: 'Privacy Policy',
    },
    about: {
        title: 'Company',
    },
    cookieNotification: {
        title: 'Use of Cookies',
        close: 'Close',
        infoLink: 'Info',
        text:
            'By using this website, you agree to the use of cookies. Details on the use of cookies can be found in our Privacy Policy',
    },
    infographics: {
        title: 'How we work',
        searchTitle: 'Simple and smart search engine',
        search:
            'Write what you are looking for or <i>search by CV</i> and we will recognise it to meaningful search.',
        engineTitle: 'Our search system',
        engine:
            'Based on your tech stack, competences, location, preferred job language ' +
            'we match you with the <i>best IT-offers</i> in Germany',
        contentTitle: 'Find yourself a next career step!',
        content:
            'Register on clusterjobs to be able to <i>save your searches and to be notified</i> about upcoming opportunities.',
        searchAlt: 'Woman surfing web with a laptop',
        engineAlt: 'A man pointing on connections in data',
        contentAlt: 'A man showing output',
    },
    account: {
        title: 'Account and settings',
        formTitle: 'Account information',
        name: 'Name',
        namePlaceholder: '',
        lastName: 'Last name',
        email: 'Email',
        updateButton: 'Update',
        successUpdate: 'Successfully updated!',
        nameUsageDetails:
            'We will use your name only to display it on website and in emails',
        emailUsageDetails: 'Email is used as a login and to restore password',
        formPasswordTitle: 'Change password',
        password: 'Current password',
        updatePasswordText: 'We need your current password to confirm access',
        newPassword: 'New password',
        newPasswordConfirmation: 'Please repeat new password',
        updatePasswordButton: 'Change',
        deleteUserTitle: 'Delete account',
        deleteText:
            'On clicking this we will delete your account and all related data, including CV and all profile information',
        deleteLabel: 'Delete',
        recruiter: {
            title: 'Recruiter cabinet settings',
            variableName: 'Source variable name',
            variableValue: 'Source variable value',
            variableNameText:
                'If you define these params, we will append variable with certain value to each your url (ex. ?utm_source=clusterjobs)',
            variableValueText: 'Both parameters need to be defined',
        },
    },
    profile: {
        title: 'Fill you profile to let us find you the best job',
        formTitle: 'We will use this information to find ',
        formTitleLink: 'most relevant results for you',
        addCv: 'Upload CV',
        successUpdate: 'We successfully updated your profile',
        cvLabel: 'We will use your CV to refine offers and show best results',
        recogniseTags: 'Recognise tags',
        recogniseTagsLabel:
            'On click we will recognise some data from your resume',
        skills: 'Skills and Frameworks',
        skillsPlaceholder:
            'ex. Architecture, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'ex. PHP, Javascript, Java, Go',
        offerTitles: 'Position name',
        offerTitlesPlaceholder: 'ex. Project Manager, Software Developer',
        locations: 'Locations',
        locationsPlaceholder: 'ex. Munich, Berlin, Cologne',
        button: 'Update profile',
        offerTypes: 'Job type',
        offerTypeFull: 'Full-time job',
        offerTypePart: 'Part-time job',
        offerTypeStudent: 'Student',
        offerTypeRemote: 'Remote job',
        offerTypesPlaceholder: 'What job are you looking for?',
        menu: 'Your cabinet:',
    },
    addOffer: {
        indexedData: 'Main information about an offer',
        contactData: 'Contact information',
        otherData: 'Other',
        button: 'Add a job!',
        title: 'Post your job',
        offerTitle: 'Job title',
        offerTitlePlaceholder: 'Please put the name of the job',
        description: 'Job description',
        miniDescription: 'Short description',
        miniDescriptionPlaceholder:
            'This description will appear in search list on job card, if less than 4 keywords are extracted from job',
        skills: 'Required skills or frameworks',
        skillsPlaceholder:
            'ex. Microservices, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'ex. PHP, Javascript, Java, Go',
        destinations: 'Job location',
        destinationsPlaceholder: 'Where job takes place?',
        offerTypes: 'Job type',
        offerTypesPlaceholder: 'Select type of job you offer',
        companyName: 'Company name',
        companyNamePlaceholder: 'You may use short name',
        uploadLogo: 'Add company logo',
        emptyText: 'Upload logo of company to get more emphasized offer',
        email: 'Email',
        emailPlaceholder: 'Email for users to send CV`s',
        url: 'Url for applying',
        urlPlaceholder:
            'Add url with `source` parameter here, or set parameter in `Account settings`',
        startsFrom: 'Starts from',
        salaryFrom: 'Year salary starts at',
        salaryFromPlaceholder: 'Displays as `from X annually`',
        salaryTo: 'Year salary up to',
        salaryToPlaceholder: 'Displays as `up to X annually`',
        successMessage: 'Job is successfully added!',
        errorNoId:
            'Sorry, something happened and we have no id of the added job!',
        recogniseTags: 'Recognise tags',
        recogniseTagsText:
            '- we can retrieve some tags(skills and languages) from your description, if you click here',
        offerLanguage: 'Required language',
        offerLanguagePlaceholder: 'Please, specify required speaking language',
    },
    offerAdded: {
        title: 'Your job is added!',
        goToList: 'List of your jobs',
        successText:
            'We need a bit of time to verify the data and approve it. It may take up to 4 hours and you will be notified, when it appears on the website.',
    },
    updateJob: {
        title: 'Update the job',
        button: 'Update',
        formSuccess: 'Job is successfully updated!',
        formError:
            'An error occurred, please contact us through feedback form.',
    },
    offersList: {
        title: 'List of your job offers',
        descriptionText: 'Jobs in this list looks the same way user sees them.',
    },
    recruiterMenu: {
        account: 'Account and settings',
        offersList: 'My jobs',
        findForCandidate: 'Find for candidate',
        addOffer: 'Post a job',
    },
    userMenu: {
        seeSavedSearches: 'Saved searches',
        seeRelevantOffers: 'Relevant jobs',
        updateProfile: 'Profile',
        account: 'Account and settings',
    },
    findForCandidate: {
        title: 'Relevant offers for your candidate',
        searchByFile: 'Keywords from `{fileName}`:',
        pageDescription:
            'Upload CV of a candidate and we will try to find him a job among your offers - ',
        button: 'Upload candidate CV',
    },
    offerPage: {
        title: '{title}',
        requirements: 'What is needed?',
        destinationsLabel: 'Location',
        benefitsLabel: 'Benefits',
        skillsLabel: 'Skills',
        programmingLanguagesLabel: 'Tech stack',
        description: 'What is it about?',
        viewOffer: 'Show Offer',
        workType: 'we offer <b itemprop="employmentType">{workType}</b>',
        createdAt: 'was posted on: ',
        backToList: 'Back to offers',
        startsAsap: 'begin <b>as soon as possible</b>',
        startsAt: 'begin <b>at {date}</b>',
        salaryRange:
            'and get from <b><span itemprop="minValue">{from}</span> {currency} to <span itemprop="maxValue">{to}</span> {currency}</b>',
        salaryFrom:
            'and get from <b><span itemprop="minValue">{salary}</span> {currency}</b>',
        salaryTo:
            'and get up to <b><span itemprop="maxValue">{salary}</span> {currency}</b>',
    },
    logoText: '{companyName} logo',
    relevantOffers: {
        title: 'Relevant offers',
        notFound:
            'Unfortunately we couldn`t find any relevant offers for you. Please, try to refine the search',
        emptyProfileLink: 'Please, fill your profile',
        emptyProfile: ', so we can find best offers for you',
        pageDescription:
            'We used your profile and CV to find most relevant offers',
    },
    message: {
        defaultFailed: 'Sorry, something went wrong.',
        missingToken: 'User token is invalid. Please, log in again.',
        badRequest: 'Bad request parameters.',

        jobNotFound: 'Offer is not found.',
        jobFail: 'Sorry, job offer request has failed.',
        jobSuccessAdd: 'Job offer was successfully added!',
        jobSuccessUpdate: 'Job offer was successfully updated!',
        jobSuccessRemove: 'Job offer successfully removed!',

        userSuccess: 'User request was successfully sent.',
        userFailed: 'Sorry, user request has failed. Contact us.',
        userNotFound: 'Sorry, user was not found. Contact us.',
        userEmptyUpdateFields: 'User updated fields were empty.',
        userBadCredentials:
            'Sorry, we didn`t find user with this email and password',
        userBadPassword: 'We think you have a mistake in current password.',

        feedbackFail:
            'Unfortunately feedback was not sent, please try again later.',
        feedbackSuccess: 'Feedback was successfully sent',

        saveSearchFailed:
            'Unfortunately, we cannot save your search at the moment',
        saveSearchSuccess: 'Search is successfully saved!',
        saveSearchUpdateSuccess: 'Schedule is successfully updated!',
    },
    accountRemoved: {
        title: 'Your account and all related data were removed',
        text:
            'All information that you provided to get relevant offers searches was removed.<br>' +
            'It and your account were removed and will not be reused anywhere.',
    },
    aria: {
        toggleMenuSection: 'Toggle menu section',
        goToOffer: 'Go to offer',
        send: 'Submit form',
        boldText: 'Make text bold',
        italicText: 'Make text italic',
        strikeText: 'Strike text through',
        underlineText: 'Underline text',
        makeCodeBlock: 'Add block of code',
        addParagraph: 'Add new paragraph',
        addH1: 'Add heading level 1',
        addH2: 'Add heading level 2',
        addH3: 'Add heading level 3',
        bulletList: 'Add bullet list',
        orderedList: 'Add ordered list',
        quote: 'Add quote',
        undo: 'Undo',
        redo: 'Redo',
        addImage: 'Add image',
        addLink: 'Add link',
        removeLink: 'Remove link',
        removeTag: 'Remove tag',
        changeSiteLanguage: 'Change site language',
        changeOffersLanguage: 'Change offers language',
        openLogInPopover: 'Open log in popover',
        openProfile: 'Open profile',
        openSiteMenu: 'Open site menu',
        close: 'Close',
        submitForm: 'Submit form',
        openSignUpPopover: 'Open sign up popover',
    },
    popularOffers: 'Popular job offers',
    popularQueries: {
        cityPagesTitle: 'Popular cities',
        title: 'Popular jobs',
        offerTitle: 'Popular jobs in this location',
        linkText: '{programming} in {location}',
    },
    similarOffersTitle: 'Similar offers',
    error: {
        noOfferTitle: 'Job offer is not found',
        noOfferText:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check our Homepage, we are sure, you can find something that suits you well!',
        noOfferTextSubstitutes:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check similar offers, we are sure, you can find something that suits you well!',
        notFound: 'Oops, page is not found',
        notfoundText: 'Unfortunately page, you are looking for is not found.',
        toHomepage: 'Here is a link to go back to homepage',
        error: 'Something weird has happened',
        errorText:
            'Unfortunately some error occurred and we are working on it already!',
    },
    blog: {
        title: '> clusterjobs magazine',
        backToMain: 'Back to blog',
        nextArticle: 'Next article',
        seoTitle: 'clusterjobs blog - we write about job searching industry',
        seoKeywords:
            'job search, AI in HR, HR, work abroad, job abroad, clusterjobs, get a job abroad',
        seoDescription:
            'We offer articles about tech and non-tech trends in job' +
            ' search and HR. We want to help people from abroad to find job all over the globe',
    },
    students: {
        features: {
            hidden: 'Our features',
            jobs: {
                title: 'More than 100,000 IT-jobs in Germany',
                text:
                    'clusterjobs aggregates offers from partners worldwide to show only best one and most relevant for our users.',
            },
            it: {
                title: 'Student and after-graduate jobs in IT',
                text:
                    'Find your werkstudent job or your early-career job in case you are graduating. clusterjobs helps you to get the best suit for you!',
            },
            ai: {
                title: 'Use smart AI search by your CV',
                text:
                    'Just upload your CV and our AI engine will recognise your strengths and will show relevant jobs.',
            },
        },
        interestedQuestion:
            'Interested? We can notify you about new relevant jobs',
        headTitle: `Find best student's jobs in Germany. Start your career in IT with clusterjobs.<br> AI will find best suiting job for you!`,
        notifyMe: 'Notify me',
        register: 'Register',
        studentJobs: 'Student jobs',
        graduateJobs: 'After-graduate jobs',
        imgAlt: 'Man with a binoculars',
        showMore: 'Show me more',
        logoTitle: 'clusterjobs - use tech search to find tech job',
        registerCta: 'Register to be notified about new and related offers',
        registerForm: {
            signUpTitle: 'Find your dream job in three steps',
        },
    },
    authentication: {
        signUp: 'Register',
        email: 'Email address',
        repeatEmail: 'Please repeat your email',
        password: 'Password',
        isRecruiter: 'I am recruiter',
    },
    partners: {
        title: 'Partners',
    },
    savedSearches: {
        title: 'Saved searches',
        pageDescription:
            'Here you can check all your saved searches and update the schedule of it',
        emptySearchesLink: ' but you can click here and save some!',
        emptySearches: 'You don`t have any saved searches,',
        saveSearchSchedule: 'Update schedule',
        removeSearchSchedule: 'Remove notification',
        listFormTitle: 'Saved searches',
        weekly: 'Send weekly on {days}',
        monthly: 'Send each month on {days}',
        scheduleTypes: 'When should we send offers?',
        weeklyDays: 'Tell us, when is it good to send you jobs?',
        monthlyDays: 'Type the dates when we should send the jobs',
        monthlyDaysExample: 'ex.15',
        monthlyDaysClarification:
            'F.e. type 15 and 28, so we will send you updates each 15 and 28 date of month',
        weeklyOption: 'Send weekly',
        monthlyOption: 'Send monthly',
    },
};
