export default {
    alphabet: {
        title: 'Jobs by alphabet',
    },
    required: 'Campo obligatorio',
    validationPassword: 'Min. 6 carácteres - 6 números y letras',
    eg: 'e.g.',
    jobsFor: 'Ofertaspara',
    seo: {
        general: {
            siteName: 'Clusterjobs - be smart in your job search!',
        },
        accountRemoved: {
            title: 'la cuenta en clusterjobs ha sidoborrada',
        },
        homepage: {
            title: 'clusterjobs – IT trabajo en Europa',
            keywords: 'buscarempleo, trabajo en Europa, programador Europa',
            description:
                'clusterjobs buscatrabajo en IT sector en Europa' +
                'Publicamos más de 200,000 ofertas de trabajo en Alemania en inglés y alemán' +
                'Buscar por Curriculum vitae o palabra clave en texto completo',
        },
        search: {
            jobsFor: '{query} oferta de trabajo en Alemania',
            emptySearch: 'clusterjobs - IT trabajo en Europa',
            keywords:
                'empleo, trabajo en Europa, programador Europa, {keywords}',
            description:
                'Buscar {query} en Europa. {number} nuevas ofertas de trabajo {query} en clusterjobs.de',
        },
        offer: {
            title: '{title} en {companyName} - find your job on clusterjobs',
            keywords:
                'job, offer, IT, find job, jobs, {skills}, {programmingLanguages}, {destinations}',
            description:
                'Oferta de trabajo {title} en {companyName}. Buscamos {skills}, {programmingLanguages} en {destinations}',
        },
        offerNotFound: {
            title: 'Job offer is not found',
        },
        impressum: {
            title: 'clusterjobs - Información de empresa',
            keywords: 'clusterjobs, contacto',
            description: 'clusterjobs imprint - Información de empresa',
        },
        termsAndCond: {
            title: 'clusterjobs - Términos y condiciones',
            keywords: 'clusterjobs, términos y condiciones',
            description: 'clusterjobs Términos y condiciones',
        },
        privacyPolicy: {
            title: 'clusterjobs - Política de privacidad',
            keywords: 'clusterjobs, política de privacidad',
            description: 'clusterjobs Política de privacidad',
        },
        company: {
            title: 'clusterjobs - Quienes somos',
            keywords: 'clusterjobs, quienes somos',
            description: 'clusterjobs - Quienes somos',
        },
    },
    homepage: {
        imgAlt: 'Man with a laptop',
        ctaTitle: 'Búsqueda intellectual de trabajo en IT',
        recentlyAddedTitle: 'Nuevas ofertas',
    },
    search: {
        label: 'Buscar por texto',
        cvLabel: 'Averigua donde encaja tu CV',
        or: 'o',
        buttonText: 'Nueva búsqueda',
        cvButtonText: 'Nueva búsqueda por CV',
        cvMobileButtonText: 'CV',
        defaultButtonText: 'Búsqueda',
        defaultCvButtonText: 'Búsqueda por CV',
        defaultCvMobileButtonText: 'CV',
        defaultQuery: 'fullstack php berlin',
        searchByQuery: 'Buscar por texto:',
        searchByFile: 'Buscar por archivo:',
        emptySearchText:
            'Describe in a free way where and what job you are looking for, what tech stack you are interested',
    },
    keywords: {
        label: 'Keywords:',
        inputTitle: 'Añadir tags a búsqueda actual:',
        inputLabel: 'Filtrar solicitud actual:',
        inputPlaceholder: 'Microservices, Berlin, Homeoffice...',
        btnText: 'Añadir tag',
        resetLabel: 'Restablecer tags',
        resetBtnText: 'Restablecer',
    },
    links: {
        home: 'Pagina principal',
        blog: 'Blog',
        alphabet: 'Jobs by Alphabet',
        impressum: 'Información legal',
        termsAndConditions: 'Terminos y condiciones',
        privacyPolicy: 'Política de privacidad',
    },
    header: {
        logIn: 'Iniciar sesión',
        signUp: 'Registrarse',
        signUpCta: 'No tienes cuenta?',
        logInCta: 'Ya tienes cuenta?',
        logInTitle: 'Entrar al perfil',
        signUpTitle: 'Registrar perfil',
        email: 'E-Mail',
        repeatEmail: 'Por favor repite el correo electrónico',
        password: 'Contraseña',
        isRecruiter: 'Soy reclutador',
        language: 'Idioma',
        offerLanguage: 'Idioma de empleos',
        cabinet: 'Cuenta personal',
        cabinetLabel: 'Icono personal de cuenta',
        selectSiteLanguage: 'Idioma',
        selectOfferLanguage: 'Idioma de empleos',
        userProfile: {
            headerTitle: 'Perfil',
            menu: 'Cuenta personal',
            logOut: 'Cerrar sesión',
        },
        menuTitle: 'Menú principal',
        mobile: {
            menu: 'Menú',
            menuTitle: 'Menú principal',
        },
    },
    footer: {
        ourCompany: 'Empresa',
        contactFormTitle: 'Contáctanos',
        copyright: 'Copyright clusterjobs ©',
        rightsReserved: 'All Rights Reserved',
        selectLanguage: 'Idioma',
        socialMedia: 'Medios',
    },
    offers: {
        h1: 'Results for {currentQuery}',
        h1CV: 'Results for search by CV',
        offers: 'Ofertas',
        page: 'Página',
        match: 'Coincidencia:',
        location: 'Ubicación:',
        benefits: 'Beneficios:',
        competences: 'Competencias:',
        programmingLanguages: 'Pila de tecnología:',
        view: 'Detalles',
        apply: 'Ir',
        btnText: 'Abrir oferta en una nueva página',
        goToOffer: 'Página de trabajo',
        update: 'Actualizar',
        delete: 'Borrar',
        indexed: 'Publicado',
        in: 'en',
        notIndexed: 'Publicar en proceso ',
        companyName: 'desde {companyName}',
        saveSearch: 'Alerta de trabajo',
    },
    saveSearch: {
        popoverTitle: 'Crear alerta de correo electrónico',
        popoverText:
            'We are going to create a cabinet for you, where you can manage email schedule.' +
            ' For the start it will be sent weekly on Thursdays.',
        popoverSubtitle:
            'We can notify you about new offers from this list, just leave the email:',
        submitNewcomer: 'Notify me',
        saveSearchSubtitle: 'Click below to track this search and get notified',
        saveSearchPopoverText:
            'We will send you mail every Thursday.' +
            ' We are working on schedule interface, and soon you ll be able to change schedule as you like.',
        submitUser: 'Track offers',
        successPopoverTitle: 'Your search is saved!',
        successPopoverText:
            'We will notify you about new offers each Thursday.' +
            ' We are working on schedule now and soon you will be able to change it as you wish.',
    },
    company: {
        email: 'Email:',
        call: 'Llamar:',
    },
    contactUs: {
        name: 'Nombre:',
        email: 'Email:',
        message: 'Mensaje:',
        send: 'Enviar',
    },
    featuresSeo: {
        title: 'Nuestrascaracterísticas',
        aiTitle: 'Sistema de búsquedabasado en AI',
        aiText:
            'Utiliza oportunitades de AI y NLP' +
            'clusterjobs puedeigualar el significado de' +
            'descripcióndel empleo con solicitudes de usuarios.' +
            'Aunque solicitudes de usuarios' +
            'aveces no coinciden con el contenido deempleo, clusterjobs escapaz de ' +
            'ofrecer siempre resultados más aplicaples.',
        cvTitle: 'Buscarpor CV',
        cvText:
            'clusterjobs utiliza la últimatecnología de AI y' +
            'Procesamiento de lenguaje natural (NLP) que facilita' +
            'al usuariobusquedas con CV. ' +
            'clusterjobs utiliza AI y NLP parareconocer la esencia de' +
            'Resumen de usuarios y los emparejan con el apropiadas' +
            'ofertas de trabajo.',
        fullTextTitle: 'Sistema de búsquedaen texto completo',
        fullTextText:
            'clusterjobs es un sistema de búsqueda avanzada por texto completo ' +
            'quepermite a los usuarios realizar búsquedas fácilmente.' +
            'La característica, por ejemplo, permite a los usuarios buscar' +
            'ofertas de trabajo, usando tanto el título del post, habilidades personales. Por ejemplo, ' +
            'puedes escribir "Data Scientist Python Tensorflow in Berlin" ' +
            'y el sistema encontrará los trabajos relevantes en Berlín..',
    },
    impressum: {
        title: 'Imprint',
        text:
            'owliphy UG (haftungsbeschränkt)<br>' +
            'Parkstraße 72<br>' +
            '40477 Dusseldorf<br>' +
            'Alemania<br>' +
            '<br>' +
            'Domicilio: Amtsgericht Düsseldorf<br>' +
            'Número de registro: HRB 85801 <br>' +
            'VAT I.D./ TAX I.D. in accordance with § 27a UStG: DE323427817 <br>' +
            'E-Mail: info@clusterjobs.com <br>' +
            'Página web: clusterjobs.de<br>',
    },
    aboutUs: {
        title: 'Empresa',
    },
    termsAndCond: {
        title: 'Términos y condiciones ',
    },
    privacy: {
        title: 'Política de privacidad',
    },
    about: {
        title: 'Empresa',
    },
    cookieNotification: {
        title: 'Uso de cookies',
        close: 'Cerrar',
        infoLink: 'Más',
        text:
            'Para utilizar esta página web usted acepta los términos de uso de las cookies. información sobre el uso de cookies que encontrará en la sección de Política de privacidad.',
    },
    infographics: {
        title: 'Cómo trabajamos',
        searchTitle: 'La búsqueda intellectuales sencillo e inteligente ',
        search:
            'Simplemente escriba el cuadro de búsqueda que está buscando y <i> o buscarpor CV </i>. ' +
            'Con palabras clave de CV encontraremos mejores ofertas!',
        engineTitle: 'Nuestrobuscador',
        engine:
            'Con su pila tecnológica, competencias, lugardeseado de trabajo y el idioma necesario para trabajar ' +
            'proporcionamos mejores ofertas del trabajo en sector IT en Alemania',
        contentTitle: '¡Encuentra el trabajo queteconvenga! ',
        content:
            'Registrarse en <b> clusterjobs </b> para guardarsus resultados de búsqueda e inscribirse a las noticias <b> ' +
            'sobre nuevas oportunidades</b>.',
        searchAlt: 'Woman surfing web with a laptop',
        engineAlt: 'A man pointing on connections in data',
        contentAlt: 'A man showing output',
    },
    account: {
        title: 'Cuenta y configuración',
        formTitle: 'Información sobre cuenta ',
        name: 'Nombre',
        namePlaceholder: '',
        lastName: 'Apellido',
        email: 'Email',
        updateButton: 'Actualizar',
        successUpdate: '¡Está actualizado!',
        nameUsageDetails:
            'Utilizamos su nombre sólo para visualización en página web y dirección de e-mail',
        emailUsageDetails:
            'Dirección de e-mail se utilizará como logín y para recuperar contraseña',
        formPasswordTitle: 'Cambiar contraseña',
        password: 'Contraseña actual',
        updatePasswordText:
            'Necesitamos su contraseña actual para iniciar sesión',
        newPassword: 'Nueva contraseña',
        newPasswordConfirmation: 'Por favor, repita la nueva contraseña',
        updatePasswordButton: 'Cambiar',
        deleteUserTitle: 'Eliminar cuenta',
        deleteText:
            'Al seleccionar esta opción, su cuenta y toda la información relacionada (CV y datos personales) se eliminarán por completo.',
        deleteLabel: 'Eliminar',
        recruiter: {
            title: 'Recruiter cabinet settings',
            variableName: 'Source variable name',
            variableValue: 'Source variable value',
            variableNameText:
                'If you define these params, we will append variable with certain value to each your url (ex. ?utm_source=clusterjobs)',
            variableValueText: 'Both parameters need to be defined',
        },
    },
    profile: {
        title: 'Llene un perfil para ayudarnos a encontrar la mejor oferta',
        formTitle: 'Utilizamos la información para ',
        formTitleLink: 'encontrar las ofertas más relevantes',
        addCv: 'Descargar CV',
        successUpdate: 'Perfil correctamente actualizado',
        cvLabel: 'Utilizamos su CV para encontrar las mejores ofertas',
        recogniseTags: 'Reconocer tags',
        recogniseTagsLabel:
            'Al seleccionar esta opción, el sistema reconocerá tags de su CV',
        skills: 'Habilidades',
        skillsPlaceholder:
            'por ejemplo, Architecture, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'por ejemplo, PHP, Javascript, Java, Go',
        offerTitles: 'Especialidad',
        offerTitlesPlaceholder:
            'por ejemplo, Project Manager, Software Developer',
        locations: 'Ubicación',
        locationsPlaceholder: 'por ejemplo, Munich, Berlin, Cologne',
        button: 'Actualizar perfil',
        offerTypes: 'Tipo del contrato',
        offerTypeFull: 'Full-time job',
        offerTypePart: 'Medio jornada',
        offerTypeStudent: 'Internado',
        offerTypeRemote: 'Trabajo al distancia',
        offerTypesPlaceholder: '¿Qué trabajo estás buscando?',
        menu: 'Su oficina:',
    },
    addOffer: {
        indexedData: 'Descripción de oferta',
        contactData: 'Información de contacto',
        otherData: 'Resto',
        button: 'Añadir oferta!',
        title: 'Colocar su oferta',
        offerTitle: 'Puesto',
        offerTitlePlaceholder: 'Por favor, agregue puesto',
        description: 'Descripción',
        miniDescription: 'Breve descripción',
        miniDescriptionPlaceholder:
            'Esta descripción se mostrará en la ficha de trabajo en la lista si la frase tiene menos de 4 tags',
        skills: 'Conocimientos imprescindibles',
        skillsPlaceholder:
            'por ejemplo, Microservices, Neural Networks, Relational Databases',
        langs: 'Tech stack',
        langsPlaceholder: 'por ejemplo, PHP, Javascript, Java, Go',
        destinations: 'Ubicación',
        destinationsPlaceholder: 'Ubicación de trabajo',
        offerTypes: 'Tipo de empleo',
        offerTypesPlaceholder: 'Seleccionar el tipo de empleo',
        companyName: 'Nombre de empresa',
        companyNamePlaceholder: 'Puedes usar el nombre abreviado',
        uploadLogo: 'Añadir logotipo',
        emptyText: 'La oferta con el logo será más visible en la lista',
        email: 'Email',
        emailPlaceholder:
            'Dirección de e-Mailpara candidatos para presentar su CV',
        url: 'Enlace de oferta',
        urlPlaceholder: '¿Dónde reorientar a los candidatos? ',
        startsFrom: 'Inicio del empleo',
        salaryFrom: 'Salario anual',
        salaryFromPlaceholder: 'Se mostrará desde X brutos al año',
        salaryTo: 'Salario anual hasta',
        salaryToPlaceholder: 'Se mostrará hasta X brutos al año `y`',
        successMessage: 'Oferta añadida con éxito',
        errorNoId:
            'Se ha producido un error y no hemos recibido el ID del trabajo',
        recogniseTags: 'Reconocer tags',
        recogniseTagsText:
            'Al seleccionar esta opción, el sistema reconoce tags (habilidades y lenguajes de programación) de sudescripción',
        offerLanguage: 'Required language',
        offerLanguagePlaceholder: 'Please, specify required speaking language',
    },
    offerAdded: {
        title: 'Oferta añadida',
        goToList: 'Listado de ofertas',
        successText:
            'Necesitamos un poco de tiempoparacomprobar los datosañadidos. Puedetardar hasta 4 horas. Gracias',
    },
    updateJob: {
        title: 'Actualizar oferta',
        button: 'Actualizar',
        formSuccess: 'Ofertase ha actualizado con éxito',
        formError: 'Se ha producido un error, por favor contacte con nosotros',
    },
    offersList: {
        title: 'Todas ofertas',
        descriptionText:
            'El formato de visualizaciónde ofertasse veigualpara los candidatos',
    },
    recruiterMenu: {
        account: 'Cuenta y configuración',
        offersList: 'Mis vacantes',
        findForCandidate: 'Encontrar candidato',
        addOffer: 'Lugar vacante',
    },
    userMenu: {
        seeRelevantOffers: 'Vacantes adecuadas',
        updateProfile: 'Perfil',
        account: 'Cuenta y configuración ',
    },
    findForCandidate: {
        title: 'Vacantes adecuadas para su candidato',
        searchByFile: 'Palabras clave de `{fileName}`:',
        pageDescription:
            'Descargar el CV del candidato y tratamos de encontrar vacante adecuada de susofertas - ',
        button: 'Upload candidato CV',
    },
    offerPage: {
        title: '{title} en {companyName}',
        requirements: 'Requisitos',
        destinationsLabel: 'Ubicación',
        benefitsLabel: 'Beneficios',
        skillsLabel: 'Habilidades',
        programmingLanguagesLabel: 'Tech stack',
        description: 'Description',
        viewOffer: 'Mostrar oferta',
        workType: 'Ofrecemos <b>{workType}</b>',
        startsAsap: 'Iniciar <b>tan pronto como sea posible</b>',
        createdAt: 'was posted on: ',
        backToList: 'Back to offers',
        startsAt: 'Iniciar <b> en {date}</b>',
        salaryRange:
            'y obtener <b><span itemprop="minValue">{from}</span> {currency} de <span itemprop="maxValue">{to}</span> {currency}</b>',
        salaryFrom:
            'y obtener<b> de <span itemprop="minValue">{salary}</span> {currency}</b>',
        salaryTo:
            'y obtener<b> hasta <span itemprop="maxValue">{salary}</span> {currency}</b>',
    },
    logoText: '{companyName} logo',
    relevantOffers: {
        title: 'Vacantes adecuadas',
        notFound:
            'Unfortunately we couldn`t find any relevant offers for you. Please, try to refine the search',
        emptyProfileLink: 'Por favor, rellene superfil',
        emptyProfile: 'para que podamos encontrar las ofertas más apropiadas',
        pageDescription:
            'Usamos tu perfil y CV para encontrar las ofertas más adecuadas',
    },
    message: {
        defaultFailed: 'Algo salió mal, póngase en contacto con nosotros',
        missingToken:
            'El token de usuario no esválido. Por favor, vuelveainiciarsesión en tucuenta.',
        badRequest: 'Parámetros de consulta falsos',

        jobNotFound: 'Oferta no encontrada',
        jobFail: 'Lo sentimos, la peticiónfalló',
        jobSuccessAdd: 'Oferta añadida con éxito ',
        jobSuccessUpdate: 'La oferta se ha actualizado con éxito',
        jobSuccessRemove: 'La oferta ha sidoeliminada con éxito',

        userFailed: 'Lo sentimos, la solicitud de usuario falló. Contáctenos.',
        userNotFound: 'Lo sentimos, el usuario no se encuentra. Contáctenos.',
        userEmptyUpdateFields:
            'Los campos de actualización de usuario están vacíos',
        userBadCredentials:
            'Lo sentimos, no podemos encontrar un usuario con tal correo electrónico y contraseña',

        feedbackFail:
            'Desafortunadamente, su opinión no se ha salvado. Por favor inténtelo de nuevomástarde',
        feedbackSuccess: 'Los comentarios se han añadido con éxito',

        saveSearchFailed:
            'Unfortunately, we cannot save your search at the moment',
        saveSearchSuccess: 'Search is successfully saved!',
        saveSearchUpdateSuccess: 'Schedule is successfully updated!',
    },
    accountRemoved: {
        title:
            'Su cuenta y todos los datos personales asociados con el hansi eliminados con éxito',
        text:
            'Se ha eliminado toda la información que ha proporcionado para buscar vacantes <br>' +
            'Los datos personales y su cuenta se ha eliminado por completo y no se pueden utilizar en ningúnotrolugar',
    },
    aria: {
        toggleMenuSection: 'Sección alternar-menú',
        goToOffer: 'Ir a vacantes',
        send: 'Enviar',
        boldText: 'Texto en negrita',
        italicText: 'Cursiva',
        strikeText: 'Tachado de texto',
        underlineText: 'Texto subrayado',
        makeCodeBlock: 'Añadir bloque de código',
        addParagraph: 'Añadir nuevo párrafo',
        addH1: 'Añadir cabecera 1',
        addH2: 'Añadir cabecera 2',
        addH3: 'Añadir cabecera 3',
        bulletList: ' Añadir lista',
        orderedList: 'Añadir lista numerada ',
        quote: 'Añadir presupuesto',
        undo: 'Cancelar',
        redo: 'Replay',
        addImage: 'Añadir imagen',
        addLink: 'Añadir enlace',
        removeLink: 'Quitar enlace',
        removeTag: 'Quitar tag',
        changeSiteLanguage: 'Cambiar idioma del sitio ',
        changeOffersLanguage: 'Cambiar el idioma de las vacantes ',
        openLogInPopover: 'Abrir ventana de inicio de sesión',
        openProfile: 'Login a la cuenta',
        openSiteMenu: 'Abrir el menú del sitio',
        close: 'Cerrar',
        submitForm: 'Enviar',
        openSignUpPopover: 'Abrir la ventana de registro',
    },
    popularOffers: 'Popular job offers',
    popularQueries: {
        title: 'Consultas populares',
        cityPagesTitle: 'Popular cities',
        offerTitle: 'Popular jobs in this location',
        linkText: '{programming} in {location}',
    },
    similarOffersTitle: 'Trabajos relacionados',
    error: {
        noOfferTitle: 'Job offer is not found',
        noOfferText:
            'Unfortunately we can`t find the offer, you are looking for. <br>' +
            ' Please, check our Homepage, we are sure, you can find something that suits you well!',
        noOfferTextSubstitutes:
            'Unfortunately we can`t find the offer, you are looking for. <br> Please, check similar offers, we are sure, you can find something that suits you well!',
        notFound: 'Oops, hemosperdido la página ',
        notfoundText:
            'Desafortunadamente, la página que estabas buscando no fue encontrada',
        toHomepage: 'Aquí hay un enlace paravolver a casa',
        error: 'Algo extraño ha sucedido',
        errorText:
            'Desafortunadamente, se ha producido un error y yaestamostrabajando en ello',
    },
    blog: {
        title: '> clusterjobs magazine',
        backToMain: 'Volver a los artículos',
        nextArticle: 'Siguiente articulo',
        seoTitle:
            'clusterjobs blog - escribimos sobre la industria de la búsqueda de empleo.',
        seoKeywords:
            'job search, AI in HR, HR, work abroad, job abroad, clusterjobs, get a job abroad',
        seoDescription:
            'Nos centramos en un artículo que le ayudará a encontrar un trabajo en el extranjero,' +
            ' tendencia en IT y AI, y características de las profesiones de IT',
    },
    students: {
        features: {
            hidden: 'Our features',
            jobs: {
                title: 'More than 100,000 IT-jobs in Germany',
                text:
                    'clusterjobs aggregates offers from partners worldwide to show only best one and most relevant for our users.',
            },
            it: {
                title: 'Student and after-graduate jobs in IT',
                text:
                    'Find your werkstudent job or your early-career job in case you are graduating. clusterjobs helps you to get the best suit for you!',
            },
            ai: {
                title: 'Use smart AI search by your CV',
                text:
                    'Just upload your CV and our AI engine will recognise your strengths and will show relevant jobs.',
            },
        },
        interestedQuestion:
            'Interested? We can notify you about new relevant jobs',
        headTitle: `Find best student's jobs in Germany. Start your career in IT with clusterjobs.<br> AI will find best suiting job for you!`,
        notifyMe: 'Notify me',
        register: 'Register',
        studentJobs: 'Student jobs',
        graduateJobs: 'After-graduate jobs',
        imgAlt: 'Man with a binoculars',
        showMore: 'Show me more',
        logoTitle: 'clusterjobs - use tech search to find tech job',
        registerCta: 'Register to be notified about new and related offers',
        registerForm: {
            signUpTitle: 'Find your dream job in three steps',
        },
    },
    authentication: {
        signUp: 'Register',
        email: 'Email address',
        repeatEmail: 'Please repeat your email',
        password: 'Password',
        isRecruiter: 'I am recruiter',
    },
    partners: {
        title: 'Partners',
    },
};
