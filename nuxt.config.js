require('dotenv').config({ path: process.env.ENV_FILE });

// eslint-disable-next-line nuxt/no-cjs-in-config
module.exports = {
    mode: 'universal',
    head: {
        meta: [
            { charset: 'utf-8' },
            {
                name: 'yandex-verification',
                content: '1c451f476b179eb9',
            },
            {
                name: 'msvalidate.01',
                content: '5B66A48B7A2952BA4AB5FAF720B4F059',
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            {
                name: 'twitter:card',
                content: 'summary',
            },
            {
                hid: 'description',
                name: 'description',
                content: 'Job offers search based on AI algorithm',
            },
            {
                hid: `og:url`,
                property: 'og:url',
                content: `https://clusterjobs.de`,
            },
            {
                hid: `og:type`,
                property: 'og:type',
                content: 'website',
            },
            {
                hid: `og:image`,
                property: 'og:image',
                content:
                    'https://storage.googleapis.com/company-public/cluster.png',
            },
            {
                hid: `og:image:secure_url`,
                property: 'og:image:secure_url',
                content:
                    'https://storage.googleapis.com/company-public/cluster.png',
            },
            {
                hid: `og:image:type`,
                property: 'og:image:type',
                content: 'image/png',
            },
            {
                hid: `og:image:alt`,
                property: 'og:image:alt',
                content: 'clusterjobs - let AI find your ideal job',
            },
        ],
        link: [
            {
                rel: 'preconnect',
                href: 'https://storage.googleapis.com',
                crossorigin: true,
            },
            {
                rel: 'preconnect',
                href: 'https://www.google-analytics.com',
                crossorigin: true,
            },
            {
                rel: 'preconnect',
                href: 'https://neuvoo.ca',
                crossorigin: true,
            },
            {
                rel: 'preconnect',
                href: 'https://joblift.de',
                crossorigin: true,
            },
            {
                rel: 'preconnect',
                href: 'https://www.googletagmanager.com',
                crossorigin: true,
            },
        ],
    },

    router: {
        middleware: ['setMinBid', 'setPartnerFilter'],
    },

    css: [
        {
            src: '@/assets/css/main.scss',
            lang: 'sass',
        },
    ],

    env: {
        searchEndpoint: process.env.SEARCH_ENDPOINT,
        phpEndpoint: process.env.PHP_ENDPOINT,
        expressEndpoint: process.env.EXPRESS_ENDPOINT,
        nodeEnv: process.env.NODE_ENV,
        searchApiKey: 'a1d07162dad914f78c972b6ece1549d9',
        suggestionsApiKey: '0ca7fd39baf63de181550b404c04870f',
        phpApiKey: 'lFi32BH1QBGGTUSCySIsbkT5X2nDga19',
        enableLog: process.env.ENABLE_LOG === 'on',
        showLog: process.env.SHOW_LOG === 'on',
    },

    modules: [
        '@nuxtjs/svg',
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
        '@nuxtjs/apollo',
        '@nuxtjs/pwa',
        '@nuxtjs/sentry',
        '@nuxtjs/style-resources',
        [
            '@nuxtjs/google-analytics',
            {
                id: 'UA-134683221-1',
                debug: false,
            },
        ],
        ['@nuxtjs/google-tag-manager', { id: 'GTM-KQ8B83L', dev: false }],
        [
            'nuxt-i18n',
            {
                seo: false,
                defaultLocale: 'de',
                locales: [
                    {
                        code: 'de',
                        iso: 'de',
                        file: 'de-DE.js',
                        name: 'Deutsch',
                        domain: 'clusterjobs.de',
                    },
                    {
                        code: 'en',
                        iso: 'en',
                        file: 'en-US.js',
                        name: 'English',
                        domain: 'en.clusterjobs.de',
                    },
                    {
                        code: 'ru',
                        iso: 'ru',
                        file: 'ru-RU.js',
                        name: 'Русский',
                        domain: 'ru.clusterjobs.de',
                    },
                    {
                        code: 'ua',
                        iso: 'uk',
                        file: 'ua-UA.js',
                        name: 'Український',
                        domain: 'ua.clusterjobs.de',
                    },
                    {
                        code: 'es',
                        iso: 'es',
                        file: 'es-ES.js',
                        name: 'Español',
                        domain: 'es.clusterjobs.de',
                    },
                    {
                        code: 'pt',
                        iso: 'pt',
                        file: 'pt-PT.js',
                        name: 'Portuguese',
                        domain: 'pt.clusterjobs.de',
                    },
                    {
                        code: 'tr',
                        iso: 'tr',
                        file: 'tr-TR.js',
                        name: 'Türkçe',
                        domain: 'tr.clusterjobs.de',
                    },
                    {
                        code: 'ar',
                        iso: 'ar',
                        file: 'ar-AR.js',
                        name: 'عربي',
                        domain: 'ar.clusterjobs.de',
                    },
                    {
                        code: 'ro',
                        iso: 'ro',
                        file: 'ro-RO.js',
                        name: 'Română',
                        domain: 'ro.clusterjobs.de',
                    },
                ],
                differentDomains: process.env.MULTILANG_DOMAINS === 'enable',
                lazy: true,
                forwardedHost: process.env.FORWARDED_HOST === 'enable',
                langDir: 'lang/',
                detectBrowserLanguage: false,
            },
        ],
    ],

    apollo: {
        tokenName: 'apollo-token',
        cookieAttributes: {
            expires: 10,
        },
        authenticationType: 'Bearer',
        defaultOptions: {
            $query: {
                loadingKey: 'loading',
                fetchPolicy: 'cache-and-network',
            },
        },
        errorHandler: '~/plugins/apolloErrorHandler.js',
        clientConfigs: {
            default: {
                httpEndpoint: process.env.GRAPH_QL_ENDPOINT,
                // See https://www.apollographql.com/docs/link/links/http.html#options
                httpLinkOptions: {
                    credentials: 'same-origin',
                },
                wsEndpoint: false,
                tokenName: 'apollo-token',
                persisting: false,
                websocketsOnly: false,
            },
            test: {
                httpEndpoint: 'http://localhost:4000/api',
                wsEndpoint: false,
                tokenName: 'apollo-token',
            },
        },
    },

    plugins: [
        { src: '~/plugins/apollo.js' },
        { src: '~/plugins/requestModules.js' },
        { src: '~/plugins/viewport.js' },
        { src: '~/plugins/storageFile.js' },
        { src: '~/plugins/axiosCache.js' },
        { src: '~/plugins/log.js' },
        { src: '~/plugins/userType.js' },

        { src: '~/plugins/onAppReady.js', mode: 'client' },
        { src: '~/plugins/vueSyncRoute.js', mode: 'client' },
        { src: '~/plugins/localStorage.js', mode: 'client' },
        { src: '~/plugins/lazysizes.js', mode: 'client' },
    ],

    proxy: {
        '/static-stmp.xml': 'https://services.clusterjobs.de/api/gcs/sitemap/',
        '/static-stmp*.xml': 'https://services.clusterjobs.de/api/gcs/sitemap/',
    },

    axios: {
        proxyHeaders: false,
        credentials: false,
    },

    render: {
        static: {
            maxAge: 1000 * 60 * 60 * 24 * 25,
        },
    },

    sentry: {
        dsn: 'https://07ebf9efc2774e63b2deaa8f254f4075@sentry.io/1379378',
        config: {
            environment: process.env.NODE_ENV,
            disabled: process.env.ENABLE_SENTRY !== 'enable',
            disableClientSide: process.env.ENABLE_SENTRY !== 'enable',
            publishRelease: process.env.ENABLE_SENTRY === 'enable',
        },
    },

    styleResources: {
        scss: ['./assets/css/partials.scss'],
    },

    build: {
        parallel: true,
        terser: true,

        extend(config, { isDev, isClient }) {
            if (isDev) {
                config.devtool = '#source-map';
            }

            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/,
                });
            }

            if (isClient) {
                config.optimization.splitChunks.maxSize = 200000;
            }
        },
    },
};
