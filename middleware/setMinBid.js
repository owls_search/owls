const campaignToMinBid = {
    g: 0.2,
    gi: 0.3,
};

const mediumToMinBid = {
    facebook: 0.2,
    linkedin: 0.2,
    vk: 0.2,
};

/**
 * If query parameter `theme` is presented, we expect it to be 42-50
 * where 42 - partner id, and 50 - is minimum bid
 *
 * It could be theme=-50 for setting only bid or theme=42- for setting only partner filetr
 *
 * In case we have minimum bid from `theme` we set it, in other case we check `sem_network`,
 * for sem_network=g and sem_network=g we have preset filter of partners
 *
 * In other case we also check utm_medium and set min bid from there
 *
 * @param query
 * @param store
 */
export default function({ query, store }) {
    const {
        sem_network: semNetwork = '',
        utm_medium: medium = '',
        theme = '',
    } = query;

    if (process.server) {
        let minBid = 0;
        const [, hiddenBid] = theme.split('-');

        if (hiddenBid) {
            minBid = hiddenBid / 100;
        } else if (semNetwork && campaignToMinBid[semNetwork]) {
            minBid = campaignToMinBid[semNetwork];
        } else if (medium && mediumToMinBid[medium]) {
            minBid = mediumToMinBid[medium];
        }

        store.commit('SEARCH_SET_MIN_BID', minBid);
    }
}
