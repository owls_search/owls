import { head } from '~/util/lodash';

/**
 * 42 - stellenanzeigen
 * 64 - getinit
 * 155 - stellenonline
 */

const campaignToPartner = {
    fg: '42,64,155',
    fgi: '42,64,155',
};

/**
 * If query parameter `theme` is presented, we expect it to be 42-50
 * where 42 - partner id, and 50 - is minimum bid
 *
 * It could be theme=-50 for setting only bid or theme=42- for setting only partner filetr
 *
 * In case we have partner id from `theme` we set it, in other case we check `sem_network`,
 * for sem_network=fg and sem_network=fgi we have preset filter of partners
 *
 * @param query
 * @param store
 */
export default function({ query, store }) {
    const { sem_network: semNetwork = '', theme = '' } = query;
    if (process.server) {
        const partnerId = head(theme.split('-'));

        if (partnerId) {
            store.commit('SEARCH_PARTNER_FILTER', partnerId);
        } else if (
            semNetwork &&
            Object.keys(campaignToPartner).includes(semNetwork)
        ) {
            store.commit(
                'SEARCH_PARTNER_FILTER',
                campaignToPartner[semNetwork],
            );
        } else {
            store.commit('SEARCH_PARTNER_FILTER', null);
        }
    }
}
