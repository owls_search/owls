import { getFromCookie, userType } from '~/util';

export default function({ store, redirect, req }) {
    const token = process.server
        ? getFromCookie(req, 'token')
        : store.getters.isAuthenticated;

    const userData = process.server
        ? getFromCookie(req, 'userData')
        : store.getters.getUserData;

    const { type = '' } = userData || {};

    if (!token || type !== userType.APPLICANT) {
        return redirect('/');
    }
}
