import * as Cookie from 'js-cookie';
import storageAvailable from 'storage-available';

const isCookieAvailable = () => {
    try {
        Cookie.set('cookie-test', 'test', { sameSite: 'lax' });
        Cookie.remove('cookie-test');
        return true;
    } catch (e) {
        return false;
    }
};

const dataStorage = {
    set: function() {
        return null;
    },
    get: function() {
        return {};
    },
    allowToWriteToStorage: undefined,
    isAllowedToCookie() {
        return this.allowToWriteToStorage;
    },
    initialize() {
        if (isCookieAvailable()) {
            this.set = function(key, value) {
                Cookie.set(key, value, {
                    expires: 1,
                    secure: false,
                    sameSite: 'lax',
                });
            };
            this.get = function(key) {
                return Cookie.getJSON(key);
            };

            this.allowToWriteToStorage = true;
            return;
        } else if (storageAvailable('localStorage')) {
            this.set = function(key, value) {
                window.localStorage.setItem(key, JSON.stringify(value));
            };
            this.get = function(key) {
                return JSON.parse(window.localStorage.getItem(key));
            };

            this.allowToWriteToStorage = true;
            return;
        }

        this.allowToWriteToStorage = false;
    },
};

export default dataStorage;
