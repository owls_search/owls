export default [
    'PHP software engineer',
    'javascript software engineer',
    'frontend software engineer',
    'UI developer',

    'android software engineer',
    'ios software engineer',
    'java engineer',
    'C# developer',

    'C++ software engineer',
    'data analyst',
    'data scientist ai',
    'machine learning software engineer',
];
