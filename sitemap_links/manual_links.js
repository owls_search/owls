export default [
    'https://en.clusterjobs.de/search/popular/php-developer-jobs-in-munich',
    'https://clusterjobs.de/search/popular/php-entwickler-stellenangebote-muenchen-de',
    'https://en.clusterjobs.de/search/popular/php-developer-offers-berlin-en',
    'https://clusterjobs.de/search/popular/php-entwickler-stellenangebote-berlin-de',
    'https://en.clusterjobs.de/search/popular/java-developer-offers-munich-en',
    'https://clusterjobs.de/search/popular/java-entwickler-stellenangebote-muenchen-de',
    'https://en.clusterjobs.de/search/popular/java-developer-offers-berlin-en',
    'https://clusterjobs.de/search/popular/java-entwickler-stellenangebote-berlin-de',
    'https://en.clusterjobs.de/search/popular/javascript-developer-offers-munich-en',
    'https://clusterjobs.de/search/popular/javascript-entwickler-stellenangebote-muenchen-de',
    'https://en.clusterjobs.de/search/popular/javascript-developer-offers-berlin-en',
    'https://clusterjobs.de/search/popular/javascript-entwickler-stellenangebote-berlin-de',
];
