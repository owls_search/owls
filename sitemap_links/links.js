export default [
    {
        title: 'student',
        lang: 'en',
    },
    {
        title: 'werkstudent',
        lang: 'de',
    },
    {
        title: 'senior php developer',
        lang: 'en',
    },
    {
        title: 'web developer',
        lang: 'en',
    },
    {
        title: 'js developer',
        lang: 'en',
    },
    {
        title: 'php developer',
        lang: 'en',
    },
    {
        title: '.net developer',
        lang: 'en',
    },
    {
        title: 'agile / devops coach',
        lang: 'en',
    },
    {
        title: 'agile java developer',
        lang: 'en',
    },
    {
        title: 'agile software tester',
        lang: 'en',
    },
    {
        title: 'agile test analyst',
        lang: 'en',
    },
    {
        title: 'agiler software entwickler',
        lang: 'de',
    },
    {
        title: 'agiler software entwickler java',
        lang: 'en',
    },
    {
        title: 'agiler softwaretester',
        lang: 'en',
    },
    {
        title: 'amg data analyst',
        lang: 'en',
    },
    {
        title: 'analyst compliance management',
        lang: 'en',
    },
    {
        title: 'analyst data analytics',
        lang: 'en',
    },
    {
        title: 'analyst digital supply chain',
        lang: 'en',
    },
    {
        title: 'analyst international shared services',
        lang: 'en',
    },
    {
        title: 'analyst quantitatives risikomanagement',
        lang: 'en',
    },
    {
        title: 'analyst business intelligence crm',
        lang: 'en',
    },
    {
        title: 'analyst consultant',
        lang: 'en',
    },
    {
        title: 'analyst consultant data scientist',
        lang: 'en',
    },
    {
        title: 'analyst corporate development',
        lang: 'en',
    },
    {
        title: 'analyst data integration',
        lang: 'en',
    },
    {
        title: 'analyst ecommerce',
        lang: 'en',
    },
    {
        title: 'analyst',
        lang: 'en',
    },
    {
        title: 'analyst itconsulting',
        lang: 'en',
    },
    {
        title: 'analyst itprojektmanagement',
        lang: 'en',
    },
    {
        title: 'analyst selffinancing administration',
        lang: 'en',
    },
    {
        title: 'analyst market data valuation',
        lang: 'en',
    },
    {
        title: 'analyst customer intelligence',
        lang: 'en',
    },
    {
        title: 'analyst/in listing data / handelsdaten',
        lang: 'en',
    },
    {
        title: 'analyst/in static data aktien/fonds',
        lang: 'en',
    },
    {
        title: 'analystin static data warrants',
        lang: 'en',
    },
    {
        title: 'analytics architect',
        lang: 'en',
    },
    {
        title: 'android developer bild',
        lang: 'en',
    },
    {
        title: 'android software developer',
        lang: 'en',
    },
    {
        title: 'angular developer',
        lang: 'en',
    },
    {
        title: 'anwendungsentwickler für software',
        lang: 'de',
    },
    {
        title: 'anwendungsentwickler java / c#',
        lang: 'de',
    },
    {
        title: 'anwendungsprogrammierer java',
        lang: 'de',
    },
    {
        title: 'app developer with c/c++ experience',
        lang: 'en',
    },
    {
        title: 'application consultant oediv kg',
        lang: 'en',
    },
    {
        title: 'application developer configurator',
        lang: 'en',
    },
    {
        title: 'application developer/business analyst',
        lang: 'en',
    },
    {
        title: 'application development engineer',
        lang: 'en',
    },
    {
        title: 'application security devops engineer',
        lang: 'en',
    },
    {
        title: 'application software developer',
        lang: 'en',
    },
    {
        title: 'application specialist /d big data',
        lang: 'en',
    },
    {
        title: 'application support analyst',
        lang: 'en',
    },
    {
        title: 'architect for robot api´s and sdk',
        lang: 'en',
    },
    {
        title: 'asic analog design engineer',
        lang: 'en',
    },
    {
        title: 'associate developer',
        lang: 'en',
    },
    {
        title: 'associate digital architect',
        lang: 'en',
    },
    {
        title: 'associate director data science m / w',
        lang: 'en',
    },
    {
        title: 'automotive software engineer',
        lang: 'en',
    },
    {
        title: 'automotive software process engineer/in',
        lang: 'en',
    },
    {
        title: 'azure cloud consultant',
        lang: 'en',
    },
    {
        title: 'backend developer',
        lang: 'en',
    },
    {
        title: 'backend developer php/java',
        lang: 'en',
    },
    {
        title: 'backend developer ecommerce',
        lang: 'en',
    },
    {
        title: 'backend developer international',
        lang: 'en',
    },
    {
        title: 'backend developer java',
        lang: 'en',
    },
    {
        title: 'backend entwickler java',
        lang: 'en',
    },
    {
        title: 'backend javaentwickler',
        lang: 'en',
    },
    {
        title: 'backend node.js developer',
        lang: 'en',
    },
    {
        title: 'basis software integrator autosar',
        lang: 'en',
    },
    {
        title: 'basissoftwareentwickler steuergeräte',
        lang: 'de',
    },
    {
        title: 'basissoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'berater data driven marketing',
        lang: 'en',
    },
    {
        title: 'berater/consultant erp/lohnbuchhaltung',
        lang: 'de',
    },
    {
        title: 'beraterin cloud data center',
        lang: 'en',
    },
    {
        title: 'berufseinstieg für softwareentwickler',
        lang: 'de',
    },
    {
        title: 'berufsstarter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'betriebssystementwickler linux',
        lang: 'de',
    },
    {
        title: 'bi consultant endtoend',
        lang: 'en',
    },
    {
        title: 'bid manager  data center',
        lang: 'en',
    },
    {
        title: 'big data / splunkconsultants',
        lang: 'en',
    },
    {
        title: 'big data analyst',
        lang: 'en',
    },
    {
        title: 'big data application operator',
        lang: 'en',
    },
    {
        title: 'big data architect',
        lang: 'en',
    },
    {
        title: 'big data devops engineer',
        lang: 'en',
    },
    {
        title: 'big data devops engineer elastic stack',
        lang: 'en',
    },
    {
        title: 'big data engineer im devopsteam',
        lang: 'en',
    },
    {
        title: 'big data engineer messdatenmanagement',
        lang: 'en',
    },
    {
        title: 'big data engineer / scientist',
        lang: 'en',
    },
    {
        title: 'big data entwickler und analyst',
        lang: 'en',
    },
    {
        title: 'big data platform engineer',
        lang: 'en',
    },
    {
        title: 'big data specialist',
        lang: 'en',
    },
    {
        title: 'bigdataarchitect vernetztes fahrzeug',
        lang: 'de',
    },
    {
        title: 'bigdatasecurityanalytiker',
        lang: 'en',
    },
    {
        title: 'biostatistician / data scientist',
        lang: 'en',
    },
    {
        title: 'brand design manager',
        lang: 'en',
    },
    {
        title: 'business / data mining analyst',
        lang: 'en',
    },
    {
        title: 'business analyst cloud technology',
        lang: 'en',
    },
    {
        title: 'business analyst softwareprojekte',
        lang: 'en',
    },
    {
        title: 'business analyst / product owner',
        lang: 'en',
    },
    {
        title: 'business analyst ecommerce /d',
        lang: 'en',
    },
    {
        title: 'business analyst finanzen',
        lang: 'de',
    },
    {
        title: 'business analyst intern',
        lang: 'en',
    },
    {
        title: 'business analyst sap hybris',
        lang: 'en',
    },
    {
        title: 'business analyst web analytics',
        lang: 'en',
    },
    {
        title: 'business analyst/ requirements engineer',
        lang: 'en',
    },
    {
        title: 'business analystin',
        lang: 'en',
    },
    {
        title: 'business architect',
        lang: 'en',
    },
    {
        title: 'business consultant prozessoptimierung',
        lang: 'en',
    },
    {
        title: 'business consultant automotive',
        lang: 'en',
    },
    {
        title: 'business consultant digitalisierung',
        lang: 'en',
    },
    {
        title: 'business consultant financial services',
        lang: 'en',
    },
    {
        title: 'business consultant automotive',
        lang: 'en',
    },
    {
        title: 'business data analyst',
        lang: 'en',
    },
    {
        title: 'business developer',
        lang: 'en',
    },
    {
        title: 'business developer audits',
        lang: 'en',
    },
    {
        title: 'business development intern',
        lang: 'en',
    },
    {
        title: 'business development manager cloud',
        lang: 'en',
    },
    {
        title: 'business intelligence analyst testing',
        lang: 'en',
    },
    {
        title: 'business intelligence consultant',
        lang: 'en',
    },
    {
        title: 'business process consultant',
        lang: 'en',
    },
    {
        title: 'business system analyst',
        lang: 'en',
    },
    {
        title: 'business technology architects',
        lang: 'en',
    },
    {
        title: 'businessanalystin mauterhebung',
        lang: 'en',
    },
    {
        title: 'c# softwareentwickler/in',
        lang: 'en',
    },
    {
        title: 'c# .net softwareentwickler',
        lang: 'en',
    },
    {
        title: 'c# software developer',
        lang: 'en',
    },
    {
        title: 'c# software engineer',
        lang: 'en',
    },
    {
        title: 'c# softwareentwickler',
        lang: 'en',
    },
    {
        title: 'c# softwareentwickler für hmi',
        lang: 'de',
    },
    {
        title: 'c#/.netsoftwareentwickler lifesciences',
        lang: 'en',
    },
    {
        title: 'c++ developer',
        lang: 'en',
    },
    {
        title: 'c++ softwareentwickler',
        lang: 'en',
    },
    {
        title: 'c/c++ softwarearchitekt /entwickler',
        lang: 'de',
    },
    {
        title: 'c/c++ softwareentwickler',
        lang: 'en',
    },
    {
        title: 'c/c++ softwareentwickler /d',
        lang: 'en',
    },
    {
        title: 'cam data specialist',
        lang: 'en',
    },
    {
        title: 'category development manager',
        lang: 'en',
    },
    {
        title: 'certified software manager',
        lang: 'en',
    },
    {
        title: 'channel planner data driven marketing',
        lang: 'en',
    },
    {
        title: 'citrix consultant mit cloud affinität',
        lang: 'en',
    },
    {
        title: 'citrixlösungen senior itconsultant',
        lang: 'en',
    },
    {
        title: 'client software manager',
        lang: 'en',
    },
    {
        title: 'climate data processing engineer',
        lang: 'en',
    },
    {
        title: 'clinical scientist',
        lang: 'en',
    },
    {
        title: 'cloud software devops engineer',
        lang: 'en',
    },
    {
        title: 'cloud architect',
        lang: 'en',
    },
    {
        title: 'cloud consultant',
        lang: 'en',
    },
    {
        title: 'cloud entwickler mit schwerpunkt php',
        lang: 'de',
    },
    {
        title: 'cloud services developer',
        lang: 'en',
    },
    {
        title: 'cloud software and network engineer',
        lang: 'en',
    },
    {
        title: 'cloud solution architect cbc',
        lang: 'en',
    },
    {
        title: 'cloud solutions architect',
        lang: 'en',
    },
    {
        title: 'cloudentwickler devops',
        lang: 'en',
    },
    {
        title: 'compliance consultant',
        lang: 'en',
    },
    {
        title: 'computational scientist',
        lang: 'en',
    },
    {
        title: 'computer analyst',
        lang: 'en',
    },
    {
        title: 'computer network architect',
        lang: 'en',
    },
    {
        title: 'computer systems analyst',
        lang: 'en',
    },
    {
        title: 'consultant',
        lang: 'en',
    },
    {
        title: 'consultant business process management',
        lang: 'en',
    },
    {
        title: 'consultant corporate data protection',
        lang: 'en',
    },
    {
        title: 'consultant data analytics',
        lang: 'en',
    },
    {
        title: 'consultant data center security',
        lang: 'en',
    },
    {
        title: 'consultant digital marketing',
        lang: 'en',
    },
    {
        title: 'consultant erp',
        lang: 'en',
    },
    {
        title: 'consultant finance / accounting',
        lang: 'en',
    },
    {
        title: 'consultant microsoft azure',
        lang: 'en',
    },
    {
        title: 'consultant modern workplace',
        lang: 'en',
    },
    {
        title: 'consultant salesforce crm',
        lang: 'en',
    },
    {
        title: 'consultant controlling accounting',
        lang: 'en',
    },
    {
        title: 'consultant / ethical hacker',
        lang: 'en',
    },
    {
        title: 'consultant amazon marketing',
        lang: 'en',
    },
    {
        title: 'consultant automotive',
        lang: 'en',
    },
    {
        title: 'consultant banken kreditgeschäft',
        lang: 'de',
    },
    {
        title: 'consultant build management',
        lang: 'en',
    },
    {
        title: 'consultant business intelligence',
        lang: 'en',
    },
    {
        title: 'consultant data and analytics / bi',
        lang: 'en',
    },
    {
        title: 'consultant datawarehouse administration',
        lang: 'en',
    },
    {
        title: 'consultant digital workplace',
        lang: 'en',
    },
    {
        title: 'consultant digitalisierung',
        lang: 'en',
    },
    {
        title: 'consultant ecommerce plattformen. .',
        lang: 'en',
    },
    {
        title: 'consultant electrical engineering',
        lang: 'en',
    },
    {
        title: 'consultant for continuous integration',
        lang: 'en',
    },
    {
        title: 'consultant identity access management',
        lang: 'en',
    },
    {
        title: 'consultant im bereich cloud computing',
        lang: 'en',
    },
    {
        title: 'consultant informationstechnologie',
        lang: 'en',
    },
    {
        title: 'consultant ms dynamics nav intern',
        lang: 'en',
    },
    {
        title: 'consultant eventim.inhouse',
        lang: 'en',
    },
    {
        title: 'consultant operative bi analytics',
        lang: 'en',
    },
    {
        title: 'consultant risk and finance',
        lang: 'en',
    },
    {
        title: 'consultant sap authorization',
        lang: 'en',
    },
    {
        title: 'consultant social collaboration',
        lang: 'en',
    },
    {
        title: 'consultant systems engineering',
        lang: 'en',
    },
    {
        title: 'consultant versicherungen / insurance',
        lang: 'en',
    },
    {
        title: 'consultant  citrix',
        lang: 'en',
    },
    {
        title: 'consultant actuarial data science',
        lang: 'en',
    },
    {
        title: 'consultant cyber security',
        lang: 'en',
    },
    {
        title: 'consultant data integration',
        lang: 'en',
    },
    {
        title: 'consultant digital solutions',
        lang: 'en',
    },
    {
        title: 'consultant ibm power systems',
        lang: 'en',
    },
    {
        title: 'consultant microsoft infrastruktur',
        lang: 'en',
    },
    {
        title: 'consultant oracle / informatica',
        lang: 'en',
    },
    {
        title: 'consultant sdl world server',
        lang: 'en',
    },
    {
        title: 'consultant web developer.net',
        lang: 'en',
    },
    {
        title: 'consultants itprojektmanager',
        lang: 'en',
    },
    {
        title: 'controller/financial analyst',
        lang: 'en',
    },
    {
        title: 'controls engineer software',
        lang: 'en',
    },
    {
        title: 'crmanalyst',
        lang: 'en',
    },
    {
        title: 'custom developer',
        lang: 'en',
    },
    {
        title: 'cyber security analyst trainer',
        lang: 'en',
    },
    {
        title: 'data analyst in der produktentwicklung',
        lang: 'en',
    },
    {
        title: 'data analyst iot',
        lang: 'en',
    },
    {
        title: 'data analyst',
        lang: 'en',
    },
    {
        title: 'data analyst research analysis',
        lang: 'en',
    },
    {
        title: 'data analyst service',
        lang: 'en',
    },
    {
        title: 'data analyst customer interface',
        lang: 'en',
    },
    {
        title: 'data analytics consultant',
        lang: 'en',
    },
    {
        title: 'data architect',
        lang: 'en',
    },
    {
        title: 'data center solution designer',
        lang: 'en',
    },
    {
        title: 'data center support specialist',
        lang: 'en',
    },
    {
        title: 'data developer',
        lang: 'en',
    },
    {
        title: 'data developer reporting',
        lang: 'en',
    },
    {
        title: 'data engineer für biprojekte',
        lang: 'de',
    },
    {
        title: 'data engineer für elasticsearch',
        lang: 'de',
    },
    {
        title: 'data engineer',
        lang: 'en',
    },
    {
        title: 'data engineer und data scientist',
        lang: 'en',
    },
    {
        title: 'data entry clerk',
        lang: 'en',
    },
    {
        title: 'data infrastructure lead',
        lang: 'en',
    },
    {
        title: 'data integrity specialists',
        lang: 'en',
    },
    {
        title: 'data management engineer',
        lang: 'en',
    },
    {
        title: 'data manager',
        lang: 'en',
    },
    {
        title: 'data preparation',
        lang: 'en',
    },
    {
        title: 'data product engineer',
        lang: 'en',
    },
    {
        title: 'data protection officer',
        lang: 'en',
    },
    {
        title: 'data quality manager im bereich media',
        lang: 'en',
    },
    {
        title: 'data quality manager',
        lang: 'en',
    },
    {
        title: 'data science consultant',
        lang: 'en',
    },
    {
        title: 'data science',
        lang: 'en',
    },
    {
        title: 'data scientist  advanced analytics',
        lang: 'en',
    },
    {
        title: 'data scientist schaden',
        lang: 'en',
    },
    {
        title: 'data scientist – retail fmcg',
        lang: 'en',
    },
    {
        title: 'data scientist / datenanalysten in',
        lang: 'en',
    },
    {
        title: 'data scientist / engineer',
        lang: 'en',
    },
    {
        title: 'data scientist automotive',
        lang: 'en',
    },
    {
        title: 'data scientist for nlp and text mining',
        lang: 'en',
    },
    {
        title: 'data scientist internet of things',
        lang: 'en',
    },
    {
        title: 'data scientist',
        lang: 'en',
    },
    {
        title: 'data scientist , fulltime, berlin',
        lang: 'en',
    },
    {
        title: 'data warehouse architect',
        lang: 'en',
    },
    {
        title: 'data warehouse consultant / entwickler',
        lang: 'en',
    },
    {
        title: 'data warehouse entwickler',
        lang: 'en',
    },
    {
        title: 'data warehouse und bispezialist',
        lang: 'de',
    },
    {
        title: 'data wrangling',
        lang: 'en',
    },
    {
        title: 'database analyst',
        lang: 'en',
    },
    {
        title: 'database developer',
        lang: 'en',
    },
    {
        title: 'database marketing analyst',
        lang: 'en',
    },
    {
        title: 'datacenter facility service engineer',
        lang: 'en',
    },
    {
        title: 'datawarehouse administrator',
        lang: 'en',
    },
    {
        title: 'datenanalyst / architekt',
        lang: 'en',
    },
    {
        title: 'defensive linux security expert',
        lang: 'en',
    },
    {
        title: 'design engineer',
        lang: 'en',
    },
    {
        title: 'design lead für das projekt wifi@db',
        lang: 'de',
    },
    {
        title: 'design thinking expert',
        lang: 'en',
    },
    {
        title: 'designengineer  excore instrumentation',
        lang: 'en',
    },
    {
        title: 'detail designer',
        lang: 'en',
    },
    {
        title: 'engineer',
        lang: 'en',
    },
    {
        title: 'developer',
        lang: 'en',
    },
    {
        title: 'developer amazon web services aws',
        lang: 'en',
    },
    {
        title: 'developer java/jee',
        lang: 'en',
    },
    {
        title: 'developer workplace cloud',
        lang: 'en',
    },
    {
        title: 'developer application integration',
        lang: 'en',
    },
    {
        title: 'developer / architect azure',
        lang: 'en',
    },
    {
        title: 'developer / consultant .net',
        lang: 'en',
    },
    {
        title: 'developer business intelligence',
        lang: 'en',
    },
    {
        title: 'developer c#',
        lang: 'en',
    },
    {
        title: 'developer continuous deployment',
        lang: 'en',
    },
    {
        title: 'developer microsoft dynamics crm',
        lang: 'en',
    },
    {
        title: 'developer microstrategy dw/bi',
        lang: 'en',
    },
    {
        title: 'developer sap',
        lang: 'en',
    },
    {
        title: 'developer sap abap',
        lang: 'en',
    },
    {
        title: 'developer sap ewm',
        lang: 'en',
    },
    {
        title: 'developer sap fi/ca',
        lang: 'en',
    },
    {
        title: 'developer sap mm',
        lang: 'en',
    },
    {
        title: 'developer sap sd/crm',
        lang: 'en',
    },
    {
        title: 'development chef asia',
        lang: 'en',
    },
    {
        title: 'development chemist composite',
        lang: 'en',
    },
    {
        title: 'development engineer emobility',
        lang: 'en',
    },
    {
        title: 'development operator automotive',
        lang: 'en',
    },
    {
        title: 'devops / system administrator',
        lang: 'en',
    },
    {
        title: 'devops engineer für alm umgebung',
        lang: 'de',
    },
    {
        title: 'devops engineer',
        lang: 'en',
    },
    {
        title: 'devops engineer web applications',
        lang: 'en',
    },
    {
        title: 'devops engineer monitoring',
        lang: 'en',
    },
    {
        title: 'devops engineer automotive',
        lang: 'en',
    },
    {
        title: 'devops entwickler',
        lang: 'en',
    },
    {
        title: 'devops evangelist / automation expert',
        lang: 'en',
    },
    {
        title: 'devops solution architect',
        lang: 'en',
    },
    {
        title: 'devops solution architekt',
        lang: 'en',
    },
    {
        title: 'devops spezialist',
        lang: 'en',
    },
    {
        title: 'digital business consultant',
        lang: 'en',
    },
    {
        title: 'digital business developer',
        lang: 'en',
    },
    {
        title: 'digital pmo analyst',
        lang: 'en',
    },
    {
        title: 'digital signature solution architect',
        lang: 'en',
    },
    {
        title: 'digital solution architect',
        lang: 'en',
    },
    {
        title: 'displayentwickler / designer',
        lang: 'de',
    },
    {
        title: 'distribution analyst',
        lang: 'en',
    },
    {
        title: 'ecommerce developer',
        lang: 'en',
    },
    {
        title: 'etrainer learning development',
        lang: 'en',
    },
    {
        title: 'ecm consultant /d',
        lang: 'en',
    },
    {
        title: 'ecommerce frontend developer',
        lang: 'en',
    },
    {
        title: 'edi consultant in der otto it',
        lang: 'en',
    },
    {
        title: 'edi/vmi consultant',
        lang: 'en',
    },
    {
        title: 'electrochemist fuel cell development',
        lang: 'en',
    },
    {
        title: 'electronic integration architect',
        lang: 'en',
    },
    {
        title: 'electronics design and test engineer',
        lang: 'en',
    },
    {
        title: 'embedded c++ developer erlangen',
        lang: 'en',
    },
    {
        title: 'embedded profinetsoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'embedded software developer',
        lang: 'en',
    },
    {
        title: 'embedded software entwickler ',
        lang: 'en',
    },
    {
        title: 'embedded software entwicklungsingenieur',
        lang: 'de',
    },
    {
        title: 'embedded software ingenieur',
        lang: 'en',
    },
    {
        title: 'embedded software tester',
        lang: 'en',
    },
    {
        title: 'embedded softwareentwickler',
        lang: 'en',
    },
    {
        title: 'embeddedlinuxentwickler',
        lang: 'de',
    },
    {
        title: 'embeddedsoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'engineer software test',
        lang: 'en',
    },
    {
        title: 'enterprise integration architect',
        lang: 'en',
    },
    {
        title: 'entwickler steuerungssoftware',
        lang: 'de',
    },
    {
        title: 'entwickler / developer full stack',
        lang: 'en',
    },
    {
        title: 'entwickler full stack java angular',
        lang: 'en',
    },
    {
        title: 'entwickler für embedded software',
        lang: 'de',
    },
    {
        title: 'entwickler software a',
        lang: 'de',
    },
    {
        title: 'entwickler software c',
        lang: 'de',
    },
    {
        title: 'entwicklungsingenieur embedded software',
        lang: 'de',
    },
    {
        title: 'entwicklungsingenieur/in devops',
        lang: 'de',
    },
    {
        title: 'erfahrene/r cloud softwareentwickler/in',
        lang: 'de',
    },
    {
        title: 'erfahrener berater data analyst',
        lang: 'de',
    },
    {
        title: 'erfahrener consultant bpm',
        lang: 'de',
    },
    {
        title: 'erfahrener ios developer in hamburg',
        lang: 'de',
    },
    {
        title: 'erp enterpriseone solution designer',
        lang: 'en',
    },
    {
        title: 'erp master data analyst m|w',
        lang: 'en',
    },
    {
        title: 'erpconsultant schwerpunkt automotive',
        lang: 'de',
    },
    {
        title: 'executive architect',
        lang: 'en',
    },
    {
        title: 'expert data science',
        lang: 'en',
    },
    {
        title: 'expert software architect mobile',
        lang: 'en',
    },
    {
        title: 'finance service consultant',
        lang: 'en',
    },
    {
        title: 'financial analyst professional',
        lang: 'en',
    },
    {
        title: 'financial data scientist',
        lang: 'en',
    },
    {
        title: 'firmware developer',
        lang: 'en',
    },
    {
        title: 'fleet data administration',
        lang: 'en',
    },
    {
        title: 'front end developer',
        lang: 'en',
    },
    {
        title: 'frontend developer javascript',
        lang: 'en',
    },
    {
        title: 'frontend developer münchen',
        lang: 'de',
    },
    {
        title: 'frontend developer / web entwickler',
        lang: 'en',
    },
    {
        title: 'frontend developer / webentwickler',
        lang: 'en',
    },
    {
        title: 'frontend developer react. .',
        lang: 'en',
    },
    {
        title: 'frontend entwickler / javascript',
        lang: 'en',
    },
    {
        title: 'frontend java entwickler kundenkanäle',
        lang: 'en',
    },
    {
        title: 'frontend web developer',
        lang: 'en',
    },
    {
        title: 'frontendentwickler javascript',
        lang: 'de',
    },
    {
        title: 'frontendentwickler / web developer',
        lang: 'en',
    },
    {
        title: 'full stack developer java',
        lang: 'en',
    },
    {
        title: 'full stack developer',
        lang: 'en',
    },
    {
        title: 'full stack entwickler java',
        lang: 'en',
    },
    {
        title: 'full stack java softwareentwickler /d',
        lang: 'en',
    },
    {
        title: 'full stack javascript entwickler',
        lang: 'en',
    },
    {
        title: 'full stack php developer',
        lang: 'en',
    },
    {
        title: 'full stacksoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'fullstack java entwickler',
        lang: 'en',
    },
    {
        title: 'fullstack javaentwickler',
        lang: 'en',
    },
    {
        title: 'fullstack developer',
        lang: 'en',
    },
    {
        title: 'fullstack developer javascript/java',
        lang: 'en',
    },
    {
        title: 'fullstack web developer energy berlin',
        lang: 'en',
    },
    {
        title: 'fullstacksoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'games developer',
        lang: 'en',
    },
    {
        title: 'gesucht: das ops aus devops',
        lang: 'de',
    },
    {
        title: 'global category manager client software',
        lang: 'en',
    },
    {
        title: 'gmt business development manager',
        lang: 'en',
    },
    {
        title: 'grafikdesigner',
        lang: 'en',
    },
    {
        title: 'graphic designer',
        lang: 'en',
    },
    {
        title: 'gruppenleiter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'gruppenleiter/in software',
        lang: 'de',
    },
    {
        title: 'hardware project device developer',
        lang: 'en',
    },
    {
        title: 'hardwareentwickler medical devices',
        lang: 'en',
    },
    {
        title: 'head of development sap® security',
        lang: 'en',
    },
    {
        title: 'head of global master data governance',
        lang: 'en',
    },
    {
        title: 'head of team software engineering',
        lang: 'en',
    },
    {
        title: 'head of ux/ui  ecommerce / travel',
        lang: 'en',
    },
    {
        title: 'hotline consultant am standort cham',
        lang: 'en',
    },
    {
        title: 'hr development lead',
        lang: 'en',
    },
    {
        title: 'hr systems and data manager',
        lang: 'en',
    },
    {
        title: 'implementation consultant',
        lang: 'en',
    },
    {
        title: 'incident data manager',
        lang: 'en',
    },
    {
        title: 'industrial designer',
        lang: 'en',
    },
    {
        title: 'informatiker für softwareintegration',
        lang: 'de',
    },
    {
        title: 'information security analyst /divers',
        lang: 'en',
    },
    {
        title: 'infrastructure developer',
        lang: 'en',
    },
    {
        title: 'ingenieur / engineer mechanical design',
        lang: 'en',
    },
    {
        title: 'ingenieur embedded software entwicklung',
        lang: 'de',
    },
    {
        title: 'ingenieur application development',
        lang: 'en',
    },
    {
        title: 'inhouse consultant edi/eai projekte',
        lang: 'en',
    },
    {
        title: 'inhouse consultant digitale agenda',
        lang: 'en',
    },
    {
        title: 'inhouse consultant output management',
        lang: 'en',
    },
    {
        title: 'inhouse consultant sap fi/co',
        lang: 'en',
    },
    {
        title: 'inhouse sap entwickler/in abap/ java',
        lang: 'en',
    },
    {
        title: 'inhouse sharepoint consultant',
        lang: 'en',
    },
    {
        title: 'innovation architect / intrapreneur',
        lang: 'en',
    },
    {
        title: 'interaction designer',
        lang: 'en',
    },
    {
        title: 'intern business development d/a/ch',
        lang: 'en',
    },
    {
        title: 'internship development',
        lang: 'en',
    },
    {
        title: 'internship data science indirect taxes',
        lang: 'en',
    },
    {
        title: 'internship warehouse network design',
        lang: 'en',
    },
    {
        title: 'ios developer',
        lang: 'en',
    },
    {
        title: 'ios developer in hamburg',
        lang: 'en',
    },
    {
        title: 'ios developer berlin',
        lang: 'en',
    },
    {
        title: 'ios entwickler / app developer',
        lang: 'en',
    },
    {
        title: 'iot developer / hardware expert',
        lang: 'en',
    },
    {
        title: 'iot developer community evangelist',
        lang: 'en',
    },
    {
        title: 'iot development operations specialist',
        lang: 'en',
    },
    {
        title: 'iot software developer',
        lang: 'en',
    },
    {
        title: 'iot software engineer',
        lang: 'en',
    },
    {
        title: 'it analyst',
        lang: 'en',
    },
    {
        title: 'it architect',
        lang: 'en',
    },
    {
        title: 'it architect java',
        lang: 'en',
    },
    {
        title: 'it architect charging in sindelfingen',
        lang: 'en',
    },
    {
        title: 'it architect infrastructure network',
        lang: 'en',
    },
    {
        title: 'it architect integration',
        lang: 'en',
    },
    {
        title: 'it architect /divers',
        lang: 'en',
    },
    {
        title: 'it architect web and mobile',
        lang: 'en',
    },
    {
        title: 'it architekt data warehouse',
        lang: 'en',
    },
    {
        title: 'it build manager abap java',
        lang: 'en',
    },
    {
        title: 'it business analyst ebusiness',
        lang: 'en',
    },
    {
        title: 'it business analyst kundenbindung',
        lang: 'de',
    },
    {
        title: 'it business analyst sap fsicm',
        lang: 'en',
    },
    {
        title: 'it business analyst',
        lang: 'en',
    },
    {
        title: 'it consultant digitale transformation',
        lang: 'en',
    },
    {
        title: 'it consultant field it vollzeit',
        lang: 'en',
    },
    {
        title: 'it consultant business analyse',
        lang: 'en',
    },
    {
        title: 'it consultant test',
        lang: 'en',
    },
    {
        title: 'it consultant / software developer',
        lang: 'en',
    },
    {
        title: 'it consultant cloud technology',
        lang: 'en',
    },
    {
        title: 'it consultant ecommerce fulfillment',
        lang: 'en',
    },
    {
        title: 'it consultant for digital commerce',
        lang: 'en',
    },
    {
        title: 'it consultant software',
        lang: 'en',
    },
    {
        title: 'it consultant store solutions',
        lang: 'en',
    },
    {
        title: 'it consultant  datacenter generalist',
        lang: 'en',
    },
    {
        title: 'it consultant microsoft cloud services',
        lang: 'en',
    },
    {
        title: 'it consultant sccm',
        lang: 'en',
    },
    {
        title: 'it consultants banken',
        lang: 'en',
    },
    {
        title: 'it data architect / business analyst',
        lang: 'en',
    },
    {
        title: 'it data architect in stuttgart',
        lang: 'en',
    },
    {
        title: 'it infrastructure architect',
        lang: 'en',
    },
    {
        title: 'it inhouse consultant digitale medien',
        lang: 'en',
    },
    {
        title: 'it inhouse consultant sap sd',
        lang: 'en',
    },
    {
        title: 'it logistics consultant',
        lang: 'en',
    },
    {
        title: 'it logistics consultant /d',
        lang: 'en',
    },
    {
        title: 'it manager architecture integration',
        lang: 'en',
    },
    {
        title: 'it projektleiter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'it projektmanager softwaretests',
        lang: 'en',
    },
    {
        title: 'it referent softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'it sales consultant /d',
        lang: 'en',
    },
    {
        title: 'it security analyst',
        lang: 'en',
    },
    {
        title: 'it security service architect',
        lang: 'en',
    },
    {
        title: 'it software engineer compliance',
        lang: 'en',
    },
    {
        title: 'it solution architect home connect',
        lang: 'en',
    },
    {
        title: 'it solution architect',
        lang: 'en',
    },
    {
        title: 'it spezialist cloud architect',
        lang: 'en',
    },
    {
        title: 'it support analyst',
        lang: 'en',
    },
    {
        title: 'it systemarchitekt linux',
        lang: 'en',
    },
    {
        title: 'it systems analyst',
        lang: 'en',
    },
    {
        title: 'it trainee  microsoft consultant',
        lang: 'en',
    },
    {
        title: 'it user consultant',
        lang: 'en',
    },
    {
        title: 'it workflow developer /d berlin',
        lang: 'en',
    },
    {
        title: 'it systemadministrator software',
        lang: 'en',
    },
    {
        title: 'it/devopsspezialist/in',
        lang: 'en',
    },
    {
        title: 'itanalyst / scrummaster',
        lang: 'en',
    },
    {
        title: 'itarchitect amazon web services',
        lang: 'en',
    },
    {
        title: 'itarchitect internet of things',
        lang: 'en',
    },
    {
        title: 'itberater für hrsoftware',
        lang: 'de',
    },
    {
        title: 'itbusiness analyst partnerplattform',
        lang: 'en',
    },
    {
        title: 'itconsultant frankfurt',
        lang: 'en',
    },
    {
        title: 'itconsultant für logistikprojekte',
        lang: 'en',
    },
    {
        title: 'itconsultant itorganisation/konzeption',
        lang: 'en',
    },
    {
        title: 'itconsultant microsoft exchange',
        lang: 'en',
    },
    {
        title: 'itconsultant portal mobile solutions',
        lang: 'en',
    },
    {
        title: 'itconsultant service',
        lang: 'en',
    },
    {
        title: 'itconsultant / analytiker',
        lang: 'en',
    },
    {
        title: 'itconsultant / itberater',
        lang: 'en',
    },
    {
        title: 'itconsultant project management office',
        lang: 'en',
    },
    {
        title: 'itconsultant | modeling consultant w|m',
        lang: 'en',
    },
    {
        title: 'itentwickler standardsoftware',
        lang: 'de',
    },
    {
        title: 'itprojektleiter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'itprojektmanager softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'itprojektmanager/consultant',
        lang: 'en',
    },
    {
        title: 'itservicetechniker softwareverteilung',
        lang: 'de',
    },
    {
        title: 'itsoftware consultant / projektleiter',
        lang: 'en',
    },
    {
        title: 'itsoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'itspezialist agile softwareentwicklung',
        lang: 'en',
    },
    {
        title: 'itspezialist datacenter networking',
        lang: 'en',
    },
    {
        title: 'itsystemadministrator für linux server',
        lang: 'de',
    },
    {
        title: 'ittestmanager / softwaretester',
        lang: 'de',
    },
    {
        title: 'java / javaee softwareentwickler',
        lang: 'en',
    },
    {
        title: 'java spring developer',
        lang: 'en',
    },
    {
        title: 'java architekt für portallösungen',
        lang: 'de',
    },
    {
        title: 'java backendentwickler',
        lang: 'en',
    },
    {
        title: 'java developer /d integrationsplattform',
        lang: 'en',
    },
    {
        title: 'java developer',
        lang: 'en',
    },
    {
        title: 'java developer/consultant',
        lang: 'en',
    },
    {
        title: 'java devops big data',
        lang: 'en',
    },
    {
        title: 'java ee entwickler web frontends',
        lang: 'en',
    },
    {
        title: 'java entwickler  ecommerce',
        lang: 'en',
    },
    {
        title: 'java entwickler  payment in berlin',
        lang: 'en',
    },
    {
        title: 'java entwickler fintech',
        lang: 'en',
    },
    {
        title: 'java entwickler middleware',
        lang: 'en',
    },
    {
        title: 'java entwickler fachliches backend',
        lang: 'de',
    },
    {
        title: 'java entwickler poslösungen',
        lang: 'en',
    },
    {
        title: 'java entwickler / developer',
        lang: 'en',
    },
    {
        title: 'java entwickler backend',
        lang: 'en',
    },
    {
        title: 'java entwickler frontend',
        lang: 'en',
    },
    {
        title: 'java entwickler fullstack',
        lang: 'en',
    },
    {
        title: 'java entwickler für `digital retail`',
        lang: 'de',
    },
    {
        title: 'java entwickler iam',
        lang: 'en',
    },
    {
        title: 'java entwickler security solutions',
        lang: 'en',
    },
    {
        title: 'java entwickler',
        lang: 'en',
    },
    {
        title: 'java entwickler commerce solutions',
        lang: 'en',
    },
    {
        title: 'java entwickler im bereich ecommerce',
        lang: 'de',
    },
    {
        title: 'java entwickler ',
        lang: 'en',
    },
    {
        title: 'java fullstack entwickler/in',
        lang: 'en',
    },
    {
        title: 'java fullstack softwareentwickler',
        lang: 'en',
    },
    {
        title: 'java se / spring developer',
        lang: 'en',
    },
    {
        title: 'java software architekt',
        lang: 'en',
    },
    {
        title: 'java software developer cloud',
        lang: 'en',
    },
    {
        title: 'java software developer/in',
        lang: 'en',
    },
    {
        title: 'java softwareentwickler research',
        lang: 'en',
    },
    {
        title: 'java softwareentwickler ',
        lang: 'en',
    },
    {
        title: 'java und/oder abap software entwickler',
        lang: 'de',
    },
    {
        title: 'javaanwendungsentwickler',
        lang: 'en',
    },
    {
        title: 'java developer crm',
        lang: 'en',
    },
    {
        title: 'javaentwickler backend',
        lang: 'en',
    },
    {
        title: 'javaentwickler cloudlösungen / iot',
        lang: 'en',
    },
    {
        title: 'javaentwickler im softwaredevelopment',
        lang: 'en',
    },
    {
        title: 'javaentwickler mit fernweh',
        lang: 'de',
    },
    {
        title: 'javaentwickler public web',
        lang: 'de',
    },
    {
        title: 'javaentwickler telecoms media',
        lang: 'en',
    },
    {
        title: 'javaentwickler webanwendungen',
        lang: 'de',
    },
    {
        title: 'javaentwickler / programmierer',
        lang: 'en',
    },
    {
        title: 'javaentwickler frontend',
        lang: 'en',
    },
    {
        title: 'javaentwickler für big data',
        lang: 'de',
    },
    {
        title: 'javaprogrammierer',
        lang: 'en',
    },
    {
        title: 'javasoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'java/jee backend developer',
        lang: 'en',
    },
    {
        title: 'java/jee entwickler',
        lang: 'en',
    },
    {
        title: 'java/spring boot entwickler  köln',
        lang: 'en',
    },
    {
        title: 'javascript entwickler middleware',
        lang: 'en',
    },
    {
        title: 'javascript fullstack entwickler',
        lang: 'en',
    },
    {
        title: 'javascript softwareentwickler',
        lang: 'en',
    },
    {
        title: 'javascript web developer in berlin',
        lang: 'en',
    },
    {
        title: 'javascript web entwickler',
        lang: 'en',
    },
    {
        title: 'javascriptentwicklerin',
        lang: 'de',
    },
    {
        title: 'junior analyst customer data',
        lang: 'en',
    },
    {
        title: 'junior analyst/ data model architect',
        lang: 'en',
    },
    {
        title: 'junior application developer',
        lang: 'en',
    },
    {
        title: 'junior backend developer',
        lang: 'en',
    },
    {
        title: 'junior backend node.js developer',
        lang: 'en',
    },
    {
        title: 'junior business software engineer',
        lang: 'en',
    },
    {
        title: 'junior consultant',
        lang: 'en',
    },
    {
        title: 'junior consultant big data',
        lang: 'en',
    },
    {
        title: 'junior consultant business analytics',
        lang: 'en',
    },
    {
        title: 'junior consultant it',
        lang: 'en',
    },
    {
        title: 'junior consultant project management',
        lang: 'en',
    },
    {
        title: 'junior consultant sap bw',
        lang: 'en',
    },
    {
        title: 'junior consultant sap fi',
        lang: 'en',
    },
    {
        title: 'junior consultant telecommunication',
        lang: 'en',
    },
    {
        title: 'junior consultant web optimization',
        lang: 'en',
    },
    {
        title: 'junior consultant java',
        lang: 'en',
    },
    {
        title: 'junior consultant sap ui/ux entwickler',
        lang: 'en',
    },
    {
        title: 'junior consultant sap test management',
        lang: 'en',
    },
    {
        title: 'junior crm analyst',
        lang: 'en',
    },
    {
        title: 'junior customer data analyst',
        lang: 'en',
    },
    {
        title: 'junior data analyst',
        lang: 'en',
    },
    {
        title: 'junior data analyst/data scientist',
        lang: 'en',
    },
    {
        title: 'junior data engineer für hadoopcluster',
        lang: 'en',
    },
    {
        title: 'junior data engineer',
        lang: 'en',
    },
    {
        title: 'junior data scientist reporting',
        lang: 'en',
    },
    {
        title: 'junior data scientist',
        lang: 'en',
    },
    {
        title: 'junior data warehouse / etl analyst',
        lang: 'en',
    },
    {
        title: 'junior database administrator',
        lang: 'en',
    },
    {
        title: 'junior database developer',
        lang: 'en',
    },
    {
        title: 'junior datawarehouse engineer',
        lang: 'en',
    },
    {
        title: 'junior devops engineer',
        lang: 'en',
    },
    {
        title: 'junior devops engineer in hamburg',
        lang: 'en',
    },
    {
        title: 'junior digital business developer',
        lang: 'en',
    },
    {
        title: 'junior entwickler java',
        lang: 'en',
    },
    {
        title: 'junior frontend developer',
        lang: 'en',
    },
    {
        title: 'junior full stack web developer',
        lang: 'en',
    },
    {
        title: 'junior it architect digital integration',
        lang: 'en',
    },
    {
        title: 'junior it business analyst',
        lang: 'en',
    },
    {
        title: 'junior itadministrator linux',
        lang: 'en',
    },
    {
        title: 'junior itconsultant / business analyst',
        lang: 'en',
    },
    {
        title: 'junior itconsultant inhouse',
        lang: 'en',
    },
    {
        title: 'junior java developer',
        lang: 'en',
    },
    {
        title: 'junior java entwickler ecommerce',
        lang: 'en',
    },
    {
        title: 'junior java entwickler insurance',
        lang: 'en',
    },
    {
        title: 'junior java entwickler /divers',
        lang: 'en',
    },
    {
        title: 'junior java entwickler',
        lang: 'en',
    },
    {
        title: 'junior java entwickler ',
        lang: 'en',
    },
    {
        title: 'junior java nlp developers',
        lang: 'en',
    },
    {
        title: 'junior java software developer',
        lang: 'en',
    },
    {
        title: 'junior javabackendentwickler',
        lang: 'en',
    },
    {
        title: 'junior java/jee entwickler/in',
        lang: 'en',
    },
    {
        title: 'junior manager data performance',
        lang: 'en',
    },
    {
        title: 'junior php entwickler',
        lang: 'en',
    },
    {
        title: 'junior phpentwickler',
        lang: 'en',
    },
    {
        title: 'junior presales consultant',
        lang: 'en',
    },
    {
        title: 'junior risk analyst',
        lang: 'en',
    },
    {
        title: 'junior sales specialist software / it',
        lang: 'en',
    },
    {
        title: 'junior sap developer abap',
        lang: 'en',
    },
    {
        title: 'junior software ingenieure gn*',
        lang: 'en',
    },
    {
        title: 'junior software developer  energy',
        lang: 'en',
    },
    {
        title: 'junior software entwickler financials',
        lang: 'en',
    },
    {
        title: 'junior software project manager',
        lang: 'en',
    },
    {
        title: 'junior software release manager',
        lang: 'en',
    },
    {
        title: 'junior software tester',
        lang: 'en',
    },
    {
        title: 'junior softwareingenieur',
        lang: 'de',
    },
    {
        title: 'junior softwareentwickler c/c++',
        lang: 'en',
    },
    {
        title: 'junior softwareentwickler java',
        lang: 'en',
    },
    {
        title: 'junior softwareentwickler java/jee',
        lang: 'en',
    },
    {
        title: 'junior softwareentwickler java oder c#',
        lang: 'en',
    },
    {
        title: 'junior softwaretester / automotive',
        lang: 'en',
    },
    {
        title: 'junior softwaretester',
        lang: 'en',
    },
    {
        title: 'junior solution consultant',
        lang: 'en',
    },
    {
        title: 'junior web developer',
        lang: 'en',
    },
    {
        title: 'junior web operations / devops engineer',
        lang: 'en',
    },
    {
        title: 'junior ux researcher ui designer',
        lang: 'en',
    },
    {
        title: 'juniorit consultant',
        lang: 'en',
    },
    {
        title: 'juniorjavaentwickler',
        lang: 'en',
    },
    {
        title: 'juniorsoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'kommunikationsdesigner/in',
        lang: 'en',
    },
    {
        title: 'lawyer data protection itlaw',
        lang: 'en',
    },
    {
        title: 'lead architect connected car',
        lang: 'en',
    },
    {
        title: 'lead architect procurementsoftware',
        lang: 'en',
    },
    {
        title: 'lead backend developer',
        lang: 'en',
    },
    {
        title: 'lead consultant advanced analytics',
        lang: 'en',
    },
    {
        title: 'lead consultant sap bo/analytics',
        lang: 'en',
    },
    {
        title: 'lead consultant self service bi',
        lang: 'en',
    },
    {
        title: 'lead consultant service management',
        lang: 'en',
    },
    {
        title: 'lead design engineer sbr',
        lang: 'en',
    },
    {
        title: 'lead developer / senior entwickler',
        lang: 'en',
    },
    {
        title: 'lead developer microsoft azure',
        lang: 'en',
    },
    {
        title: 'lead developer php',
        lang: 'en',
    },
    {
        title: 'lead iot platform developer',
        lang: 'en',
    },
    {
        title: 'lead software solution architect',
        lang: 'en',
    },
    {
        title: 'learning architect',
        lang: 'en',
    },
    {
        title: 'leiter data analytics ki',
        lang: 'en',
    },
    {
        title: 'leiter it / softwareentwickler',
        lang: 'de',
    },
    {
        title: 'leiter softwarentwicklung',
        lang: 'de',
    },
    {
        title: 'leiter user interface design',
        lang: 'de',
    },
    {
        title: 'lenker software projectmanagement',
        lang: 'en',
    },
    {
        title: 'linux administrator',
        lang: 'en',
    },
    {
        title: 'linux administrator devops',
        lang: 'en',
    },
    {
        title: 'linux engineer',
        lang: 'en',
    },
    {
        title: 'linux systemadministrator',
        lang: 'en',
    },
    {
        title: 'linux systemadministrator devops',
        lang: 'en',
    },
    {
        title: 'linuxentwickler c/c++',
        lang: 'en',
    },
    {
        title: 'linuxspezialist',
        lang: 'en',
    },
    {
        title: 'logistic consultant',
        lang: 'en',
    },
    {
        title: 'logistics consultant intralogistics',
        lang: 'en',
    },
    {
        title: 'logistik network designer',
        lang: 'en',
    },
    {
        title: 'logistiksoftwareentwickler /d',
        lang: 'en',
    },
    {
        title: 'manager data governance data quality',
        lang: 'en',
    },
    {
        title: 'manager softwaretest',
        lang: 'de',
    },
    {
        title: 'manager data analytics ki',
        lang: 'en',
    },
    {
        title: 'manager design changes rd',
        lang: 'en',
    },
    {
        title: 'manager supplier development',
        lang: 'en',
    },
    {
        title: 'manager w|m press and analyst relations',
        lang: 'en',
    },
    {
        title: 'manager/ teamlead sales development',
        lang: 'en',
    },
    {
        title: 'managing business consultant',
        lang: 'en',
    },
    {
        title: 'managing consultant data warehouse',
        lang: 'en',
    },
    {
        title: 'managing consultant it management',
        lang: 'en',
    },
    {
        title: 'managing itconsultant plm',
        lang: 'en',
    },
    {
        title: 'master data coordinator',
        lang: 'en',
    },
    {
        title: 'master data expert  meerbusch',
        lang: 'en',
    },
    {
        title: 'master data manager',
        lang: 'en',
    },
    {
        title: 'master data specialist',
        lang: 'en',
    },
    {
        title: 'mes consultant automotive',
        lang: 'en',
    },
    {
        title: 'mes consultant electronics',
        lang: 'en',
    },
    {
        title: 'mes consultant quality engineering',
        lang: 'en',
    },
    {
        title: 'mes consultant shop floor integration',
        lang: 'en',
    },
    {
        title: 'mes consultant',
        lang: 'en',
    },
    {
        title: 'mitarbeiter devops',
        lang: 'de',
    },
    {
        title: 'mitarbeiter softwaretests und support',
        lang: 'de',
    },
    {
        title: 'mitarbeiter softwarepaketierung',
        lang: 'de',
    },
    {
        title: 'mixedsignal designer',
        lang: 'en',
    },
    {
        title: 'mobile developer android',
        lang: 'en',
    },
    {
        title: 'mobile developer android / ios /d',
        lang: 'en',
    },
    {
        title: 'mobile developer für ios und android',
        lang: 'en',
    },
    {
        title: 'mobile softwareentwickler android',
        lang: 'en',
    },
    {
        title: 'mobile ui/ux design',
        lang: 'en',
    },
    {
        title: 'module architect hmi',
        lang: 'en',
    },
    {
        title: 'ms dynamics nav developer',
        lang: 'en',
    },
    {
        title: 'msbiztalk application developer /d',
        lang: 'en',
    },
    {
        title: 'msp program consultant',
        lang: 'en',
    },
    {
        title: 'network analyst',
        lang: 'en',
    },
    {
        title: 'network architect',
        lang: 'en',
    },
    {
        title: 'node.js developer',
        lang: 'en',
    },
    {
        title: 'openshift / paas / devops engineer',
        lang: 'en',
    },
    {
        title: 'oracle business consultant',
        lang: 'en',
    },
    {
        title: 'packaging consultant',
        lang: 'en',
    },
    {
        title: 'partnermanager software',
        lang: 'de',
    },
    {
        title: 'pega senior system architect',
        lang: 'en',
    },
    {
        title: 'php backend developer/in',
        lang: 'en',
    },
    {
        title: 'php developer',
        lang: 'en',
    },
    {
        title: 'php entwickler und berater',
        lang: 'de',
    },
    {
        title: 'php entwickler kochboxenshop',
        lang: 'en',
    },
    {
        title: 'php entwickler/developer',
        lang: 'en',
    },
    {
        title: 'php developer',
        lang: 'en',
    },
    {
        title: 'php software engineer',
        lang: 'en',
    },
    {
        title: 'php developer precision team',
        lang: 'en',
    },
    {
        title: 'php software engineer – backend',
        lang: 'en',
    },
    {
        title: 'php softwareentwickler',
        lang: 'en',
    },
    {
        title: 'php web developer',
        lang: 'en',
    },
    {
        title: 'senior php developer',
        lang: 'en',
    },
    {
        title: 'php webentwickler',
        lang: 'de',
    },
    {
        title: 'phpentwickler',
        lang: 'de',
    },
    {
        title: 'phpentwickler in direktvermittlung',
        lang: 'de',
    },
    {
        title: 'phpentwickler google partner anbindung',
        lang: 'de',
    },
    {
        title: 'phpentwickler / developer',
        lang: 'en',
    },
    {
        title: 'phpentwickler ecommerce',
        lang: 'en',
    },
    {
        title: 'phpentwickler/in',
        lang: 'de',
    },
    {
        title: 'phpsoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'phpwebentwickler',
        lang: 'de',
    },
    {
        title: 'praktikant interior design',
        lang: 'en',
    },
    {
        title: 'praktikant / werkstudent data science',
        lang: 'en',
    },
    {
        title: 'praktikant /d business development',
        lang: 'en',
    },
    {
        title: 'praktikant/in datawarehouse',
        lang: 'en',
    },
    {
        title: 'praktikant/in im bereich can software',
        lang: 'de',
    },
    {
        title: 'praktikant/in im softwareeinkauf',
        lang: 'de',
    },
    {
        title: 'praktikant/in software development java',
        lang: 'en',
    },
    {
        title: 'praktikant/in softwareprogrammierung',
        lang: 'en',
    },
    {
        title: 'praktikum im bereich data science',
        lang: 'de',
    },
    {
        title: 'praktikum in der softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'praktikum industriedesign/ architektur',
        lang: 'en',
    },
    {
        title: 'praktikum product development',
        lang: 'en',
    },
    {
        title: 'praktikum user experience designer',
        lang: 'en',
    },
    {
        title: 'premaster/in im bereich datawarehouse',
        lang: 'de',
    },
    {
        title: 'presales consultant finance',
        lang: 'en',
    },
    {
        title: 'principal solution architect',
        lang: 'en',
    },
    {
        title: 'principle technical solutions architect',
        lang: 'en',
    },
    {
        title: 'process data manager',
        lang: 'en',
    },
    {
        title: 'process manager master data governance',
        lang: 'en',
    },
    {
        title: 'product data analyst',
        lang: 'en',
    },
    {
        title: 'product designer',
        lang: 'en',
    },
    {
        title: 'product development manager',
        lang: 'en',
    },
    {
        title: 'product development manager men active',
        lang: 'en',
    },
    {
        title: 'product manager software development',
        lang: 'en',
    },
    {
        title: 'product owner embedded software iot',
        lang: 'en',
    },
    {
        title: 'product owner data science',
        lang: 'en',
    },
    {
        title: 'product owner backend development',
        lang: 'en',
    },
    {
        title: 'product owner infotainment software',
        lang: 'en',
    },
    {
        title: 'product owner solution architect cbc',
        lang: 'en',
    },
    {
        title: 'produktentwickler industrial big data',
        lang: 'en',
    },
    {
        title: 'produktmanager software systeme',
        lang: 'en',
    },
    {
        title: 'produktmanager applikationssoftware',
        lang: 'en',
    },
    {
        title: 'produktmanager für finanzsoftware',
        lang: 'de',
    },
    {
        title: 'professional web developer java',
        lang: 'en',
    },
    {
        title: 'programmer analyst',
        lang: 'en',
    },
    {
        title: 'programmierer / web developer',
        lang: 'en',
    },
    {
        title: 'programmierer für spielesoftware',
        lang: 'de',
    },
    {
        title: 'project engineer big data solutions',
        lang: 'en',
    },
    {
        title: 'projektsystemdesigner/in für emobility',
        lang: 'de',
    },
    {
        title: 'projektleiter embedded software',
        lang: 'en',
    },
    {
        title: 'projektleiter java / java ee',
        lang: 'en',
    },
    {
        title: 'projektleiter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'projektleiter software',
        lang: 'de',
    },
    {
        title: 'projektleiter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'projektleiter/in business development',
        lang: 'en',
    },
    {
        title: 'projektleiter/in software engineering',
        lang: 'en',
    },
    {
        title: 'projektleiter/in softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'projektmanager data analytics',
        lang: 'en',
    },
    {
        title: 'projektmanager software / steuergeräte',
        lang: 'de',
    },
    {
        title: 'projektmanager it softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'projektmanager/in softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'prozessmanager softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'qa ux manager',
        lang: 'en',
    },
    {
        title: 'qa analyst',
        lang: 'en',
    },
    {
        title: 'qa engineer/ schwerpunkt linux',
        lang: 'de',
    },
    {
        title: 'qualitätsmanager  embedded software',
        lang: 'en',
    },
    {
        title: 'qualitätsmanager software',
        lang: 'de',
    },
    {
        title: 'qualitätsmanager softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'quantitative analyst',
        lang: 'en',
    },
    {
        title: 'quantitative analyst / developer',
        lang: 'en',
    },
    {
        title: 'quereinsteiger softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'rd scientist/ research engineer',
        lang: 'de',
    },
    {
        title: 'rd software engineer',
        lang: 'en',
    },
    {
        title: 'referent analyse big data betrieb',
        lang: 'en',
    },
    {
        title: 'referent kostenanalyse software',
        lang: 'en',
    },
    {
        title: 'release manager softwarekonfiguration',
        lang: 'en',
    },
    {
        title: 'reliability engineer dwh/big data',
        lang: 'en',
    },
    {
        title: 'requirementsingenieur software',
        lang: 'en',
    },
    {
        title: 'research scientist',
        lang: 'en',
    },
    {
        title: 'risk analyst',
        lang: 'en',
    },
    {
        title: 'risk consultant',
        lang: 'en',
    },
    {
        title: 'safety software architekt/in',
        lang: 'en',
    },
    {
        title: 'sales consultant schwerpunkt ispo',
        lang: 'de',
    },
    {
        title: 'sales consultant document intelligence',
        lang: 'en',
    },
    {
        title: 'sales consultant it',
        lang: 'en',
    },
    {
        title: 'sales data analyst',
        lang: 'en',
    },
    {
        title: 'sales development analyst',
        lang: 'en',
    },
    {
        title: 'sales development representative',
        lang: 'en',
    },
    {
        title: 'sales manager hard / software',
        lang: 'en',
    },
    {
        title: 'sales manager für softwarelösungen,',
        lang: 'de',
    },
    {
        title: 'sales manager in der softwarebranche',
        lang: 'de',
    },
    {
        title: 'sales manager für softwarelösungen',
        lang: 'de',
    },
    {
        title: 'sales representative softwarevertrieb',
        lang: 'en',
    },
    {
        title: 'sales systems and data manager dach',
        lang: 'en',
    },
    {
        title: 'salesforce developer /d',
        lang: 'en',
    },
    {
        title: 'sap abap developer',
        lang: 'en',
    },
    {
        title: 'sap abap/abap oo entwickler/ developer',
        lang: 'en',
    },
    {
        title: 'sap authorisation consultant',
        lang: 'en',
    },
    {
        title: 'sap basis consultant',
        lang: 'en',
    },
    {
        title: 'sap bi/bw consultant',
        lang: 'en',
    },
    {
        title: 'sap business bydesign entwickler',
        lang: 'en',
    },
    {
        title: 'sap bw architect',
        lang: 'en',
    },
    {
        title: 'sap consultant crm migration',
        lang: 'en',
    },
    {
        title: 'sap consultant hcm personalmanagement',
        lang: 'en',
    },
    {
        title: 'sap consultant im bereich security',
        lang: 'en',
    },
    {
        title: 'sap consultant it strategie',
        lang: 'en',
    },
    {
        title: 'sap developer',
        lang: 'en',
    },
    {
        title: 'sap developer / sap entwickler',
        lang: 'en',
    },
    {
        title: 'sap developer im bereich ams inhouse',
        lang: 'de',
    },
    {
        title: 'sap fi co inhouse consultant',
        lang: 'en',
    },
    {
        title: 'sap fi developer',
        lang: 'en',
    },
    {
        title: 'sap frontend developer',
        lang: 'en',
    },
    {
        title: 'sap hybris commerce developer',
        lang: 'en',
    },
    {
        title: 'sap inhouse consultant mm',
        lang: 'en',
    },
    {
        title: 'sap inhouse consultant bw / bi',
        lang: 'en',
    },
    {
        title: 'sap integration architect',
        lang: 'en',
    },
    {
        title: 'sap junior entwickler / developer',
        lang: 'en',
    },
    {
        title: 'sap portal consultant',
        lang: 'en',
    },
    {
        title: 'sap sd mm consultant',
        lang: 'en',
    },
    {
        title: 'sap senior developer digital business',
        lang: 'en',
    },
    {
        title: 'sap senior inhouse consultant',
        lang: 'en',
    },
    {
        title: 'sap solution consultant',
        lang: 'en',
    },
    {
        title: 'sap technical consultant',
        lang: 'en',
    },
    {
        title: 'sap ui 5 / fiori developer',
        lang: 'en',
    },
    {
        title: 'sapconsultant',
        lang: 'en',
    },
    {
        title: 'sap/abap application developer',
        lang: 'en',
    },
    {
        title: 'scientist',
        lang: 'en',
    },
    {
        title: 'scrum master / software architekt',
        lang: 'de',
    },
    {
        title: 'scrum master automotive software',
        lang: 'en',
    },
    {
        title: 'security analyst',
        lang: 'en',
    },
    {
        title: 'security architect inhouse',
        lang: 'en',
    },
    {
        title: 'security architect',
        lang: 'en',
    },
    {
        title: 'senior aem developer',
        lang: 'en',
    },
    {
        title: 'senior agile java developer otto.de',
        lang: 'en',
    },
    {
        title: 'senior analyst digitale strategie',
        lang: 'en',
    },
    {
        title: 'senior analyst financial reporting',
        lang: 'en',
    },
    {
        title: 'senior android developer',
        lang: 'en',
    },
    {
        title: 'senior application developer',
        lang: 'en',
    },
    {
        title: 'senior application developer team lead',
        lang: 'en',
    },
    {
        title: 'senior back end developer',
        lang: 'en',
    },
    {
        title: 'senior backend developer php',
        lang: 'en',
    },
    {
        title: 'senior big data engineer',
        lang: 'en',
    },
    {
        title: 'senior business analyst fi/co/ps',
        lang: 'en',
    },
    {
        title: 'senior business analyst selfservice bi',
        lang: 'en',
    },
    {
        title: 'senior business analyst',
        lang: 'en',
    },
    {
        title: 'senior business consultant accounting',
        lang: 'en',
    },
    {
        title: 'senior business consultant mdm, pim',
        lang: 'en',
    },
    {
        title: 'senior business consultant banking',
        lang: 'en',
    },
    {
        title: 'senior business development engineer',
        lang: 'en',
    },
    {
        title: 'senior business intelligence analyst',
        lang: 'en',
    },
    {
        title: 'senior business/data analyst',
        lang: 'en',
    },
    {
        title: 'senior clinical research scientist',
        lang: 'en',
    },
    {
        title: 'senior cloud architect erlangen',
        lang: 'en',
    },
    {
        title: 'senior cloud architect',
        lang: 'en',
    },
    {
        title: 'senior cloud developer java ee',
        lang: 'en',
    },
    {
        title: 'senior computer vision developer',
        lang: 'en',
    },
    {
        title: 'senior consultant advanced analytics',
        lang: 'en',
    },
    {
        title: 'senior consultant anwendungsbetreuung',
        lang: 'de',
    },
    {
        title: 'senior consultant aufsichtsrecht',
        lang: 'de',
    },
    {
        title: 'senior consultant bi microstrategy',
        lang: 'en',
    },
    {
        title: 'senior consultant core banking',
        lang: 'en',
    },
    {
        title: 'senior consultant crm analytics',
        lang: 'en',
    },
    {
        title: 'senior consultant cyber forensik',
        lang: 'en',
    },
    {
        title: 'senior consultant egovernment',
        lang: 'en',
    },
    {
        title: 'senior consultant it finance',
        lang: 'en',
    },
    {
        title: 'senior consultant itarchitektur',
        lang: 'en',
    },
    {
        title: 'senior consultant itservice management',
        lang: 'en',
    },
    {
        title: 'senior consultant konsumgüter',
        lang: 'en',
    },
    {
        title: 'senior consultant meldewesen',
        lang: 'en',
    },
    {
        title: 'senior consultant mobile applications',
        lang: 'en',
    },
    {
        title: 'senior consultant salesforce crm',
        lang: 'en',
    },
    {
        title: 'senior consultant sap bo/analytics',
        lang: 'en',
    },
    {
        title: 'senior consultant sap bw/hana',
        lang: 'en',
    },
    {
        title: 'senior consultant sap controlling',
        lang: 'en',
    },
    {
        title: 'senior consultant sap hybris marketing',
        lang: 'en',
    },
    {
        title: 'senior consultant sap innovations',
        lang: 'en',
    },
    {
        title: 'senior consultant sap sd und le',
        lang: 'en',
    },
    {
        title: 'senior consultant self service bi',
        lang: 'en',
    },
    {
        title: 'senior consultant telekommunikation',
        lang: 'en',
    },
    {
        title: 'senior consultant manager insurance',
        lang: 'en',
    },
    {
        title: 'senior consultant /d',
        lang: 'en',
    },
    {
        title: 'senior consultant android development',
        lang: 'en',
    },
    {
        title: 'senior consultant big data analytics',
        lang: 'en',
    },
    {
        title: 'senior consultant business technology',
        lang: 'en',
    },
    {
        title: 'senior consultant core banking',
        lang: 'en',
    },
    {
        title: 'senior consultant datenschutz privacy',
        lang: 'en',
    },
    {
        title: 'senior consultant devops',
        lang: 'en',
    },
    {
        title: 'senior consultant energiewirtschaft',
        lang: 'de',
    },
    {
        title: 'senior consultant financial markets',
        lang: 'en',
    },
    {
        title: 'senior consultant financial services',
        lang: 'en',
    },
    {
        title: 'senior consultant integration',
        lang: 'en',
    },
    {
        title: 'senior consultant ios development',
        lang: 'en',
    },
    {
        title: 'senior consultant it',
        lang: 'en',
    },
    {
        title: 'senior consultant java ee',
        lang: 'en',
    },
    {
        title: 'senior consultant microsoft',
        lang: 'en',
    },
    {
        title: 'senior consultant mobile development',
        lang: 'en',
    },
    {
        title: 'senior consultant sap sd/le',
        lang: 'en',
    },
    {
        title: 'senior consultant sap ui/ux entwickler',
        lang: 'en',
    },
    {
        title: 'senior consultant testautomatisierung',
        lang: 'en',
    },
    {
        title: 'senior consultant digital construction',
        lang: 'en',
    },
    {
        title: 'senior consultant tax compliance',
        lang: 'en',
    },
    {
        title: 'senior consultant zahlungsverkehr',
        lang: 'de',
    },
    {
        title: 'senior consultant/ berater',
        lang: 'en',
    },
    {
        title: 'senior consultants bi / cognos',
        lang: 'en',
    },
    {
        title: 'senior customer data scientist',
        lang: 'en',
    },
    {
        title: 'senior data analyst technische daten',
        lang: 'de',
    },
    {
        title: 'senior data analyst/data scientist',
        lang: 'en',
    },
    {
        title: 'senior data architect',
        lang: 'en',
    },
    {
        title: 'senior data scientist carit analytics',
        lang: 'en',
    },
    {
        title: 'senior data stream engineer',
        lang: 'en',
    },
    {
        title: 'senior database administrator',
        lang: 'en',
    },
    {
        title: 'senior developer erp business suite',
        lang: 'en',
    },
    {
        title: 'senior developer integration services',
        lang: 'en',
    },
    {
        title: 'senior developer it service management',
        lang: 'en',
    },
    {
        title: 'senior developer java/jee',
        lang: 'en',
    },
    {
        title: 'senior developer microsoft . net',
        lang: 'en',
    },
    {
        title: 'senior developer microsoft sharepoint',
        lang: 'en',
    },
    {
        title: 'senior developer',
        lang: 'en',
    },
    {
        title: 'senior developer java',
        lang: 'en',
    },
    {
        title: 'senior developer java/jee',
        lang: 'en',
    },
    {
        title: 'senior devops engineer',
        lang: 'en',
    },
    {
        title: 'senior digital applications developer',
        lang: 'en',
    },
    {
        title: 'senior digital architect',
        lang: 'en',
    },
    {
        title: 'senior embedded software engineer hmi',
        lang: 'en',
    },
    {
        title: 'senior enterprise business architect',
        lang: 'en',
    },
    {
        title: 'senior entwickler java',
        lang: 'en',
    },
    {
        title: 'senior expert cyber security analyst',
        lang: 'en',
    },
    {
        title: 'senior expert it security architect',
        lang: 'en',
    },
    {
        title: 'senior experte data analytics',
        lang: 'en',
    },
    {
        title: 'senior fpa analyst  troisdorf',
        lang: 'en',
    },
    {
        title: 'senior front end developer',
        lang: 'en',
    },
    {
        title: 'senior frontend developer',
        lang: 'en',
    },
    {
        title: 'senior frontend web developer',
        lang: 'en',
    },
    {
        title: 'senior full stack entwickler java',
        lang: 'en',
    },
    {
        title: 'senior fullstack .net developer',
        lang: 'en',
    },
    {
        title: 'senior fullstack web developer',
        lang: 'en',
    },
    {
        title: 'senior hardware designer',
        lang: 'en',
    },
    {
        title: 'senior ingenieur software prozesse',
        lang: 'de',
    },
    {
        title: 'senior inhouse consultant',
        lang: 'en',
    },
    {
        title: 'senior iot software developer',
        lang: 'en',
    },
    {
        title: 'senior it architect',
        lang: 'en',
    },
    {
        title: 'senior it architect schwerpunkt plm',
        lang: 'de',
    },
    {
        title: 'senior it consultant test management',
        lang: 'en',
    },
    {
        title: 'senior it procurement analyst software',
        lang: 'en',
    },
    {
        title: 'senior itconsultant life science',
        lang: 'en',
    },
    {
        title: 'senior itconsultant',
        lang: 'en',
    },
    {
        title: 'senior itinfrastructure architect',
        lang: 'en',
    },
    {
        title: 'senior itqa analyst in hamburg',
        lang: 'en',
    },
    {
        title: 'senior java developer operations /d',
        lang: 'en',
    },
    {
        title: 'senior java developer',
        lang: 'en',
    },
    {
        title: 'senior java developer/in für pim',
        lang: 'en',
    },
    {
        title: 'senior java ee developer',
        lang: 'en',
    },
    {
        title: 'senior java entwickler framework',
        lang: 'en',
    },
    {
        title: 'senior java entwickler inhouse',
        lang: 'en',
    },
    {
        title: 'senior java software developer',
        lang: 'en',
    },
    {
        title: 'senior jee developer',
        lang: 'en',
    },
    {
        title: 'senior manager software engineering c#',
        lang: 'en',
    },
    {
        title: 'senior manager / java developer',
        lang: 'en',
    },
    {
        title: 'senior network architect',
        lang: 'en',
    },
    {
        title: 'senior php backend developer',
        lang: 'en',
    },
    {
        title: 'senior php entwickler',
        lang: 'de',
    },
    {
        title: 'senior phpentwickler/in',
        lang: 'de',
    },
    {
        title: 'senior presales consultant',
        lang: 'en',
    },
    {
        title: 'senior programmer analyst',
        lang: 'en',
    },
    {
        title: 'senior recruiting consultant',
        lang: 'en',
    },
    {
        title: 'senior risk analyst',
        lang: 'en',
    },
    {
        title: 'senior ruby developer',
        lang: 'en',
    },
    {
        title: 'senior safety software entwickler/in',
        lang: 'en',
    },
    {
        title: 'senior sales consultant',
        lang: 'en',
    },
    {
        title: 'senior sales development manager',
        lang: 'en',
    },
    {
        title: 'senior sap fi consultant',
        lang: 'en',
    },
    {
        title: 'senior sap abap developer/pp analyst',
        lang: 'en',
    },
    {
        title: 'senior sap consultant rückversicherung',
        lang: 'de',
    },
    {
        title: 'senior sap solution manager consultant',
        lang: 'en',
    },
    {
        title: 'senior scrum master consultant',
        lang: 'en',
    },
    {
        title: 'senior servicemanager data logistix',
        lang: 'en',
    },
    {
        title: 'senior software entwickler c++ gui',
        lang: 'de',
    },
    {
        title: 'senior software analyst',
        lang: 'en',
    },
    {
        title: 'senior software architect',
        lang: 'en',
    },
    {
        title: 'senior software architekt java / ee',
        lang: 'en',
    },
    {
        title: 'senior software architekt java ee',
        lang: 'en',
    },
    {
        title: 'senior software asset lizenzmanager',
        lang: 'de',
    },
    {
        title: 'senior software developer  full stack',
        lang: 'en',
    },
    {
        title: 'senior software developer hybris',
        lang: 'en',
    },
    {
        title: 'senior software developer in hamburg',
        lang: 'en',
    },
    {
        title: 'senior software developer c++/c#',
        lang: 'en',
    },
    {
        title: 'senior software developer bpm berlin',
        lang: 'en',
    },
    {
        title: 'senior software developer java',
        lang: 'en',
    },
    {
        title: 'senior software developer java payment',
        lang: 'en',
    },
    {
        title: 'senior software developer',
        lang: 'en',
    },
    {
        title: 'senior software developer/in',
        lang: 'en',
    },
    {
        title: 'senior software engineer devops',
        lang: 'en',
    },
    {
        title: 'senior software engineer',
        lang: 'en',
    },
    {
        title: 'senior software entwickler api / java',
        lang: 'en',
    },
    {
        title: 'senior software entwickler autosar',
        lang: 'de',
    },
    {
        title: 'senior software entwickler java',
        lang: 'de',
    },
    {
        title: 'senior software entwickler java',
        lang: 'de',
    },
    {
        title: 'senior software entwickler/in',
        lang: 'de',
    },
    {
        title: 'senior software ingenieur mainframe',
        lang: 'de',
    },
    {
        title: 'senior software quality engineer',
        lang: 'en',
    },
    {
        title: 'senior software testautomatisierer',
        lang: 'en',
    },
    {
        title: 'senior software tester automotive',
        lang: 'en',
    },
    {
        title: 'senior softwarearchitekt',
        lang: 'en',
    },
    {
        title: 'senior softwareentwickler php / mysql',
        lang: 'en',
    },
    {
        title: 'senior softwareingenieur public sector',
        lang: 'de',
    },
    {
        title: 'senior softwareentwickler  automotive',
        lang: 'en',
    },
    {
        title: 'senior softwareentwickler android',
        lang: 'en',
    },
    {
        title: 'senior softwareentwickler automotive',
        lang: 'en',
    },
    {
        title: 'senior softwareentwickler backend',
        lang: 'de',
    },
    {
        title: 'senior softwareentwickler embedded',
        lang: 'de',
    },
    {
        title: 'senior softwareentwickler ios',
        lang: 'de',
    },
    {
        title: 'senior softwareentwickler java',
        lang: 'en',
    },
    {
        title: 'senior softwareentwickler',
        lang: 'de',
    },
    {
        title: 'senior softwareentwickler/in',
        lang: 'de',
    },
    {
        title: 'senior softwareentwickler/in frontend',
        lang: 'en',
    },
    {
        title: 'senior softwareentwickler/in hmi',
        lang: 'de',
    },
    {
        title: 'senior solution architect',
        lang: 'en',
    },
    {
        title: 'senior solution designer',
        lang: 'en',
    },
    {
        title: 'senior specialist, tool design',
        lang: 'en',
    },
    {
        title: 'senior spezialist data analytics',
        lang: 'en',
    },
    {
        title: 'senior staff engineer embedded software',
        lang: 'en',
    },
    {
        title: 'senior strategy consultant',
        lang: 'en',
    },
    {
        title: 'senior system administrator software',
        lang: 'en',
    },
    {
        title: 'senior system analyst',
        lang: 'en',
    },
    {
        title: 'senior system architect',
        lang: 'en',
    },
    {
        title: 'senior system designer',
        lang: 'en',
    },
    {
        title: 'senior system engineer linux',
        lang: 'en',
    },
    {
        title: 'senior systems analyst',
        lang: 'en',
    },
    {
        title: 'senior systems architect',
        lang: 'en',
    },
    {
        title: 'senior systems software engineer',
        lang: 'en',
    },
    {
        title: 'senior technical consultant / devops',
        lang: 'en',
    },
    {
        title: 'senior technical consultant plm',
        lang: 'en',
    },
    {
        title: 'senior technology architect',
        lang: 'en',
    },
    {
        title: 'senior test architect',
        lang: 'en',
    },
    {
        title: 'senior testdesigner',
        lang: 'en',
    },
    {
        title: 'senior ui/ux designer',
        lang: 'en',
    },
    {
        title: 'senior ux interaction designer',
        lang: 'en',
    },
    {
        title: 'senior ux user researcher',
        lang: 'en',
    },
    {
        title: 'senior web designer',
        lang: 'de',
    },
    {
        title: 'senior web developer',
        lang: 'en',
    },
    {
        title: 'senior webdeveloper/webengineer',
        lang: 'en',
    },
    {
        title: 'senior berater big data',
        lang: 'en',
    },
    {
        title: 'senior consultant sap mm / sd / pp',
        lang: 'en',
    },
    {
        title: 'senior data engineer für itbi von otto',
        lang: 'de',
    },
    {
        title: 'senior software developer realtime',
        lang: 'en',
    },
    {
        title: 'senior softwareingenieur',
        lang: 'de',
    },
    {
        title: 'seniorconsultant datenschutz',
        lang: 'en',
    },
    {
        title: 'seniorjavaarchitekt',
        lang: 'en',
    },
    {
        title: 'seniorjavaentwickler',
        lang: 'en',
    },
    {
        title: 'seniorsoftwareentwickler',
        lang: 'de',
    },
    {
        title: 'service architect workplace /d',
        lang: 'en',
    },
    {
        title: 'service consultant',
        lang: 'en',
    },
    {
        title: 'service consultants',
        lang: 'en',
    },
    {
        title: 'service design quality manager',
        lang: 'en',
    },
    {
        title: 'service designer',
        lang: 'en',
    },
    {
        title: 'service owner managed database',
        lang: 'en',
    },
    {
        title: 'sharepoint administrator/consultant',
        lang: 'en',
    },
    {
        title: 'sharepoint developer /divers',
        lang: 'en',
    },
    {
        title: 'sharepoint solution developer',
        lang: 'en',
    },
    {
        title: 'software entwickler java',
        lang: 'de',
    },
    {
        title: 'software application developer medicine',
        lang: 'en',
    },
    {
        title: 'software architect cbc',
        lang: 'en',
    },
    {
        title: 'software architect / technical lead',
        lang: 'en',
    },
    {
        title: 'software architect embedded systems',
        lang: 'en',
    },
    {
        title: 'software architekt automotive',
        lang: 'en',
    },
    {
        title: 'software architekt embedded systems',
        lang: 'en',
    },
    {
        title: 'software architekt für big data',
        lang: 'de',
    },
    {
        title: 'software architekt für embedded systems',
        lang: 'de',
    },
    {
        title: 'software architekt public sector',
        lang: 'en',
    },
    {
        title: 'software architekt/in infotainment',
        lang: 'de',
    },
    {
        title: 'software architekten java spring/jee',
        lang: 'en',
    },
    {
        title: 'software cloud architect',
        lang: 'en',
    },
    {
        title: 'software designer agile umgebung /d',
        lang: 'de',
    },
    {
        title: 'software designer finance /d',
        lang: 'en',
    },
    {
        title: 'software developer  badenbaden',
        lang: 'en',
    },
    {
        title: 'software developer  mobile apps',
        lang: 'en',
    },
    {
        title: 'software developer für den rechenkern',
        lang: 'de',
    },
    {
        title: 'software developer java oder c#',
        lang: 'en',
    },
    {
        title: 'software developer java/jee',
        lang: 'en',
    },
    {
        title: 'software developer php',
        lang: 'en',
    },
    {
        title: 'software developer / entwickler',
        lang: 'en',
    },
    {
        title: 'software developer / programmer',
        lang: 'en',
    },
    {
        title: 'software developer ax in vollzeit',
        lang: 'en',
    },
    {
        title: 'software developer c#',
        lang: 'en',
    },
    {
        title: 'software developer datenmanagement',
        lang: 'en',
    },
    {
        title: 'software developer eai',
        lang: 'en',
    },
    {
        title: 'software developer embedded',
        lang: 'en',
    },
    {
        title: 'software developer for webgui',
        lang: 'en',
    },
    {
        title: 'software developer frontend',
        lang: 'en',
    },
    {
        title: 'software developer infotainment',
        lang: 'en',
    },
    {
        title: 'software developer distributed systems',
        lang: 'en',
    },
    {
        title: 'software developer rendering',
        lang: 'en',
    },
    {
        title: 'software developer search algorithms',
        lang: 'en',
    },
    {
        title: 'software developer webgui',
        lang: 'en',
    },
    {
        title: 'software developer web/jee',
        lang: 'en',
    },
    {
        title: 'software development',
        lang: 'en',
    },
    {
        title: 'software development engneer',
        lang: 'en',
    },
    {
        title: 'software engineer / itcoordinator',
        lang: 'en',
    },
    {
        title: 'software engineer frontend',
        lang: 'en',
    },
    {
        title: 'software engineer java',
        lang: 'en',
    },
    {
        title: 'software engineer liferay',
        lang: 'en',
    },
    {
        title: 'software engineer mobile apps',
        lang: 'en',
    },
    {
        title: 'software engineer scala',
        lang: 'en',
    },
    {
        title: 'software engineer .net. .',
        lang: 'en',
    },
    {
        title: 'software engineer abap',
        lang: 'en',
    },
    {
        title: 'software engineer aftersales',
        lang: 'en',
    },
    {
        title: 'software engineer backend  java',
        lang: 'en',
    },
    {
        title: 'software engineer c#',
        lang: 'en',
    },
    {
        title: 'software engineer c++',
        lang: 'en',
    },
    {
        title: 'software engineer embedded system',
        lang: 'en',
    },
    {
        title: 'software engineer embedded systems',
        lang: 'en',
    },
    {
        title: 'software engineer java',
        lang: 'en',
    },
    {
        title: 'software engineer mes systeme',
        lang: 'en',
    },
    {
        title: 'software engineer test automation',
        lang: 'en',
    },
    {
        title: 'software engineer web development',
        lang: 'en',
    },
    {
        title: 'software engineer/softwareentwickler',
        lang: 'en',
    },
    {
        title: 'software engineering manager',
        lang: 'en',
    },
    {
        title: 'software entwickler .net',
        lang: 'de',
    },
    {
        title: 'software entwickler java oder c#',
        lang: 'en',
    },
    {
        title: 'software entwickler .net',
        lang: 'de',
    },
    {
        title: 'software entwickler / php entwickler',
        lang: 'de',
    },
    {
        title: 'software entwickler automotive embedded',
        lang: 'en',
    },
    {
        title: 'software entwickler backend',
        lang: 'de',
    },
    {
        title: 'software entwickler diagnose',
        lang: 'de',
    },
    {
        title: 'software entwickler erp',
        lang: 'de',
    },
    {
        title: 'software entwickler java backend',
        lang: 'en',
    },
    {
        title: 'software entwickler php',
        lang: 'de',
    },
    {
        title: 'software entwickler server backend',
        lang: 'de',
    },
    {
        title: 'software entwickler/in safety',
        lang: 'de',
    },
    {
        title: 'software funktionsentwickler',
        lang: 'de',
    },
    {
        title: 'software ingenieur defect management',
        lang: 'en',
    },
    {
        title: 'software ingenieur/in embedded systems',
        lang: 'en',
    },
    {
        title: 'software integrator',
        lang: 'en',
    },
    {
        title: 'software license asset specialist',
        lang: 'en',
    },
    {
        title: 'software procurement contract manager',
        lang: 'en',
    },
    {
        title: 'software projektleiter – automotive',
        lang: 'de',
    },
    {
        title: 'software projektleitung',
        lang: 'de',
    },
    {
        title: 'software quality assurance',
        lang: 'en',
    },
    {
        title: 'software quality assurance analyst',
        lang: 'en',
    },
    {
        title: 'software quality engineer',
        lang: 'en',
    },
    {
        title: 'software quality test engineer',
        lang: 'en',
    },
    {
        title: 'software quality test ingenieur',
        lang: 'en',
    },
    {
        title: 'software requirements engineer',
        lang: 'en',
    },
    {
        title: 'software security architect',
        lang: 'en',
    },
    {
        title: 'software service manager',
        lang: 'en',
    },
    {
        title: 'software solutionarchitekt',
        lang: 'en',
    },
    {
        title: 'software support engineer',
        lang: 'en',
    },
    {
        title: 'software support spezialist',
        lang: 'en',
    },
    {
        title: 'software system designer / architekt',
        lang: 'de',
    },
    {
        title: 'software systemdesigner/in steuergeräte',
        lang: 'de',
    },
    {
        title: 'software test automation engineer',
        lang: 'en',
    },
    {
        title: 'software test engineer',
        lang: 'en',
    },
    {
        title: 'software test engineer,',
        lang: 'en',
    },
    {
        title: 'software test manager',
        lang: 'en',
    },
    {
        title: 'software tester mobile',
        lang: 'en',
    },
    {
        title: 'software tester / qa manager it',
        lang: 'en',
    },
    {
        title: 'software tester eventim.fansale',
        lang: 'en',
    },
    {
        title: 'software tester plm',
        lang: 'en',
    },
    {
        title: 'software tester/in',
        lang: 'en',
    },
    {
        title: 'software testing',
        lang: 'en',
    },
    {
        title: 'software testingenieur',
        lang: 'en',
    },
    {
        title: 'software testingenieur mit labview',
        lang: 'de',
    },
    {
        title: 'software testingenieur ',
        lang: 'en',
    },
    {
        title: 'software toolchain engineer',
        lang: 'en',
    },
    {
        title: 'software verification manager',
        lang: 'en',
    },
    {
        title: 'software vertriebsexperte banking',
        lang: 'en',
    },
    {
        title: 'software / systemtestingenieur',
        lang: 'de',
    },
    {
        title: 'software integrator/in',
        lang: 'en',
    },
    {
        title: 'software/ produktentwickler',
        lang: 'de',
    },
    {
        title: 'software/diagnosetechniker',
        lang: 'de',
    },
    {
        title: 'softwareanalyst marktkommunikation',
        lang: 'en',
    },
    {
        title: 'softwareanwendungsberater',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt java',
        lang: 'en',
    },
    {
        title: 'softwarearchitekt rail',
        lang: 'en',
    },
    {
        title: 'softwarearchitekten/in',
        lang: 'de',
    },
    {
        title: 'softwaredatenarchitekt /d',
        lang: 'de',
    },
    {
        title: 'softwaredeveloper java',
        lang: 'en',
    },
    {
        title: 'softwareengineer build management',
        lang: 'en',
    },
    {
        title: 'softwareengineer front end gui',
        lang: 'en',
    },
    {
        title: 'softwareengineer locationbased services',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c++',
        lang: 'de',
    },
    {
        title: 'softwareentwickler embedded',
        lang: 'de',
    },
    {
        title: 'softwareentwickler integrator',
        lang: 'de',
    },
    {
        title: 'softwareentwickler java, c',
        lang: 'en',
    },
    {
        title: 'softwareentwickler',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c#',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c#/.net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c++ / c# / windows',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c++',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c++/linux',
        lang: 'en',
    },
    {
        title: 'softwareentwickler für embedded systeme',
        lang: 'de',
    },
    {
        title: 'softwareentwickler java/c++',
        lang: 'en',
    },
    {
        title: 'softwareentwickler mobilfunk',
        lang: 'de',
    },
    {
        title: 'softwareentwickler optimierung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler sps/ br',
        lang: 'de',
    },
    {
        title: 'softwareentwickler symfony',
        lang: 'en',
    },
    {
        title: 'softwareentwickler touch inspect',
        lang: 'en',
    },
    {
        title: 'softwareentwickler',
        lang: 'de',
    },
    {
        title: 'softwareentwickler embedded systems',
        lang: 'en',
    },
    {
        title: 'softwareentwickler.net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in .nettechnologie',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in linuxentwicklung',
        lang: 'de',
    },
    {
        title: 'softwareingenieure c#, java oder c++',
        lang: 'en',
    },
    {
        title: 'softwareintegrator',
        lang: 'de',
    },
    {
        title: 'softwareprojektleiter automotive',
        lang: 'en',
    },
    {
        title: 'softwareprojektleiter/in',
        lang: 'de',
    },
    {
        title: 'softwareprojektmanager automotive',
        lang: 'en',
    },
    {
        title: 'softwaresupport / consultant',
        lang: 'en',
    },
    {
        title: 'softwaresystemdesigner/in powertrain',
        lang: 'en',
    },
    {
        title: 'softwaretester für testautomatisierung',
        lang: 'de',
    },
    {
        title: 'softwaretester/in',
        lang: 'de',
    },
    {
        title: 'softwaretestingenieur preevision',
        lang: 'en',
    },
    {
        title: 'softwarearchitect / projectmanager .net',
        lang: 'en',
    },
    {
        title: 'softwarearchitekt embedded systems',
        lang: 'en',
    },
    {
        title: 'softwarearchitekt steuerungstechnik',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt / softwareentwickler',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt java',
        lang: 'en',
    },
    {
        title: 'softwarearchitekt sap hybris commerce',
        lang: 'en',
    },
    {
        title: 'softwareberater',
        lang: 'de',
    },
    {
        title: 'softwaredeveloper java ecommerce',
        lang: 'en',
    },
    {
        title: 'softwaredeveloper microsoft dynamics ax',
        lang: 'en',
    },
    {
        title: 'softwareentwickler .net, c#',
        lang: 'de',
    },
    {
        title: 'softwareentwickler .net/c#',
        lang: 'de',
    },
    {
        title: 'softwareentwickler ada',
        lang: 'de',
    },
    {
        title: 'softwareentwickler angularjs',
        lang: 'en',
    },
    {
        title: 'softwareentwickler backend c#',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c# .net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c# mit wpf',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c# und .net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c#, .net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c#/java',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c++ / c#/.net',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c++ braunschweig',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c++ medizintechnik',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c++, c# und java',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c, c++, c#',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c/c++ automotive',
        lang: 'en',
    },
    {
        title: 'softwareentwickler cloud tools',
        lang: 'en',
    },
    {
        title: 'softwareentwickler commercial vehicles',
        lang: 'en',
    },
    {
        title: 'softwareentwickler embedded c / c++',
        lang: 'en',
    },
    {
        title: 'softwareentwickler embedded software',
        lang: 'en',
    },
    {
        title: 'softwareentwickler embedded systeme',
        lang: 'en',
    },
    {
        title: 'softwareentwickler fokus itsm',
        lang: 'en',
    },
    {
        title: 'softwareentwickler frontend / ux',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für c und linux',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für testfahrten',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für testsysteme',
        lang: 'de',
    },
    {
        title: 'softwareentwickler healthcare',
        lang: 'en',
    },
    {
        title: 'softwareentwickler hmi',
        lang: 'de',
    },
    {
        title: 'softwareentwickler in festanstellung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler infotainment',
        lang: 'de',
    },
    {
        title: 'softwareentwickler intranet',
        lang: 'de',
    },
    {
        title: 'softwareentwickler java / jee',
        lang: 'en',
    },
    {
        title: 'softwareentwickler java eclipse',
        lang: 'en',
    },
    {
        title: 'softwareentwickler java, c#',
        lang: 'en',
    },
    {
        title: 'softwareentwickler javabackend',
        lang: 'de',
    },
    {
        title: 'softwareentwickler java/c#/c++',
        lang: 'en',
    },
    {
        title: 'softwareentwickler javascript backend',
        lang: 'en',
    },
    {
        title: 'softwareentwickler kamera',
        lang: 'de',
    },
    {
        title: 'softwareentwickler mechatronikmodule',
        lang: 'de',
    },
    {
        title: 'softwareentwickler mit schwerpunkt wms',
        lang: 'de',
    },
    {
        title: 'softwareentwickler ms dynamics crm',
        lang: 'en',
    },
    {
        title: 'softwareentwickler robotik',
        lang: 'en',
    },
    {
        title: 'softwareentwickler simulationsumgebung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler testautomatisierung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler web frontend',
        lang: 'de',
    },
    {
        title: 'softwareentwickler webapplikationen',
        lang: 'en',
    },
    {
        title: 'softwareentwickler webfrontend',
        lang: 'de',
    },
    {
        title: 'softwareentwickler schwerpunkt backend',
        lang: 'de',
    },
    {
        title: 'softwareentwickler .',
        lang: 'de',
    },
    {
        title: 'softwareentwickler .net, c#',
        lang: 'de',
    },
    {
        title: 'softwareentwickler / berater',
        lang: 'de',
    },
    {
        title: 'softwareentwickler / in',
        lang: 'de',
    },
    {
        title: 'softwareentwickler /d',
        lang: 'de',
    },
    {
        title: 'softwareentwickler adaptive autosar',
        lang: 'en',
    },
    {
        title: 'softwareentwickler autosar ',
        lang: 'de',
    },
    {
        title: 'softwareentwickler backend',
        lang: 'de',
    },
    {
        title: 'softwareentwickler bildverarbeitung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler bildverarbeitung ',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c # im .net umfeld',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c# / c++',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c# im .net umfeld',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c#, wpf',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c#/ .net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c# ',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c++ / c# ',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c/c++',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c/c++ embedded linux',
        lang: 'en',
    },
    {
        title: 'softwareentwickler delphi',
        lang: 'de',
    },
    {
        title: 'softwareentwickler devops',
        lang: 'en',
    },
    {
        title: 'softwareentwickler digital solutions',
        lang: 'de',
    },
    {
        title: 'softwareentwickler embedded systems',
        lang: 'en',
    },
    {
        title: 'softwareentwickler embedded systems ',
        lang: 'en',
    },
    {
        title: 'softwareentwickler erp',
        lang: 'de',
    },
    {
        title: 'softwareentwickler fiserviceportal',
        lang: 'en',
    },
    {
        title: 'softwareentwickler frontend / web',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für java jee',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für webanwendungen',
        lang: 'de',
    },
    {
        title: 'softwareentwickler gui für wpf / c#',
        lang: 'de',
    },
    {
        title: 'softwareentwickler hochsprache',
        lang: 'de',
    },
    {
        title: 'softwareentwickler hybridgetriebe m|w',
        lang: 'en',
    },
    {
        title: 'softwareentwickler integration',
        lang: 'de',
    },
    {
        title: 'softwareentwickler iot,',
        lang: 'en',
    },
    {
        title: 'softwareentwickler it / cyber security',
        lang: 'en',
    },
    {
        title: 'softwareentwickler java',
        lang: 'en',
    },
    {
        title: 'softwareentwickler java,',
        lang: 'en',
    },
    {
        title: 'softwareentwickler javaee',
        lang: 'en',
    },
    {
        title: 'softwareentwickler java ',
        lang: 'en',
    },
    {
        title: 'softwareentwickler ki gesucht!',
        lang: 'de',
    },
    {
        title: 'softwareentwickler medizintechnik',
        lang: 'de',
    },
    {
        title: 'softwareentwickler mobile apps ',
        lang: 'en',
    },
    {
        title: 'softwareentwickler modellbasiert',
        lang: 'de',
    },
    {
        title: 'softwareentwickler netzwerksysteme',
        lang: 'de',
    },
    {
        title: 'softwareentwickler obddiagnose',
        lang: 'en',
    },
    {
        title: 'softwareentwickler phpwebanwendung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler sap pi / po',
        lang: 'en',
    },
    {
        title: 'softwareentwickler sas',
        lang: 'de',
    },
    {
        title: 'softwareentwickler suchalgorithmen',
        lang: 'en',
    },
    {
        title: 'softwareentwickler testautomatisierung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler embedded c',
        lang: 'en',
    },
    {
        title: 'softwareentwickler für appanwendungen',
        lang: 'de',
    },
    {
        title: 'softwareentwickler linux / tools',
        lang: 'en',
    },
    {
        title: 'softwareentwickler truconnect',
        lang: 'en',
    },
    {
        title: 'softwareentwickler /div',
        lang: 'de',
    },
    {
        title: 'softwareentwickler.net m / w',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/ developer asp.net',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/developer  c#/.net',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/fachinformatiker *',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in backend',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in car multimedia',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in cloud services',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in cybsec',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in embedded linux',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in embedded systems',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in für plattformen',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in im bereich hmi',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in motorsteuergeräte',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in planisware',
        lang: 'en',
    },
    {
        title: 'softwareentwickler ',
        lang: 'de',
    },
    {
        title: 'softwareentwicklung autonomes fahren ',
        lang: 'de',
    },
    {
        title: 'softwareentwicklung testautomatisierung',
        lang: 'de',
    },
    {
        title: 'softwareentwicklungsingenieur',
        lang: 'de',
    },
    {
        title: 'softwareentwicklungsingenieur/in',
        lang: 'de',
    },
    {
        title: 'softwareent­wickler c#/ .net',
        lang: 'en',
    },
    {
        title: 'softwareintegrator embedded systeme',
        lang: 'en',
    },
    {
        title: 'softwareprojektleiter',
        lang: 'de',
    },
    {
        title: 'softwaretester mobility',
        lang: 'en',
    },
    {
        title: 'softwaretester testautomatisierung',
        lang: 'de',
    },
    {
        title: 'softwaretester / testmanager',
        lang: 'de',
    },
    {
        title: 'softwaretester im agilen umfeld',
        lang: 'de',
    },
    {
        title: 'softwaretester im bereich banking',
        lang: 'de',
    },
    {
        title: 'softwaretester interne tools',
        lang: 'en',
    },
    {
        title: 'softwaretester /d testautomatisierung',
        lang: 'de',
    },
    {
        title: 'softwaretestingenieure',
        lang: 'en',
    },
    {
        title: 'solution architect finance operations',
        lang: 'en',
    },
    {
        title: 'solution architect in sindelfingen',
        lang: 'en',
    },
    {
        title: 'solution architect ptc windchill',
        lang: 'en',
    },
    {
        title: 'solution architect dynamics crm',
        lang: 'en',
    },
    {
        title: 'solution architect fleet connect',
        lang: 'en',
    },
    {
        title: 'solution architect',
        lang: 'en',
    },
    {
        title: 'solution architect datacenter',
        lang: 'en',
    },
    {
        title: 'solution architect sap technology',
        lang: 'en',
    },
    {
        title: 'solution architect. .',
        lang: 'en',
    },
    {
        title: 'solution designer hr recruiting',
        lang: 'en',
    },
    {
        title: 'solution designer data warehouse',
        lang: 'en',
    },
    {
        title: 'solution developer for innovation hub',
        lang: 'en',
    },
    {
        title: 'solution manager / presales consultant',
        lang: 'en',
    },
    {
        title: 'solutions architect',
        lang: 'en',
    },
    {
        title: 'specialist devopsengineer',
        lang: 'en',
    },
    {
        title: 'spezialist big data',
        lang: 'en',
    },
    {
        title: 'spezialist data engineering – big data',
        lang: 'en',
    },
    {
        title: 'spezialist für fensterbausoftware',
        lang: 'de',
    },
    {
        title: 'spezialist radar design',
        lang: 'en',
    },
    {
        title: 'spezialist/in data warehouse etl',
        lang: 'en',
    },
    {
        title: 'spezialisten network design',
        lang: 'en',
    },
    {
        title: 'sps programmierer / softwareentwickler',
        lang: 'de',
    },
    {
        title: 'spsprogrammierer / softwareentwickler',
        lang: 'de',
    },
    {
        title: 'spsprogrammierer /softwarekonstrukteur',
        lang: 'en',
    },
    {
        title: 'spssoftwareentwickler',
        lang: 'de',
    },
    {
        title: 'statistical scientist',
        lang: 'en',
    },
    {
        title: 'stv. leiter itsoftware',
        lang: 'de',
    },
    {
        title: 'supply chain data scientist',
        lang: 'en',
    },
    {
        title: 'swarchitect/in',
        lang: 'en',
    },
    {
        title: 'swsystemdesigner/in motorsteuerungen',
        lang: 'de',
    },
    {
        title: 'system architect',
        lang: 'en',
    },
    {
        title: 'system engineer data center automation',
        lang: 'en',
    },
    {
        title: 'system engineer linux / monitoring',
        lang: 'en',
    },
    {
        title: 'system engineer data center',
        lang: 'en',
    },
    {
        title: 'system engineer devops',
        lang: 'en',
    },
    {
        title: 'system engineer – linux/open source',
        lang: 'en',
    },
    {
        title: 'system und softwareingenieur hil',
        lang: 'de',
    },
    {
        title: 'systemadministrator linux / aix',
        lang: 'en',
    },
    {
        title: 'systemadministrator cloud / devops',
        lang: 'en',
    },
    {
        title: 'systemadministrator linux',
        lang: 'en',
    },
    {
        title: 'systemadministrator linux/windows',
        lang: 'en',
    },
    {
        title: 'systemanalyst / sap support',
        lang: 'en',
    },
    {
        title: 'systemanalyst im bereich operations',
        lang: 'de',
    },
    {
        title: 'systemarchitect digital key',
        lang: 'en',
    },
    {
        title: 'systemarchitekt / softwarearchitekt',
        lang: 'de',
    },
    {
        title: 'systemarchitekt softwareportfolio',
        lang: 'en',
    },
    {
        title: 'systemintegrator / devop',
        lang: 'en',
    },
    {
        title: 'systems architect',
        lang: 'en',
    },
    {
        title: 'systems designer',
        lang: 'en',
    },
    {
        title: 'systems engineer linux',
        lang: 'en',
    },
    {
        title: 'systems software engineer​',
        lang: 'en',
    },
    {
        title: 'talend data engineer',
        lang: 'en',
    },
    {
        title: 'teamlead cloud big data',
        lang: 'en',
    },
    {
        title: 'teamleiter data science',
        lang: 'en',
    },
    {
        title: 'teamleiter it software development',
        lang: 'en',
    },
    {
        title: 'teamleiter security and data privacy',
        lang: 'en',
    },
    {
        title: 'teamleiter software automotive',
        lang: 'en',
    },
    {
        title: 'teamleiter agile software entwicklung',
        lang: 'de',
    },
    {
        title: 'teamleiter embedded softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'teamleiter solution architecture',
        lang: 'en',
    },
    {
        title: 'teamleiter /d java produktentwicklung',
        lang: 'en',
    },
    {
        title: 'teamleiter web development',
        lang: 'en',
    },
    {
        title: 'teamleiter/in webdesign',
        lang: 'de',
    },
    {
        title: 'teamleiter_in data processing',
        lang: 'en',
    },
    {
        title: 'teamleiterin static data fonds',
        lang: 'en',
    },
    {
        title: 'technical consultant big data',
        lang: 'en',
    },
    {
        title: 'technical consultant sap',
        lang: 'en',
    },
    {
        title: 'technical consultant',
        lang: 'en',
    },
    {
        title: 'technical consultant citrix technology',
        lang: 'en',
    },
    {
        title: 'technical consultants',
        lang: 'en',
    },
    {
        title: 'technical lead data foundation',
        lang: 'en',
    },
    {
        title: 'technical manager/ senior consultant',
        lang: 'en',
    },
    {
        title: 'technical presales consultant',
        lang: 'en',
    },
    {
        title: 'technical support api analyst',
        lang: 'en',
    },
    {
        title: 'techniker für den velux kundendienst',
        lang: 'de',
    },
    {
        title: 'technischer consultant',
        lang: 'en',
    },
    {
        title: 'technischer leiter / softwarearchitekt',
        lang: 'de',
    },
    {
        title: 'technischer produktdesigner automotive',
        lang: 'de',
    },
    {
        title: 'technologe softwareverteilungslösung',
        lang: 'de',
    },
    {
        title: 'technology architect',
        lang: 'en',
    },
    {
        title: 'territory sales manager software dach',
        lang: 'en',
    },
    {
        title: 'test analyst / software test engineer',
        lang: 'en',
    },
    {
        title: 'test automation engineer linux/qnx',
        lang: 'en',
    },
    {
        title: 'test data engineer',
        lang: 'en',
    },
    {
        title: 'test manager / test analyst',
        lang: 'en',
    },
    {
        title: 'testspezialist software',
        lang: 'en',
    },
    {
        title: 'testdesigner /d',
        lang: 'en',
    },
    {
        title: 'testingenieur/in software und systeme',
        lang: 'de',
    },
    {
        title: 'testmanager softwaretest',
        lang: 'de',
    },
    {
        title: 'testmanager software adas',
        lang: 'en',
    },
    {
        title: 'tooldesigner/in im werkzeugbau',
        lang: 'de',
    },
    {
        title: 'toolentwickler/in big data/reporting',
        lang: 'en',
    },
    {
        title: 'trainee big data',
        lang: 'en',
    },
    {
        title: 'trainee data analytics insurance',
        lang: 'en',
    },
    {
        title: 'trainee product/solution developer',
        lang: 'en',
    },
    {
        title: 'trainee software engineering java',
        lang: 'en',
    },
    {
        title: 'trainee software engineering – java',
        lang: 'en',
    },
    {
        title: 'trainee softwareentwicklung java',
        lang: 'de',
    },
    {
        title: 'trainee presales consultant',
        lang: 'en',
    },
    {
        title: 'trainee sap consultant sap hana',
        lang: 'en',
    },
    {
        title: 'trainee sap consultant successfactors',
        lang: 'en',
    },
    {
        title: 'trainee sap consultant',
        lang: 'en',
    },
    {
        title: 'trainee software engineering iiot',
        lang: 'en',
    },
    {
        title: 'trainee softwareengineering',
        lang: 'en',
    },
    {
        title: 'trainee softwareentwicklung',
        lang: 'en',
    },
    {
        title: 'trainee big data architekt',
        lang: 'en',
    },
    {
        title: 'trainee webentwickler/developer',
        lang: 'en',
    },
    {
        title: 'trainer für softwarequalitätssicherung',
        lang: 'de',
    },
    {
        title: 'training content designer',
        lang: 'en',
    },
    {
        title: 'tv advertising analyst',
        lang: 'en',
    },
    {
        title: 'ui designer',
        lang: 'en',
    },
    {
        title: 'ui developer für inhousewebapplications',
        lang: 'en',
    },
    {
        title: 'uientwickler java',
        lang: 'en',
    },
    {
        title: 'ui/ux designer berlin',
        lang: 'en',
    },
    {
        title: 'ui/ux designer inhouse',
        lang: 'en',
    },
    {
        title: 'ui/ux designer web',
        lang: 'en',
    },
    {
        title: 'ui/ux designer, ecommerce',
        lang: 'en',
    },
    {
        title: 'ui/ux designer/in',
        lang: 'en',
    },
    {
        title: 'ui/ux designer/konzepter',
        lang: 'en',
    },
    {
        title: 'ui/uxentwickler',
        lang: 'de',
    },
    {
        title: 'user experience interaction designer',
        lang: 'en',
    },
    {
        title: 'user experience architect',
        lang: 'en',
    },
    {
        title: 'user experience developer ui',
        lang: 'en',
    },
    {
        title: 'user experience ux researcher',
        lang: 'en',
    },
    {
        title: 'user interaction designer',
        lang: 'en',
    },
    {
        title: 'user interface designer/in',
        lang: 'de',
    },
    {
        title: 'ux designer java',
        lang: 'en',
    },
    {
        title: 'ux designer,',
        lang: 'en',
    },
    {
        title: 'ux designer/in für software',
        lang: 'de',
    },
    {
        title: 'ux designer/researcher',
        lang: 'en',
    },
    {
        title: 'ux lead',
        lang: 'en',
    },
    {
        title: 'ux researcher',
        lang: 'en',
    },
    {
        title: 'ux researcher/in für fintech unicorn',
        lang: 'de',
    },
    {
        title: 'ux/ui',
        lang: 'en',
    },
    {
        title: 'ux/ui designer digital lab',
        lang: 'en',
    },
    {
        title: 'ux/ui designer /d',
        lang: 'en',
    },
    {
        title: 'vertriebsexperten software / it',
        lang: 'de',
    },
    {
        title: 'video game developer',
        lang: 'en',
    },
    {
        title: 'vp product development',
        lang: 'en',
    },
    {
        title: 'vui/gui interaction designer',
        lang: 'en',
    },
    {
        title: 'web analyst',
        lang: 'en',
    },
    {
        title: 'web analyst / digital analyst',
        lang: 'en',
    },
    {
        title: 'web application developer',
        lang: 'en',
    },
    {
        title: 'web application developer / integrator',
        lang: 'en',
    },
    {
        title: 'web application devop',
        lang: 'en',
    },
    {
        title: 'web developer im bereich seo / sea',
        lang: 'de',
    },
    {
        title: 'web developer in vollzeit',
        lang: 'en',
    },
    {
        title: 'web developer php',
        lang: 'en',
    },
    {
        title: 'web developer / webentwickler',
        lang: 'en',
    },
    {
        title: 'web developer für `digital retail`',
        lang: 'de',
    },
    {
        title: 'web developer',
        lang: 'en',
    },
    {
        title: 'web entwickler / frontend developer',
        lang: 'en',
    },
    {
        title: 'web entwickler / web designer',
        lang: 'de',
    },
    {
        title: 'web/frontend developer html, css, js',
        lang: 'en',
    },
    {
        title: 'webanalytics consultant',
        lang: 'en',
    },
    {
        title: 'webentwickler data analytics',
        lang: 'de',
    },
    {
        title: 'webentwickler frontend / webdesigner',
        lang: 'de',
    },
    {
        title: 'webentwickler php',
        lang: 'de',
    },
    {
        title: 'web/c++softwareentwickler',
        lang: 'en',
    },
    {
        title: 'web/javascript entwickler',
        lang: 'en',
    },
    {
        title: 'webdesigner ui/ux',
        lang: 'en',
    },
    {
        title: 'webdesigner',
        lang: 'de',
    },
    {
        title: 'webdeveloper',
        lang: 'en',
    },
    {
        title: 'webentwickler php/javascript',
        lang: 'en',
    },
    {
        title: 'werkstudent business media analyst',
        lang: 'en',
    },
    {
        title: 'werkstudent im bereich datamanagement',
        lang: 'de',
    },
    {
        title: 'werkstudent in der softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'werkstudent international data quality',
        lang: 'en',
    },
    {
        title: 'werkstudent solution developer',
        lang: 'en',
    },
    {
        title: 'werkstudent data science gn*',
        lang: 'en',
    },
    {
        title: 'werkstudent im bereich java development',
        lang: 'en',
    },
    {
        title: 'werkstudent im data warehouse bereich',
        lang: 'de',
    },
    {
        title: 'werkstudent software entwicklung',
        lang: 'de',
    },
    {
        title: 'werkstudent software tester',
        lang: 'en',
    },
    {
        title: 'werkstudent softwareentwicklung java',
        lang: 'en',
    },
    {
        title: 'workplace architect',
        lang: 'en',
    },
    {
        title: 'young professional softwareentwicklung',
        lang: 'en',
    },
    {
        title: '.net developer/consultant',
        lang: 'en',
    },
    {
        title: '.net softwareengineer',
        lang: 'en',
    },
    {
        title: 'abap entwickler / sap abap consultant',
        lang: 'en',
    },
    {
        title: 'adas system architect',
        lang: 'en',
    },
    {
        title: 'advanced projects software engineer',
        lang: 'en',
    },
    {
        title: 'agile software engineer',
        lang: 'en',
    },
    {
        title: 'android developer',
        lang: 'en',
    },
    {
        title: 'anwendungsentwickler java in teilzeit',
        lang: 'de',
    },
    {
        title: 'application analyst',
        lang: 'en',
    },
    {
        title: 'application consultant',
        lang: 'en',
    },
    {
        title: 'architecture lead',
        lang: 'en',
    },
    {
        title: 'asic digital design engineer',
        lang: 'en',
    },
    {
        title: 'associate consultant / berater',
        lang: 'en',
    },
    {
        title: 'automotive linux engineer erlangen',
        lang: 'en',
    },
    {
        title: 'azure solution developer',
        lang: 'en',
    },
    {
        title: 'backend devops engineer java',
        lang: 'en',
    },
    {
        title: 'basissoftwareentwickler/in',
        lang: 'en',
    },
    {
        title: 'big data developer',
        lang: 'en',
    },
    {
        title: 'big data devops engineer openshift',
        lang: 'en',
    },
    {
        title: 'big data engineer dwh',
        lang: 'en',
    },
    {
        title: 'big data engineer advanced analytics',
        lang: 'en',
    },
    {
        title: 'big data solution designer/in',
        lang: 'en',
    },
    {
        title: 'bio informatiker / java entwickler',
        lang: 'en',
    },
    {
        title: 'business analyst digital',
        lang: 'en',
    },
    {
        title: 'business architect customer interaction',
        lang: 'en',
    },
    {
        title: 'business consultant',
        lang: 'en',
    },
    {
        title: 'business developer oem',
        lang: 'en',
    },
    {
        title: 'business intelligence developer',
        lang: 'en',
    },
    {
        title: 'business systems analyst',
        lang: 'en',
    },
    {
        title: 'business value consultant',
        lang: 'en',
    },
    {
        title: 'c# .netsoftwareentwickler',
        lang: 'en',
    },
    {
        title: 'c#softwareentwickler',
        lang: 'en',
    },
    {
        title: 'c++ senior software engineer erlangen',
        lang: 'en',
    },
    {
        title: 'cloud solution architect',
        lang: 'en',
    },
    {
        title: 'computer scientist',
        lang: 'en',
    },
    {
        title: 'computer vision developer',
        lang: 'en',
    },
    {
        title: 'consultant cyber security management',
        lang: 'en',
    },
    {
        title: 'consultant salesforce',
        lang: 'en',
    },
    {
        title: 'consultant bi/ dwh',
        lang: 'en',
    },
    {
        title: 'consultant itsicherheit und effizienz',
        lang: 'de',
    },
    {
        title: 'cyber security analyst',
        lang: 'en',
    },
    {
        title: 'data analyst app',
        lang: 'en',
    },
    {
        title: 'data analyst customerinsights',
        lang: 'en',
    },
    {
        title: 'data engineer für hadoop',
        lang: 'de',
    },
    {
        title: 'data engineer / data architect',
        lang: 'en',
    },
    {
        title: 'data engineer internet of things',
        lang: 'en',
    },
    {
        title: 'data integration engineer',
        lang: 'en',
    },
    {
        title: 'data scientist advanced analytics',
        lang: 'en',
    },
    {
        title: 'data scientist user profiling',
        lang: 'en',
    },
    {
        title: 'data scientist ai',
        lang: 'en',
    },
    {
        title: 'data warehouse architekt',
        lang: 'en',
    },
    {
        title: 'data/etl inhouse consultant',
        lang: 'en',
    },
    {
        title: 'datenbankentwickler / datenanalysten',
        lang: 'en',
    },
    {
        title: 'developer  sap abapoo',
        lang: 'en',
    },
    {
        title: 'developer erp business suite',
        lang: 'en',
    },
    {
        title: 'developer softwarearchitektur adas',
        lang: 'en',
    },
    {
        title: 'developer mobile clients',
        lang: 'en',
    },
    {
        title: 'developer/devops',
        lang: 'en',
    },
    {
        title: 'development engineer power tools',
        lang: 'en',
    },
    {
        title: 'development engineer software',
        lang: 'en',
    },
    {
        title: 'devops / software engineer',
        lang: 'en',
    },
    {
        title: 'devops /cloud engineer',
        lang: 'en',
    },
    {
        title: 'devops architect',
        lang: 'en',
    },
    {
        title: 'devops continuous integration expert',
        lang: 'en',
    },
    {
        title: 'devops engineer cloud services',
        lang: 'en',
    },
    {
        title: 'devops engineer automatisiertes fahren',
        lang: 'en',
    },
    {
        title: 'devops engineer cloud services,',
        lang: 'en',
    },
    {
        title: 'devops engineer linux',
        lang: 'en',
    },
    {
        title: 'devops engineer mobility services',
        lang: 'en',
    },
    {
        title: 'devops engineer public sector',
        lang: 'en',
    },
    {
        title: 'devops engineer schwerpunkt integration',
        lang: 'en',
    },
    {
        title: 'devops engineer und linux administrator',
        lang: 'en',
    },
    {
        title: 'devops engineer /d',
        lang: 'en',
    },
    {
        title: 'devops experte',
        lang: 'en',
    },
    {
        title: 'devops ingenieur cloud lösungen',
        lang: 'en',
    },
    {
        title: 'devops senior engineer',
        lang: 'en',
    },
    {
        title: 'devopsengineer',
        lang: 'en',
    },
    {
        title: 'devopsqualitätsmanager',
        lang: 'en',
    },
    {
        title: 'devopsspezialist',
        lang: 'en',
    },
    {
        title: 'digital architect',
        lang: 'en',
    },
    {
        title: 'embedded linux entwickler',
        lang: 'de',
    },
    {
        title: 'embedded softwareentwickler automotive',
        lang: 'en',
    },
    {
        title: 'embedded softwareentwickler autosar',
        lang: 'en',
    },
    {
        title: 'embedded softwareentwickler c++',
        lang: 'en',
    },
    {
        title: 'embedded softwareentwickler automotive',
        lang: 'en',
    },
    {
        title: 'embedded softwareentwickler ',
        lang: 'en',
    },
    {
        title: 'embedded systems developer wanted!',
        lang: 'en',
    },
    {
        title: 'engineer software dev ops',
        lang: 'en',
    },
    {
        title: 'engineer ai/ml data analyst',
        lang: 'en',
    },
    {
        title: 'engineer embedded software',
        lang: 'en',
    },
    {
        title: 'enterprise data architect',
        lang: 'en',
    },
    {
        title: 'enterprise data warehouse spezialist',
        lang: 'en',
    },
    {
        title: 'entwickler für embedded software',
        lang: 'de',
    },
    {
        title: 'entwickler prozesssensorik software',
        lang: 'de',
    },
    {
        title: 'entwickler / developer frontend',
        lang: 'en',
    },
    {
        title: 'entwickler anwendersoftware',
        lang: 'de',
    },
    {
        title: 'entwickler hardwarenahe software',
        lang: 'de',
    },
    {
        title: 'entwicklerin devops',
        lang: 'en',
    },
    {
        title: 'expert it software engineer',
        lang: 'en',
    },
    {
        title: 'expert java solution architect',
        lang: 'en',
    },
    {
        title: 'financial analyst',
        lang: 'en',
    },
    {
        title: 'fpgadesignerin oder fpgadesigner',
        lang: 'en',
    },
    {
        title: 'frontend developer',
        lang: 'en',
    },
    {
        title: 'frontend developer ecommerce',
        lang: 'en',
    },
    {
        title: 'full stack developer für tonline.de',
        lang: 'en',
    },
    {
        title: 'full stack javascript /javaentwickler',
        lang: 'en',
    },
    {
        title: 'fullstack .net developer',
        lang: 'en',
    },
    {
        title: 'fullstack senior developer',
        lang: 'en',
    },
    {
        title: 'fullstack java developer',
        lang: 'en',
    },
    {
        title: 'graduate program sap development',
        lang: 'en',
    },
    {
        title: 'grc data specialist in stuttgart',
        lang: 'en',
    },
    {
        title: 'ingenieur dataanalysis datamining',
        lang: 'en',
    },
    {
        title: 'ingenieur embedded software automotive',
        lang: 'en',
    },
    {
        title: 'ingenieur für software design autosar',
        lang: 'de',
    },
    {
        title: 'ingenieur softwarequalität',
        lang: 'de',
    },
    {
        title: 'inhouse big data engineer analytics',
        lang: 'en',
    },
    {
        title: 'inhouse softwareentwickler',
        lang: 'en',
    },
    {
        title: 'international sales manager benelux',
        lang: 'en',
    },
    {
        title: 'iot software architect',
        lang: 'en',
    },
    {
        title: 'it application consultant',
        lang: 'en',
    },
    {
        title: 'it architect adobe campaign',
        lang: 'en',
    },
    {
        title: 'it architect ecommerce sap hybris',
        lang: 'en',
    },
    {
        title: 'it architect integration',
        lang: 'en',
    },
    {
        title: 'it architect salesforce',
        lang: 'en',
    },
    {
        title: 'it architekt / software architekt',
        lang: 'de',
    },
    {
        title: 'it business consultant',
        lang: 'en',
    },
    {
        title: 'it consultant',
        lang: 'en',
    },
    {
        title: 'it ingenieur für hard und software',
        lang: 'de',
    },
    {
        title: 'it lead developer compliance',
        lang: 'en',
    },
    {
        title: 'it security consultant / engineer',
        lang: 'en',
    },
    {
        title: 'it software engineer',
        lang: 'en',
    },
    {
        title: 'it solution architect operations',
        lang: 'en',
    },
    {
        title: 'it specialist big data',
        lang: 'en',
    },
    {
        title: 'itconsultant business intelligence',
        lang: 'en',
    },
    {
        title: 'itentwickler webumfeld und java/ jee',
        lang: 'en',
    },
    {
        title: 'itprojektmanager, softwareconsultant',
        lang: 'en',
    },
    {
        title: 'itsystemadministrator linux',
        lang: 'en',
    },
    {
        title: 'java / hybris entwickler ecommerce',
        lang: 'en',
    },
    {
        title: 'java / spring engineer',
        lang: 'en',
    },
    {
        title: 'java developer  mehrere stellen',
        lang: 'en',
    },
    {
        title: 'java developer internet of things',
        lang: 'en',
    },
    {
        title: 'java developer microservices',
        lang: 'en',
    },
    {
        title: 'java enterprise developer jee/spring',
        lang: 'en',
    },
    {
        title: 'java enterprise developer possolutions',
        lang: 'en',
    },
    {
        title: 'java entwickler  digitale karten',
        lang: 'en',
    },
    {
        title: 'java entwickler / java consultant',
        lang: 'en',
    },
    {
        title: 'java entwickler in leipzig',
        lang: 'en',
    },
    {
        title: 'java entwickler frontend',
        lang: 'en',
    },
    {
        title: 'java entwickler/in',
        lang: 'en',
    },
    {
        title: 'java frontend entwickler',
        lang: 'en',
    },
    {
        title: 'java fullstackdeveloper',
        lang: 'en',
    },
    {
        title: 'java programmierer sap®security',
        lang: 'en',
    },
    {
        title: 'java script / typescript entwickler',
        lang: 'en',
    },
    {
        title: 'java software developer',
        lang: 'en',
    },
    {
        title: 'java software engineers',
        lang: 'en',
    },
    {
        title: 'java software entwickler',
        lang: 'en',
    },
    {
        title: 'java softwareentwickler',
        lang: 'en',
    },
    {
        title: 'java / jeeentwickler',
        lang: 'en',
    },
    {
        title: 'javabackend developer',
        lang: 'en',
    },
    {
        title: 'javabackend entwickler',
        lang: 'de',
    },
    {
        title: 'javaentwickler automotive',
        lang: 'en',
    },
    {
        title: 'javaentwickler in direktvermittlung',
        lang: 'de',
    },
    {
        title: 'javaentwickler',
        lang: 'en',
    },
    {
        title: 'javasoftwareentwickler/in',
        lang: 'en',
    },
    {
        title: 'javascript developer',
        lang: 'en',
    },
    {
        title: 'junior .net software entwickler',
        lang: 'de',
    },
    {
        title: 'junior accountmanager data logistix',
        lang: 'en',
    },
    {
        title: 'junior business analyst',
        lang: 'en',
    },
    {
        title: 'junior developer servicenow',
        lang: 'en',
    },
    {
        title: 'junior ecommerce developer',
        lang: 'en',
    },
    {
        title: 'junior fullstack entwickler java ee',
        lang: 'en',
    },
    {
        title: 'junior fullstack developer',
        lang: 'en',
    },
    {
        title: 'junior inhouse software engineer',
        lang: 'en',
    },
    {
        title: 'junior it consultant / java entwickler',
        lang: 'en',
    },
    {
        title: 'junior it softwareentwickler java',
        lang: 'en',
    },
    {
        title: 'junior software architekt',
        lang: 'en',
    },
    {
        title: 'junior software developer',
        lang: 'en',
    },
    {
        title: 'junior software developer java / web',
        lang: 'en',
    },
    {
        title: 'junior software developer otto.de',
        lang: 'en',
    },
    {
        title: 'junior software engineer java',
        lang: 'en',
    },
    {
        title: 'junior software engineer php',
        lang: 'en',
    },
    {
        title: 'junior software tester mit c/c++ ',
        lang: 'en',
    },
    {
        title: 'junior softwareengineer',
        lang: 'en',
    },
    {
        title: 'junior softwareingenieur public sector',
        lang: 'en',
    },
    {
        title: 'junior softwareentwickler frontend',
        lang: 'en',
    },
    {
        title: 'junior softwareentwickler',
        lang: 'en',
    },
    {
        title: 'junior softwareentwickler/in',
        lang: 'en',
    },
    {
        title: 'junior solution architect dynamics nav',
        lang: 'en',
    },
    {
        title: 'junior uidesigner',
        lang: 'en',
    },
    {
        title: 'juniorentwickler java',
        lang: 'en',
    },
    {
        title: 'juniorsoftwareentwickler java',
        lang: 'en',
    },
    {
        title: 'koordinator softwareentwicklung',
        lang: 'en',
    },
    {
        title: 'lead consultant data management',
        lang: 'en',
    },
    {
        title: 'lead developer enterpriseapplikation',
        lang: 'en',
    },
    {
        title: 'lead developer für big data',
        lang: 'en',
    },
    {
        title: 'lead developer backend',
        lang: 'en',
    },
    {
        title: 'lead developer | technical expert',
        lang: 'en',
    },
    {
        title: 'lead software developer .net',
        lang: 'en',
    },
    {
        title: 'lead software engineer java',
        lang: 'en',
    },
    {
        title: 'lead developer',
        lang: 'en',
    },
    {
        title: 'leiter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'leiter software engineering',
        lang: 'de',
    },
    {
        title: 'linux administrator devops engineer',
        lang: 'en',
    },
    {
        title: 'linux/oracle datenbank spezialist/in',
        lang: 'en',
    },
    {
        title: 'manager data governance quality',
        lang: 'en',
    },
    {
        title: 'manager business development',
        lang: 'en',
    },
    {
        title: 'master coder / architect',
        lang: 'en',
    },
    {
        title: 'master data engineer bus',
        lang: 'en',
    },
    {
        title: 'mes prozessingenieur / consultant',
        lang: 'en',
    },
    {
        title: 'mitarbeiter software development',
        lang: 'de',
    },
    {
        title: 'multicloud solutions architect',
        lang: 'en',
    },
    {
        title: 'network data engineer',
        lang: 'en',
    },
    {
        title: 'php backend developer',
        lang: 'en',
    },
    {
        title: 'php software engineer backend /divers',
        lang: 'en',
    },
    {
        title: 'phpentwickler/backend developer',
        lang: 'en',
    },
    {
        title: 'presales consultant',
        lang: 'en',
    },
    {
        title: 'premaster programm softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'pricing analyst',
        lang: 'en',
    },
    {
        title: 'product developer sap',
        lang: 'en',
    },
    {
        title: 'product development engineer',
        lang: 'en',
    },
    {
        title: 'produktmanager software',
        lang: 'en',
    },
    {
        title: 'professional data security partner/in',
        lang: 'en',
    },
    {
        title: 'project data manager',
        lang: 'en',
    },
    {
        title: 'project engineer in device development',
        lang: 'en',
    },
    {
        title: 'quality engineer medical software',
        lang: 'en',
    },
    {
        title: 'qualitätsmanager software',
        lang: 'de',
    },
    {
        title: 'react / javascript entwickler',
        lang: 'en',
    },
    {
        title: 'sales consultant',
        lang: 'en',
    },
    {
        title: 'sales development manager',
        lang: 'en',
    },
    {
        title: 'sap consultant software engineer',
        lang: 'en',
    },
    {
        title: 'sap ewm berater / softwareentwickler',
        lang: 'de',
    },
    {
        title: 'sap full stack developer',
        lang: 'en',
    },
    {
        title: 'sap lead master data management',
        lang: 'en',
    },
    {
        title: 'sap sd, jit/jis senior analyst',
        lang: 'en',
    },
    {
        title: 'scala backend software engineer',
        lang: 'en',
    },
    {
        title: 'senior application analyst in hamburg',
        lang: 'en',
    },
    {
        title: 'senior architect',
        lang: 'en',
    },
    {
        title: 'senior backend entwickler java',
        lang: 'en',
    },
    {
        title: 'senior business developer',
        lang: 'en',
    },
    {
        title: 'senior c++ architect',
        lang: 'en',
    },
    {
        title: 'senior cloud developer',
        lang: 'en',
    },
    {
        title: 'senior cloud solution architect',
        lang: 'en',
    },
    {
        title: 'senior consultant',
        lang: 'en',
    },
    {
        title: 'senior consultant digital marketing',
        lang: 'en',
    },
    {
        title: 'senior consultant finance consulting',
        lang: 'en',
    },
    {
        title: 'senior consultant big data',
        lang: 'en',
    },
    {
        title: 'senior consultant digitalisierung',
        lang: 'en',
    },
    {
        title: 'senior consultant iot',
        lang: 'en',
    },
    {
        title: 'senior consultant java',
        lang: 'en',
    },
    {
        title: 'senior consultant qs und testmanager',
        lang: 'en',
    },
    {
        title: 'senior crm analyst',
        lang: 'en',
    },
    {
        title: 'senior data analyst',
        lang: 'en',
    },
    {
        title: 'senior data analyst procurement',
        lang: 'en',
    },
    {
        title: 'senior data scientist',
        lang: 'en',
    },
    {
        title: 'senior data scientist/data engineer',
        lang: 'en',
    },
    {
        title: 'senior data warehouse engineer',
        lang: 'en',
    },
    {
        title: 'senior datastage developer /*',
        lang: 'en',
    },
    {
        title: 'senior db developer',
        lang: 'en',
    },
    {
        title: 'senior developer datawarehouse / bi',
        lang: 'en',
    },
    {
        title: 'senior developer sap / abap',
        lang: 'en',
    },
    {
        title: 'senior developer sap support',
        lang: 'en',
    },
    {
        title: 'senior devops it organisation',
        lang: 'en',
    },
    {
        title: 'senior digital business developer',
        lang: 'en',
    },
    {
        title: 'senior embedded systemdeveloper',
        lang: 'en',
    },
    {
        title: 'senior entwickler big data',
        lang: 'de',
    },
    {
        title: 'senior ethical hacker embedded devices',
        lang: 'en',
    },
    {
        title: 'senior field application scientist',
        lang: 'en',
    },
    {
        title: 'senior frontend developer welt.de',
        lang: 'en',
    },
    {
        title: 'senior full stack software architect',
        lang: 'en',
    },
    {
        title: 'senior full stack web developer',
        lang: 'en',
    },
    {
        title: 'senior fullstack developer crm',
        lang: 'en',
    },
    {
        title: 'senior fullstack javascript developer',
        lang: 'en',
    },
    {
        title: 'senior ingenieur pcb design ulm',
        lang: 'de',
    },
    {
        title: 'senior ingenieur software werkzeuge',
        lang: 'en',
    },
    {
        title: 'senior inhouse consultant celonis',
        lang: 'en',
    },
    {
        title: 'senior ios developer',
        lang: 'en',
    },
    {
        title: 'senior it consultant',
        lang: 'en',
    },
    {
        title: 'senior itarchitect plm/automotive',
        lang: 'en',
    },
    {
        title: 'senior java entwickler /divers',
        lang: 'en',
    },
    {
        title: 'senior java softwareentwickler frontend',
        lang: 'en',
    },
    {
        title: 'senior piping design engineer',
        lang: 'en',
    },
    {
        title: 'senior program developer',
        lang: 'en',
    },
    {
        title: 'senior project engineer software focus',
        lang: 'en',
    },
    {
        title: 'senior software developer iot',
        lang: 'en',
    },
    {
        title: 'senior software developer java / web',
        lang: 'en',
    },
    {
        title: 'senior software developer .net',
        lang: 'en',
    },
    {
        title: 'senior software engineer frontend',
        lang: 'en',
    },
    {
        title: 'senior software engineer .net',
        lang: 'en',
    },
    {
        title: 'senior software engineer golang',
        lang: 'en',
    },
    {
        title: 'senior software engineer  darmstadt',
        lang: 'de',
    },
    {
        title: 'senior software entwickler für .net',
        lang: 'de',
    },
    {
        title: 'senior software entwicklungsingenieur',
        lang: 'de',
    },
    {
        title: 'senior software tester/in embedded',
        lang: 'en',
    },
    {
        title: 'senior softwarearchitekt/in',
        lang: 'en',
    },
    {
        title: 'senior softwaredeveloper java/jee',
        lang: 'en',
    },
    {
        title: 'senior softwareentwickler delphi',
        lang: 'de',
    },
    {
        title: 'senior softwareentwickler m|w',
        lang: 'en',
    },
    {
        title: 'senior softwareentwickler java',
        lang: 'en',
    },
    {
        title: 'senior solution architect hybris',
        lang: 'en',
    },
    {
        title: 'senior solution software architect',
        lang: 'en',
    },
    {
        title: 'senior solution software developer',
        lang: 'en',
    },
    {
        title: 'senior solution software projektleiter',
        lang: 'en',
    },
    {
        title: 'senior system engineer linux / devops',
        lang: 'en',
    },
    {
        title: 'senior talend data engineer',
        lang: 'en',
    },
    {
        title: 'senior technical solution architect',
        lang: 'en',
    },
    {
        title: 'senior ui developer for prototyping',
        lang: 'en',
    },
    {
        title: 'senior software engineer sap',
        lang: 'en',
    },
    {
        title: 'seniorentwickler java',
        lang: 'de',
    },
    {
        title: 'seniorentwickler powerbuilder/java',
        lang: 'en',
    },
    {
        title: 'seniorsoftwareentwickler web / mobile',
        lang: 'en',
    },
    {
        title: 'service analyst it security',
        lang: 'en',
    },
    {
        title: 'service engineer für medizinsoftware',
        lang: 'de',
    },
    {
        title: 'software analyst',
        lang: 'en',
    },
    {
        title: 'software applikationingenieur',
        lang: 'en',
    },
    {
        title: 'software architect / softwarearchitekt',
        lang: 'en',
    },
    {
        title: 'software architekt  köln',
        lang: 'de',
    },
    {
        title: 'software architekt braunschweig',
        lang: 'de',
    },
    {
        title: 'software architekt / software architect',
        lang: 'en',
    },
    {
        title: 'software architekt',
        lang: 'en',
    },
    {
        title: 'software architekt/in autosar',
        lang: 'en',
    },
    {
        title: 'software architekten',
        lang: 'de',
    },
    {
        title: 'software asset license manager',
        lang: 'de',
    },
    {
        title: 'software configuration manager',
        lang: 'en',
    },
    {
        title: 'software design',
        lang: 'en',
    },
    {
        title: 'software developer  schwerpunkt devops',
        lang: 'en',
    },
    {
        title: 'software developer abrechnungssoftware',
        lang: 'de',
    },
    {
        title: 'software developer java',
        lang: 'en',
    },
    {
        title: 'software developer android',
        lang: 'en',
    },
    {
        title: 'software developer c++',
        lang: 'en',
    },
    {
        title: 'software developer c++/c#',
        lang: 'en',
    },
    {
        title: 'software developer c/autosar',
        lang: 'en',
    },
    {
        title: 'software developer im consulting',
        lang: 'en',
    },
    {
        title: 'software developer in der otto it',
        lang: 'en',
    },
    {
        title: 'software developer javascript',
        lang: 'en',
    },
    {
        title: 'software developer python',
        lang: 'en',
    },
    {
        title: 'software developer pl/sql, java',
        lang: 'en',
    },
    {
        title: 'software developer/iot',
        lang: 'en',
    },
    {
        title: 'software development engineer',
        lang: 'en',
    },
    {
        title: 'software development trainer',
        lang: 'en',
    },
    {
        title: 'software engineer  informatiker',
        lang: 'en',
    },
    {
        title: 'software engineer absolvent',
        lang: 'en',
    },
    {
        title: 'software engineer backend java',
        lang: 'en',
    },
    {
        title: 'software engineer datenverarbeitung',
        lang: 'de',
    },
    {
        title: 'software engineer java se / ee',
        lang: 'en',
    },
    {
        title: 'software engineer systementwicklung',
        lang: 'de',
    },
    {
        title: 'software engineer driver development',
        lang: 'en',
    },
    {
        title: 'software engineer embedded security',
        lang: 'en',
    },
    {
        title: 'software engineer mobile anwendungen',
        lang: 'de',
    },
    {
        title: 'software engineer / branche banking',
        lang: 'en',
    },
    {
        title: 'software engineer agile products',
        lang: 'en',
    },
    {
        title: 'software engineer customizing',
        lang: 'en',
    },
    {
        title: 'software engineer erp',
        lang: 'en',
    },
    {
        title: 'software engineer frontend',
        lang: 'en',
    },
    {
        title: 'software engineer java / javascript',
        lang: 'en',
    },
    {
        title: 'software engineer javascript',
        lang: 'en',
    },
    {
        title: 'software engineer  java',
        lang: 'en',
    },
    {
        title: 'software engineer machine learning',
        lang: 'en',
    },
    {
        title: 'software engineer qa standort ilmenau',
        lang: 'en',
    },
    {
        title: 'software engineer qualitätssicherung',
        lang: 'de',
    },
    {
        title: 'software engineers / it consultants c#',
        lang: 'en',
    },
    {
        title: 'software entwickler web ui',
        lang: 'de',
    },
    {
        title: 'software entwickler c++ automotive',
        lang: 'en',
    },
    {
        title: 'software entwickler clientsysteme',
        lang: 'de',
    },
    {
        title: 'software entwickler java remote',
        lang: 'en',
    },
    {
        title: 'software entwickler c#.net',
        lang: 'de',
    },
    {
        title: 'software entwickler/in',
        lang: 'de',
    },
    {
        title: 'software entwickler/in adas',
        lang: 'de',
    },
    {
        title: 'software entwickler/in autosar',
        lang: 'de',
    },
    {
        title: 'software entwickler/in hmi',
        lang: 'de',
    },
    {
        title: 'software entwicklungsingenieur rd',
        lang: 'de',
    },
    {
        title: 'software expertise engineer',
        lang: 'en',
    },
    {
        title: 'software project lead',
        lang: 'en',
    },
    {
        title: 'software quality analyst',
        lang: 'en',
    },
    {
        title: 'software qualitätssicherungsingenieur',
        lang: 'de',
    },
    {
        title: 'software test engineer / qa',
        lang: 'en',
    },
    {
        title: 'software test engineer cloud services,',
        lang: 'en',
    },
    {
        title: 'software test engineer ',
        lang: 'en',
    },
    {
        title: 'software tester  mobile applikationen',
        lang: 'en',
    },
    {
        title: 'software tester embedded',
        lang: 'en',
    },
    {
        title: 'software testingenieur automotive',
        lang: 'en',
    },
    {
        title: 'software testmanager embedded systems',
        lang: 'en',
    },
    {
        title: 'software testmanager',
        lang: 'en',
    },
    {
        title: 'software / fertigungsingenieur',
        lang: 'de',
    },
    {
        title: 'software und systemarchitekt autosar',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt fahrzeugkonfiguration',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt/in c++',
        lang: 'en',
    },
    {
        title: 'softwarearchitekt/in',
        lang: 'en',
    },
    {
        title: 'softwareassetmanagementconsultants',
        lang: 'en',
    },
    {
        title: 'softwaredeveloper mit schwerpunkt java',
        lang: 'en',
    },
    {
        title: 'softwareentwickler für c#, .net, wpf',
        lang: 'de',
    },
    {
        title: 'softwareentwickler in aachen',
        lang: 'de',
    },
    {
        title: 'softwareentwickler als data engineer',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c# .net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler java / spring',
        lang: 'en',
    },
    {
        title: 'softwareentwickler php / symfony',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in grafikkonvergenz',
        lang: 'en',
    },
    {
        title: 'softwareintegrator/in',
        lang: 'de',
    },
    {
        title: 'softwareprogrammierer',
        lang: 'en',
    },
    {
        title: 'softwaresupportingenieur oder techniker',
        lang: 'de',
    },
    {
        title: 'softwaresystemarchitekt',
        lang: 'en',
    },
    {
        title: 'softwaretester',
        lang: 'en',
    },
    {
        title: 'softwaretestmanager',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt .net',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt elektronik',
        lang: 'en',
    },
    {
        title: 'softwarearchitekt diagnoseprodukte',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt für mobile apps',
        lang: 'de',
    },
    {
        title: 'softwarearchitekt',
        lang: 'en',
    },
    {
        title: 'softwaredeveloper processmodelling',
        lang: 'en',
    },
    {
        title: 'softwaredeveloper java/jee',
        lang: 'en',
    },
    {
        title: 'softwareentwickler `virtuelle fabrik`',
        lang: 'de',
    },
    {
        title: 'softwareentwickler  perl',
        lang: 'de',
    },
    {
        title: 'softwareentwickler .net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler autosar automotive',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c#, .net, wpf',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c#/­.net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c++/linux',
        lang: 'en',
    },
    {
        title: 'softwareentwickler cobol',
        lang: 'en',
    },
    {
        title: 'softwareentwickler flugzeugsimulation',
        lang: 'de',
    },
    {
        title: 'softwareentwickler frontend',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für c++',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für edisysteme',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für jee mobile',
        lang: 'de',
    },
    {
        title: 'softwareentwickler ima wop',
        lang: 'de',
    },
    {
        title: 'softwareentwickler java braunschweig',
        lang: 'de',
    },
    {
        title: 'softwareentwickler java für fansale.de',
        lang: 'de',
    },
    {
        title: 'softwareentwickler jee',
        lang: 'en',
    },
    {
        title: 'softwareentwickler mit c++kenntnissen',
        lang: 'de',
    },
    {
        title: 'softwareentwickler mit schwerpunkt wpf',
        lang: 'de',
    },
    {
        title: 'softwareentwickler service app',
        lang: 'en',
    },
    {
        title: 'softwareentwickler sps',
        lang: 'de',
    },
    {
        title: 'softwareentwickler automotive',
        lang: 'en',
    },
    {
        title: 'softwareentwickler / administrator',
        lang: 'en',
    },
    {
        title: 'softwareentwickler / softwareingenieur',
        lang: 'de',
    },
    {
        title: 'softwareentwickler angular / javascript',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c# / java',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c++ ',
        lang: 'de',
    },
    {
        title: 'softwareentwickler cloud computing',
        lang: 'en',
    },
    {
        title: 'softwareentwickler connectivity',
        lang: 'en',
    },
    {
        title: 'softwareentwickler embedded',
        lang: 'de',
    },
    {
        title: 'softwareentwickler embedded automotive',
        lang: 'en',
    },
    {
        title: 'softwareentwickler für embedded systems',
        lang: 'de',
    },
    {
        title: 'softwareentwickler für functionalsafety',
        lang: 'de',
    },
    {
        title: 'softwareentwickler hmi grafikframework',
        lang: 'de',
    },
    {
        title: 'softwareentwickler hochsprachen',
        lang: 'de',
    },
    {
        title: 'softwareentwickler infotainment',
        lang: 'de',
    },
    {
        title: 'softwareentwickler inland',
        lang: 'de',
    },
    {
        title: 'softwareentwickler java remote',
        lang: 'en',
    },
    {
        title: 'softwareentwickler java / semantic web',
        lang: 'en',
    },
    {
        title: 'softwareentwickler javaee / c# / .net',
        lang: 'en',
    },
    {
        title: 'softwareentwickler kernentwicklung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler produktentwicklung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler senior delphi / sql',
        lang: 'en',
    },
    {
        title: 'softwareentwickler sps',
        lang: 'de',
    },
    {
        title: 'softwareentwickler diagnosesysteme',
        lang: 'de',
    },
    {
        title: 'softwareentwickler embedded linux',
        lang: 'en',
    },
    {
        title: 'softwareentwickler full stack',
        lang: 'en',
    },
    {
        title: 'softwareentwickler java',
        lang: 'en',
    },
    {
        title: 'softwareentwickler maschinenautomation',
        lang: 'de',
    },
    {
        title: 'softwareentwickler webanwendungen',
        lang: 'de',
    },
    {
        title: 'softwareentwickler – java automotive',
        lang: 'en',
    },
    {
        title: 'softwareentwicklerphp',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in für ebike abs',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in java – crmsystem',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in php',
        lang: 'en',
    },
    {
        title: 'softwareentwickler/in medizintechnik',
        lang: 'de',
    },
    {
        title: 'softwareentwickler/in sps',
        lang: 'de',
    },
    {
        title: 'softwaretester / automotive',
        lang: 'en',
    },
    {
        title: 'softwaretester für hilsysteme',
        lang: 'de',
    },
    {
        title: 'softwaretester in der spielebranche',
        lang: 'de',
    },
    {
        title: 'softwaretester tps',
        lang: 'en',
    },
    {
        title: 'softwaretester automotive',
        lang: 'en',
    },
    {
        title: 'softwaretester/ qa',
        lang: 'en',
    },
    {
        title: 'softwaretesterin',
        lang: 'de',
    },
    {
        title: 'softwaretestingenieur',
        lang: 'de',
    },
    {
        title: 'solution architect crm',
        lang: 'en',
    },
    {
        title: 'solution architect  iot',
        lang: 'en',
    },
    {
        title: 'solution architect für versicherungen',
        lang: 'de',
    },
    {
        title: 'solution entwickler/in memory software',
        lang: 'en',
    },
    {
        title: 'spezialist softwareentwicklung java',
        lang: 'de',
    },
    {
        title: 'spezialist security design',
        lang: 'en',
    },
    {
        title: 'staff consultant for it deployment',
        lang: 'en',
    },
    {
        title: 'support engineer  linux betrieb',
        lang: 'en',
    },
    {
        title: 'support it analyst',
        lang: 'en',
    },
    {
        title: 'system administrator schwerpunkt linux',
        lang: 'de',
    },
    {
        title: 'system architect data',
        lang: 'en',
    },
    {
        title: 'system developer / engineer devops',
        lang: 'en',
    },
    {
        title: 'system engineer java',
        lang: 'en',
    },
    {
        title: 'systemadministrator schwerpunkt linux',
        lang: 'de',
    },
    {
        title: 'systementwickler embedded software ',
        lang: 'en',
    },
    {
        title: 'systemingenieur im bereich software',
        lang: 'de',
    },
    {
        title: 'systemingenieur embedded software',
        lang: 'en',
    },
    {
        title: 'systems analyst',
        lang: 'en',
    },
    {
        title: 'team lead data foundation',
        lang: 'en',
    },
    {
        title: 'team lead java – acquisition loyalty',
        lang: 'en',
    },
    {
        title: 'teamassistenz softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'teamleiter softwareentwicklung java',
        lang: 'de',
    },
    {
        title: 'teamleiter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'technical architect',
        lang: 'en',
    },
    {
        title: 'technical consultant  new business',
        lang: 'en',
    },
    {
        title: 'technical engineering consultant /d',
        lang: 'en',
    },
    {
        title: 'technical lead/technical architect',
        lang: 'en',
    },
    {
        title: 'technical solution architect',
        lang: 'en',
    },
    {
        title: 'testentwickler und softwaretester',
        lang: 'de',
    },
    {
        title: 'testingenieur software / systeme',
        lang: 'de',
    },
    {
        title: 'ui/uxdesigner/in',
        lang: 'en',
    },
    {
        title: 'user experience designer',
        lang: 'en',
    },
    {
        title: 'user experience designer mobile app',
        lang: 'en',
    },
    {
        title: 'user interface designer',
        lang: 'en',
    },
    {
        title: 'ux / ui designer mobile apps',
        lang: 'en',
    },
    {
        title: 'ux/ uidesigner',
        lang: 'en',
    },
    {
        title: 'uxdesigner',
        lang: 'en',
    },
    {
        title: 'web client developer',
        lang: 'en',
    },
    {
        title: 'web frontend developer  javascript',
        lang: 'en',
    },
    {
        title: 'webdeveloper c# m / w',
        lang: 'en',
    },
    {
        title: 'webdeveloper frontend',
        lang: 'en',
    },
    {
        title: 'webentwickler php',
        lang: 'de',
    },
    {
        title: 'webfrontend developer',
        lang: 'en',
    },
    {
        title: 'webentwickler python und javascript',
        lang: 'de',
    },
    {
        title: 'webentwickler / developer',
        lang: 'en',
    },
    {
        title: 'webentwickler / phpprogrammierer',
        lang: 'de',
    },
    {
        title: 'webentwickler / softwareentwickler',
        lang: 'de',
    },
    {
        title: 'webentwickler javascipt',
        lang: 'de',
    },
    {
        title: 'webentwickler javascript / java / jee',
        lang: 'en',
    },
    {
        title: 'werkstudent data center infrastructure',
        lang: 'en',
    },
    {
        title: 'werkstudent softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'werkstudent datawarehouse entwicklung',
        lang: 'de',
    },
    {
        title: 'analyst it advisory',
        lang: 'en',
    },
    {
        title: 'analytics consultant data engineer',
        lang: 'en',
    },
    {
        title: 'application developer',
        lang: 'en',
    },
    {
        title: 'art director / ui designer',
        lang: 'en',
    },
    {
        title: 'big data engineer spark',
        lang: 'en',
    },
    {
        title: 'big data java devops streaming',
        lang: 'en',
    },
    {
        title: 'business analyst data warehouse',
        lang: 'en',
    },
    {
        title: 'business analyst',
        lang: 'en',
    },
    {
        title: 'consultant data engineering',
        lang: 'en',
    },
    {
        title: 'consultants / software engineers java',
        lang: 'en',
    },
    {
        title: 'continuous integration architect',
        lang: 'en',
    },
    {
        title: 'data center engineer',
        lang: 'en',
    },
    {
        title: 'data engineer für die finanzbranche',
        lang: 'de',
    },
    {
        title: 'data warehouse/ etldeveloper',
        lang: 'en',
    },
    {
        title: 'database administrator',
        lang: 'en',
    },
    {
        title: 'devops / cloud engineer',
        lang: 'en',
    },
    {
        title: 'devops / system integration engineer /*',
        lang: 'en',
    },
    {
        title: 'devops engineer for big data',
        lang: 'en',
    },
    {
        title: 'devops engineer commerce solutions',
        lang: 'en',
    },
    {
        title: 'devops linux engineer',
        lang: 'en',
    },
    {
        title: 'elasticsearch devops engineer big data',
        lang: 'en',
    },
    {
        title: 'embedded software engineer lidar',
        lang: 'en',
    },
    {
        title: 'embedded software entwickler',
        lang: 'de',
    },
    {
        title: 'entwickler produktsoftware',
        lang: 'de',
    },
    {
        title: 'entwicklungsingenieur software',
        lang: 'de',
    },
    {
        title: 'frontend entwickler/in html/javascript',
        lang: 'de',
    },
    {
        title: 'frontend react.js developer',
        lang: 'en',
    },
    {
        title: 'hardware/software entwicklungsingenieur',
        lang: 'de',
    },
    {
        title: 'informatikerin/ softwareentwicklerin',
        lang: 'en',
    },
    {
        title: 'infrastructure architect',
        lang: 'en',
    },
    {
        title: 'ingenieur softwaretests',
        lang: 'de',
    },
    {
        title: 'ios developer lidlapp',
        lang: 'en',
    },
    {
        title: 'itconsultant',
        lang: 'en',
    },
    {
        title: 'javadeveloper warehouse management',
        lang: 'en',
    },
    {
        title: 'java/ javascript software developer',
        lang: 'en',
    },
    {
        title: 'javascript frontend engineer',
        lang: 'en',
    },
    {
        title: 'junior consultant business intelligence',
        lang: 'en',
    },
    {
        title: 'junior javaentwickler',
        lang: 'en',
    },
    {
        title: 'junior software engineer frontend',
        lang: 'en',
    },
    {
        title: 'junior softwareentwickler c# / .net ',
        lang: 'en',
    },
    {
        title: 'junior virus analyst',
        lang: 'en',
    },
    {
        title: 'lead devops engineer',
        lang: 'en',
    },
    {
        title: 'lead software build release engineer',
        lang: 'en',
    },
    {
        title: 'sap hybris developer',
        lang: 'en',
    },
    {
        title: 'sap user experience ux designer',
        lang: 'en',
    },
    {
        title: 'senior consultant digital analytics',
        lang: 'en',
    },
    {
        title: 'senior data engineer autonomous driving',
        lang: 'en',
    },
    {
        title: 'senior devops engineer / architect',
        lang: 'en',
    },
    {
        title: 'senior java software entwickler',
        lang: 'en',
    },
    {
        title: 'senior software engineer backend',
        lang: 'en',
    },
    {
        title: 'senior software engineer frontend',
        lang: 'en',
    },
    {
        title: 'senior software engineer java',
        lang: 'en',
    },
    {
        title: 'senior softwarearchitekt java',
        lang: 'en',
    },
    {
        title: 'senior softwaretester',
        lang: 'de',
    },
    {
        title: 'senior system engineer devops',
        lang: 'en',
    },
    {
        title: 'senior user experience designer',
        lang: 'en',
    },
    {
        title: 'senior ux designer',
        lang: 'en',
    },
    {
        title: 'software engineer car it',
        lang: 'en',
    },
    {
        title: 'software engineer cloud mobile',
        lang: 'en',
    },
    {
        title: 'software engineer xamarin',
        lang: 'en',
    },
    {
        title: 'software engineer, backend welt.de',
        lang: 'en',
    },
    {
        title: 'software entwickler',
        lang: 'de',
    },
    {
        title: 'software security engineer',
        lang: 'en',
    },
    {
        title: 'software test automation architect',
        lang: 'en',
    },
    {
        title: 'softwareapplikationsingenieur',
        lang: 'en',
    },
    {
        title: 'softwaretester im .net umfeld',
        lang: 'de',
    },
    {
        title: 'softwareengineer autosar',
        lang: 'en',
    },
    {
        title: 'softwareentwickler  java entwicklung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler backend / java',
        lang: 'en',
    },
    {
        title: 'softwareentwickler bildverarbeitung',
        lang: 'de',
    },
    {
        title: 'softwareentwickler c# / .net',
        lang: 'de',
    },
    {
        title: 'softwareentwickler erpprojekte',
        lang: 'en',
    },
    {
        title: 'softwareentwickler salesforce',
        lang: 'en',
    },
    {
        title: 'softwareentwickler / java entwickler',
        lang: 'en',
    },
    {
        title: 'softwareentwickler automotive',
        lang: 'en',
    },
    {
        title: 'softwareentwickler codesys',
        lang: 'en',
    },
    {
        title: 'softwareentwickler microsoft .net gn*',
        lang: 'de',
    },
    {
        title: 'softwareingenieur',
        lang: 'de',
    },
    {
        title: 'softwaretest engineer applikation',
        lang: 'en',
    },
    {
        title: 'softwaretester / softwareentwickler',
        lang: 'de',
    },
    {
        title: 'softwaretester / software test engineer',
        lang: 'en',
    },
    {
        title: 'solution architect salesforce crm',
        lang: 'en',
    },
    {
        title: 'solution developer',
        lang: 'en',
    },
    {
        title: 'system architekt embedded linux',
        lang: 'en',
    },
    {
        title: 'system engineer devops',
        lang: 'en',
    },
    {
        title: 'technische/r softwareentwickler/in',
        lang: 'de',
    },
    {
        title: 'technischer produktdesigner',
        lang: 'de',
    },
    {
        title: 'web data analyst',
        lang: 'en',
    },
    {
        title: 'business development manager',
        lang: 'en',
    },
    {
        title: 'cloud developer',
        lang: 'en',
    },
    {
        title: 'cloud devops engineer',
        lang: 'en',
    },
    {
        title: 'embedded software engineer',
        lang: 'en',
    },
    {
        title: 'embedded softwareentwickler automotive',
        lang: 'en',
    },
    {
        title: 'frontend developer',
        lang: 'en',
    },
    {
        title: 'full stack web developer',
        lang: 'en',
    },
    {
        title: 'java senior developer/consultant',
        lang: 'en',
    },
    {
        title: 'junior it consultant',
        lang: 'en',
    },
    {
        title: 'junior software engineer',
        lang: 'en',
    },
    {
        title: 'junior software entwickler java',
        lang: 'en',
    },
    {
        title: 'lead data scientist',
        lang: 'en',
    },
    {
        title: 'leiter softwareentwicklung',
        lang: 'de',
    },
    {
        title: 'php entwickler',
        lang: 'de',
    },
    {
        title: 'senior data engineer',
        lang: 'en',
    },
    {
        title: 'senior java entwickler',
        lang: 'en',
    },
    {
        title: 'senior javaentwickler',
        lang: 'en',
    },
    {
        title: 'senior software engineer java',
        lang: 'en',
    },
    {
        title: 'software engineer / adtech',
        lang: 'en',
    },
    {
        title: 'software engineer',
        lang: 'en',
    },
    {
        title: 'software engineers',
        lang: 'en',
    },
    {
        title: 'software ingenieur',
        lang: 'de',
    },
    {
        title: 'software entwickler c / c++',
        lang: 'en',
    },
    {
        title: 'software entwickler c# / .net',
        lang: 'de',
    },
    {
        title: 'ui/ux designer',
        lang: 'en',
    },
    {
        title: 'entwickler embedded software',
        lang: 'de',
    },
    {
        title: 'java entwickler / java developer',
        lang: 'en',
    },
    {
        title: 'javascriptentwickler',
        lang: 'en',
    },
    {
        title: 'junior software ingenieur/in java se/ee',
        lang: 'en',
    },
    {
        title: 'produktsoftwareentwickler/in',
        lang: 'de',
    },
    {
        title: 'senior software engineer cybersecurity',
        lang: 'en',
    },
    {
        title: 'senior software entwickler',
        lang: 'de',
    },
    {
        title: 'senior ui designer',
        lang: 'en',
    },
    {
        title: 'senior uxdesigner',
        lang: 'en',
    },
    {
        title: 'software entwickler',
        lang: 'de',
    },
    {
        title: 'software architect',
        lang: 'en',
    },
    {
        title: 'software developer',
        lang: 'en',
    },
    {
        title: 'software tester',
        lang: 'en',
    },
    {
        title: 'softwarearchitekt automotive',
        lang: 'en',
    },
    {
        title: 'software entwickler c#/.net',
        lang: 'de',
    },
    {
        title: 'software entwickler mit javakenntnissen',
        lang: 'de',
    },
    {
        title: 'softwaretester automotive',
        lang: 'en',
    },
    {
        title: 'ux/ui designer',
        lang: 'en',
    },
    {
        title: 'itconsultant/software entwickler',
        lang: 'en',
    },
    {
        title: 'java ee entwickler/developer',
        lang: 'en',
    },
    {
        title: 'javaentwickler/in',
        lang: 'en',
    },
    {
        title: 'junior itconsultant',
        lang: 'en',
    },
    {
        title: 'software engineer speech backend',
        lang: 'en',
    },
    {
        title: 'softwareentwickler c#',
        lang: 'de',
    },
    {
        title: 'softwareentwickler .net',
        lang: 'de',
    },
    {
        title: 'software entwickler java',
        lang: 'de',
    },
    {
        title: 'ux designer',
        lang: 'en',
    },
    {
        title: 'big data engineer',
        lang: 'en',
    },
];
