import { getRequest } from '../request';
import { addToken } from '~/util';

const requestUri = `${process.env.searchEndpoint}/v1/suggestions_by_type/`;

// types are skill, language, path, title
export const apiByTypeSuggestions = (query, type) => {
    const data = addToken({
        type,
        query,
    });

    return getRequest(requestUri, data).catch(error => error); // TODO (@starikovm) add logger for API errors
};
