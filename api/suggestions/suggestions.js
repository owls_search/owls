import { getRequest } from '../request';
import { addToken } from '~/util';

const requestUri = `${process.env.searchEndpoint}/v1/suggestions/`;

export const apiSuggestions = query => {
    const data = addToken({
        query,
    });

    return getRequest(requestUri, data).catch(error => error); // TODO (@starikovm) add logger for API errors
};
