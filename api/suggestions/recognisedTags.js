import { postRequest } from '../request';
import { addToken } from '~/util';

const requestUri = `${process.env.searchEndpoint}/v1/recogniseTags/`;

export const apiRecognisedTags = ({ query, file }) => {
    const data = addToken({
        query,
        file,
    });

    return postRequest(requestUri, data).catch(error => error);
};
