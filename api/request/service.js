import axios from 'axios';

export function getBlobRequest(url, data) {
    const encoded = Object.keys(data)
        .map(
            key =>
                `${encodeURIComponent(key)}=${encodeURIComponent(
                    data[key] && data[key] !== null ? data[key] : '',
                )}`,
        )
        .join('&');

    return axios({
        method: 'get',
        url: `${url}?${encoded}`,
        responseType: 'blob',
    });
}

export function getRequest(url, data = {}, useCache = false) {
    const encoded = Object.keys(data)
        .map(
            key =>
                `${encodeURIComponent(key)}=${encodeURIComponent(
                    data[key] && data[key] !== null ? data[key] : '',
                )}`,
        )
        .join('&');

    return axios.get(`${url}?${encoded}`, { useCache });
}

const formatDataParameter = param =>
    param !== null && typeof param === 'object' && !(param instanceof File)
        ? JSON.stringify(param)
        : retrieveDataParameter(param);

const retrieveDataParameter = param => param || '';

export function postRequest(url, data = {}, useCache = false) {
    if (process.server) {
        const FormData = require('form-data');

        const fd = new FormData();

        Object.keys(data).map(key =>
            fd.append(key, formatDataParameter(data[key])),
        );

        return axios.post(url, fd, {
            headers: fd.getHeaders(),
            useCache,
        });
    } else {
        const fd = new FormData();
        Object.keys(data).map(key =>
            fd.append(key, formatDataParameter(data[key])),
        );

        return axios.post(url, fd, {
            headers: { 'Content-Type': 'multipart/form-data' },
        });
    }
}
