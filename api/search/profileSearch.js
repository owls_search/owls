import { postRequest } from '../request';
import { addToken } from '~/util';

const requestUri = `${process.env.searchEndpoint}/v1/listjobs/`;

export const apiProfileSearch = ({
    titles,
    skills,
    programmingLanguages,
    locations,
    file,
    pageSize,
    language,
    page = 1,
}) => {
    const data = addToken({
        page_size: pageSize, // eslint-disable-line camelcase
        page_index: page - 1, // eslint-disable-line camelcase,
        language,
        bid: 0,
        file,
        value: [titles, skills, programmingLanguages, locations].join(' '),
    });

    return postRequest(requestUri, data);
};
