import { postRequest } from '../request';
import { addToken } from '~/util';

const requestUri = `${process.env.searchEndpoint}/v1/listjobs/`;

export const apiFileSearch = (
    file,
    { pageSize, language, minBid = 0, page = 1, createdUserId = null },
) => {
    const data = addToken({
        page_size: pageSize, // eslint-disable-line camelcase
        page_index: page - 1, // eslint-disable-line camelcase,
        bid: minBid, // eslint-disable-line camelcase
        language,
        file,
    });

    if (createdUserId) {
        data.created_user_id = createdUserId; // eslint-disable-line camelcase
    }

    return postRequest(requestUri, data);
};
