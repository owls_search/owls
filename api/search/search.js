import { postRequest } from '../request';
import { addToken } from '~/util';

const requestUri = `${process.env.searchEndpoint}/v1/listjobs/`;

export const apiSearch = (
    query,
    { pageSize, language, minBid = 0, page = 1, createdUserId = null },
    useCache = false,
) => {
    const data = addToken({
        page_size: pageSize, // eslint-disable-line camelcase
        page_index: page - 1, // eslint-disable-line camelcase
        bid: minBid, // eslint-disable-line camelcase
        language,
        value: query,
    });

    if (createdUserId) {
        data.created_user_id = createdUserId; // eslint-disable-line camelcase
    }

    return postRequest(requestUri, data, useCache);
};
