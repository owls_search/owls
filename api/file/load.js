import { postRequest } from '../request';
import { addPhpKey } from '~/util';

const requestUri = `${process.env.phpEndpoint}/api/file/store`;

export const apiStoreFile = file => {
    const data = addPhpKey({ file });
    return postRequest(requestUri, data);
};
