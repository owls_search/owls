import { getBlobRequest } from '../request';
import { addPhpKey } from '~/util';

const requestUri = `${process.env.phpEndpoint}/api/file/get`;

export const apiGetFile = fileName => {
    const data = addPhpKey({ fileName });
    return getBlobRequest(requestUri, data);
};
