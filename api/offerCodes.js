import axios from 'axios';

const requestUri = `https://api.clusterjobs.de/sitemap_offers`; // we cannot use env var as it is used when nuxt only is setting up
export const apiGetOfferCodes = (sitemapIndex = 0) =>
    axios.get(`${requestUri}?index=${sitemapIndex}`);
