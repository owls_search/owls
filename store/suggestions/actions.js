import { apiSuggestions } from '~/api/suggestions';

const appendSearchQuery = (query, ssgs) =>
    ssgs.some(
        ssg =>
            ssg.suggestion.toLowerCase().trim() === query.toLowerCase().trim(),
    )
        ? ssgs
        : [...ssgs, { suggestion: query.toLowerCase().trim(), type: 'title' }];

const gotSuggestionResults = (commit, result, query) => {
    const { data = {} } = result;
    const { suggestions = [], tags = [] } = data;

    commit('SSG_GOT_SUGGESTIONS', appendSearchQuery(query, suggestions));
    commit('SEARCH_SET_RECOGNISED_TAGS', tags);
};

const actions = {
    selectPrevSuggestion({ commit, state: { selectedSuggestion } }) {
        if (selectedSuggestion === null) return;
        const newSelectedSsg =
            selectedSuggestion > 0 ? selectedSuggestion - 1 : 0;
        commit('SSG_HOVER_SUGGESTION', newSelectedSsg);
    },
    selectNextSuggestion({ commit, state: { list, selectedSuggestion } }) {
        let newSelectedSsg = 0;
        if (selectedSuggestion !== null) {
            newSelectedSsg =
                selectedSuggestion === list.length - 1
                    ? selectedSuggestion
                    : selectedSuggestion + 1;
        }
        commit('SSG_HOVER_SUGGESTION', newSelectedSsg);
    },
    selectSuggestion({ dispatch }, { ssgText }) {
        dispatch('search', { query: ssgText });
    },
    getSuggestions({ commit }, query) {
        if (query.length === 0) {
            commit('SSG_RESET_SUGGESTIONS');
            return;
        }
        apiSuggestions(query).then(result =>
            gotSuggestionResults(commit, result, query),
        );
    },
    hoverSuggestion({ commit }, newSelectedSuggestion) {
        commit('SSG_HOVER_SUGGESTION', newSelectedSuggestion);
    },
    closeSuggestions({ commit }) {
        commit('SSG_CLOSE_SUGGESTIONS');
    },
};

export default actions;
