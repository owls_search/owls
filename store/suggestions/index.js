import actions from './actions.js';
import { mapSuggestionsFromRemote } from '~/util';

const state = () => ({
    showSuggestions: false,
    suggestionsLoading: false,
    list: [],
    selectedSuggestion: null,
});

const mutations = {
    SEARCH_START_SEARCH(state) {
        state.list = [];
        state.showSuggestions = false;
        state.suggestionsLoading = false;

        state.tagsSuggestions = [];
        state.showTagsSuggestions = false;
        state.tagsSuggestionsLoading = false;
    },
    SSG_HOVER_SUGGESTION(state, newSelectedSuggestion) {
        state.selectedSuggestion = newSelectedSuggestion;
    },
    SSG_REQUEST_STARTED(state) {
        state.suggestionsLoading = true;
        state.showSuggestions = true;
    },
    SSG_RESET_SUGGESTIONS(state) {
        state.list = [];
        state.suggestionsLoading = false;
        state.showSuggestions = false;
    },
    SSG_GOT_SUGGESTIONS(state, ssgs) {
        state.list = mapSuggestionsFromRemote(ssgs);
        state.suggestionsLoading = false;
    },
    SSG_CLOSE_SUGGESTIONS(state) {
        state.list = [];
        state.showSuggestions = false;
        state.suggestionsLoading = false;
    },
};

const suggestions = {
    state,
    mutations,
    actions,
};

export default suggestions;
