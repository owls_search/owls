const state = () => ({
    campaign: null,
    source: null,
    medium: null,
    isGoogleJobs: false,
});

const GOOGLE_JOBS_SOURCE = 'google_jobs_apply';
const actions = {
    setSeoCampaign({ commit }, campaign) {
        commit('SEO_PARAMETERS_SET_CAMPAIGN', campaign);
    },
    setSeoSource({ commit, rootState }, source) {
        if (source === GOOGLE_JOBS_SOURCE) {
            commit('SEO_PARAMETERS_SET_IS_GOOGLE_JOBS', true);
        }

        commit('SEO_PARAMETERS_SET_SOURCE', source);
    },
    resetIsGoogleJobs({ commit }) {
        commit('SEO_PARAMETERS_SET_IS_GOOGLE_JOBS', false);
    },
    setSeoMedium({ commit }, medium) {
        commit('SEO_PARAMETERS_SET_MEDIUM', medium);
    },
};

const mutations = {
    SEO_PARAMETERS_SET_CAMPAIGN(state, campaign) {
        state.campaign = campaign;
    },
    SEO_PARAMETERS_SET_SOURCE(state, source) {
        state.source = source;
    },
    SEO_PARAMETERS_SET_MEDIUM(state, medium) {
        state.medium = medium;
    },
    SEO_PARAMETERS_SET_IS_GOOGLE_JOBS(state, isGJ) {
        state.isGoogleJobs = isGJ;
    },
};

const seoParameters = {
    state,
    actions,
    mutations,
};

export default seoParameters;
