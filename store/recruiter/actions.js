import {
    requestModules,
    mapTagsToString,
    handleError,
    getTagsFromRemote,
    mapFromRemoteApiOffer,
} from '~/util';
import { get, property } from '~/util/lodash';
import { apiFileSearch } from '~/api/search';
import ADD_OFFER from '~/api/apollo/offers/mutation/addOffer.gql';
import DELETE_OFFER from '~/api/apollo/offers/mutation/deleteOffer.gql';
import UPDATE_OFFER from '~/api/apollo/offers/mutation/updateOffer.gql';
import GET_RECRUITER_OFFERS from '~/api/apollo/offers/query/getUserOffers.gql';
import { apiStoreFile } from '~/api/file/load';

export const recruiterActions = {
    async addOffer(
        {
            commit,
            dispatch,
            rootGetters: { isAuthenticated },
            rootState,
        },
        { data },
    ) {
        if (!isAuthenticated) {
            dispatch('requestFailed', {
                message: 'There is no valid token',
                component: requestModules.OFFER_FORM,
            });
            return;
        }

        dispatch('requestStart', requestModules.OFFER_FORM);

        let logo = data.logo;
        if (typeof logo !== 'string') {
            const data = await apiStoreFile(logo).then(property('data'));
            if (!data.hasOwnProperty('name')) {
                dispatch('requestFailed', {
                    message: 'Error during file storing',
                    component: this.$requestModules.OFFER_FORM,
                });
                return;
            }
            logo = data.name;
        }

        this.app
            .gqlRequest(ADD_OFFER, {
                token: get(rootState, ['user', 'token'], null),
                offer: {
                    title: data.title,
                    description: data.description,
                    miniDescription: data.miniDescription,
                    destinations: mapTagsToString(data.destinations),
                    skills: mapTagsToString(data.skills),
                    programmingLanguages: mapTagsToString(
                        data.programmingLanguages,
                    ),
                    url: data.url,
                    companyName: data.companyName,
                    offerType: data.offerType,
                    startsFrom: data.startsFrom,
                    email: data.email,
                    salaryFrom: parseInt(data.salaryFrom, 10),
                    salaryTo: parseInt(data.salaryTo, 10),
                    logo,
                    language: data.language,
                },
            })
            .then(({ data }) => {
                const { addOffer } = data;
                const { id } = addOffer;
                if (id) {
                    dispatch('requestFinish', requestModules.OFFER_FORM);
                    dispatch('afterOfferAdd', id);
                } else {
                    dispatch('requestFailed', {
                        message: this.$t('updateOffer.errorNoId'),
                        component: requestModules.OFFER_FORM,
                    });
                }
            })
            .catch(handleError(dispatch, requestModules.OFFER_FORM));
    },
    afterOfferAdd(ctx, id) {
        this.app.router.push(
            this.app.localePath({
                name: 'cabinet-recruiter-offer-added-id',
                params: {
                    id,
                },
            }),
        );
    },
    async updateOffer(
        {
            commit,
            dispatch,
            rootGetters: { isAuthenticated },
            rootState,
        },
        { id, data },
    ) {
        if (!isAuthenticated) {
            dispatch('requestFailed', {
                message: 'There is no valid token',
                component: requestModules.OFFER_FORM,
            });
            return;
        }

        let logo = data.logo;
        if (typeof logo !== 'string') {
            const data = await apiStoreFile(logo).then(property('data'));
            if (!data.hasOwnProperty('name')) {
                dispatch('requestFailed', {
                    message: 'Error during file storing',
                    component: this.$requestModules.OFFER_FORM,
                });
                return;
            }
            logo = data.name;
        }

        dispatch('requestStart', requestModules.OFFER_FORM);
        this.app
            .gqlRequest(UPDATE_OFFER, {
                token: get(rootState, ['user', 'token'], null),
                offer: {
                    title: data.title,
                    description: data.description,
                    miniDescription: data.miniDescription,
                    destinations: mapTagsToString(data.destinations),
                    skills: mapTagsToString(data.skills),
                    programmingLanguages: mapTagsToString(
                        data.programmingLanguages,
                    ),
                    url: data.url,
                    companyName: data.companyName,
                    offerType: data.offerType,
                    startsFrom: data.startsFrom,
                    email: data.email,
                    salaryFrom: parseInt(data.salaryFrom, 10),
                    salaryTo: parseInt(data.salaryTo, 10),
                    logo,
                    language: data.language,
                },
                id,
            })
            .then(({ data }) => {
                const { updateOffer } = data;
                const { message } = updateOffer;
                dispatch('requestSucceed', {
                    message,
                    component: requestModules.OFFER_FORM,
                });
            })
            .catch(handleError(dispatch, requestModules.OFFER_FORM));
    },
    deleteOffer({ dispatch, rootState }, { offerId, page }) {
        dispatch('requestStart', requestModules.OFFERS_LIST);

        this.app
            .gqlRequest(DELETE_OFFER, {
                id: offerId,
                token: get(rootState, ['user', 'token'], null),
            })
            .then(_ => {
                dispatch('requestFinish', requestModules.OFFERS_LIST);
                dispatch('getRecruiterOffers', page);
            });
    },
    getRecruiterOffers({ commit, dispatch, rootState }, pageIndex = 1) {
        dispatch('requestStart', requestModules.OFFERS_LIST);
        const token = get(rootState, ['user', 'token'], null);
        token &&
            this.app
                .gqlRequest(GET_RECRUITER_OFFERS, {
                    token,
                    page: pageIndex - 1,
                })
                .then(property(['data', 'getUserOffers']))
                .then(({ offers, totalNumber }) => {
                    commit('RECRUITER_SET_OFFERS_LIST', offers);
                    commit('RECRUITER_SET_TOTAL_OFFER_NUMBER', totalNumber);
                    dispatch('requestFinish', requestModules.OFFERS_LIST);
                })
                .catch(_ => {
                    dispatch('requestFinish', requestModules.OFFERS_LIST);
                });
    },
    getRecruiterOffersByCandidateCv(
        {
            commit,
            dispatch,
            rootState,
            rootGetters: { getId },
            state: { recruiterSearchFile },
        },
        { pageSize, page },
    ) {
        dispatch('requestStart', requestModules.FIND_FOR_CANDIDATE);
        apiFileSearch(recruiterSearchFile, {
            pageSize: pageSize,
            page: page,
            createdUserId: getId,
            language: get(
                rootState,
                ['search', 'offerLanguages', 'all', 'param'],
                'all',
            ),
        })
            .then(property('data'))
            // eslint-disable-next-line camelcase
            .then(({ jobs, tags = {}, number_of_jobs = 0 }) => {
                commit(
                    'RECRUITER_SET_CANDIDATE_CV_KEYWORDS',
                    getTagsFromRemote(tags),
                );
                dispatch('requestFinish', requestModules.FIND_FOR_CANDIDATE);
                commit(
                    'RECRUITER_SET_OFFERS_LIST',
                    jobs ? jobs.map(mapFromRemoteApiOffer) : [],
                ); // eslint-disable-line camelcase
                commit('RECRUITER_SET_TOTAL_OFFER_NUMBER', number_of_jobs);
            })
            .catch(e => {
                dispatch('requestFinish', requestModules.FIND_FOR_CANDIDATE);
            });
    },
    recruiterResetCandidateCv({ commit }) {
        commit('RECRUIT_RESET_CANDIDATE_CV');
    },
    recruiterSetCandidateCv({ commit }, file) {
        commit('RECRUIT_SET_CANDIDATE_CV', file);
    },
};
