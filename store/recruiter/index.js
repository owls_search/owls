import { recruiterActions } from './actions.js';

const state = () => ({
    recruiterOffers: [],
    totalNumber: 0,
    recruiterSearchFile: null,
    candidateCvKeywords: null,
});

const mutations = {
    RECRUITER_SET_OFFERS_LIST(state, offersList) {
        state.recruiterOffers = offersList;
    },
    RECRUITER_SET_TOTAL_OFFER_NUMBER(state, totalNumber) {
        state.totalNumber = totalNumber;
    },
    RECRUITER_SET_CANDIDATE_CV_KEYWORDS(state, keywords) {
        state.candidateCvKeywords = keywords;
    },
    RECRUIT_RESET_CANDIDATE_CV(state) {
        state.recruiterSearchFile = null;
    },
    RECRUIT_SET_CANDIDATE_CV(state, file) {
        state.recruiterSearchFile = file;
    },
};

const request = {
    state,
    actions: recruiterActions,
    mutations,
};

export default request;
