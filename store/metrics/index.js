import { get } from '~/util/lodash';

const actions = {
    clickout(app, { analyticsModule, partner, event, bid, gtm = null }) {
        analyticsModule &&
            analyticsModule.query &&
            analyticsModule.query(
                'send',
                'event',
                'clickout',
                partner,
                event,
                (bid * 100) | 0,
                {
                    metric1: bid,
                },
            );

        gtm &&
            gtm.pushEvent({
                event: 'clickout',
            });
    },
    selectSuggestion(app, { analyticsModule }) {
        analyticsModule &&
            analyticsModule.event &&
            analyticsModule.event({
                eventCategory: 'action',
                eventAction: 'search',
                eventLabel: 'search',
            });
    },
    saveSearch(app, { analyticsModule }) {
        analyticsModule &&
            analyticsModule.event &&
            analyticsModule.event({
                eventCategory: 'retention',
                eventAction: 'save-search',
                eventLabel: 'save',
            });
    },
    saveSearchAndAddNewcomer(app, { analyticsModule }) {
        analyticsModule &&
            analyticsModule.event &&
            analyticsModule.event({
                eventCategory: 'retention',
                eventAction: 'save-search',
                eventLabel: 'registration',
            });
    },
    registerUser(app, { analyticsModule, isApplicant }) {
        analyticsModule &&
            analyticsModule.event({
                eventCategory: 'retention',
                eventAction: 'registration',
                eventLabel: isApplicant ? 'applicant' : 'recruiter',
            });
    },
    loginUser(app, { analyticsModule }) {
        analyticsModule &&
            analyticsModule.event({
                eventCategory: 'retention',
                eventAction: 'login',
                eventLabel: 'user',
            });
    },
    registerStudent(app, { analyticsModule }) {
        analyticsModule &&
            analyticsModule.event({
                eventCategory: 'retention',
                eventAction: 'registration',
                eventLabel: 'studentRegistration',
            });
    },
    viewOffer({ rootState }, { ga, gtm, id, location, pageType }) {
        const viewTotalValue = String(
            get(rootState, ['user', 'jobTotalValue'], ''),
        );

        ga && ga.query && ga.query('set', 'dimension1', id);
        ga && ga.query && ga.query('set', 'dimension2', pageType);
        ga && ga.query && ga.query('set', 'dimension3', viewTotalValue);
        ga && ga.query && ga.query('set', 'dimension4', location);

        gtm &&
            gtm.pushEvent({
                event: 'view_item',
                jobId: id,
                // eslint-disable-next-line camelcase
                locationId: location,
                jobViewTotalValue: viewTotalValue,
                items: [
                    {
                        id,
                        // eslint-disable-next-line camelcase
                        location_id: location,
                        // eslint-disable-next-line camelcase
                        google_business_vertical: 'jobs',
                    },
                ],
            });
    },
};

const metrics = {
    state: () => {},
    mutations: {},
    actions,
};

export default metrics;
