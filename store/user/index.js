import index from './actions/index.js';

const state = () => ({
    token: null,
    clientId: null,
    startTimestamp: null,
    userData: null,
    jobTotalValue: null,
    savedSearches: [],
    workTypes: [
        {
            type: 'full',
            translation: 'profile.offerTypeFull',
        },
        {
            type: 'part',
            translation: 'profile.offerTypePart',
        },
        {
            type: 'student',
            translation: 'profile.offerTypeStudent',
        },
        {
            type: 'remote',
            translation: 'profile.offerTypeRemote',
        },
    ],
});

const mutations = {
    USER_SET_TOKEN(state, token) {
        state.token = token;
        state.isRequestLoading = false;
    },
    USER_RESET_TOKEN(state) {
        state.token = null;
    },
    USER_SET_USER_DATA(state, userData) {
        state.userData = userData;
        state.isRequestLoading = false;
    },
    USER_SET_SAVED_SEARCHES(state, savedSearches) {
        state.savedSearches = savedSearches;
    },
    USER_SET_SAVED_SEARCH(state, savedSearch) {
        const savedSearchIndex = state.savedSearches.findIndex(
            search => search.id === savedSearch.id,
        );

        if (savedSearchIndex > -1) {
            const forceReactivitySaveSearches = state.savedSearches.slice();
            forceReactivitySaveSearches[savedSearchIndex] = savedSearch;
            state.savedSearches = forceReactivitySaveSearches;
        } else {
            state.savedSearches.push(savedSearch);
        }
    },
    USER_REMOVE_SAVED_SEARCH(state, id) {
        const savedSearchIndex = state.savedSearches.findIndex(
            search => parseInt(search.id, 10) === parseInt(id, 10),
        );

        if (savedSearchIndex > -1) {
            const forceReactivitySaveSearches = state.savedSearches.slice();
            forceReactivitySaveSearches.splice(savedSearchIndex, 1);
            state.savedSearches = forceReactivitySaveSearches;
        }
    },
    USER_SET_CLIENT_ID(state, clientId) {
        state.clientId = clientId;
    },
    USER_SET_JOB_TOTAL_VALUE(state, val) {
        state.jobTotalValue = val;
    },
    USER_SET_SESSION_START(state, startTimestamp) {
        state.startTimestamp = startTimestamp;
    },
};

const getters = {
    isAuthenticated(state) {
        return !!state.token;
    },
    getUserType(state) {
        return state.userData && state.userData.type
            ? state.userData.type
            : null;
    },
    getUserData(state) {
        return state.userData || null;
    },
    getId(state) {
        return state.userData && state.userData.id ? state.userData.id : null;
    },
};

const user = {
    state,
    mutations,
    actions: index,
    getters,
};

export default user;
