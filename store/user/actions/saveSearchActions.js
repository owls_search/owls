import { gotUserInfo } from './index';
import { requestModules, handleError } from '~/util';
import SAVE_SEARCH from '~/api/apollo/saveSearch/add.gql';
import SAVE_SEARCH_UPDATE from '~/api/apollo/saveSearch/update.gql';
import SAVE_SEARCH_REMOVE from '~/api/apollo/saveSearch/remove.gql';
import SAVE_SEARCH_NEWCOMER from '~/api/apollo/saveSearch/addNewcomer.gql';

const saveSearchActions = {
    updateSaveSearch(
        {
            commit,
            dispatch,
            state: { token },
        },
        { id, isWeekly, weekDays, monthDays },
    ) {
        dispatch('requestStart', requestModules.SAVE_SEARCH);

        this.app
            .gqlRequest(SAVE_SEARCH_UPDATE, {
                token: token,
                id,
                isWeekly,
                dayOfWeek: weekDays.join(','),
                dayOfMonth: monthDays.join(','),
            })
            .then(response => {
                const {
                    data: { saveSearchUpdate },
                } = response;

                const { result, savedSearch } = saveSearchUpdate;

                commit('USER_SET_SAVED_SEARCH', savedSearch);
                dispatch('requestSucceed', {
                    ...result,
                    component: requestModules.SAVE_SEARCH,
                });
            })
            .catch(handleError(dispatch, requestModules.SAVE_SEARCH));
    },
    removeSearchSchedule(
        {
            commit,
            dispatch,
            state: { token },
        },
        id,
    ) {
        dispatch('requestStart', requestModules.SAVE_SEARCH);

        this.app
            .gqlRequest(SAVE_SEARCH_REMOVE, {
                token: token,
                id,
            })
            .then(response => {
                const {
                    data: { saveSearchRemove },
                } = response;

                commit('USER_REMOVE_SAVED_SEARCH', id);
                dispatch('requestSucceed', {
                    ...saveSearchRemove,
                    component: requestModules.SAVE_SEARCH,
                });
            })
            .catch(handleError(dispatch, requestModules.SAVE_SEARCH));
    },
    saveSearch(
        {
            commit,
            dispatch,
            state: { token },
        },
        { query },
    ) {
        dispatch('requestStart', requestModules.SAVE_SEARCH);

        this.app
            .gqlRequest(SAVE_SEARCH, {
                token: token,
                query: query,
                isWeekly: true,
                dayOfWeek: 'Thursday',
            })
            .then(response => {
                const {
                    data: { saveSearchAdd },
                } = response;

                const { result, savedSearch } = saveSearchAdd;

                commit('USER_SET_SAVED_SEARCH', savedSearch);
                dispatch('requestSucceed', {
                    ...result,
                    component: requestModules.SAVE_SEARCH,
                });
                commit('ENV_CHANGE_POPOVER_MODE', 'saveSearchSuccess');
            })
            .catch(handleError(dispatch, requestModules.SAVE_SEARCH));
    },
    saveSearchAndAddNewcomer({ commit, dispatch, state }, { query, email }) {
        dispatch('requestStart', requestModules.SAVE_SEARCH);

        this.app
            .gqlRequest(SAVE_SEARCH_NEWCOMER, {
                email: email,
                query: query,
                isWeekly: true,
                dayOfWeek: 'Thursday',
            })
            .then(result => {
                const {
                    data: { saveSearchAddNewcomer },
                } = result;
                const { token, user } = saveSearchAddNewcomer;

                commit('USER_SET_TOKEN', token);
                commit('USER_SET_SAVED_SEARCH', query);
                commit('ENV_CHANGE_POPOVER_MODE', 'saveSearchSuccess');
                dispatch('requestSucceed', {
                    message: 'message.saveSearchSuccess',
                    component: requestModules.SAVE_SEARCH,
                });
                gotUserInfo(commit, user);
            })
            .catch(handleError(dispatch, requestModules.SAVE_SEARCH));
    },
};

export default saveSearchActions;
