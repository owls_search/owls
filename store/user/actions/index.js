import saveSearchActions from './saveSearchActions';
import {
    userType,
    getTagsFromString,
    requestModules,
    handleError,
} from '~/util';
import { get } from '~/util/lodash';
import REGISTER_USER from '~/api/apollo/user/mutation/register.gql';
import DELETE_USER from '~/api/apollo/user/mutation/delete.gql';
import UPDATE_PASSWORD from '~/api/apollo/user/mutation/updatePassword.gql';
import UPDATE_ACCOUNT from '~/api/apollo/user/mutation/updateAccountInfo.gql';
import UPDATE_PROFILE from '~/api/apollo/user/mutation/updateProfileInfo.gql';
import LOGIN_USER from '~/api/apollo/user/query/login.gql';

export const gotTokenResult = (commit, data) => {
    const { token = '' } = data;
    commit('USER_SET_TOKEN', token);
};

export const gotUserInfo = (commit, data) => {
    const { user } = data;
    const responseData = {
        id: get(user, 'id'),
        offerType: get(user, 'offerType'),
        type: get(user, 'type'),
        fileName: get(user, 'fileName'),
        email: get(user, 'email'),
        name: get(user, 'name'),
        lastName: get(user, 'lastName'),
        partnerParam: get(user, 'partnerParam'),
        partnerValue: get(user, 'partnerValue'),
    };
    if (user.skills) {
        responseData.skills = getTagsFromString('skills', user.skills);
    }
    if (user.programmingLanguages) {
        responseData.programmingLanguages = getTagsFromString(
            'languages',
            user.programmingLanguages,
        );
    }
    if (user.titles) {
        responseData.titles = getTagsFromString('titles', user.titles);
    }
    if (user.locations) {
        responseData.locations = getTagsFromString(
            'destinations',
            user.locations,
        );
    }

    commit('USER_SET_USER_DATA', responseData);
    commit('USER_SET_SAVED_SEARCHES', user.savedSearches);
};

const actions = {
    ...saveSearchActions,
    registerUser(
        { dispatch, commit },
        { email, emailConfirmation, password, isApplicant },
    ) {
        dispatch('requestStart', requestModules.USER);
        this.app
            .gqlRequest(REGISTER_USER, {
                email,
                emailConfirmation,
                password,
                type: isApplicant ? userType.APPLICANT : userType.RECRUITER,
            })
            .then(result => {
                const {
                    data: { registerUser },
                } = result;
                gotTokenResult(commit, registerUser);
                gotUserInfo(commit, registerUser);
                dispatch('requestFinish', requestModules.USER);
            })
            .catch(handleError(dispatch, requestModules.USER));
    },
    loginUser({ dispatch, commit }, { email, password }) {
        dispatch('requestStart', requestModules.USER);

        this.app
            .gqlRequest(LOGIN_USER, {
                email,
                password,
            })
            .then(result => {
                const {
                    data: { loginUser },
                } = result;

                gotTokenResult(commit, loginUser);
                gotUserInfo(commit, loginUser);
                dispatch('afterLoginUser', loginUser);
                dispatch('requestFinish', requestModules.USER);
            })
            .catch(handleError(dispatch, requestModules.USER));
    },
    logoutUser({ dispatch, commit, rootState }, noRedirect = false) {
        const routeName = get(rootState, ['route', 'name'], '');

        commit('USER_RESET_TOKEN');

        if (!noRedirect && routeName.indexOf('cabinet') === 0) {
            dispatch('goToMainPage');
        }
    },
    updateUserAccount(
        {
            commit,
            dispatch,
            state: { token },
        },
        { userData, successMessage },
    ) {
        dispatch('requestStart', requestModules.USER);
        this.app
            .gqlRequest(UPDATE_ACCOUNT, {
                ...userData,
                token,
            })
            .then(result => {
                const {
                    data: { updateAccountInfo },
                } = result;
                gotUserInfo(commit, { user: updateAccountInfo });
                dispatch('requestSucceed', {
                    message: successMessage,
                    component: requestModules.USER,
                });
            })
            .catch(handleError(dispatch, requestModules.USER));
    },
    updateUserProfile(
        {
            commit,
            dispatch,
            state: { token },
        },
        { userData, successMessage },
    ) {
        dispatch('requestStart', requestModules.USER);
        this.app
            .gqlRequest(UPDATE_PROFILE, {
                ...userData,
                token,
            })
            .then(result => {
                const {
                    data: { updateProfileInfo },
                } = result;
                gotUserInfo(commit, { user: updateProfileInfo });
                dispatch('requestSucceed', {
                    message: successMessage,
                    component: requestModules.USER,
                });
            })
            .catch(handleError(dispatch, requestModules.USER));
    },
    deleteUser({
        dispatch,
        state: { token },
        rootGetters: { isAuthenticated },
    }) {
        if (!isAuthenticated) {
            dispatch('requestFailed', {
                message: 'There is no valid token',
                component: requestModules.USER,
            });
        }

        dispatch('requestStart', requestModules.USER);
        this.app
            .gqlRequest(DELETE_USER, {
                token,
            })
            .then(() => {
                dispatch('requestFinish', requestModules.USER);
                const noRedirect = true;
                dispatch('logoutUser', noRedirect);
                this.app.router.push(
                    this.app.localePath({ name: 'account-removed' }),
                );
            })
            .catch(handleError(dispatch, requestModules.USER));
    },
    updatePassword(
        {
            dispatch,
            state: { token },
            rootGetters: { isAuthenticated },
        },
        { password, newPassword, newPasswordConfirmation },
    ) {
        if (!isAuthenticated) {
            dispatch('requestFailed', {
                message: 'There is no valid token',
                component: requestModules.USER_PASSWORD,
            });
        }

        dispatch('requestStart', requestModules.USER_PASSWORD);

        this.app
            .gqlRequest(UPDATE_PASSWORD, {
                token,
                password,
                newPassword,
                newPasswordConfirmation,
            })
            .then(result => {
                const {
                    data: { updatePassword },
                } = result;
                dispatch('requestSucceed', {
                    ...updatePassword,
                    component: requestModules.USER_PASSWORD,
                });
            })
            .catch(handleError(dispatch, requestModules.USER));
    },
    setClientId({ commit }, clientId) {
        commit('USER_SET_CLIENT_ID', clientId);
    },
    setUserJobTotalValue({ commit }, value) {
        commit('USER_SET_JOB_TOTAL_VALUE', value);
    },
    viewOffer({ commit, state: { jobTotalValue } }) {
        commit('USER_SET_JOB_TOTAL_VALUE', jobTotalValue + 5);
    },
    setSessionStart({ commit }, startTimestamp) {
        commit('USER_SET_SESSION_START', startTimestamp);
    },
};

export default actions;
