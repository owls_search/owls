import {
    getStateSearchParams,
    mapFromGqlApiOffer,
    mapFromRemoteApiOffer,
} from '~/util';
import { get, property } from '~/util/lodash';
import { apiSubstituteForNotFound } from '~/api/search/substituteForNotFound';
import GET_POPULAR_OFFERS from '~/api/apollo/offers/query/popularOffers.gql';
import GET_POPULAR_OFFERS_CONFIG from '~/api/apollo/offers/query/popularOffersConfig.gql';

const state = () => ({
    title: '',
    substituteOffers: [],
    popularOffers: [],
    seoPages: [],
    citySeoPages: [],
});

const mutations = {
    POPULAR_OFFERS_SET_TITLE(state, title) {
        state.title = title;
    },
    POPULAR_OFFERS_SUBSTITUTE_OFFERS(state, substituteOffers) {
        state.substituteOffers = substituteOffers.map(mapFromRemoteApiOffer);
    },
    POPULAR_OFFERS_POPULAR_OFFERS(state, popularOffers) {
        state.popularOffers = popularOffers.map(mapFromGqlApiOffer);
    },
    POPULAR_OFFERS_SEO_PAGES(state, seoPages) {
        state.seoPages = seoPages;
    },
    POPULAR_OFFERS_CITY_SEO_PAGES(state, citySeoPages) {
        state.citySeoPages = citySeoPages;
    },
};

const getters = {
    offersToShow(state) {
        const { substituteOffers, popularOffers } = state;
        return substituteOffers.length > 0 ? substituteOffers : popularOffers;
    },
};

const actions = {
    setNotFoundOfferTitle({ commit }, title) {
        console.log('set not found');
        commit('POPULAR_OFFERS_SET_TITLE', title);
    },

    async getNotFoundSubstituteOffers({ commit, state = {}, rootState }) {
        const { title = '' } = state;

        if (!title) {
            return;
        }

        await apiSubstituteForNotFound(title, {
            ...getStateSearchParams(get(rootState, 'search', {})),
            page: 1,
            pageSize: 8,
            minBid: 0,
        })
            .then(result => {
                const { data = {} } = result;
                const { jobs = [] } = data;

                commit('POPULAR_OFFERS_SUBSTITUTE_OFFERS', jobs);
            })
            .catch();
    },

    async getPopularOffers({ commit }) {
        const currentLocale = this.app.i18n.locale;
        const homepageConfig = await this.app
            .gqlRequest(GET_POPULAR_OFFERS, { locale: currentLocale })
            .then(property(['data', 'getHomepageConfiguration']));

        const { recentOffers } = homepageConfig;
        commit('POPULAR_OFFERS_POPULAR_OFFERS', recentOffers);
    },
    async getPopularOffersSeoPages({ commit }) {
        const currentLocale = this.app.i18n.locale;
        const homepageConfig = await this.app
            .gqlRequest(GET_POPULAR_OFFERS_CONFIG, { locale: currentLocale })
            .then(property(['data', 'getHomepageConfiguration']));

        const { seoPages } = homepageConfig;
        commit(
            'POPULAR_OFFERS_SEO_PAGES',
            seoPages.filter(
                ({ programming = '' }) => programming && programming.length > 0,
            ),
        );
        commit(
            'POPULAR_OFFERS_CITY_SEO_PAGES',
            seoPages.filter(
                ({ programming = '' }) =>
                    !programming || programming.length === 0,
            ),
        );
    },
};

const popularOffers = {
    state,
    mutations,
    getters,
    actions,
};

export default popularOffers;
