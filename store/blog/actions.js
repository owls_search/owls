import GET_RECENT_POSTS from '~/api/apollo/blog/getRecentPosts.gql';
import GET_POST from '~/api/apollo/blog/getPost.gql';

const actions = {
    async blogLoadRecentPosts(
        {
            commit,
            state: { posts },
        },
        locale,
    ) {
        if (posts && posts.length > 0) return;

        await this.app
            .gqlRequest(GET_RECENT_POSTS, {
                locale,
            })
            .then(result => {
                const {
                    data: { getRecentPosts },
                } = result;

                commit('BLOG_SET_RECENT_POSTS', getRecentPosts);
            });
    },

    async blogLoadPost(
        {
            state: { posts },
            rootState,
            commit,
            app,
        },
        postCode,
    ) {
        if (posts[postCode] && posts[postCode].text) {
            commit('BLOG_SET_CURRENT_POST', posts[postCode]);
            return;
        }

        await this.app
            .gqlRequest(GET_POST, {
                code: postCode,
            })
            .then(result => {
                const {
                    data: { getPost },
                } = result;
                const post = getPost;

                if (!post) {
                    throw new Error('Post not found');
                }

                commit('BLOG_SET_CURRENT_POST', post);
            });
    },
};

export default actions;
