import actions from './actions';
import { head, drop } from '~/util/lodash';

const state = () => ({
    posts: {},
    recentPosts: [],
    currentPostCode: '',
    nextPostCode: '',
});

const cachePost = state => post => {
    if (
        !state.posts[post.code] ||
        !hasFullInformtaion(state.posts[post.code])
    ) {
        state.posts[post.code] = post;
    }
};

const hasFullInformtaion = post =>
    post.hasOwnProperty('text') && post.hasOwnProperty('nextPostCode');

const mutations = {
    BLOG_SET_RECENT_POSTS(state, posts) {
        posts.forEach(cachePost(state));
        state.recentPosts = posts.map(({ code }) => code);
    },
    BLOG_SET_CURRENT_POST(state, post) {
        cachePost(state)(post);
        state.currentPostCode = post.code;
        state.nextPostCode = post.nextPostCode || null;
    },
};

const getters = {
    blogFeaturedArticle(state) {
        const featuredPost = head(state.recentPosts);
        return state.posts[featuredPost];
    },
    blogRecentArticles(state) {
        return drop(state.recentPosts).map(code => state.posts[code]);
    },
};

const blog = {
    state,
    mutations,
    getters,
    actions,
};

export default blog;
