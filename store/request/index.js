import { requestModules } from '~/util';

const initialResponsesModules = Object.values(requestModules).reduce(
    (modulesObj, moduleName) => {
        modulesObj[moduleName] = {
            isSuccess: false,
            isRequestLoading: false,
            response: {},
        };

        return modulesObj;
    },
    {},
);

const state = () => ({
    responses: initialResponsesModules,
});

const actions = {
    resetLastResponseMessage({ commit }, component) {
        commit('REQUEST_RESET_RESPONSE_MESSAGE', component);
    },
    requestStart({ commit }, component) {
        commit('REQUEST_SET_IS_LOADING', {
            component,
            value: true,
        });
    },
    requestFinish({ commit }, component) {
        commit('REQUEST_SET_IS_LOADING', {
            component,
            value: false,
        });
    },
    requestFailed({ commit }, { message, details, component }) {
        commit('REQUEST_FAILED', { message, details, component });
    },
    requestSucceed({ commit }, { message, details, component }) {
        commit('REQUEST_SUCCEED', { message, details, component });
    },
};

const mutations = {
    REQUEST_SET_IS_LOADING(state, { component, value }) {
        state.responses[component] = {
            ...state.responses[component],
            isRequestLoading: value,
        };
    },
    REQUEST_FAILED(state, { message, details, component }) {
        state.responses[component] = {
            ...state.responses[component],
            isSuccess: false,
            isRequestLoading: false,
            response: { message, details },
        };
    },
    REQUEST_SUCCEED(state, { message, details, component }) {
        state.responses[component] = {
            ...state.responses[component],
            isSuccess: true,
            isRequestLoading: false,
            response: { message, details },
        };
    },
    REQUEST_RESET_RESPONSE_MESSAGE(state, component) {
        state.responses[component] = {
            ...state.responses[component],
            response: null,
        };
    },
};

const request = {
    state,
    actions,
    mutations,
};

export default request;
