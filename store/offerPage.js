import { property, isObject } from '~/util/lodash';
import GET_OFFER from '~/api/apollo/offers/query/getOffer.gql';

const state = () => ({
    offerData: {},
    offerCode: null,
    similarOffers: [],
    popularJobs: [],
});

const mutations = {
    OFFER_PAGE_SET_OFFER_CODE(state, code) {
        state.offerCode = code;
    },
    OFFER_PAGE_SET_DATA(state, offerData) {
        state.offerData = offerData;
    },
    OFFER_PAGE_SET_SIMILAR_OFFERS(state, similarOffers = []) {
        state.similarOffers = similarOffers;
    },
    OFFER_PAGE_SET_POPULAR_JOBS(state, popularJobs = []) {
        state.popularJobs = popularJobs;
    },
};

const actions = {
    loadOfferPage({ commit }, id) {
        return new Promise((resolve, reject) =>
            this.app
                .gqlRequest(GET_OFFER, {
                    id,
                })
                .then(property(['data', 'offer']))
                .then(offer => {
                    if (!isObject(offer) && !offer)
                        throw new Error('Offer not found');
                    resolve(offer);
                })
                .catch(err => reject(err)),
        );
    },

    offerSetRawOfferCode({ commit }, rawOfferCode) {
        commit('OFFER_PAGE_SET_OFFER_CODE', rawOfferCode);
    },

    setOfferPageData({ commit }, data) {
        commit('OFFER_PAGE_SET_DATA', data);
    },

    setSimilarOffers({ commit }, similarOffers = []) {
        commit('OFFER_PAGE_SET_SIMILAR_OFFERS', similarOffers);
    },

    setPopularJobs({ commit }, popularJobs = []) {
        const currentLocale = this.app.i18n.locale;
        commit(
            'OFFER_PAGE_SET_POPULAR_JOBS',
            popularJobs.filter(
                ({ programming, language }) =>
                    programming && language === currentLocale,
            ),
        );
    },
};

const offerData = {
    state,
    mutations,
    actions,
};

export default offerData;
