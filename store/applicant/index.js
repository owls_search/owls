import { flow, get, property } from '~/util/lodash';
import {
    requestModules,
    blobToFile,
    mapTagsToString,
    getStateSearchParams,
    mapFromRemoteApiOffer,
} from '~/util';
import { apiProfileSearch } from '~/api/search';
import { apiGetFile } from '~/api/file/get';

const state = () => ({
    offers: [],
    offersNumber: 0,
});

const actions = {
    async applicantProfileSearch(
        { rootState, commit, dispatch },
        { pageSize, page },
    ) {
        dispatch('requestStart', requestModules.RELEVANT_OFFERS);

        const fileName = get(rootState, ['user', 'userData', 'fileName'], null);
        const titles = get(rootState, ['user', 'userData', 'titles'], []);
        const skills = get(rootState, ['user', 'userData', 'skills'], []);
        const programmingLanguages = get(
            rootState,
            ['user', 'userData', 'programmingLanguages'],
            [],
        );
        const locations = get(rootState, ['user', 'userData', 'locations'], []);

        const offerLanguages = get(
            rootState,
            ['user', 'search', 'offerLanguages'],
            [],
        );
        const offerLanguage = get(
            rootState,
            ['user', 'search', 'offerLanguage'],
            '',
        );

        const loadedFile =
            fileName && fileName.length
                ? await apiGetFile(this.fileName).then(
                      flow(
                          property('data'),
                          blobToFile,
                      ),
                  )
                : '';

        const data = await apiProfileSearch({
            file: loadedFile,
            titles: mapTagsToString(titles),
            skills: mapTagsToString(skills),
            programmingLanguages: mapTagsToString(programmingLanguages),
            locations: mapTagsToString(locations),
            ...getStateSearchParams({
                pageSize: pageSize,
                currentPage: page,
                offerLanguages: offerLanguages,
                offerLanguage: offerLanguage,
            }),
        }).then(property('data'));

        const { jobs, number_of_jobs = 0 } = data; // eslint-disable-line camelcase

        dispatch('requestFinish', requestModules.RELEVANT_OFFERS);
        commit('APPLICANT_PROFILE_SEARCH_SET_OFFERS_NUMBER', number_of_jobs); // eslint-disable-line camelcase
        commit(
            'APPLICANT_PROFILE_SEARCH_SET_OFFERS',
            jobs ? jobs.map(mapFromRemoteApiOffer) : [],
        );
    },
};

const mutations = {
    APPLICANT_PROFILE_SEARCH_SET_OFFERS(state, offers) {
        state.offers = offers;
    },
    APPLICANT_PROFILE_SEARCH_SET_OFFERS_NUMBER(state, offersNumber) {
        state.offersNumber = offersNumber;
    },
};

const applicantCabinet = {
    state,
    mutations,
    actions,
};

export default applicantCabinet;
