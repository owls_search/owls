export const gotRecognisedTagsResults = commit => result => {
    const { data = {} } = result;
    const { tags = [] } = data;
    commit('SEARCH_SET_RECOGNISED_TAGS', tags);
};

export const getKeywordsText = tags =>
    (tags && tags.map(tag => (tag && tag.text) || '').join(' ')) || '';

export const getSearchRoutePathParams = (
    searchPath,
    offerLanguage = '',
    page = 1,
) => {
    const routeParams = {
        name:
            `${offerLanguage.length > 0 ? 'offerLanguage-' : ''}` +
            `${searchPath}` +
            `${page > 1 ? '-p-page' : ''}`,
    };
    const pageParams = {};

    if (page > 1) pageParams.page = page;
    if (offerLanguage.length > 0) pageParams.offerLanguage = offerLanguage;
    routeParams.params = pageParams;
    return routeParams;
};

export const offerLanguages = {
    en: {
        code: 'en',
        title: 'English',
        param: 'en',
        urlParam: 'job-en',
    },
    de: {
        code: 'de',
        title: 'Deutsch',
        param: 'de',
        urlParam: 'job-de',
    },
    all: {
        code: 'en+de',
        title: 'Both',
        param: 'all',
        urlParam: '',
    },
};
