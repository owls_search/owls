import { getSearchRoutePathParams } from '../utils';

const actions = {
    goToMainPage() {
        this.app.router.push(this.app.localePath({ name: 'index' }));
    },
    goToFilePage({ commit, state }, params = {}) {
        const { page = 1 } = params;
        const { offerLanguage, offerLanguages } = state;
        const routeParams = getSearchRoutePathParams(
            'search-by-cv',
            offerLanguages.hasOwnProperty(offerLanguage)
                ? offerLanguages[offerLanguage].urlParam
                : '',
            page,
        );
        this.app.router.push(this.app.localePath(routeParams));
    },
    async goToSearchPage({ commit, state }, params = {}) {
        const { query = '', page = 1 } = params;
        const { offerLanguage, offerLanguages } = state;
        const routeParams = getSearchRoutePathParams(
            'search-query',
            offerLanguages.hasOwnProperty(offerLanguage)
                ? offerLanguages[offerLanguage].urlParam
                : '',
            page,
        );

        if (query && query.length > 0) routeParams.params.query = query;
        await this.app.router.push(this.app.localePath(routeParams));
    },
    async goToPopularJobPage({ commit, state }, params = {}) {
        const { code = '', page = 1 } = params;
        const routeParams = getSearchRoutePathParams(
            'search-popular-code',
            '',
            page,
        );

        if (code.length > 0) routeParams.params.code = code;
        await this.app.router.push(this.app.localePath(routeParams));
    },
};

export default actions;
