import { getKeywordsText } from '../utils';
import { getTagsFromRemote } from '~/util';
import { get } from '~/util/lodash';

const actions = {
    gotServerResults({ commit }, { offers, keywords, offersNumber }) {
        commit('SEARCH_GOT_SEARCH_RESULTS', {
            offers,
            keywords,
            offersNumber,
        });
    },
    processResults({ commit, dispatch }, data = {}) {
        if (!data || typeof data === 'undefined') {
            commit('SEARCH_SET_PAGE', 1);
            dispatch('searchformInputReset');
            dispatch('goToSearchPage');
            return;
        }

        const { jobs, tags = {}, number_of_jobs = 0 } = data; // eslint-disable-line camelcase

        const keywords = getTagsFromRemote(tags);
        commit('SEARCH_GOT_SEARCH_RESULTS', {
            offers: jobs,
            keywords,
            offersNumber: number_of_jobs, // eslint-disable-line camelcase
        });
    },
    appendResults(
        {
            commit,
            dispatch,
            state: { query, currentPage, offers },
        },
        data = {},
    ) {
        if (!data || typeof data === 'undefined') {
            return;
        }

        const { jobs } = data; // eslint-disable-line camelcase
        commit('OFFERS_LIST_APPEND', jobs);
    },
    updateSearchUrl(
        {
            dispatch,
            state: { query, currentPage },
        },
        data = {},
    ) {
        if (process.server) return;
        const { tags = {} } = data; // eslint-disable-line camelcase

        const keywords = getTagsFromRemote(tags);
        const currentQuery = keywords.length
            ? getKeywordsText(keywords)
            : query;

        dispatch('goToSearchPage', {
            query: currentQuery,
            page: currentPage,
        });
    },
    updatePopularJobUrl({ dispatch, state: { currentPage }, rootState }) {
        if (process.server) return;

        const code = get(rootState, ['offersList', 'seoPageCode'], '');

        dispatch('goToPopularJobPage', {
            code: code,
            page: currentPage,
        });
    },
};

export default actions;
