import searchActions from './search';
import keywordsActions from './keywords';
import afterTextSearchActions from './afterTextSearch';
import routeActions from './route';
import { elClasses, scrollToElement, waitTillElementAppears } from '~/util';

const actions = {
    ...searchActions,
    ...keywordsActions,
    ...afterTextSearchActions,
    ...routeActions,
    searchformInputChange({ commit }, query) {
        commit('SEARCH_UPDATE_QUERY', query);
        commit('SSG_REQUEST_STARTED');
    },
    searchformInputReset({ commit }) {
        commit('SEARCH_RESET_QUERY');
    },
    updateFileSearchUrl({ dispatch, state: { currentPage } }) {
        if (process.server) return;
        dispatch('goToFilePage', {
            page: currentPage,
        });
    },
    scrollAfterSearch() {
        if (process.server) return;
        // Wait till keywords appears and scroll to search form
        const keywordsEl = document.querySelector(
            `.${elClasses.searchFormKeywords}`,
        );
        const searchFormEl = document.querySelector(
            `.${elClasses.searchFormKeywords}`,
        );
        const resultsPageEl = document.querySelector(
            `.${elClasses.resultsPage}`,
        );
        if (keywordsEl) scrollToElement(searchFormEl);
        else if (resultsPageEl)
            waitTillElementAppears(resultsPageEl, elClasses.keywords, () =>
                scrollToElement(
                    document.querySelector(`.${elClasses.searchFormKeywords}`),
                ),
            );
    },
};

export default actions;
