const keywordsActions = {
    searchResetKeywords({ commit }) {
        commit('SEARCH_RESET_KEYWORDS');
        commit('SEARCH_SET_PAGE', 1);
        commit('SEARCH_RESET_FILE');
        commit('SEARCH_RESET_SEARCH_RESULTS');
    },
    searchUpdateKeywords({ commit, dispatch }, keywords) {
        commit('SEARCH_UPDATE_KEYWORDS', keywords);
    },
};

export default keywordsActions;
