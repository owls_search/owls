import { getKeywordsText } from '../utils';
import { apiSearch, apiFileSearch } from '~/api/search';
import { getStateSearchParams } from '~/util';
import { get, isObject, property } from '~/util/lodash';

const abortSearch = dispatch => _ => {
    const data = { jobs: [], tags: {}, number_of_offers: 0 }; // eslint-disable-line camelcase
    dispatch('processResults', data);
    dispatch('scrollAfterSearch');
};

const gotSearchResults = dispatch => data => {
    dispatch('processResults', data);
};

const updateRouteAfterSearch = (
    dispatch,
    updateAction = 'updateSearchUrl',
) => data => {
    dispatch(updateAction, data);
    dispatch('scrollAfterSearch');
};

const appendSearchResults = dispatch => result => {
    dispatch('appendResults', result);
};

const goToRouteBeforeSearch = async (
    app,
    route,
    dispatch,
    currentRoute,
    currentQuery,
) => {
    if (isObject(route)) {
        await app.router.push(app.localePath(route));
        return;
    }

    const isSearchPage =
        currentRoute.includes('search-by-cv') ||
        currentRoute.includes('search-query');
    if (!isSearchPage) {
        await dispatch('goToSearchPage', {
            query: currentQuery,
        });
    }
};

const searchActions = {
    async search(
        { commit, dispatch, state, rootState },
        { query, route = null },
    ) {
        commit('SEARCH_RESET_FILE');
        commit('SEARCH_SET_PAGE', 1);
        commit('SEARCH_UPDATE_QUERY', query);

        await goToRouteBeforeSearch(
            this.app,
            route,
            dispatch,
            get(rootState, ['route', 'name'], ''),
            query.toLowerCase(),
        );

        if (query && query.trim().length > 0) {
            commit('SEARCH_START_SEARCH');
            apiSearch(query, getStateSearchParams(state))
                .then(property('data'))
                .then(gotSearchResults(dispatch))
                .then(data => !route && updateRouteAfterSearch(dispatch)(data))
                .catch(abortSearch(dispatch));
        }
    },
    fileSearch({ commit, dispatch, state }, file) {
        commit('SEARCH_SET_FILE', file);
        dispatch('searchformInputReset');
        commit('SEARCH_SET_PAGE', 1);
        commit('SEARCH_START_SEARCH');
        dispatch('updateFileSearchUrl');
        apiFileSearch(file, getStateSearchParams(state))
            .then(property('data'))
            .then(gotSearchResults(dispatch))
            .catch(abortSearch(dispatch));
    },
    searchByKeywords({ commit, dispatch, state }) {
        const { keywords } = state;
        const queryText = getKeywordsText(keywords);
        commit('SEARCH_START_SEARCH');
        commit('SEARCH_RESET_FILE');
        commit('SEARCH_UPDATE_QUERY', queryText);
        commit('SEARCH_SET_PAGE', 1);
        apiSearch(queryText, getStateSearchParams(state))
            .then(property('data'))
            .then(gotSearchResults(dispatch))
            .then(updateRouteAfterSearch(dispatch))
            .catch(abortSearch(dispatch));
    },
    changePage({ commit, dispatch, state, rootState }, page) {
        const { query, file } = state;
        commit('SEARCH_SET_PAGE', page);
        commit('SEARCH_START_SEARCH');
        const routeName = get(rootState, ['route', 'name'], '');

        const requestPromise = routeName.includes('search-by-cv')
            ? apiFileSearch(file, getStateSearchParams(state))
            : apiSearch(query, getStateSearchParams(state));

        let actionToGoToPage = 'updateSearchUrl';

        if (routeName.includes('search-popular')) {
            actionToGoToPage = 'updatePopularJobUrl';
        }

        if (routeName.includes('search-by-cv')) {
            actionToGoToPage = 'updateFileSearchUrl';
        }

        requestPromise
            .then(property('data'))
            .then(gotSearchResults(dispatch))
            .then(updateRouteAfterSearch(dispatch, actionToGoToPage))
            .catch(abortSearch(dispatch));
    },
    appendOffers({ commit, dispatch, state, rootState }, page) {
        const { query, file } = state;
        commit('SEARCH_SET_PAGE', page);
        commit('SEARCH_START_SEARCH');
        const routeName = get(rootState, ['route', 'name'], '');

        const requestPromise = routeName.includes('search-by-cv')
            ? apiFileSearch(file, getStateSearchParams(state))
            : apiSearch(query, getStateSearchParams(state));

        requestPromise
            .then(property('data'))
            .then(appendSearchResults(dispatch))
            .catch(abortSearch(dispatch));
    },
    changeCurrentOfferLanguage(
        { commit, dispatch, state, rootState },
        newJobLanguage,
    ) {
        const { query, file } = state;
        commit('SEARCH_SET_CURRENT_JOB_LANGUAGE', newJobLanguage);
        commit('SEARCH_START_SEARCH');
        commit('ENV_RESET_POPOVER_MODE');
        const routeName = get(rootState, ['route', 'name'], '');

        const defaultQuery = 'fullstack PHP berlin';

        const requestPromise = routeName.includes('search-by-cv')
            ? apiFileSearch(file, getStateSearchParams(state))
            : apiSearch(query || defaultQuery, getStateSearchParams(state));

        requestPromise
            .then(property('data'))
            .then(gotSearchResults(dispatch))
            .then(updateRouteAfterSearch(dispatch))
            .catch(abortSearch(dispatch));
    },
    serverSearch({ commit, dispatch, state }, params) {
        const { query = '', page = 1 } = params;
        commit('SEARCH_START_SEARCH');
        return new Promise(resolve =>
            query && query.trim().length > 0
                ? apiSearch(query, {
                      ...getStateSearchParams(state),
                      page,
                  })
                      .then(property('data'))
                      .then(resolve)
                      .catch(() =>
                          resolve({ jobs: [], tags: {}, number_of_offers: 0 }),
                      )
                : resolve({ jobs: [], tags: {}, number_of_offers: 0 }),
        );
    },
    serverSetQuery({ commit }, query) {
        commit('SEARCH_UPDATE_QUERY', query);
    },
    setPage({ commit }, page = 1) {
        commit('SEARCH_SET_PAGE', page);
    },
};

export default searchActions;
