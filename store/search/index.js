import actions from './actions';
import { offerLanguages } from './utils';

const state = () => ({
    offerLanguages,
    offerLanguage: 'en+de',
    query: '',
    file: undefined,
    keywords: [],
    currentPage: 1,
    pageSize: 15,
    minBid: 0,
    partnerFilter: null,
    recognisedSearchTags: [],
});

const mutations = {
    SEARCH_RESET_FILE(state) {
        state.file = undefined;
    },
    SEARCH_SET_FILE(state, file) {
        state.file = file;
    },
    SEARCH_RESET_QUERY(state) {
        state.query = '';
    },
    SEARCH_UPDATE_QUERY(state, query) {
        state.query = query;
    },
    SEARCH_UPDATE_KEYWORDS(state, keywords) {
        state.keywords = keywords;
    },
    SEARCH_RESET_KEYWORDS(state) {
        state.keywords = [];
    },
    SEARCH_GOT_SEARCH_RESULTS(state, processedData = {}) {
        const { keywords = [] } = processedData;
        state.keywords = keywords;
    },
    SEARCH_RESET_SEARCH_RESULTS(state) {
        state.keywords = [];
    },
    SEARCH_SET_PAGE(state, page) {
        state.currentPage = page;
    },
    SEARCH_SET_RECOGNISED_TAGS(state, tags) {
        state.recognisedSearchTags = tags;
    },
    SEARCH_SET_CURRENT_JOB_LANGUAGE(state, offerLanguage) {
        state.offerLanguage = offerLanguage;
    },
    SEARCH_SET_MIN_BID(state, minBid) {
        state.minBid = minBid;
    },
    SEARCH_PARTNER_FILTER(state, partnerIds) {
        state.partnerFilter = partnerIds;
    },
};

const getters = {
    hasNextPage(state, getters, rootState) {
        const { pageSize, currentPage } = state;
        const {
            offersList: { offersNumber },
        } = rootState;

        if (!offersNumber) {
            return false;
        }

        return currentPage * pageSize < offersNumber;
    },
    hasPrevPage(state) {
        const { currentPage } = state;
        return currentPage > 1;
    },
};

const search = {
    state,
    mutations,
    getters,
    actions,
};

export default search;
