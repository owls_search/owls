import { mapFromRemoteApiOffer } from '~/util';
import GET_SEO_PAGE from '~/api/apollo/seoPage.gql';
import { property } from '~/util/lodash';

const state = () => ({
    offers: [],
    offersNumber: 0,
    offersLoading: false,
    seoContent: '',
    seoPageCode: '',
});

const mutations = {
    SEARCH_GOT_SEARCH_RESULTS(state, processedData = {}) {
        const { offers = [], offersNumber } = processedData;
        state.offers = offers.map(mapFromRemoteApiOffer);
        state.offersNumber = offersNumber;
        state.offersLoading = false;
    },
    OFFERS_LIST_APPEND(state, offers) {
        state.offers = state.offers.concat(offers.map(mapFromRemoteApiOffer));
        state.offersLoading = false;
    },
    OFFERS_SET_SEO_CONTENT(state, content) {
        state.seoContent = content;
    },
    OFFERS_SET_SEO_CODE(state, code) {
        state.seoPageCode = code;
    },
    SEARCH_RESET_SEARCH_RESULTS(state) {
        state.offers = [];
        state.offersNumber = 0;
    },
    SEARCH_START_SEARCH(state) {
        state.offersLoading = true;
    },
};

const actions = {
    getSeoPage({ commit, dispatch, state }, code) {
        return new Promise(resolve =>
            code && code.trim().length > 0
                ? this.app
                      .gqlRequest(GET_SEO_PAGE, { code })

                      .then(property(['data', 'getSeoPage']))
                      .then(resolve)
                      .catch(_ => {
                          resolve({});
                      })
                : resolve({}),
        );
    },
    setSeoPageContent({ commit }, content) {
        commit('OFFERS_SET_SEO_CONTENT', content);
    },
    setSeoPageCode({ commit }, code) {
        commit('OFFERS_SET_SEO_CODE', code);
    },
};

const offersList = {
    state,
    actions,
    mutations,
};

export default offersList;
