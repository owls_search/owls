import ADD_FEEDBACK from '~/api/apollo/addFeedback.gql';
import { handleError, requestModules } from '~/util';
import { get } from '~/util/lodash';

const actions = {
    sendFeedback({ dispatch, commit }, feedbackFormData) {
        dispatch('requestStart', requestModules.FEEDBACK_FORM);

        this.app
            .gqlRequest(ADD_FEEDBACK, feedbackFormData)
            .then(result => {
                const addFeedback = get(result, ['data', 'addFeedback'], {});
                dispatch('requestSucceed', {
                    ...addFeedback,
                    component: requestModules.FEEDBACK_FORM,
                });
            })
            .catch(handleError(dispatch, requestModules.FEEDBACK_FORM));
    },
};

const feedback = {
    state: () => {},
    actions,
    mutations: {},
};

export default feedback;
