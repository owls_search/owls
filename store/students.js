import { gotTokenResult, gotUserInfo } from './user/actions/index.js';
import { apiSearch } from '~/api/search';
import {
    getStateSearchParams,
    mapFromRemoteApiOffer,
    userType,
    requestModules,
    handleError,
} from '~/util';
import { get } from '~/util/lodash';
import REGISTER_USER from '~/api/apollo/user/mutation/register.gql';

const state = () => ({
    studentsJobs: [],
    currentSearchQuery: '',
    originalSearchQuery: '',
});

const mutations = {
    STUDENTS_PAGE_SET_JOBS(state, jobs) {
        state.studentsJobs = jobs.map(mapFromRemoteApiOffer);
    },
    STUDENTS_PAGE_SET_ORIGINAL_QUERY(state, query) {
        state.originalSearchQuery = query;
    },
    STUDENTS_PAGE_SET_SEARCH_QUERY(state, currentSearchQuery) {
        state.currentSearchQuery = currentSearchQuery;
    },
};

const actions = {
    async getStudentsJobs(
        {
            commit,
            dispatch,
            rootState,
            state: { currentSearchQuery = '', originalSearchQuery = '' },
        },
        { isStudent, query },
    ) {
        if (query && !originalSearchQuery) {
            commit('STUDENTS_PAGE_SET_ORIGINAL_QUERY', query);
        }

        const searchQuery = query || originalSearchQuery || 'data scientist';
        const listQuery = isStudent
            ? `werkstudent ${searchQuery}`
            : searchQuery;
        commit('STUDENTS_PAGE_SET_SEARCH_QUERY', listQuery);
        dispatch('requestStart', requestModules.STUDENTS);
        await apiSearch(listQuery, {
            ...getStateSearchParams(get(rootState, 'search', {})),
            page: 1,
            pageSize: 5,
            minBid: 0.1,
            partnerFilter: null,
        })
            .then(result => {
                const { data = {} } = result;
                const { jobs = [] } = data;
                dispatch('requestFinish', requestModules.STUDENTS);
                commit('STUDENTS_PAGE_SET_JOBS', jobs);
            })
            .catch(e => {
                dispatch('requestFinish', requestModules.STUDENTS);
            });
    },
    registerStudent(
        {
            dispatch,
            commit,
            state: { currentSearchQuery = '' },
        },
        { email, emailConfirmation, password },
    ) {
        dispatch('requestStart', requestModules.USER);
        this.app
            .gqlRequest(REGISTER_USER, {
                email,
                emailConfirmation,
                password,
                type: userType.APPLICANT,
            })
            .then(result => {
                const {
                    data: { register },
                } = result;
                gotTokenResult(commit, register);
                gotUserInfo(commit, register);
                dispatch('search', {
                    query: currentSearchQuery,
                });
            })
            .catch(handleError(dispatch, requestModules.USER));
    },
};

const students = {
    state,
    mutations,
    actions,
};

export default students;
