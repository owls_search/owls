import { mapFromGqlApiOffer } from '~/util';
import { property } from '~/util/lodash';
import GET_HOMEPAGE_CONFIG from '~/api/apollo/offers/query/homepageConfig.gql';

const state = () => ({
    list: [],
    seoPages: [],
    citySeoPages: [],
});

const mutations = {
    HOMEPAGE_SET_RECENTLY_ADDED(state, recentlyAdded) {
        state.list = recentlyAdded.map(mapFromGqlApiOffer);
    },
    HOMEPAGE_SET_SEO_PAGES(state, seoPages) {
        state.seoPages = seoPages;
    },
    HOMEPAGE_SET_CITY_SEO_PAGES(state, citySeoPages) {
        state.citySeoPages = citySeoPages;
    },
};

const actions = {
    async loadHomepageConfiguration({ commit }) {
        const currentLocale = this.app.i18n.locale;
        const homepageConfig = await this.app
            .gqlRequest(GET_HOMEPAGE_CONFIG, { locale: currentLocale })
            .then(property(['data', 'getHomepageConfiguration']));

        const { recentOffers, seoPages } = homepageConfig;

        commit('HOMEPAGE_SET_RECENTLY_ADDED', recentOffers);
        commit(
            'HOMEPAGE_SET_SEO_PAGES',
            seoPages.filter(
                ({ programming = '' }) => programming && programming.length > 0,
            ),
        );
        commit(
            'HOMEPAGE_SET_CITY_SEO_PAGES',
            seoPages.filter(
                ({ programming = '' }) =>
                    !programming || programming.length === 0,
            ),
        );
    },
};

const homepageConfiguration = {
    state,
    actions,
    mutations,
};

export default homepageConfiguration;
