const state = () => ({ currentLetter: 'A', currentTitle: null });

const mutations = {
    ALPHABET_SET_TITLE(state, title) {
        state.currentTitle = title;
    },
    ALPHABET_SET_LETTER(state, letter) {
        state.currentLetter = letter;
    },
};

const actions = {
    alphabetSetTitle({ commit }, title) {
        commit('ALPHABET_SET_TITLE', title);
    },
    alphabetSetLetter({ commit }, letter) {
        commit('ALPHABET_SET_LETTER', letter);
    },
};

const alphabet = {
    state,
    mutations,
    actions,
};

export default alphabet;
