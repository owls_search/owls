const state = () => ({
    links: [],
});

const mutations = {
    HOMEPAGE_LINKS_SET(state, links) {
        state.links = links;
    },
};

const homepageLinks = {
    state,
    mutations,
};

export default homepageLinks;
