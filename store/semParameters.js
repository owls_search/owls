const state = () => ({
    network: null,
    campaign: null,
    keywords: null,
});

const actions = {
    setSemNetwork({ commit }, network) {
        commit('SEM_PARAMETERS_SET_NETWORK', network);
    },
    setSemCampaign({ commit }, campaign) {
        commit('SEM_PARAMETERS_SET_CAMPAIGN', campaign);
    },
    setSemKeywords({ commit }, keywords) {
        commit('SEM_PARAMETERS_SET_KEYWORDS', keywords);
    },
};

const mutations = {
    SEM_PARAMETERS_SET_NETWORK(state, network) {
        state.network = network;
    },
    SEM_PARAMETERS_SET_CAMPAIGN(state, campaign) {
        state.campaign = campaign;
    },
    SEM_PARAMETERS_SET_KEYWORDS(state, keywords) {
        state.keywords = keywords;
    },
};

const semParameters = {
    state,
    actions,
    mutations,
};

export default semParameters;
