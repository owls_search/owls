import uniqid from 'uniqid';
import Vue from 'vue';
import Vuex from 'vuex';
import alphabet from './alphabet';
import logging from './logging';
import metrics from './metrics';
import search from './search';
import suggestions from './suggestions';
import offersList from './offersList';
import offerPage from './offerPage';
import popularOffers from './popularOffers';
import feedback from './feedback';
import environment from './environment';
import homepageConfiguration from './homepageConfiguration';
import homepageLinks from './homepageLinks';
import students from './students';
import request from './request';
import user from './user';
import recruiter from './recruiter';
import applicantCabinet from './applicant';
import semParameters from './semParameters';
import seoParameters from './seoParameters';
import blog from './blog';
import { get } from '~/util/lodash';

Vue.use(Vuex);

const assignUserWithClientId = ({ rootStore, dispatch }) => {
    let clientId = get(rootStore, ['user', 'clientId'], null);
    if (!clientId) {
        clientId = uniqid();
        dispatch('setClientId', clientId);
    }
};

const saveSessionStartTimestamp = ({ dispatch }) => {
    dispatch('setSessionStart', Date.now());
};

const setUserTotalJobValue = ({ dispatch }) => {
    dispatch('setUserJobTotalValue', 0);
};

const getUserUrl = req => get(req, ['url'], '');
const getUrl = req => {
    const baseURL = `https://${get(
        req,
        ['headers', 'host'],
        'clusterjobs.de',
    )}/`;

    let url;
    if (process.server) {
        const URL = require('url').URL;
        url = new URL(get(req, ['url'], ''), baseURL);
    } else {
        url = new URL(get(req, ['url'], ''), baseURL);
    }

    return url;
};

const saveUserLandingParameters = ({ dispatch }, req) => {
    if (getUserUrl(req).length === 0) {
        dispatch('setSemNetwork', '');
        dispatch('setSemCampaign', '');
        dispatch('setSemKeywords', '');
        dispatch('setSeoCampaign', '');
        dispatch('setSeoSource', '');
        dispatch('setSeoMedium', '');
        return;
    }

    /* eslint-disable camelcase */
    const currentParamValues = {
        campaign: null,
        sem_network: null,
        sem_keyword: null,
        utm_campaign: null,
        utm_source: null,
        utm_medium: null,
    };

    Object.keys(currentParamValues).forEach(param => {
        currentParamValues[param] = getUrl(req).searchParams.get(param);
    });

    dispatch('setSemNetwork', currentParamValues.sem_network);
    dispatch('setSemCampaign', currentParamValues.campaign);
    dispatch('setSemKeywords', currentParamValues.sem_keyword);
    dispatch('setSeoCampaign', currentParamValues.utm_campaign);
    dispatch('setSeoSource', currentParamValues.utm_source);
    dispatch('setSeoMedium', currentParamValues.utm_medium);
    /* eslint-enable camelcase */
};

const store = () =>
    new Vuex.Store({
        actions: {
            async nuxtServerInit(app, { req }) {
                await assignUserWithClientId(app);
                await saveSessionStartTimestamp(app);
                await setUserTotalJobValue(app);
                await saveUserLandingParameters(app, req);
            },
        },

        modules: {
            alphabet,
            recruiter,
            applicantCabinet,
            semParameters,
            seoParameters,
            request,
            search,
            suggestions,
            offersList,
            offerPage,
            popularOffers,
            feedback,
            environment,
            homepageConfiguration,
            homepageLinks,
            students,
            user,
            blog,
            metrics,
            logging,
        },
    });
export default store;
