import { getCommonDetails } from './index';
import { DETAILS, EVENT_TYPES } from '~/helpers/log';
import { get } from '~/util/lodash';

export const saveSearchLogging = {
    saveSearch({ rootState }, { query }) {
        this.app.log(EVENT_TYPES.SAVE_SEARCH_AUTHORIZED, {
            [DETAILS.SEARCH_QUERY]: query,
            [DETAILS.PAGINATION]: get(
                rootState,
                ['search', 'currentPage'],
                null,
            ),
            ...getCommonDetails(rootState, this.app.i18n),
        });
    },
    saveSearchAndAddNewcomer({ rootState }, { query }) {
        this.app.log(EVENT_TYPES.SAVE_SEARCH_NEWCOMER, {
            [DETAILS.SEARCH_QUERY]: query,
            [DETAILS.PAGINATION]: get(
                rootState,
                ['search', 'currentPage'],
                null,
            ),
            ...getCommonDetails(rootState, this.app.i18n),
        });
    },
};
