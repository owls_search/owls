import { getCommonDetails } from './index';
import { EVENT_TYPES } from '~/helpers/log';
import { userType } from '~/util';

export const userLogging = {
    registerUser({ rootState }, { isApplicant }) {
        this.app.log(
            isApplicant
                ? EVENT_TYPES.APPLICANT_REGISTER
                : EVENT_TYPES.RECRUITER_REGISTER,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
    afterLoginUser({ rootState }, { type }) {
        this.app.log(
            type === userType.APPLICANT
                ? EVENT_TYPES.APPLICANT_LOGIN
                : EVENT_TYPES.RECRUITER_LOGIN,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
    addOffer({ rootState }) {
        this.app.log(
            EVENT_TYPES.RECRUITER_ADD_JOB,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
    updateOffer({ rootState }) {
        this.app.log(
            EVENT_TYPES.RECRUITER_UPDATE_JOB,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
    deleteOffer({ rootState }) {
        this.app.log(
            EVENT_TYPES.RECRUITER_DELETE_JOB,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
    getRecruiterOffersByCandidateCv({ rootState }) {
        this.app.log(
            EVENT_TYPES.RECRUITER_FIND_FOR_CANDIDATE_SEARCH,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
    updateUserProfile({ rootState }) {
        this.app.log(
            EVENT_TYPES.APPLICANT_UPDATE_PROFILE,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
    updateUserAccount({ rootState }) {
        this.app.log(
            EVENT_TYPES.APPLICANT_UPDATE_ACCOUNT,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
    async applicantProfileSearch({ rootState }) {
        await this.app.log(
            EVENT_TYPES.APPLICANT_RELEVANT_OFFERS_SEARCH,
            getCommonDetails(rootState, this.app.i18n),
        );
    },
};
