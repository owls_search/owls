import { userLogging } from './user';
import { searchLogging } from './search';
import { saveSearchLogging } from './saveSearch';
import { EVENT_TYPES, DETAILS } from '~/helpers/log';
import { get } from '~/util/lodash';

export const getCommonDetails = (rootState, i18n) => ({
    [DETAILS.BROWSER]:
        !process.server && get(window, ['navigator', 'userAgent'], ''),
    [DETAILS.DEVICE]:
        !process.server && get(window, ['navigator', 'oscpu'], ''),
    [DETAILS.SESSION_ID]: get(rootState, ['user', 'clientId'], ''),
    [DETAILS.PAGE_TYPE]: get(rootState, ['route', 'name'], null),
    [DETAILS.JOB_LANGUAGE]: get(rootState, ['search', 'offerLanguage'], ''),
    [DETAILS.WEBSITE_LANGUAGE]: get(i18n, ['locale'], ''),
    [DETAILS.SESSION_DURATION]:
        (Date.now() - get(rootState, ['user', 'startTimestamp'], Date.now())) /
        1000,
    [DETAILS.SEM_NETWORK]: get(rootState, ['semParameters', 'network'], null),
    [DETAILS.SEM_KEYWORDS]: get(rootState, ['semParameters', 'keywords'], null),
    [DETAILS.SEM_CAMPAIGN]: get(rootState, ['semParameters', 'campaign'], null),
});

const actions = {
    ...searchLogging,
    ...userLogging,
    ...saveSearchLogging,
    clickout({ rootState }, { partner, event, bid, offerId = null }) {
        const offerPosition = offerId
            ? get(rootState, ['offersList', 'offers'], []).findIndex(
                  offer => parseInt(offer.id, 10) === parseInt(offerId, 10),
              )
            : -1;

        this.app.log(EVENT_TYPES.CLICKOUT, {
            ...getCommonDetails(rootState, this.app.i18n),
            [DETAILS.PARTNER]: partner,
            [DETAILS.CLICKOUT_TYPE]: event,
            [DETAILS.BID]: bid,
            [DETAILS.SEARCH_QUERY]: get(rootState, ['search', 'query'], null),
            [DETAILS.OFFER_POSITION]: offerPosition,
            [DETAILS.PAGINATION]: get(
                rootState,
                ['search', 'currentPage'],
                null,
            ),
        });
    },
    sendFeedback({ rootState }) {
        this.app.log(EVENT_TYPES.SEND_FEEDBACK, getCommonDetails(rootState));
    },
    offerPageNotFound({ rootState }, offerId) {
        this.app.log(EVENT_TYPES.PAGE_NOT_FOUND, {
            ...getCommonDetails(rootState, this.app.i18n),
            [DETAILS.OFFER_ID]: offerId,
        });
    },
    userLanded({ rootState }) {
        this.app.log(EVENT_TYPES.USER_LANDED, {
            ...getCommonDetails(rootState, this.app.i18n),
            [DETAILS.SEARCH_QUERY]: get(rootState, ['search', 'query'], null),
            [DETAILS.PAGINATION]: get(
                rootState,
                ['search', 'currentPage'],
                null,
            ),
        });
    },
};

const logging = {
    state: () => {},
    mutations: {},
    actions,
};

export default logging;
