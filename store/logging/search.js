import { getCommonDetails } from './index';
import { DETAILS, EVENT_TYPES } from '~/helpers/log';
import { get } from '~/util/lodash';

const logSearchAction = (log, event, addDetails, rootState, i18n) =>
    log(event, {
        [DETAILS.PAGINATION]: get(rootState, ['search', 'currentPage'], null),
        ...getCommonDetails(rootState, i18n),
        ...addDetails,
    });

export const searchLogging = {
    search({ rootState }, { query }) {
        logSearchAction(
            this.app.log,
            EVENT_TYPES.SEARCH_EXECUTED,
            { [DETAILS.SEARCH_QUERY]: query },
            rootState,
            this.app.i18n,
        );
    },
    fileSearch({ rootState }) {
        logSearchAction(
            this.app.log,
            EVENT_TYPES.CV_SEARCH_EXECUTED,
            {},
            rootState,
            this.app.i18n,
        );
    },
    getSuggestions({ rootState }, query) {
        logSearchAction(
            this.app.log,
            EVENT_TYPES.SEARCHFORM_TYPING,
            { [DETAILS.SEARCH_QUERY]: query },
            rootState,
            this.app.i18n,
        );
    },
    selectSuggestion({ rootState }, { ssgText }) {
        logSearchAction(
            this.app.log,
            EVENT_TYPES.SEARCHFORM_TYPING,
            { [DETAILS.SEARCH_QUERY]: ssgText },
            rootState,
            this.app.i18n,
        );
    },
};
