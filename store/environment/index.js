import actions from './actions.js';

const state = () => ({
    popoverMode: null,
    cookieNotificationClosed: false,
    enteredWebsite: null,
    allowedToWriteCookies: null,
});

const mutations = {
    ENV_RESET_POPOVER_MODE(state) {
        state.popoverMode = null;
    },
    ENV_CHANGE_POPOVER_MODE(state, popoverMode) {
        state.popoverMode = popoverMode;
    },
    ENV_CLOSE_COOKIE_NOTIFICATION(state) {
        state.cookieNotificationClosed = true;
    },
    ENV_SET_ALLOWED_TO_WRITE_COOKIES(state, isAllowed) {
        state.allowedToWriteCookies = isAllowed;
    },
};

const environment = {
    state,
    mutations,
    actions,
};

export default environment;
