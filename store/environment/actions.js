const actions = {
    envChangePopoverMode({ commit }, mode) {
        commit('ENV_CHANGE_POPOVER_MODE', mode);
    },
    envResetPopoverMode({ commit }) {
        commit('ENV_RESET_POPOVER_MODE');
    },
    envCloseCookie({ commit }) {
        commit('ENV_CLOSE_COOKIE_NOTIFICATION');
    },
    envSetAllowedToWriteCookies({ commit }, isAllowed) {
        commit('ENV_SET_ALLOWED_TO_WRITE_COOKIES', isAllowed);
    },
};

export default actions;
