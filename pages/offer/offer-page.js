import { mapState, mapActions } from 'vuex';
import {
    getTagsFromString,
    getFormattedDate,
    getFormattedSalary,
    mapTagsToString,
    getUrlWithTracking,
    getPartnerLoggingName,
    getOfferPageCode,
} from '~/util';
import { drop, head, get } from '~/util/lodash';
import TagsList from '~/components/common/TagsList';
import LocationTags from '~/components/offer-page/LocationTags';
import { getSearchRoutePathParams } from '~/store/search/utils';
import SuggestedOffers from '~/components/offer-page/SuggestedOffers';
import PopularJobs from '~/components/offer-page/PopularJobs';
import ArrowBack from '~/static/svg/arrow_back.svg?inline';

const getRedirectPath = (path, needsDomainRedirect) =>
    `${needsDomainRedirect ? 'https://clusterjobs.de' : ''}${path}`;

export default {
    components: {
        LocationTags,
        TagsList,
        SuggestedOffers,
        ArrowBack,
        PopularJobs,
    },
    data: () => ({
        backLink: '',
    }),
    head() {
        return {
            title: this.$t('seo.offer.title', {
                title: this.title,
                companyName: this.companyName,
            }),
            meta: [
                {
                    hid: 'keywords',
                    name: 'keywords',
                    content: this.$t('seo.offer.keywords', {
                        skills: mapTagsToString(this.skills),
                        programmingLanguages: mapTagsToString(
                            this.programmingLanguages,
                        ),
                        destinations: mapTagsToString(this.destinations),
                    }),
                },
                {
                    hid: 'description',
                    name: 'description',
                    content: this.$t('seo.offer.description', {
                        title: this.title,
                        companyName: this.companyName,
                        skills: mapTagsToString(this.skills),
                        programmingLanguages: mapTagsToString(
                            this.programmingLanguages,
                        ),
                        destinations: mapTagsToString(this.destinations),
                    }),
                },
                {
                    hid: `og:title`,
                    property: 'og:title',
                    content: this.$t('seo.offer.title', { title: this.title }),
                },
                {
                    hid: `og:description`,
                    property: 'og:description',
                    content: this.$t('seo.offer.description', {
                        title: this.title,
                        companyName: this.companyName,
                        skills: mapTagsToString(this.skills),
                        programmingLanguages: mapTagsToString(
                            this.programmingLanguages,
                        ),
                        destinations: mapTagsToString(this.destinations),
                    }),
                },
            ],
            link: [
                {
                    rel: 'canonical',
                    link: `https://clusterjobs.de${this.localePath(
                        {
                            name: 'offer-id',
                            params: { id: this.offerCode },
                        },
                        'de',
                    )}`,
                },
            ],
        };
    },
    computed: {
        ...mapState({
            id: state => state.offerPage.offerData.id,
            title: state => state.offerPage.offerData.title,
            offerCode: state => state.offerPage.offerCode,
            originalTitle: state => state.offerPage.offerData.originalTitle,
            companyName: state => state.offerPage.offerData.companyName,
            htmlDescription: state => state.offerPage.offerData.htmlDescription,
            createdAt: state => {
                const dateTimestamp = parseInt(
                    state.offerPage.offerData.createdAt,
                    10,
                );
                if (dateTimestamp === 0 || !dateTimestamp) {
                    const yesterday = new Date();
                    yesterday.setDate(yesterday.getDate() - 1);
                    return getFormattedDate(yesterday);
                }

                const createdDate = new Date(dateTimestamp);
                return getFormattedDate(createdDate);
            },
            skills: state =>
                getTagsFromString('skills', state.offerPage.offerData.skills),
            benefits: state =>
                getTagsFromString(
                    'benefits',
                    state.offerPage.offerData.benefits,
                ),
            programmingLanguages: state =>
                getTagsFromString(
                    'languages',
                    state.offerPage.offerData.programmingLanguages,
                ),
            destinations: state =>
                getTagsFromString(
                    'destinations',
                    state.offerPage.offerData.destinations || 'Germany',
                ),
            url: state => getUrlWithTracking(state.offerPage.offerData),
            salaryString: function(state) {
                return getFormattedSalary(
                    state.offerPage.offerData.salaryFrom,
                    state.offerPage.offerData.salaryTo,
                    {
                        from: this.$t('offerPage.salaryFrom', {
                            salary: state.offerPage.offerData.salaryFrom,
                            currency: 'eur',
                        }),
                        to: this.$t('offerPage.salaryTo', {
                            salary: state.offerPage.offerData.salaryTo,
                            currency: 'eur',
                        }),
                        range: this.$t('offerPage.salaryRange', {
                            from: state.offerPage.offerData.salaryFrom,
                            to: state.offerPage.offerData.salaryTo,
                            currency: 'eur',
                        }),
                    },
                );
            },
            logo: state => state.offerPage.offerData.logo,
            startsFrom: function(state) {
                const startsFrom = state.offerPage.offerData.startsFrom;
                if (!startsFrom) {
                    return;
                }

                const dateStartsFrom = new Date(parseInt(startsFrom, 10));
                return dateStartsFrom < new Date()
                    ? this.$t('offerPage.startsAsap')
                    : this.$t('offerPage.startsAt', {
                          date: getFormattedDate(dateStartsFrom),
                      });
            },
            workTypeString: function(state) {
                const offerType = state.user.workTypes.find(
                    type => type.type === state.offerPage.offerData.offerType,
                );
                return offerType
                    ? this.$t('offerPage.workType', {
                          workType: this.$t(
                              offerType.translation,
                          ).toLowerCase(),
                      })
                    : '';
            },
            partner: state => getPartnerLoggingName(state.offerPage.offerData),
            bid: state => state.offerPage.offerData.bid,
            offerLanguages: state => state.search.offerLanguages,
            offerLanguage: state => state.search.offerLanguage,
            similarOffers: state => state.offerPage.similarOffers,
            popularJobs: state => state.offerPage.popularJobs,
        }),
        companySearchLink() {
            const routeParams = getSearchRoutePathParams(
                'search-query',
                this.offerLanguages.hasOwnProperty(this.offerLanguage)
                    ? this.offerLanguages[this.offerLanguage].urlParam
                    : '',
                1,
            );
            routeParams.params.query = this.companyName;

            try {
                return this.localePath(routeParams, this.$i18n.locale);
            } catch (e) {
                return this.localePath(routeParams, 'de');
            }
        },
        restOfTitle() {
            return this.originalTitle &&
                this.originalTitle.length < this.title.length
                ? this.title.split(this.originalTitle).join('')
                : '';
        },
    },
    async fetch({ params, store, redirect, app, error }) {
        const offerCode = params.id.split('-');
        await store.dispatch('offerSetRawOfferCode', params.id);
        const id = head(offerCode);
        const isGoogleJobs = get(
            store,
            ['state', 'seoParameters', 'isGoogleJobs'],
            false,
        );
        const currentLocale = app.i18n.locale;
        const needsDomainRedirect =
            isGoogleJobs &&
            currentLocale !== 'de' &&
            process.env.NODE_ENV === 'production';
        const localeToRedirect = needsDomainRedirect ? 'de' : currentLocale;

        const data = await store
            .dispatch('loadOfferPage', id)
            .catch(async _ => {
                await store.dispatch('offerPageNotFound', id);
                await store.dispatch('resetIsGoogleJobs');
                error({
                    offerCode: drop(offerCode, 1).join('-'),
                    statusCode: 410,
                });
            });

        if (!data) return;

        const title = get(data, 'title', '');
        const code = get(data, 'code', '');
        const pageCode = title ? getOfferPageCode(id, title, code) : id;
        if (localeToRedirect !== currentLocale || pageCode !== params.id) {
            await store.dispatch('resetIsGoogleJobs');
            redirect(
                getRedirectPath(
                    app.localePath(
                        {
                            name: 'offer-id',
                            params: { id: pageCode },
                        },
                        localeToRedirect,
                    ),
                    needsDomainRedirect,
                ),
            );
            return;
        }

        await store.dispatch('setOfferPageData', data);
        await store.dispatch(
            'setSimilarOffers',
            (data && data.similarOffers) || [],
        );
        await store.dispatch(
            'setPopularJobs',
            (data && data.popularJobs) || [],
        );
    },
    mounted() {
        this.$ga &&
            this.$ga.event({
                eventCategory: 'page',
                eventAction: 'offer',
                eventLabel: 'offerFound',
            });
        this.viewOffer({
            ga: this.$ga,
            gtm: this.$gtm,
            id: this.id,
            location: mapTagsToString(this.destinations),
            pageType: 'offerdetail',
        });

        const searchPageRegex = /.+clusterjobs\.de.*\/search\/.+$/;
        if (searchPageRegex.test(document.referrer)) {
            this.backLink = document.referrer;
        } else {
            const routeParams = getSearchRoutePathParams(
                'search-query',
                this.offerLanguages.hasOwnProperty(this.offerLanguage)
                    ? this.offerLanguages[this.offerLanguage].urlParam
                    : '',
                1,
            );
            routeParams.params.query = [
                this.skills.map(skill => skill.text).join(' '),
                this.programmingLanguages.map(lang => lang.text).join(' '),
            ]
                .join(' ')
                .trim();

            try {
                this.backLink = this.localePath(routeParams, this.$i18n.locale);
            } catch (e) {
                this.backLink = this.localePath(routeParams, 'de');
            }
        }
    },
    methods: {
        ...mapActions(['clickout', 'viewOffer']),
        logOfferPageClickout() {
            this.clickout({
                analyticsModule: this.$ga,
                gtm: this.$gtm,
                partner: this.partner,
                event: 'clickoutOfferPage',
                bid: this.bid,
            });
        },
    },
};
