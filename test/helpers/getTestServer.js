const { resolve } = require('path');
const { Nuxt, Builder } = require('nuxt');

module.exports = {
    getServer: async () => {
        const rootDir = resolve(__dirname, '../..');
        let config = {};
        try {
            process.env.ENV_FILE = './test/helpers/env';
            config = require('../../nuxt.config.js');
        } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e);
            return;
        }
        config.rootDir = rootDir;
        config.dev = false;
        config.mode = 'universal';
        const nuxt = new Nuxt(config);
        await new Builder(nuxt).build();
        nuxt.listen(4000, 'localhost');
        return nuxt;
    },
};
