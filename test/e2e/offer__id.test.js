import test from 'ava';
import { getServer } from '../helpers/getTestServer';

test.before(async t => {
    t.context.nuxt = await getServer();
}, 30000);

let offerPageUrl = '';

test.serial('Route /search/developer shows offers with links', async t => {
    const { nuxt } = t.context;
    const window = await nuxt.renderAndGetWindow(
        'http://localhost:4000/search/developer',
    );
    const firstLink = window.document.querySelector('.js-offer-page-url');

    t.not(firstLink, null);

    offerPageUrl = firstLink.href;
    console.log(offerPageUrl);
    t.not(offerPageUrl, null);
});

test('Route /offer/99999 shows offer page', async t => {
    const { nuxt } = t.context;
    const window = await nuxt.renderAndGetWindow(offerPageUrl);

    const offerTitle = window.document.querySelector('.js-offer-title');
    t.not(offerTitle, null, `${offerPageUrl} has no title`);
    t.not(offerTitle.innerHTML, null, `${offerPageUrl} has no js-offer-title`);
});

test('Route /offer/99999 has http://schema.org/JobPosting data markup', async t => {
    const { nuxt } = t.context;
    const window = await nuxt.renderAndGetWindow(offerPageUrl);

    const itemprops = [
        'title',
        'hiringOrganization',
        'jobLocation',
        'description',
        'datePosted',
    ];

    itemprops.forEach(itemProp => {
        const prop = window.document.querySelector(`[itemprop="${itemProp}"]`);
        t.not(
            prop,
            null,
            `${itemProp} is not found on page of offer ${offerPageUrl}`,
        );
        t.not(
            prop.innerHTML,
            null,
            `${itemProp} on page of offer ${offerPageUrl} is empty`,
        );
    });
});

test.after('Closing server and nuxt.js', t => {
    const { nuxt } = t.context;
    nuxt.close();
});
