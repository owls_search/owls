import test from 'ava';
import { getServer } from '../helpers/getTestServer';

test.before(async t => {
    t.context.nuxt = await getServer();
}, 30000);

test('Route / exits and render HTML', async t => {
    const { nuxt } = t.context;
    const window = await nuxt.renderAndGetWindow('http://localhost:4000');
    const element = window.document.querySelector('.js-homepage-search');
    t.not(element, null);
});

test('Route / exits and render some offers', async t => {
    const { nuxt } = t.context;
    const window = await nuxt.renderAndGetWindow('http://localhost:4000');
    const firstLink = window.document.querySelector('.js-offer-page-url');
    t.not(firstLink, null);
    t.not(firstLink.href, null);
});

test('Route / exits and render popular queries', async t => {
    const { nuxt } = t.context;
    const window = await nuxt.renderAndGetWindow('http://localhost:4000');
    const firstLinkToSearch = window.document.querySelector(
        '.js-link-to-search',
    );
    t.not(firstLinkToSearch, null);
    t.not(firstLinkToSearch.href, null);
});

test.after('Closing server and nuxt.js', t => {
    const { nuxt } = t.context;
    nuxt.close();
});
