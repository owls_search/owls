module.exports = {
    apps: [
        {
            name: 'clusterjobs web app',
            exec_mode: 'cluster',
            instances: 4,
            script: './node_modules/nuxt/bin/nuxt.js',
            args: 'start',
            autorestart: true,
            watch: true,
            max_memory_restart: '1G',
        },
    ],
};
